<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

Route::get('/', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@login')->name('post-login');
Route::get('/logout', 'LoginController@logout')->name('logout');

// Role Super Admin, Penyelia dan User
Route::group(['middleware' => ['auth', 'checkrole:0,1,2,
']], function(){
    // Dashboard
    Route::get('/dashboard', function() {
        return view('dashboard');
    })->name('dashboard');
    
    // Ubah Password
    Route::post('/change-password', 'UsersController@change_password')->name('change-password');

    // KOR
    Route::get('/view-kor', 'KORController@index')->name('view-kor');
    // Route::post('/save-kor', 'KORController@save_kor')->name('save-kor');
    Route::get('/kor/edit/{id}', 'KORController@edit')->name('edit-kor');
    Route::post('/kor/update', 'KORController@update')->name('update-kor');
    Route::get('/kor/history/{id}', 'KORController@history')->name('history-kor');
    Route::post('/import-kor', 'KORController@import_kor')->name('import-kor');
    Route::get('/template-kor', 'KORController@template')->name('template-kor');
    Route::get('/export-excel-kor', 'KORController@excel')->name('export-excel-kor');
    Route::get('/export-pdf-kor', 'KORController@pdf')->name('export-pdf-kor');
    Route::get('/validasi-kor/{id}', 'KORController@validasi')->name('validasi-kor');
    Route::get('/validasi-all-kor', 'KORController@validasi_all')->name('validasi-all-kor');

    // SPM
    Route::get('/view-spm', 'SPMController@index')->name('view-spm');
    Route::post('/save-spm', 'SPMController@save_spm')->name('save-spm');
    Route::get('/spm/edit/{id}', 'SPMController@edit')->name('edit-spm');
    Route::post('/spm/update', 'SPMController@update')->name('update-spm');
    Route::get('/spm/history/{id}', 'SPMController@history')->name('history-spm');
    Route::post('/import-spm', 'SPMController@import_spm')->name('import-spm');
    Route::get('/template-spm', 'SPMController@template')->name('template-spm');
    Route::get('/export-excel-spm', 'SPMController@excel')->name('export-excel-spm');
    Route::get('/export-pdf-spm', 'SPMController@pdf')->name('export-pdf-spm');
    Route::get('/validasi-spm/{id}', 'SPMController@validasi')->name('validasi-spm');
    Route::get('/validasi-all-spm', 'SPMController@validasi_all')->name('validasi-all-spm');

    // SDGs
    Route::get('/view-sdgs', 'SDGsController@index')->name('view-sdgs');
    Route::post('/save-sdgs', 'SDGsController@save_sdgs')->name('save-sdgs');
    Route::get('/sdgs/edit/{id}', 'SDGsController@edit')->name('edit-sdgs');
    Route::post('/sdgs/update', 'SDGsController@update')->name('update-sdgs');
    Route::get('/sdgs/history/{id}', 'SDGsController@history')->name('history-sdgs');
    Route::post('/import-sdgs', 'SDGsController@import_sdgs')->name('import-sdgs');
    Route::get('/template-sdgs', 'SDGsController@template')->name('template-sdgs');
    Route::get('/export-excel-sdgs', 'SDGsController@excel')->name('export-excel-sdgs');
    Route::get('/export-pdf-sdgs', 'SDGsController@pdf')->name('export-pdf-sdgs');
    Route::get('/validasi-sdgs/{id}', 'SDGsController@validasi')->name('validasi-sdgs');
    Route::get('/validasi-all-sdgs', 'SDGsController@validasi_all')->name('validasi-all-sdgs');

    // Data Dukung
    Route::get('/view-datadukung', 'DataDukungController@index')->name('view-datadukung');
    Route::post('/save-datadukung', 'DataDukungController@save_datadukung')->name('save-datadukung');
    Route::get('/datadukung/edit/{id}', 'DataDukungController@edit')->name('edit-datadukung');
    Route::post('/datadukung/update', 'DataDukungController@update')->name('update-datadukung');
    Route::get('/datadukung/history/{id}', 'DataDukungController@history')->name('history-datadukung');
    Route::post('/import-datadukung', 'DataDukungController@import_datadukung')->name('import-datadukung');
    Route::get('/template-datadukung', 'DataDukungController@template')->name('template-datadukung');
    Route::get('/export-excel-datadukung', 'DataDukungController@excel')->name('export-excel-datadukung');
    Route::get('/export-pdf-datadukung', 'DataDukungController@pdf')->name('export-pdf-datadukung');
    Route::get('/validasi-datadukung/{id}', 'DataDukungController@validasi')->name('validasi-datadukung');
    Route::get('/validasi-datadukung-child/{id}', 'DataDukungController@validasi_child')->name('validasi-datadukung-child');
    Route::get('/validasi-all-datadukung', 'DataDukungController@validasi_all')->name('validasi-all-datadukung');

    // BMD
    Route::get('/view-bmd', 'BMDController@index')->name('view-bmd');
    Route::post('/save-bmd', 'BMDController@save_bmd')->name('save-bmd');
    Route::get('/bmd/edit/{id}', 'BMDController@edit')->name('edit-bmd');
    Route::post('/bmd/update', 'BMDController@update')->name('update-bmd');
    Route::get('/bmd/history/{id}', 'BMDController@history')->name('history-bmd');
    Route::post('/import-bmd', 'BMDController@import_bmd')->name('import-bmd');
    Route::get('/template-bmd', 'BMDController@template')->name('template-bmd');
    Route::get('/export-excel-bmd', 'BMDController@excel')->name('export-excel-bmd');
    Route::get('/export-pdf-bmd', 'BMDController@pdf')->name('export-pdf-bmd');
    Route::get('/validasi-bmd/{id}', 'BMDController@validasi')->name('validasi-bmd');
    Route::get('/validasi-bmd-child/{id}', 'BMDController@validasi_child')->name('validasi-bmd-child');
    Route::get('/validasi-all-bmd', 'BMDController@validasi_all')->name('validasi-all-bmd');

    // IKU
    Route::get('/view-iku', 'IKUController@index')->name('view-iku');
    Route::get('/add-iku', 'IKUController@add_iku')->name('add-iku');
    Route::post('/save-iku', 'IKUController@save_iku')->name('save-iku');
    Route::get('/change-iku/{id}', 'IKUController@change_iku')->name('change-iku');
    Route::get('/delete-iku/{id}', 'IKUController@delete_iku')->name('delete-iku');
    Route::get('/export-excel-iku', 'IKUController@excel')->name('export-excel-iku');
    Route::get('/export-pdf-iku', 'IKUController@pdf')->name('export-pdf-iku');

    // eDatabase
    Route::get('/view-edatabase', 'eDatabaseController@index')->name('view-edatabase');
    Route::post('/save-edatabase', 'eDatabaseController@save_edatabase')->name('save-edatabase');
    Route::get('/change-edatabase/{id}', 'eDatabaseController@change_edatabase')->name('change-edatabase');

    Route::get('/validasi-edb/{id}', 'eDatabaseController@validasi')->name('validasi-edb');
    Route::get('/validasi-all-edb', 'eDatabaseController@validasi_all')->name('validasi-all-edb');
    
    Route::get('/export-excel-edatabase', 'eDatabaseController@excel')->name('export-excel-edatabase');
    Route::get('/export-pdf-edatabase', 'eDatabaseController@pdf')->name('export-pdf-edatabase');
});

Route::group(['middleware' => ['auth', 'checkrole:0']], function(){

    // Master KOR
    Route::get('/master-kor', 'KORController@master_kor')->name('master-kor');
    Route::get('/change-master-kor/{id}','KORController@change_kor')->name('change-master-kor');
    Route::get('/add-kor','KORController@add_kor')->name('add-kor');
    Route::post('/save-kor','KORController@save_kor')->name('save-kor');
    Route::get('/delete-kor/{id}','KORController@delete_kor')->name('delete-kor');
    Route::get('/export-excel-master-kor', 'KORController@excel_master')->name('export-excel-master-kor');
    Route::get('/export-pdf-master-kor', 'KORController@pdf_master')->name('export-pdf-master-kor');

    // Master SPM
    Route::get('/master-spm', 'SPMController@master_spm')->name('master-spm');
    Route::get('/change-master-spm/{id}','SPMController@change_spm')->name('change-master-spm');
    Route::get('/add-spm','SPMController@add_spm')->name('add-spm');
    Route::post('/save-spm','SPMController@save_spm')->name('save-spm');
    Route::get('/delete-spm/{id}','SPMController@delete_spm')->name('delete-spm');
    Route::get('/export-excel-master-spm', 'SPMController@excel_master')->name('export-excel-master-spm');
    Route::get('/export-pdf-master-spm', 'SPMController@pdf_master')->name('export-pdf-master-spm');

    // Master SDGs
    Route::get('/master-sdgs', 'SDGsController@master_sdgs')->name('master-sdgs');
    Route::get('/change-master-sdgs/{id}','SDGsController@change_sdgs')->name('change-master-sdgs');
    Route::get('/add-sdgs','SDGsController@add_sdgs')->name('add-sdgs');
    Route::post('/save-sdgs','SDGsController@save_sdgs')->name('save-sdgs');
    Route::get('/delete-sdgs/{id}', 'SDGsController@delete_sdgs')->name('delete-sdgs');
    Route::get('/export-excel-master-sdgs', 'SDGsController@excel_master')->name('export-excel-master-sdgs');
    Route::get('/export-pdf-master-sdgs', 'SDGsController@pdf_master')->name('export-pdf-master-sdgs');

    // Master Data Dukung
    Route::get('/master-datadukung', 'DataDukungController@master_datadukung')->name('master-datadukung');
    Route::get('/change-master-datadukung/{id}','DataDukungController@change_datadukung')->name('change-master-datadukung');
    Route::get('/add-datadukung','DataDukungController@add_datadukung')->name('add-datadukung');
    Route::post('/save-datadukung','DataDukungController@save_datadukung')->name('save-datadukung');
    Route::get('/delete-datadukung/{id}', 'DataDukungController@delete_datadukung')->name('delete-datadukung');
    Route::get('/datadukung/kategori', 'DataDukungController@kategori')->name('kategori-datadukung');
    Route::get('/datadukung/kategori/add', 'DataDukungController@kategori_add')->name('add-kategori-datadukung');
    Route::get('/change-kategori-datadukung/{id}','DataDukungController@change_kategori')->name('change-kategori-datadukung');
    Route::get('/delete-datadukung-kategori/{id}', 'DataDukungController@delete_kategori')->name('delete-datadukung-kategori');
    Route::get('/export-excel-master-datadukung', 'DataDukungController@excel_master')->name('export-excel-master-datadukung');
    Route::get('/export-pdf-master-datadukun', 'DataDukungController@pdf_master')->name('export-pdf-master-datadukung');

    // Master BMD
    Route::get('/master-bmd', 'BMDController@master_bmd')->name('master-bmd');
    Route::get('/change-master-bmd/{id}','BMDController@change_bmd')->name('change-master-bmd');
    Route::get('/add-bmd','BMDController@add_bmd')->name('add-bmd');
    Route::post('/save-bmd','BMDController@save_bmd')->name('save-bmd');
    Route::get('/delete-bmd/{id}', 'BMDController@delete_bmd')->name('delete-bmd');
    Route::get('/bmd/kategori', 'BMDController@kategori')->name('kategori-bmd');
    Route::get('/bmd/kategori/add', 'BMDController@kategori_add')->name('add-kategori-bmd');
    Route::get('/change-kategori-bmd/{id}','BMDController@change_kategori')->name('change-kategori-bmd');
    Route::get('/delete-bmd-kategori/{id}', 'BMDController@delete_kategori')->name('delete-bmd-kategori');
    Route::get('/export-excel-master-bmd', 'BMDController@excel_master')->name('export-excel-master-bmd');
    Route::get('/export-pdf-master-bmd', 'BMDController@pdf_master')->name('export-pdf-master-bmd');

    // SKPD
    Route::get('/view-skpd', 'SkpdController@index')->name('view-skpd');
    Route::post('/save-skpd','SkpdController@save_skpd')->name('save-skpd');
    Route::get('/delete-skpd/{id}','SkpdController@delete_skpd')->name('delete-skpd');
    Route::post('/import-skpd', 'SkpdController@import_skpd')->name('import-skpd');
    Route::get('/template-skpd', 'SkpdController@template')->name('template-skpd');
    Route::get('/export-excel-skpd', 'SkpdController@excel')->name('export-excel-skpd');
    Route::get('/export-pdf-skpd', 'SkpdController@pdf')->name('export-pdf-skpd');

    // Kecamatan
    Route::get('/master-kecamatan', 'KecamatanController@index')->name('master-kecamatan');
    Route::get('/change-kecamatan/{id}','KecamatanController@change_kecamatan')->name('change-kecamatan');
    Route::post('/update-status-kecamatan', 'KecamatanController@update_status_kecamatan')->name('update-status-kecamatan');
    Route::get('/add-kecamatan','KecamatanController@add_kecamatan')->name('add-kecamatan');
    Route::post('/save-kecamatan','KecamatanController@save_kecamatan')->name('save-kecamatan');
    Route::get('/delete-kecamatan/{id}', 'KecamatanController@delete_kecamatan')->name('delete-kecamatan');
    Route::post('/import-kecamatan', 'KecamatanController@import_kecamatan')->name('import-kecamatan');
    Route::get('/template-kecamatan', 'KecamatanController@template')->name('template-kecamatan');
    Route::get('/export-excel-kecamatan', 'KecamatanController@excel')->name('export-excel-kecamatan');
    Route::get('/export-pdf-kecamatan', 'KecamatanController@pdf')->name('export-pdf-kecamatan');

    // Kelurahan
    Route::get('/master-kelurahan', 'KelurahanController@index')->name('master-kelurahan');
    Route::get('/change-kelurahan/{id}','KelurahanController@change_kelurahan')->name('change-kelurahan');
    Route::post('/update-status-kelurahan', 'KelurahanController@update_status_kelurahan')->name('update-status-kelurahan');
    Route::get('/add-kelurahan','KelurahanController@add_kelurahan')->name('add-kelurahan');
    Route::post('/save-kelurahan','KelurahanController@save_kelurahan')->name('save-kelurahan');
    Route::get('/delete-kelurahan/{id}', 'KelurahanController@delete_kelurahan')->name('delete-kelurahan');
    Route::post('/import-kelurahan', 'KelurahanController@import_kelurahan')->name('import-kelurahan');
    Route::get('/template-kelurahan', 'KelurahanController@template')->name('template-kelurahan');
    Route::get('/export-excel-kelurahan', 'KelurahanController@excel')->name('export-excel-kelurahan');
    Route::get('/export-pdf-kelurahan', 'KelurahanController@pdf')->name('export-pdf-kelurahan');

    // User
    Route::get('/view-users', 'UsersController@index')->name('view-users');
    Route::get('/update-status-user', 'UsersController@update_status_user')->name('update-status-user');
    Route::get('/change-user/{id}','UsersController@change_user')->name('change-user');
    Route::get('/add-user','UsersController@add_user')->name('add-user');
    Route::post('/save-user','UsersController@save_user')->name('save-user');
    Route::post('/import-user', 'UsersController@import_user')->name('import-user');
    Route::get('/template-user', 'UsersController@template')->name('template-user');
    Route::get('/export-excel-user', 'UsersController@excel')->name('export-excel-user');
    Route::get('/export-pdf-user', 'UsersController@pdf')->name('export-pdf-user');

    // Business
    Route::get('/view-business', 'BusinessController@index')->name('view-business');
    Route::post('/save-business','BusinessController@save_business')->name('save-business');
    Route::get('/business/edit/{id}', 'BusinessController@edit')->name('edit-business');
    Route::get('/delete-business/{id}','BusinessController@delete_business')->name('delete-business');
    Route::post('/import-business', 'BusinessController@import_business')->name('import-business');
    Route::get('/template-business', 'BusinessController@template')->name('template-business');
    Route::get('/export-excel-business', 'BusinessController@excel_business')->name('export-excel-business');
    Route::get('/export-pdf-business', 'BusinessController@pdf_business')->name('export-pdf-business');

    // Business Detail
    Route::get('/detail-business/{id}','BusinessController@detail_business')->name('detail-business');
    Route::post('/save-area','BusinessController@save_area')->name('save-area');
    Route::get('/area/edit/{id}', 'BusinessController@edit_area')->name('edit-area');
    Route::get('/delete-area/{id}','BusinessController@delete_area')->name('delete-area');
    Route::get('/export-excel-area/{id}', 'BusinessController@excel_area')->name('export-excel-area');
    Route::get('/export-pdf-area/{id}', 'BusinessController@pdf_area')->name('export-pdf-area');
    

    // Sub Area
    Route::get('/sub-area','BusinessController@sub_area')->name('sub-area');
    Route::post('/save-sub-area','BusinessController@save_sub_area')->name('save-sub-area');
    Route::get('/subarea/edit/{id}', 'BusinessController@edit_sub_area')->name('edit-subarea');
    Route::get('/delete-sub-area/{id}','BusinessController@delete_sub_area')->name('delete-sub-area');
    Route::get('/export-excel-sub-area/{id}', 'BusinessController@excel_sub_area')->name('export-excel-sub-area');
    Route::get('/export-pdf-sub-area/{id}', 'BusinessController@pdf_sub_area')->name('export-pdf-sub-area');

    // Indicator
    Route::get('/view-indicators','BusinessController@indicators')->name('view-indicators');
    Route::post('/save-indicator','BusinessController@save_indicator')->name('save-indicator');
    Route::get('/indicator/edit/{id}', 'BusinessController@edit_indicator')->name('edit-indicator');
    Route::get('/delete-indicator/{id}','BusinessController@delete_indicator')->name('delete-indicator');
    Route::get('/export-excel-indicator/{id}', 'BusinessController@excel_indicator')->name('export-excel-indicator');
    Route::get('/export-pdf-indicator/{id}', 'BusinessController@pdf_indicator')->name('export-pdf-indicator');

    // Sub Indicator
    Route::get('/view-sub-indicators','BusinessController@sub_indicators')->name('view-sub-indicators');
    Route::post('/save-sub-indicator','BusinessController@save_sub_indicator')->name('save-sub-indicator');
    Route::get('/sub-indicator/edit/{id}', 'BusinessController@edit_sub_indicator')->name('edit-sub-indicator');
    Route::get('/delete-sub-indicator/{id}','BusinessController@delete_sub_indicator')->name('delete-sub-indicator');
    Route::get('/export-excel-sub-indicator/{id}', 'BusinessController@excel_sub_indicator')->name('export-excel-sub-indicator');
    Route::get('/export-pdf-sub-indicator/{id}', 'BusinessController@pdf_sub_indicator')->name('export-pdf-sub-indicator');

    // Parent Sub
    Route::get('/parent-sub/{id}', 'BusinessController@parent_sub')->name('view-parent-sub');
    Route::get('/export-excel-parent', 'BusinessController@excel_parent_sub')->name('export-excel-parent-sub');
    Route::get('/export-pdf-parent', 'BusinessController@pdf_parent_sub')->name('export-pdf-parent-sub');
    Route::get('/export-excel-parent-sub/{id}', 'BusinessController@excel_parent_sub')->name('export-excel-parent-sub');
    Route::get('/export-pdf-parent-sub/{id}', 'BusinessController@pdf_parent_sub')->name('export-pdf-parent-sub');

    // eDatabase
    Route::get('/add-edatabase', 'eDatabaseController@add_edatabase')->name('add-edatabase');
});
