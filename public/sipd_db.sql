-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 01 Sep 2020 pada 03.17
-- Versi server: 5.5.60
-- Versi PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sipd_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `area_name` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `areas`
--

INSERT INTO `areas` (`id`, `business_id`, `area_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 'sub bidang 88', 0, NULL, '2020-08-27 18:35:11'),
(2, 1, 'area name 2', 0, NULL, NULL),
(3, 2, 'area name34444444444d', 0, NULL, '2020-08-27 18:02:57'),
(4, 2, 'area name 4', 0, NULL, NULL),
(5, 2, 'area name5', 1, '2020-08-27 17:58:42', '2020-08-27 18:04:46'),
(6, 2, 'SUB BIDANG 3', 0, '2020-08-27 18:34:03', '2020-08-27 18:34:03');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bmd`
--

CREATE TABLE `bmd` (
  `id` int(11) NOT NULL,
  `bmd_description` varchar(100) DEFAULT NULL,
  `satuan_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bps`
--

CREATE TABLE `bps` (
  `id` int(11) NOT NULL,
  `bps_category_id` int(11) NOT NULL,
  `bps_subject_id` int(11) NOT NULL,
  `variable` varchar(100) DEFAULT NULL,
  `note` varchar(100) DEFAULT NULL,
  `variable_detail` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bps_categories`
--

CREATE TABLE `bps_categories` (
  `id` int(11) NOT NULL,
  `category` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `bps_subjects`
--

CREATE TABLE `bps_subjects` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `subject` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `business`
--

CREATE TABLE `business` (
  `id` int(11) NOT NULL,
  `business_name` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `business`
--

INSERT INTO `business` (`id`, `business_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'business17', 0, NULL, '2020-08-27 13:42:06'),
(2, 'business78', 0, NULL, '2020-08-27 13:42:22'),
(3, 'nnnnnd', 0, '2020-08-27 13:37:03', '2020-08-27 13:37:03'),
(4, 'hhhh', 0, '2020-08-27 13:37:36', '2020-08-27 13:37:36'),
(5, 'hhhh', 1, '2020-08-27 13:37:36', '2020-08-27 13:44:57');

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_supports`
--

CREATE TABLE `data_supports` (
  `id` int(11) NOT NULL,
  `data_description` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `edatabase`
--

CREATE TABLE `edatabase` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `sub_area_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `sub_indicator_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `iku`
--

CREATE TABLE `iku` (
  `id` int(11) NOT NULL,
  `iku_description` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `target` varchar(100) DEFAULT NULL,
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `iku_detail`
--

CREATE TABLE `iku_detail` (
  `id` int(11) NOT NULL,
  `iku_id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `indicators`
--

CREATE TABLE `indicators` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `sub_area_id` int(11) NOT NULL,
  `indicator_name` text,
  `active` tinyint(2) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `indicators`
--

INSERT INTO `indicators` (`id`, `business_id`, `area_id`, `sub_area_id`, `indicator_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 2, 'anannahhshsh', 0, '2020-08-27 19:08:45', '2020-08-27 19:45:18'),
(2, 2, 3, 2, 'ggggggggggggggfsss', 1, '2020-08-27 19:12:17', '2020-08-27 19:17:10'),
(3, 1, 2, 3, 'nn', 0, '2020-08-27 19:55:08', '2020-08-27 19:55:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kor`
--

CREATE TABLE `kor` (
  `id` int(11) NOT NULL,
  `kor_name` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sdgs`
--

CREATE TABLE `sdgs` (
  `id` int(11) NOT NULL,
  `sdgs_description` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `skpd`
--

CREATE TABLE `skpd` (
  `id` int(11) NOT NULL,
  `skpd_name` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `skpd`
--

INSERT INTO `skpd` (`id`, `skpd_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 'dinas kependiikanw', 0, NULL, '2020-08-27 04:51:37'),
(2, 'tes2', 0, NULL, NULL),
(3, NULL, 0, '2020-08-26 21:08:32', '2020-08-26 21:08:32'),
(4, NULL, 0, '2020-08-26 21:08:43', '2020-08-26 21:08:43'),
(5, NULL, 0, '2020-08-26 21:10:36', '2020-08-26 21:10:36'),
(6, 'tesnnn', 0, '2020-08-26 21:11:08', '2020-08-26 21:11:08'),
(7, 'dgh', 0, '2020-08-26 21:15:20', '2020-08-26 21:15:20'),
(8, 'nnnfff', 0, '2020-08-26 21:15:30', '2020-08-27 05:21:28'),
(9, 'tes', 1, '2020-08-26 21:16:09', '2020-08-27 05:21:15'),
(10, 'dd', 1, '2020-08-27 04:40:49', '2020-08-27 05:20:56'),
(11, 'tos', 1, '2020-08-27 04:51:44', '2020-08-27 05:21:02'),
(12, NULL, 0, '2020-08-27 12:18:44', '2020-08-27 12:18:44'),
(13, NULL, 0, '2020-08-27 12:24:00', '2020-08-27 12:24:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `spm`
--

CREATE TABLE `spm` (
  `id` int(11) NOT NULL,
  `spm_description` varchar(100) DEFAULT NULL,
  `unit_id` int(11) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  `validate` tinyint(2) NOT NULL DEFAULT '0',
  `validate_final` tinyint(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_areas`
--

CREATE TABLE `sub_areas` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `subarea_name` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sub_areas`
--

INSERT INTO `sub_areas` (`id`, `business_id`, `area_id`, `subarea_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 'sub bidang 166', 1, '2020-08-27 18:29:42', '2020-08-27 18:37:26'),
(2, 2, 3, 'bbbbbb', 0, '2020-08-27 18:37:33', '2020-08-27 18:37:33'),
(3, 1, 2, 'mmm', 0, '2020-08-27 19:52:52', '2020-08-27 19:52:52');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sub_indicators`
--

CREATE TABLE `sub_indicators` (
  `id` int(11) NOT NULL,
  `business_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `sub_area_id` int(11) NOT NULL,
  `indicator_id` int(11) NOT NULL,
  `sub_indicator_name` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `sub_indicators`
--

INSERT INTO `sub_indicators` (`id`, `business_id`, `area_id`, `sub_area_id`, `indicator_id`, `sub_indicator_name`, `active`, `created_at`, `updated_at`) VALUES
(1, 2, 3, 2, 1, 'anannasnsns', 0, '2020-08-27 19:43:32', '2020-08-27 19:45:39'),
(2, 2, 3, 2, 1, 'SKSKKSKS', 1, '2020-08-27 19:46:55', '2020-08-27 19:47:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `units`
--

CREATE TABLE `units` (
  `id` int(11) NOT NULL,
  `unit` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `password` varchar(100) NOT NULL,
  `role` varchar(100) NOT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `active`, `password`, `role`, `full_name`, `created_at`, `updated_at`) VALUES
(1, 'ranipe', 'rani@gmail.comnnn', 0, '$2y$10$PwMq0tGZJP2.CPSPp.bsm.thdu794QQNR3HeDuwMgb9mpdZfiC9YO', 'Penyelia', 'Rani Pebriantis', NULL, '2020-08-27 12:53:15'),
(2, 'kabayan', 'rani@kabayan.id', 0, 'hhh', 'Administrator', 'Rina Febriana', NULL, '2020-08-27 10:39:31'),
(3, 'hh', NULL, 0, '$2y$10$RYPnqOnILqIlclLw7TF0e.SUWGJ2Kd1r8GbSK.Sl7FfW4xhTUNs4i', 'Administrator', 'tes baeu', '2020-08-27 13:11:08', '2020-08-27 13:19:53');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users_skpd`
--

CREATE TABLE `users_skpd` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skpd_id` int(11) NOT NULL,
  `active` tinyint(2) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users_skpd`
--

INSERT INTO `users_skpd` (`id`, `user_id`, `skpd_id`, `active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(6, 1, 1, 0, '2020-08-27 12:44:09', '2020-08-27 12:46:19', '2020-08-27 12:46:19'),
(7, 1, 2, 0, '2020-08-27 12:44:09', '2020-08-27 12:46:19', '2020-08-27 12:46:19'),
(8, 1, 3, 0, '2020-08-27 12:44:09', '2020-08-27 12:46:19', '2020-08-27 12:46:19'),
(11, 1, 5, 0, '2020-08-27 12:44:35', '2020-08-27 12:46:19', '2020-08-27 12:46:19'),
(12, 1, 1, 0, '2020-08-27 12:46:36', '2020-08-27 12:46:51', '2020-08-27 12:46:51'),
(13, 1, 5, 0, '2020-08-27 12:46:36', '2020-08-27 12:46:51', '2020-08-27 12:46:51'),
(14, 1, 8, 0, '2020-08-27 12:46:36', '2020-08-27 12:50:00', NULL),
(15, 1, 12, 0, '2020-08-27 12:46:36', '2020-08-27 12:50:00', NULL),
(16, 1, 3, 0, '2020-08-27 12:47:59', '2020-08-27 12:50:00', NULL),
(17, 1, 4, 0, '2020-08-27 12:47:59', '2020-08-27 12:50:00', NULL),
(18, 1, 1, 0, '2020-08-27 12:49:03', '2020-08-27 12:50:00', NULL),
(19, 1, 6, 0, '2020-08-27 12:49:03', '2020-08-27 12:50:00', NULL),
(20, 3, 12, 0, '2020-08-27 13:19:53', '2020-08-27 13:19:53', NULL),
(21, 3, 13, 0, '2020-08-27 13:19:53', '2020-08-27 13:19:53', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`,`business_id`),
  ADD KEY `business_id` (`business_id`);

--
-- Indeks untuk tabel `bmd`
--
ALTER TABLE `bmd`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_satuan` (`satuan_id`);

--
-- Indeks untuk tabel `bps`
--
ALTER TABLE `bps`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_bps_category` (`bps_category_id`),
  ADD KEY `fk_subject_category` (`bps_subject_id`);

--
-- Indeks untuk tabel `bps_categories`
--
ALTER TABLE `bps_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `bps_subjects`
--
ALTER TABLE `bps_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_category` (`category_id`);

--
-- Indeks untuk tabel `business`
--
ALTER TABLE `business`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_supports`
--
ALTER TABLE `data_supports`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_unit_id` (`unit_id`);

--
-- Indeks untuk tabel `edatabase`
--
ALTER TABLE `edatabase`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_business_id` (`business_id`),
  ADD KEY `fk_area_id` (`area_id`),
  ADD KEY `fk_sub_area_id` (`sub_area_id`),
  ADD KEY `fk_indicator_id` (`indicator_id`),
  ADD KEY `fk_sub_indicator_id` (`sub_indicator_id`);

--
-- Indeks untuk tabel `iku`
--
ALTER TABLE `iku`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iku_unit` (`unit_id`);

--
-- Indeks untuk tabel `iku_detail`
--
ALTER TABLE `iku_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_iku_business` (`business_id`);

--
-- Indeks untuk tabel `indicators`
--
ALTER TABLE `indicators`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_business_indicator` (`business_id`),
  ADD KEY `fk_area_indicator` (`area_id`),
  ADD KEY `fk_sub_area_indicator` (`sub_area_id`);

--
-- Indeks untuk tabel `kor`
--
ALTER TABLE `kor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_kor_unit` (`unit_id`);

--
-- Indeks untuk tabel `sdgs`
--
ALTER TABLE `sdgs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sdgs_unit` (`unit_id`);

--
-- Indeks untuk tabel `skpd`
--
ALTER TABLE `skpd`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `spm`
--
ALTER TABLE `spm`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_spm_unit` (`unit_id`);

--
-- Indeks untuk tabel `sub_areas`
--
ALTER TABLE `sub_areas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_sub_area_business` (`business_id`),
  ADD KEY `fk_sub_area_area` (`area_id`);

--
-- Indeks untuk tabel `sub_indicators`
--
ALTER TABLE `sub_indicators`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users_skpd`
--
ALTER TABLE `users_skpd`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `bmd`
--
ALTER TABLE `bmd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bps`
--
ALTER TABLE `bps`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bps_categories`
--
ALTER TABLE `bps_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `bps_subjects`
--
ALTER TABLE `bps_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `business`
--
ALTER TABLE `business`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `data_supports`
--
ALTER TABLE `data_supports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `edatabase`
--
ALTER TABLE `edatabase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `iku`
--
ALTER TABLE `iku`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `iku_detail`
--
ALTER TABLE `iku_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `indicators`
--
ALTER TABLE `indicators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `kor`
--
ALTER TABLE `kor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sdgs`
--
ALTER TABLE `sdgs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `skpd`
--
ALTER TABLE `skpd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `spm`
--
ALTER TABLE `spm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `sub_areas`
--
ALTER TABLE `sub_areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `sub_indicators`
--
ALTER TABLE `sub_indicators`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `units`
--
ALTER TABLE `units`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `users_skpd`
--
ALTER TABLE `users_skpd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `bmd`
--
ALTER TABLE `bmd`
  ADD CONSTRAINT `fk_satuan` FOREIGN KEY (`satuan_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `bps`
--
ALTER TABLE `bps`
  ADD CONSTRAINT `fk_bps_category` FOREIGN KEY (`bps_category_id`) REFERENCES `bps_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_subject_category` FOREIGN KEY (`bps_subject_id`) REFERENCES `bps_subjects` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `bps_subjects`
--
ALTER TABLE `bps_subjects`
  ADD CONSTRAINT `fk_category` FOREIGN KEY (`category_id`) REFERENCES `bps_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `data_supports`
--
ALTER TABLE `data_supports`
  ADD CONSTRAINT `fk_unit_id` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `edatabase`
--
ALTER TABLE `edatabase`
  ADD CONSTRAINT `fk_area_id` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_business_id` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_indicator_id` FOREIGN KEY (`indicator_id`) REFERENCES `indicators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_area_id` FOREIGN KEY (`sub_area_id`) REFERENCES `sub_areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_indicator_id` FOREIGN KEY (`sub_indicator_id`) REFERENCES `sub_indicators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `iku`
--
ALTER TABLE `iku`
  ADD CONSTRAINT `fk_iku_unit` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `iku_detail`
--
ALTER TABLE `iku_detail`
  ADD CONSTRAINT `fk_iku_business` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `indicators`
--
ALTER TABLE `indicators`
  ADD CONSTRAINT `fk_area_indicator` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_business_indicator` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_area_indicator` FOREIGN KEY (`sub_area_id`) REFERENCES `sub_areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `kor`
--
ALTER TABLE `kor`
  ADD CONSTRAINT `fk_kor_unit` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `sdgs`
--
ALTER TABLE `sdgs`
  ADD CONSTRAINT `fk_sdgs_unit` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `spm`
--
ALTER TABLE `spm`
  ADD CONSTRAINT `fk_spm_unit` FOREIGN KEY (`unit_id`) REFERENCES `sub_indicators` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ketidakleluasaan untuk tabel `sub_areas`
--
ALTER TABLE `sub_areas`
  ADD CONSTRAINT `fk_sub_area_area` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_sub_area_business` FOREIGN KEY (`business_id`) REFERENCES `business` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
