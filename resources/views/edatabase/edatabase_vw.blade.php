@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li class="active">
                    eDatabase
                </li>
            </ol>
            <div class="page-header">
                <h1> eDatabase </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- start: RESPONSIVE TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-12">


                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <select id="form-field-select-3" class="form-control search-select urusan">
                                        <option value=""></option>
                                        @foreach($urusan as $item)
                                        <option value="{{$item->id}}">{{$item->business_name}}</option>
                                        @endforeach
                                    </select>

                                </fieldset>
                            </div>
                            <div class="col-md-6" align="right">

                                <fieldset class="form-group">


                                    
                                    <a href="{{route('export-pdf-edatabase')}}" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
                                    <a href="{{route('export-excel-edatabase')}}" type="button" class="btn btn-success">Export Excel</a>

                                    <button type="button" class="btn btn-warning ajax_delete">Validasi Final</button>
                                    @if(auth()->user()->role === '0')
                                    <a class="btn btn-success" role="button" href="{{route('add-edatabase')}}">
                                        Tambah Data
                                    </a>
                                    @endif
                                    <!--
                            <button type="button" class="btn btn-danger">Tambah Data</button>
                        -->
                                </fieldset>

                            </div>

                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover data-table">
                            <thead>
                                <tr>
                                    <th style="display: none;"></th>
                                    <th style="display: none;"></th>
                                    <th>Urusan / Bidang</th>
                                    <th class="text-center">Satuan</th>
                                    <th class="text-center">Nilai</th>
                                    <th class="text-center" style="width: 100px; text-align: center;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
    </div>





</div>
</div>

</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    $(function() {


        var table = $('.data-table').DataTable({
            "scrollX": false,
            "scrollY": false,
            processing: true,
            pageLength: 100,
            serverSide: true,
            ajax: {
                url: "{{route('view-edatabase')}}",
                data: function(d){
                    d.urusan = $('.urusan').val(),
                    d.search = $('input[type="search"]').val()
                }
            },
            "aoColumnDefs": [

                {
                    "sWidth": "60%",
                    "aTargets": [2],
                },
                {
                    "sWidth": "15%",
                    "aTargets": [3]
                },
                {
                    "sWidth": "10%",
                    "aTargets": [5],
                    "className": "text-center"
                }
            ],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'master_data',
                    name: 'master_data'
                },
                {
                    data: 'urusan',
                    name: 'urusan'
                },
                {
                    data: 'satuan',
                    name: 'satuan'
                },
                {
                    data: 'value',
                    name: 'value'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
        table.column(0).visible(false, false);
        table.column(1).visible(false, false);
        $('.urusan').change(function(){
            table.draw();
        });

    });
</script>
<script>
    $(document).ready(function() {
        $('body').on('click', '.target_validasi', function() {

            var validasi_url = $(this).attr('data-url');

            swal({
                    title: "Peringatan!",
                    text: "Anda yakin akan validasi  data ini",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya!",
                    cancelButtonText: "Batalkan!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = validasi_url;
                    } else {
                        swal("Batalkan", "Data ini batal divalidasi :)", "error");
                    }
                });

        });
    });
</script>
<script>
    $(".ajax_delete").on("click", function() {

        swal({
                title: "Peringatan!",
                text: "Anda yakin akan validasi final data ini",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "{{route('validasi-all-edb')}}";
                } else {
                    swal("Batalkan", "Data batal divalidasi :)", "error");
                }
            });


    });
</script>
@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection