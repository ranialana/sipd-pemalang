@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('dashboard')}}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{route('view-edatabase')}}">
                        eDatabase
                    </a>
                </li>
                <li class="active">
                    Ubah eDatabase
                </li>
            </ol>
            <div class="page-header">
                <h1>Ubah eDatabase </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form action="{{route('save-edatabase')}}" method="POST" role="form" id="form2">
                        @csrf
                        <input type="hidden" name="id" value="{{$edatabase->id}}">
                        <div class="form-group">
                            <label>
                                Urusan
                            </label>
                            <select id="urusan_edit" name="business_id" class="form-control search-select business" oninvalid="this.setCustomValidity('Urusan tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value="">Pilih Urusan</option>
                                @foreach($urusan as $business)
                                <option value="{{$business->id}}" @if($business->id === $edatabase->business_id) selected @endif>{{$business->business_name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            <label>
                                Bidang
                            </label>
                            <select class="form-control search-select1 area" name="area_id" id="bidang_edit">
                                <option value=""> Pilih Bidang </option>
                                @foreach($area as $bidang)
                                <option value="{{$bidang->id}}" class="{{$bidang->business_id}}" @if($bidang->id === $edatabase->area_id) selected @endif>{{$bidang->area_name}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>
                                Sub Bidang
                            </label>
                            <select class="form-control search-select2 sub-area" name="sub_area_id" id="sub_bidang_edit" required oninvalid="this.setCustomValidity('Sub Bidang tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Sub Bidang </option>
                                @foreach($sub_area as $sub_bidang)
                                <option value="{{$sub_bidang->id}}" class="{{$sub_bidang->area_id}}" @if($sub_bidang->id === $edatabase->sub_area_id) selected @endif>{{$sub_bidang->subarea_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>
                                Indikator
                            </label>
                            <select class="form-control search-select3 indicator" name="indicator_id" id="indikator_edit" required oninvalid="this.setCustomValidity('Indikator tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Indikator </option>
                                @foreach($indicator as $indikator)
                                <option value="{{$indikator->id}}" class="{{$indikator->sub_area_id}}" @if($indikator->id === $edatabase->indicator_id) selected @endif>{{$indikator->indicator_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Sub Indikator
                            </label>
                            <select class="form-control search-select subindicator" name="sub_indicator_id" id="subindikator_edit" required oninvalid="this.setCustomValidity('Indikator tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Sub Indikator </option>
                                @foreach($sub_indicator as $subindikator)
                                <?php
                                $space = '';
                                for($i=0; $i<$subindikator['space']; $i++){
                                    $space = $space.'&nbsp;';
                                }
                                ?>
                                <option value="{{$subindikator['id']}}" class="{{$subindikator['indicator_id']}}" data-satuan="{{$subindikator['unit_id']}}" @if($subindikator['id'] === $edatabase->sub_indicator_id) selected @endif>{!!$space!!}- {{$subindikator['sub_indicator_name']}}</option>
                                
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="satuan" id="satuan" value="{{$edatabase->unit_id}}">
                        <div class="form-group">
                            <label>
                                Satuan
                            </label>
                            <select name="" class="form-control search-select satuan"  id="satuan" oninvalid="this.setCustomValidity('Satuan tidak boleh kosong')" oninput="setCustomValidity('')" disabled>
                                <option value="">Pilih Satuan</option>
                                @foreach($unit as $satuan)
                                <option value="{{$satuan->id}}" @if($satuan->id === $edatabase->unit_id) selected @endif>{{$satuan->unit}}</option>
                                @endforeach
                            </select>
                            <!-- <input type="" name="unit_id" class="form-control" value="" readonly> -->
                        </div>
                        <div class="form-group">
                            <label>
                                Nilai
                            </label>
                            <input type="text" id="target" name="value" placeholder="Masukan Nilai ..." class="form-control" required oninvalid="this.setCustomValidity('Nilai tidak boleh kosong')" oninput="setCustomValidity('')" value="{{$edatabase->value}}">
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<script src="//code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="http://www.appelsiini.net/projects/chained/jquery.chained.js?v=0.9.10"></script>
@if(session('success'))
<script>
    // $("#urusan").prop('disabled', true);
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
<script>
    <?php
    if(auth()->user()->role != "0"){
        ?>
        $('#urusan_edit').prop('disabled', true);
        $('#bidang_edit').prop('disabled', true);
        $('#sub_bidang_edit').prop('disabled', true);
        $('#indikator_edit').prop('disabled', true);
        $('#subindikator_edit').prop('disabled', true);
        <?php
    }
    ?>
</script>
@endsection