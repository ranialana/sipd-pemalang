@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('dashboard')}}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{route('view-edatabase')}}">
                        eDatabase
                    </a>
                </li>
                <li class="active">
                    Tambah eDatabase
                </li>
            </ol>
            <div class="page-header">
                <h1>Tambah eDatabase </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form action="{{route('save-edatabase')}}" method="POST" role="form" id="form2">
                        @csrf
                        <div class="form-group">
                            <label>
                                Urusan
                            </label>
                            <select id="urusan" name="business_id" class="form-control search-select urusan" oninvalid="this.setCustomValidity('Urusan tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value="">Pilih Urusan</option>
                                @foreach($urusan as $business)
                                <option value="{{$business->id}}" data-satuan="{{$business->unit_id}}">{{$business->business_name}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group">
                            <label>
                                Bidang
                            </label>
                            <select class="form-control search-select1 bidang" name="area_id" id="bidang">
                                <option value=""> Pilih Bidang </option>
                                @foreach($area as $bidang)
                                <?php
                                // if($bidang->getBusiness->role === 'Kecamatan' && $bidang->getBusiness->check_sub === 0 || $bidang->getBusiness->role === 'Kelurahan' && $bidang->getBusiness->check_sub === 0){

                                // }
                                // else {
                                ?>
                                <option value="{{$bidang->id}}" class="{{$bidang->business_id}}" data-satuan="{{$bidang->unit_id}}">{{$bidang->area_name}}</option>
                                <?php
                                // }
                                ?>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>
                                Sub Bidang
                            </label>
                            <select class="form-control search-select2" name="sub_area_id" id="sub_bidang" required oninvalid="this.setCustomValidity('Sub Bidang tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Sub Bidang </option>
                                @foreach($sub_area as $sub_bidang)
                                <?php
                                // if($sub_bidang->getArea->role === 'Kecamatan' && $sub_bidang->getArea->check_sub === 0 || $sub_bidang->getArea->role === 'Kelurahan' && $sub_bidang->getArea->check_sub === 0){

                                // }
                                // else {
                                ?>
                                <option value="{{$sub_bidang->id}}" class="{{$sub_bidang->area_id}}" data-satuan="{{$sub_bidang->unit_id}}">{{$sub_bidang->subarea_name}}</option>
                                <?php
                                // }
                                ?>
                                
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>
                                Indikator
                            </label>
                            <select class="form-control search-select3" name="indicator_id" id="indikator" required oninvalid="this.setCustomValidity('Indikator tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Indikator </option>
                                @foreach($indicator as $indikator)
                                <?php
                                // if($indikator->getSubArea->role === 'Kecamatan' && $indkator->getSubArea->check_sub === 0 || $indikator->getSubArea->role === 'Kelurahan' && $indikator->getSubArea->check_sub === 0){

                                // }
                                // else {
                                ?>
                                <option value="{{$indikator->id}}" class="{{$indikator->sub_area_id}}" data-satuan="{{$indikator->unit_id}}">{{$indikator->indicator_name}}</option>
                                <?php
                                // }
                                ?>
                                
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Sub Indikator
                            </label>
                            <select class="form-control search-select subindicator" name="sub_indicator_id" id="subindikator" required oninvalid="this.setCustomValidity('Indikator tidak boleh kosong')" oninput="setCustomValidity('')">
                                <option value=""> Pilih Sub Indikator </option>
                                @foreach($sub_indicator as $subindikator)
                                <?php
                                $space = '';
                                for($i=0; $i<$subindikator['space']; $i++){
                                    $space = $space.'&nbsp;';
                                }
                                ?>
                                <option value="{{$subindikator['id']}}" class="{{$subindikator['indicator_id']}}" data-satuan="{{$subindikator['unit_id']}}" @if($subindikator['status'] === 0) disabled @endif>{!!$space!!}- {{$subindikator['sub_indicator_name']}}</option>
                                
                                @endforeach
                            </select>
                        </div>
                        <input type="hidden" name="satuan" id="satuan" value="">
                        <div class="form-group">
                            <label>
                                Satuan
                            </label>
                            <select name="" class="form-control search-select satuan"  id="satuan" oninvalid="this.setCustomValidity('Satuan tidak boleh kosong')" style="width: 100%;" oninput="setCustomValidity('')" disabled>
                                <option value="">Pilih Satuan</option>
                                @foreach($unit as $satuan)
                                <option value="{{$satuan->id}}">{{$satuan->unit}}</option>
                                @endforeach
                            </select>
                            <!-- <input type="" name="unit_id" class="form-control" value="" readonly> -->
                        </div>
                        <div class="form-group">
                            <label>
                                Nilai
                            </label>
                            <input type="text" id="target" name="value" placeholder="Masukan Nilai ..." class="form-control" required oninvalid="this.setCustomValidity('Nilai tidak boleh kosong')" oninput="setCustomValidity('')">
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>

<script src="//code.jquery.com/jquery.js"></script>
<script type="text/javascript" src="http://www.appelsiini.net/projects/chained/jquery.chained.js?v=0.9.10"></script>
@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection