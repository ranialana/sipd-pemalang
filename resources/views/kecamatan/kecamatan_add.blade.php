@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('master-kecamatan') }}">
                        Master Data Kecamatan
                    </a>
                </li>
                <li class="active">
                    Tambah Master Data Kecamatan
                </li>
            </ol>
            <div class="page-header">
                <h1>Tambah Master Data Kecamatan</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('save-kecamatan') }}" id="myform" onSubmit="return validasi()">
                        @csrf

                        <div class="form-group">
                            <label for="form-field-22">
                                Nama Kecamatan
                            </label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Kecamatan" required id="nama">
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Status
                            </label>
                            <select id="form-field-select-3" class="form-control search-select status_kec" style="width: 100%;" name="status" required>
                                <option value="2">Status kecamatan</option>
                                <option value="1" selected="true">Aktif</option>
                                <option value="0">Tidak Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    function validasi() {
        var nama = document.forms["myform"]["nama"].value;

        if (nama == null || nama == "") {
            swal({
                title: "Peringatan!",
                text: "Nama Kecamatan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data Master Kecamatan berhasil diubah!", "success");
    });
</script>
@endif
@endsection