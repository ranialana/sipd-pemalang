@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('master-kor') }}">
                        Master Data Kelurahan
                    </a>
                </li>
                <li class="active">
                    Ubah Master Data Kelurahan
                </li>
            </ol>
            <div class="page-header">
                <h1>Ubah Master Data Kelurahan</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('update-status-kelurahan') }}" id="myform" onSubmit="return validasi()">
                        @csrf
                        <input type="hidden" id="id" name="id" placeholder="Name" class="form-control" value="{{$kelurahan->id}}">
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Nama Kecamatan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select kecamatan_kel" style="width: 100%;" id="" name="kecamatan">
                                <option value=""></option>
                                @foreach($kecamatan as $row)
                                    <option value="{{$row->id}}" @if($row->id === $kelurahan->id_kec) selected @endif>{{$row->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="form-field-22">
                                Nama Kelurahan
                            </label>
                            <input type="text" class="form-control" name="nama" placeholder="Nama Kelurahan" required id="nama" value="{{$kelurahan->nama}}">
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Status
                            </label>
                            <select id="form-field-select-3" class="form-control search-select status_kel" style="width: 100%;" name="status" required>
                                <option value="2">Status kelurahan</option>
                                <option value="1" @if($kelurahan->status == 1) selected @endif>Aktif</option>
                                <option value="0" @if($kelurahan->status == 0) selected @endif>Tidak Aktif</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    function validasi() {
        var nama = document.forms["myform"]["nama"].value;

        if (nama == null || nama == "") {
            swal({
                title: "Peringatan!",
                text: "Nama Kelurahan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data Master Kelurahan berhasil diubah!", "success");
    });
</script>
@endif
@endsection