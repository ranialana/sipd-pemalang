@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li class="active">
                    Dashboard
                </li>
            </ol>
            <div class="page-header">
                <h1>Dashboard </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-edatabase')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-database circle-icon circle-green"></i>
                        <h2>eDatabase</h2>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{ route('view-kor') }}" class="href-index">
                    <div class="heading">
                        <i class="clip-stats circle-icon circle-green"></i>
                        <h2>KOR</h2>
                    </div>
                </a>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-spm')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-health circle-icon circle-green"></i>
                        <h2>SPM</h2>
                    </div>
                </a>

            </div>
        </div>
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-sdgs')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-transfer circle-icon circle-green"></i>
                        <h2>SDGs</h2>
                    </div>
                </a>
            </div>
        </div>

    </div>


    <div class="row">
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-datadukung')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-data circle-icon circle-green"></i>
                        <h2>Data Dukung</h2>
                    </div>
                </a>

            </div>
        </div>
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-bmd')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-stack-2 circle-icon circle-green"></i>
                        <h2>BMD</h2>
                    </div>
                </a>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-6">
            <div class="core-box">
                <a href="{{route('view-iku')}}" class="href-index">
                    <div class="heading">
                        <i class="clip-rating-2 circle-icon circle-green"></i>
                        <h2>Indikator Kinerja Urusan</h2>
                    </div>
                </a>

            </div>
        </div>
        <!-- <div class="col-sm-6">
            <div class="core-box">
                <a href="bps.html" class="href-index">
                    <div class="heading">
                        <i class="clip-banknote circle-icon circle-green"></i>
                        <h2>BPS</h2>
                    </div>
                </a>
            </div>
        </div> -->

    </div>

</div>
</div>

</div>
@endsection