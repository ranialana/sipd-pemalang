<div class="modal-dialog modal-md">
    <div class="modal-content">
        <form method="post" action="{{ route('save-business') }}" id="myform_edit" onSubmit="return validasi_edit()">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Ubah Urusan</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">

                    @csrf
                    <div class="form-group">
                        <label for="form-field-22">
                            Urusan
                        </label>
                        <input type="text" class="form-control business_name" placeholder="Urusan" id="uraian_edit" value="{{$business->business_name}}" name="uraian">
                        <input type="hidden" class="form-control id" value="{{$business->id}}" name="id">
                    </div>
                    <div class="form-group" id="role" name="role">
                        <label for="my-input">Pengisi {{$business->role}}</label>
                        <select class="form-control" name="role" style="width: 100%;" id="pengampu_edit">
                            <option value="">Pilih Pengisi</option>
                            <!-- <option value="Administrator">Administrator</option>
                <option value="Perangkat Daerah">Perangkat Daerah</option> -->

                            @foreach($skpd as $data)
                            <option value="{{$data->skpd_name}}">{{$data->skpd_name}}</option>
                            @endforeach
                            <option value="Kecamatan">Kecamatan</option>
                            <option value="Kelurahan">Kelurahan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="my-input">Satuan</label>
                        <select class="form-control" style="width: 100%;" id="satuan_edit" name="satuan">
                            <option value=""></option>
                            @foreach($unit as $row)
                            <option value="{{$row->id}}">{{$row->unit}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="checkbox">
                        <label>
                            <?php
                            if ($business->check_sub === 1) {
                                $check_sub = 'checked';
                            } else {
                                $check_sub = '';
                            }
                            ?>
                            <input type="checkbox" id="check_sub_edit" name="check_sub" onchange="validasi()" {{$check_sub}}>
                            Ceklis Jika Ada Bidang
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                    Tutup
                </button>
                <button class="btn btn-default" type="submit">
                    Simpan
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    $('#satuan_edit').val(<?php echo $business->unit_id; ?>);
    $("#satuan_edit").select2({
        dropdownParent: $("#satuan_edit").parent(),
        placeholder: "Pilih Satuan",
        allowClear: true
    }).trigger('change');

    $('#pengampu_edit').val("<?php echo $business->role; ?>");
    $("#pengampu_edit").select2({
        dropdownParent: $("#pengampu_edit").parent(),
        placeholder: "Pilih Pengampu",
        allowClear: true
    }).trigger('change');
</script>