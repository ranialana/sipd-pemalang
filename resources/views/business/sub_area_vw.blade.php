@extends('layouts/master_vw')
@section('content')


<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <!-- start: STYLE SELECTOR BOX -->
      <ol class="breadcrumb">
        <li>
          <i class="clip-home-3"></i>
          <a href="{{ route('dashboard') }}}">
            eDatabase SIPD
          </a>
        </li>
        <li>
          <a href="{{ url('/view-business') }}">
            Urusan
          </a>
        </li>
        <li>
          <a href="{{ url('/detail-business') }}/<?php echo $business_id_get; ?>">
            {{$business_name}}
          </a>
        </li>

        <li class="active">
          {{$area_name}}
        </li>
      </ol>
      <div class="page-header">
        <h1> Sub Bidang</h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-external-link-square"></i> Master
        </div>
        <div class="panel-body">

          <dl class="dl-horizontal">
            <dt>
              Urusan :
            </dt>
            <dd>
              {{$business_name}}
            </dd>
            <dt>
              Bidang :
            </dt>
            <dd>
              {{$area_name}}
            </dd>

          </dl>


        </div>

      </div>
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-external-link-square"></i> eDatabase SIPD
        </div>
        <div class="panel-body">

          <div class="row">
            <div class="col-12">


              <div class="col-md-6">
              </div>
              <div class="col-md-6" align="right">

                <fieldset class="form-group">
                <a href="{{url('/export-pdf-sub-area')}}/{{$area_id_get}}" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
                  <a href="{{url('/export-excel-sub-area')}}/{{$area_id_get}}" type="button" class="btn btn-success">Export Excel</a>
                <?php
                  if($role === 'Kecamatan' && $check_sub === 0 || $role === 'Kelurahan' && $check_sub === 0){

                  }
                  else {
                    ?>
                  <a data-toggle="modal" class="btn btn-success" role="button" href="#myModal1">
                    Tambah Data
                  </a>
                    <?php
                  }
                  ?>
                </fieldset>

              </div>

            </div>
          </div>

          <div class="table-responsive">
            <table class="table table-bordered data-table">
              <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th>Sub Bidang</th>
                  <th width="100px" class="text-center">Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </div>



        </div>
      </div>
      <!-- end: RESPONSIVE TABLE PANEL -->
    </div>
  </div>




</div>
</div>

</div>


<div class="modal fade" id="myModal1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">Tambah Sub Bidang</h4>
      </div>
      <div class="modal-body">
        <div class="panel-body">
          <form method="post" action="{{ route('save-sub-area') }}" id="myform" onSubmit="return validasi()">
            @csrf
            <div class="form-group">
              <label for="form-field-22">
                Sub Bidang
              </label>
              <input type="hidden" class="form-control" placeholder="Urusan" name="business_id" value="{{$business_id_get}}">
              <input type="hidden" class="form-control" placeholder="Urusan" name="area_id" value="{{$area_id_get}}">
              <input type="text" class="form-control" placeholder="Sub Bidang" id="uraian" name="uraian">
            </div>
            <div class="form-group" id="role" name="role">
              <label for="my-input">Pengisi</label>
              <select class="form-control search-select pengisi" name="role" style="width: 100%;">
                <option value="">Pilih Pengisi</option>
                <!-- <option value="Administrator">Administrator</option> -->
                @foreach($skpd as $data)
                <option value="{{$data->skpd_name}}">{{$data->skpd_name}}</option>
                @endforeach
                <option value="Kecamatan">Kecamatan</option>
                <option value="Kelurahan">Kelurahan</option>
              </select>
            </div>
            <div class="form-group">
              <label for="my-input">Satuan</label>
              <select id="form-field-select-3" class="form-control search-select satuan" style="width: 100%;" id="satuan_edit" name="satuan" required>
                <option value=""></option>
                @foreach($unit as $row)
                <option value="{{$row->id}}">{{$row->unit}}</option>
                @endforeach
              </select>
            </div>
              <div class="checkbox">
              <label>
                <input type="checkbox" name="check_sub" class="grey check_sub1" onchange="validasi()">
                Ceklis Jika Ada Indikator
              </label>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
          Tutup
        </button>
        <button class="btn btn-default" type="submit">
          Simpan
        </button>
      </div>
    </div>
  </div>
  </form>
</div>


<div class="modal fade" id="edit">

</div>
<?php
$business_id_get = json_decode($business_id_get);
$area_id_get = json_decode($area_id_get);
?>
<script src="//code.jquery.com/jquery.js"></script>
<script type="text/javascript">
  $(function() {

    var business = <?php echo $business_id_get; ?>;
    var area = <?php echo $area_id_get; ?>;

    var table = $('.data-table').DataTable({
      "scrollX": false,
      "scrollY": false,
      processing: true,
      serverSide: true,
      ajax: "{{ url('sub-area') }}?id=" + area + "&business_id=" + business,
      "aoColumnDefs": [

        {
          "sWidth": "5%",
          "aTargets": [0],
          "className": "text-center"
        },
        {
          "aTargets": [1]
        },
        {
          "sWidth": "25%",
          "aTargets": [2],
          "className": "text-center"
        }
      ],
      columns: [{
          data: 'DT_RowIndex',
          name: 'DT_RowIndex'
        },
        {
          data: 'subarea_name',
          name: 'subarea_name'
        },
        {
          data: 'action',
          name: 'action',
          orderable: false,
          searchable: false
        },
      ]
    });

  });



  function validasi() {
    var uraian = document.forms["myform"]["uraian"].value;
    if (uraian == null || uraian == "") {
      swal({
        title: "Peringatan!",
        text: "Sub Bidang tidak boleh kosong",
        type: "warning",
        confirmButtonClass: 'btn btn-warning',
        buttonsStyling: false,
      });
      return false;
    };
  }


  function validasi_edit() {
    var uraian_edit = document.forms["myform_edit"]["uraian_edit"].value;
    if (uraian_edit == null || uraian_edit == "") {
      swal({
        title: "Peringatan!",
        text: "Sub Bidang tidak boleh kosong",
        type: "warning",
        confirmButtonClass: 'btn btn-warning',
        buttonsStyling: false,
      });
      return false;
    };
  }



  $(document).on("click", '.edit_button', function(e) {
    var id = $(this).data('id');
    var subarea_name = $(this).data('subarea_name');
    
    var check_sub = $(this).data('check_sub');
    var role = $(this).data('role');
    var satuan_id = $(this).data('satuan');

    $(".role").val(role);

    $(".id").val(id);
    $(".subarea_name").val(subarea_name);
    $(".indicator_name").val(indicator_name);
    if (check_sub === 1) {
      $("#myModalEdit .cek_sub").prop('checked', true);
      $('.search-select').val(satuan_id);
    $('.search-select').select2().trigger('change');
    }


  });
</script>
@if (session('success'))
<script>
  $(document).ready(function() {

    swal("Berhasil!", "Data sub bidang berhasil disimpan!", "success");
  });
</script>
@endif

@if (session('success_delete'))
<script>
  $(document).ready(function() {

    swal("Berhasil!", "Data sub bidang berhasil dihapus!", "success");
  });
</script>
@endif
<script>
  $(document).ready(function() {
    $('body').on('click', '.hapus_kor', function() {

      var delete_url = $(this).attr('data-url');

      swal({
        title: "Hapus Sub Bidang?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Hapus !",
        cancelButtonText: "Batalkan",
        closeOnConfirm: false
      }, function() {

        window.location.href = delete_url;
      });

      return false;
    });
  });

  // Modal Edit
  function edit(id) {
    var id = id;
    var url = "{{url('subarea/edit/:id')}}".replace(':id', id);
    $.ajax({
      url: url,
      type: "GET",
      data: {
        id: id
      },
      success: function(ajaxData) {
        $("#edit").html(ajaxData);
        $("#edit").modal('show', {
          backdrop: 'true'
        });
      }
    });
  }

  $(document).ready(function() {
    $(document).on("click", ".edit", function() {
      var m = $(this).attr("id");
      var link = "{{url('subarea/edit/:id')}}".replace(':id', m);
      $.ajax({
        url: link,
        type: "GET",
        data: {
          id: m
        },
        success: function(ajaxData) {
          $("#edit").html(ajaxData);
          $("#edit").modal('show', {
            backdrop: 'true'
          });
        }
      });
    });
  });
</script>
@endsection