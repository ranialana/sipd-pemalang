<div class="modal-dialog modal-md">
    <div class="modal-content">
        <form method="post" action="{{ route('save-sub-indicator') }}" id="myform_edit" onSubmit="return validasi_edit()">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Ubah Sub Indikator</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    @csrf
                    <div class="form-group">
                        <label for="form-field-22">
                            Sub Indikator
                        </label>
                        <input type="text" class="form-control sub_indicator_name" placeholder="Indikator" id="uraian_edit" name="uraian" value="{{$subindicator->sub_indicator_name}}">
                        <input type="hidden" class="form-control id" placeholder="Sub Bidang" name="id" value="{{$subindicator->id}}">

                        <input type="hidden" class="form-control" placeholder="Urusan" name="business_id" value="{{$subindicator->business_id}}">
                        <input type="hidden" class="form-control" placeholder="Urusan" name="area_id" value="{{$subindicator->area_id}}">
                        <input type="hidden" class="form-control" placeholder="Urusan" name="sub_area_id" value="{{$subindicator->sub_area_id}}">

                        <input type="hidden" class="form-control" placeholder="Urusan" name="indicator_id" value="{{$subindicator->indicator_id}}">



                    </div>
                    <div class="form-group">

                        <select class="form-control" name="role" style="width: 100%;" id="pengampu_edit">
                            <option value="">Pilih Pengisi</option>
                            <!-- <option value="Administrator">Administrator</option>
                <option value="Perangkat Daerah">Perangkat Daerah</option> -->

                            @foreach($skpd as $data)
                            <option value="{{$data->skpd_name}}">{{$data->skpd_name}}</option>
                            @endforeach
                            <option value="Kecamatan">Kecamatan</option>
                            <option value="Kelurahan">Kelurahan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="my-input">Satuan</label>
                        <select id="form-field-select-3" class="form-control" style="width: 100%;" id="satuan_edit" name="satuan">
                            <option value=""></option>
                            @foreach($unit as $row)
                            <option value="{{$row->id}}">{{$row->unit}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="checkbox">
                        <label>
                            <?php
                            if ($subindicator->check_sub === 1) {
                                $check_sub = 'checked';
                            } else {
                                $check_sub = '';
                            }
                            ?>
                            <input type="checkbox" name="check_sub" class="grey cek_sub" {{$check_sub}}>
                            Ceklis Jika Ada Sub Indikator
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                    Tutup
                </button>
                <button class="btn btn-default" type="submit">
                    Simpan
                </button>
            </div>
        </form>
    </div>
</div>
<script>
    $('#satuan_edit').val(<?php echo $subindicator->unit_id; ?>);
    $("#satuan_edit").select2({
        dropdownParent: $("#satuan_edit").parent(),
        placeholder: "Pilih Satuan",
        allowClear: true
    }).trigger('change');

    $('#pengampu_edit').val("<?php echo $subindicator->role; ?>");
    $("#pengampu_edit").select2({
        dropdownParent: $("#pengampu_edit").parent(),
        placeholder: "Pilih Pengampu",
        allowClear: true
    }).trigger('change');
</script>