@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li class="active">
                    Indikator Kinerja Urusan
                </li>
            </ol>
            <div class="page-header">
                <h1> Indikator Kinerja Urusan  <small>(IKU)</small></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- start: RESPONSIVE TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-12">

                            <div class="col-md-6">
                                <fieldset class="form-group">
                                    <select id="form-field-select-3" class="form-control search-select urusan">
                                        <option value=""></option>
                                        @foreach($urusan as $item)
                                        <option value="{{$item->business_name}}">{{$item->business_name}}</option>
                                        @endforeach
                                    </select>

                                </fieldset>
                            </div>
                            <div class="col-md-6" align="right">

                                <fieldset class="form-group">

                                <a href="{{route('export-pdf-iku')}}" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
                                <a href="{{route('export-excel-iku')}}" type="button" class="btn btn-success">Export Excel</a>


                                    <a class="btn btn-danger" role="button" href="{{route('add-iku')}}">Tambah Data</a>
                                </fieldset>

                            </div>

                        </div>
                    </div>


                    <div class="table-responsive">

                        <table class="table table-bordered table-hover data-table">
                            <thead>
                                <tr>

                                    <th style="width: 5%;text-align: center;">No</th>
                                    <th>Uraian</th>
                                    <th>Urusan</th>
                                    <th>Satuan</th>
                                    <th>Target <?php echo date('Y')+1; ?></th>
                                    <th style="text-align: center;">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>

<script>
    $(function() {

        var table = $('.data-table').DataTable({
            "scrollX": false,
            "scrollY": false,
            processing: true,
            serverSide: true,
            pageLength: 100,
            columnDefs: [{
                searchable: true,
            }],
            ajax: {
                url: "{{route('view-iku')}}",
                data: function(d){
                    d.urusan = $('.search-select').val(),
                    d.search = $('input[type="search"]').val()
                }
            },
            "aoColumnDefs": [

                {
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                },
                {
                    "aTargets": [1]
                },
                {
                    "aTargets": [2]
                },
                {
                    "sWidth": "15%",
                    "aTargets": [3]
                },
                {
                    "aTargets": [4]
                },
                {
                    "sWidth": "10%",
                    "aTargets": [5],
                    "className": "text-center"
                }
            ],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'iku_description',
                    name: 'iku_description'
                },
                {
                    data: 'urusan',
                    name: 'urusan'
                },
                {
                    data: 'satuan',
                    name: 'satuan'
                },
                {
                    data: 'target',
                    name: 'target'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
        $('.search-select').change(function(){
            table.draw();
        });
    });

    function validasi() {
        var uraian = document.forms['myform']['uraian'].value;
        var satuan = document.forms['myform']['satuan'].value;

        if (uraian == null || uraian == '') {
            swal({
                title: "Peringatan!",
                text: "Uraian tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
        if (satuan == null || satuan == '') {
            swal({
                title: "Peringatan!",
                text: "Satuan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
    }
</script>

@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
<script>
    $(document).ready(function() {
        $('body').on('click', '.hapus_iku', function() {

            var delete_url = $(this).attr('data-url');

            swal({
                title: "Hapus IKU?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus !",
                cancelButtonText: "Batalkan",
                closeOnConfirm: false
            }, function() {

                window.location.href = delete_url;
            });

            return false;
        });
    });

    function validasi_import() {
        var fileExtensions = [".xls", ".xlsx", ".csv"];

        var uraian_import = document.forms["myform_import"]["uraian_import"].value;

        if (uraian_import.length > 0) {
            var validation = false;
            for (var j = 0; j < fileExtensions.length; j++) {
                var Extention = fileExtensions[j];
                if (uraian_import.substr(uraian_import.length - Extention.length, Extention.length).toLowerCase() == Extention.toLowerCase()) {
                    validation = true;
                    break;
                }
            }

            if (!validation) {
                swal({
                    title: "Peringatan!",
                    text: "Format file tidak didukung",
                    type: "warning",
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false,
                });
                return false;
            }
        }
        return true;
    }
</script>
@endsection