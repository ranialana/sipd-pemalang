@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('view-iku') }}">
                        Indikator Kinerja Urusan
                    </a>
                </li>
                <li class="active">
                    Ubah Indikator Kinerja Urusan
                </li>
            </ol>
            <div class="page-header">
                <h1>Ubah Indikator Kinerja Urusan <small>(IKU)</small></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form method="POST" action="{{route('save-iku')}}">
                        @csrf()
                        <input type="hidden" name="id" value="{{$iku->id}}">
                        <div class="form-group">
                            <label>
                                Uraian
                            </label>
                            <textarea placeholder="Uraian" id="form-field-22" name="uraian" class="form-control" required oninvalid="this.setCustomValidity('Uraian tidak boleh kosong')" oninput="setCustomValidity('')">{{$iku->iku_description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>
                                Urusan
                            </label>
                            <select multiple="multiple" id="form-field-select-4" class="form-control search-select urusan" name="urusan[]">
                                @foreach($urusan as $list)
                                    <?php
                                    $businessID=$list->id;
                                    $selected = null;
                                    foreach($detail_iku as $urusan) {
                                        if ($businessID == $urusan->business_id) {
                                            $selected= ' selected=""';
                                        break;
                                        }
                                    }
                                    ?>
                                    <option value="{{$list->id}}" {{$selected}}>{{$list->business_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Satuan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select satuan" style="width: 100%;" id="satuan" name="satuan" required>
                                <option value=""></option>
                                @foreach($satuan as $row)
                                <option value="{{$row->id}}" {{ $row->id == $iku->unit_id ? 'selected' : ''}}>{{$row->unit}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>
                                Target
                            </label>
                            <input type="text" name="target" id="target" placeholder="Target" class="form-control" value="{{$iku->target}}">
                        </div>

                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary" id="ubah_indikator">Simpan</button>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>

<script>
    function validasi() {
        var uraian = document.forms['myform']['uraian'].value;
        var satuan = document.forms['myform']['satuan'].value;

        if (uraian == null || uraian == '') {
            swal({
                title: "Peringatan!",
                text: "Uraian tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
        if (satuan == null || satuan == '') {
            swal({
                title: "Peringatan!",
                text: "Satuan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
    }
</script>

@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection