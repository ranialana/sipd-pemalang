@extends('layouts/master_vw')
@section('content')
<div class="container">
                <!-- start: PAGE HEADER -->
                <div class="row">
                    <div class="col-sm-12">
                        <!-- start: STYLE SELECTOR BOX -->
                        <ol class="breadcrumb">
                            <li>
                                <i class="clip-home-3"></i>
                                <a href="{{ route('dashboard') }}">
                                    eDatabase SIPD
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view-iku') }}">
                                 Indikator Kinerja Urusan
                             </a>
                         </li>
                         <li class="active">
                          Tambah Indikator Kinerja Urusan
                      </li>
                  </ol>
                  <div class="page-header">
                    <h1>Tambah Indikator Kinerja Urusan <small>(IKU)</small></h1>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <i class="fa fa-external-link-square"></i> eDatabase SIPD
                    </div>
                    <div class="panel-body">
                        <form action="{{route('save-iku')}}" role="form" id="form2" method="POST">
                            @csrf()
                            <div class="form-group">
                                <label>
                                    Uraian
                                </label>
                                <textarea name="uraian" placeholder="Uraian" id="form-field-22" class="form-control" required oninvalid="this.setCustomValidity('Uraian tidak boleh kosong')" oninput="setCustomValidity('')"></textarea>
                            </div>
                            <div class="form-group">
                                <label>
                                    Urusan
                                </label>
                                <select name="urusan[]" multiple="multiple" id="form-field-select-4" class="form-control search-select urusan">
                                    <option value=""></option>
                                    @foreach($urusan as $item)
                                    <option value="{{$item->id}}">{{$item->business_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>
                                    Satuan
                                </label>
                                <!-- <input type="text" id="satuan" placeholder="Satuan" class="form-control" required oninvalid="this.setCustomValidity('Satuan tidak boleh kosong')" oninput="setCustomValidity('')"> -->
                                <select name="satuan" id="form-field-select-3" class="form-control search-select satuan" required>
                                    <option value=""></option>
                                    @foreach($unit as $satuan)
                                    <option value="{{$satuan->id}}">{{$satuan->unit}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label>
                                   Target
                                </label>
                                <input name="target" type="text" id="target" placeholder="Target" class="form-control">
                            </div>

                            <div class="form-group">
                                <button type="reset" class="btn btn-default">Reset</button>
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>   
        </div>
    </div>
<script src="//code.jquery.com/jquery.js"></script>

<script>
    function validasi() {
        var uraian = document.forms['myform']['uraian'].value;
        var satuan = document.forms['myform']['satuan'].value;

        if (uraian == null || uraian == '') {
            swal({
                title: "Peringatan!",
                text: "Uraian tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
        if (satuan == null || satuan == '') {
            swal({
                title: "Peringatan!",
                text: "Satuan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
    }
</script>

@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection