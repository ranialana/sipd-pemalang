@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('view-users') }}">
                        User
                    </a>
                </li>
                <li class="active">
                    Ubah User
                </li>
            </ol>
            <div class="page-header">
                <h1>Ubah User</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    @foreach($user as $row)
                    <form method="post" action="{{route('save-user')}}" id="myform" onSubmit="">
                        @csrf

                        <div class="form-group">
                            <label>
                                Name
                            </label>

                            <input type="hidden" id="id" name="id_user" placeholder="Name" class="form-control" value="{{$row->id}}">
                            <input type="text" id="full_name" name="full_name" placeholder="Name" class="form-control" value="{{$row->full_name}}">
                        </div>
                        <div class="form-group">
                            <label>
                                Username
                            </label>
                            <input type="text" id="username" placeholder="Username" class="form-control" value="{{$row->username}}"" name=" username">
                        </div>

                        <div class="form-group">
                            <label>
                                Email
                            </label>
                            <input type="text" id="email" placeholder="Email" class="form-control" value="{{$row->email}}" name="email">
                        </div>

                        <div class="form-group">
                            <label>
                                Role
                            </label>
                            <select class="form-control" name="role" id="role">
                                <option value="2" @if($row->role=="2") selected @endif>User</option>
                                <option value="1" @if($row->role=="1") selected @endif>Penyelia</option>
                                <option value="0" @if($row->role=="0") selected @endif>Super Admin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Tipe User
                            </label>
                            <select class="form-control search-select4" name="type_user" id="tipe_user" required oninvalid="this.setCustomValidity('Tipe User tidak boleh kosong')">
                                <option value=""> Tipe User </option>
                                <option value="0" class="2" @if($row->type_user=== 0) selected @endif>Perangkat Daerah</option>
                                <option value="1" class="2" @if($row->type_user===1) selected @endif>Kecamatan</option>
                                <option value="2" class="2" @if($row->type_user===2) selected @endif>Kelurahan</option>
                                <option value="0" class="1" @if($row->type_user=== 0) selected @endif>Perangkat Daerah</option>
                                <option value="1" class="1" @if($row->type_user===1) selected @endif>Kecamatan</option>
                                <option value="2" class="1" @if($row->type_user===2) selected @endif>Kelurahan</option>
                            </select>
                        </div>
                        <div class="form-group" id="skpd_uk">
                            <label>
                                Unit Kerja
                            </label>
                            <select name="skpd_id[]" multiple="multiple" id="skpd" id="form-field-select-4" class="form-control search-select skpd">
                                @foreach($skpd_list as $list)
                                <?php
                                $skpdID = $list->id;
                                $selected = null;
                                foreach ($user_skpd as $u_sk) {
                                    if ($skpdID == $u_sk->skpd_id) {
                                        $selected = ' selected=""';
                                        break;
                                    }
                                }
                                ?>
                                <option value="{{$list->id}}" class="2" {{$selected}}>{{$list->skpd_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="kecamatan">
                            <label for="form-field-22">
                                Kecamatan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select kecamatan_kel" style="width: 100%;" name="kecamatan">
                                @if($row->id_kec != null)
                                @foreach($kecamatan as $kec)
                                <option value="{{$kec->id}}" @if($kec->id === $row->id_kec) selected @endif>{{$kec->nama}}</option>
                                @endforeach
                                @endif
                                @if($row->id_kec === null)
                                @foreach($kecamatan as $kec)
                                <option value="{{$kec->id}}">{{$kec->nama}}</option>
                                @endforeach
                                @endif
                            </select>

                        </div>
                        <div class="form-group" id="kelurahan">
                            <label for="form-field-22">
                                Kelurahan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select kelurahan_user" style="width: 100%;" name="kelurahan">
                                @if($row->id_kel != null)
                                @foreach($kelurahan as $kel)
                                <option value="{{$kel->id}}" @if($kel->id === $row->id_kel) selected @endif>{{$kel->nama}}</option>
                                @endforeach
                                @endif
                                @if($row->id_kel === null)
                                @foreach($kelurahan as $kel)
                                <option value="{{$kel->id}}">{{$kel->nama}}</option>
                                @endforeach
                                @endif
                            </select>

                        </div>


                        <div class="form-group">
                            <label>
                                Password
                            </label>
                            <input type="password" id="password" placeholder="Isi jika akan ganti password" class="form-control" name="password">
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    @endforeach
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    function validasi() {
        var full_name = document.forms["myform"]["full_name"].value;
        var username = document.forms["myform"]["username"].value;
        var role = document.forms["myform"]["role"].value;

        if (full_name == null || full_name == "") {
            swal({
                title: "Peringatan!",
                text: "Nama user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (username == null || username == "") {
            swal({
                title: "Peringatan!",
                text: "Username tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (role == null || role == "") {
            swal({
                title: "Peringatan!",
                text: "Peran user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }
</script>
<script>
    $(document).ready(function() {
        $('#tipe_user').on('change', function(){
            $('#tipe_user').val();
        });
    });
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data user berhasil diubah!", "success");
    });
</script>
@endif
@endsection