@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('view-users') }}">
                        User
                    </a>
                </li>
                <li class="active">
                    Tambah User
                </li>
            </ol>
            <div class="page-header">
                <h1>Tambah User</h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('save-user') }}" id="myform" onSubmit="return validasi()">
                        @csrf
                        <div class="form-group">
                            <label>
                                Name
                            </label>
                            <input type="text" id="full_name" name="full_name" placeholder="Name" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>
                                Username
                            </label>
                            <input type="text" id="username" placeholder="Username" name="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>
                                Email
                            </label>
                            <input type="text" id="email" placeholder="Email" class="form-control" name="email">
                        </div>
                        <div class="form-group">
                            <label>
                                Role
                            </label>
                            <select class="form-control" name="role" id="role">
                                <option value="2">User</option>
                                <option value="1">Penyelia</option>
                                <option value="0">Super Admin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Tipe User
                            </label>
                            <select class="form-control search-select4" name="type_user" id="tipe_user" required>

                                <option value=""> Tipe User </option>
                                <option value="0" class="2">Perangkat Daerah</option>
                                <option value="1" class="2">Kecamatan</option>
                                <option value="2" class="2">Kelurahan</option>
                                <option value="0" class="1">Perangkat Daerah</option>
                                <option value="1" class="1">Kecamatan</option>
                                <option value="2" class="1">Kelurahan</option>
                            </select>
                        </div>
                        <div class="form-group" id="skpd_uk">
                            <label>
                                Unit Kerja
                            </label>
                            <select name="skpd_id[]" multiple="multiple" id="skpd" id="form-field-select-4" class="form-control skpd">
                                @foreach($skpd_list as $list)
                                <option value="{{$list->id}}" class="2">{{$list->skpd_name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="kecamatan">
                            <label for="form-field-22">
                                Kecamatan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select kecamatan_kel" style="width: 100%;" name="kecamatan">
                                @foreach($kecamatan as $kec)
                                <option value="{{$kec->id}}">{{$kec->nama}}</option>
                                @endforeach
                            </select>

                        </div>
                        <div class="form-group" id="kelurahan">
                            <label for="form-field-22">
                                Kelurahan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select kelurahan_user" style="width: 100%;" name="kelurahan">
                                @foreach($kelurahan as $kel)
                                <option value="{{$kel->id}}" >{{$kel->nama}}</option>
                                @endforeach
                            </select>

                        </div>

                        <div class="form-group">
                            <label>
                                Password
                            </label>
                            <input type="text" placeholder="Password" class="form-control" id="password" name="password">
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>

<script>
    function validasi() {
        var full_name = document.forms["myform"]["full_name"].value;
        var username = document.forms["myform"]["username"].value;
        var role = document.forms["myform"]["role"].value;
        var password = document.forms["myform"]["password"].value;

        if (full_name == null || full_name == "") {
            swal({
                title: "Peringatan!",
                text: "Nama user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (username == null || username == "") {
            swal({
                title: "Peringatan!",
                text: "Username tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (role == null || role == "") {
            swal({
                title: "Peringatan!",
                text: "Peran user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };


        if (password == null || password == "") {
            swal({
                title: "Peringatan!",
                text: "Password tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data user berhasil ditambah!", "success");
    });
</script>
@endif
@endsection