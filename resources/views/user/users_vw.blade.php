@extends('layouts/master_vw')
@section('content')


<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li class="active">
                    User
                </li>
            </ol>
            <div class="page-header">
                <h1> User </h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <!-- start: RESPONSIVE TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-12">


                            <div class="col-md-6">
                                <!--
                              <fieldset class="form-group">
                               <input type="text" placeholder="Search" class="form-control">

                              </fieldset>
                          -->
                            </div>
                            <div class="col-md-6" align="right">

                                <fieldset class="form-group">
                                    <a href="{{route('export-pdf-user')}}" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
                                    <a href="{{route('export-excel-user')}}" type="button" class="btn btn-success">Export Excel</a>
                                    <a class="btn btn-primary" data-toggle="modal" href='#myModalImport'>Import Excel</a>
                                    <a class="btn btn-success" role="button" href="{{ route('add-user') }}">
                                        Tambah Data
                                    </a>
                                </fieldset>

                            </div>

                        </div>
                    </div>


                    <div class="table-responsive">



                        <table class="table table-bordered table-hover data-table">

                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Nama</th>
                                    <th>Role</th>
                                    <th>Username</th>
                                    <th>Unit Kerja</th>
                                    <th>Status Aktif</th>
                                    <th width="100px" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>


                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
    </div>




</div>
</div>

</div>

<div class="modal fade" id="myModalImport" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Import Excel</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form action="{{route('import-user')}}" method="POST" id="myform_import" role="form" enctype="multipart/form-data" onSubmit="return validasi_import()">
                        @csrf
                        <div class="form-group">
                            <div class="col-sm-12">
                                <label>
                                    Pilih File Excel
                                </label>
                                <input type="file" class="file" data-show-preview="false" id="uraian_import" placeholder="Import Excel" name="uraian_import">
                                <a href="{{route('template-user')}}"><i class="clip-download"></i> Download Template Excel</a>
                            </div>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </form>
            </div>
        </div>
    </div>
</div>




<script src="//code.jquery.com/jquery.js"></script>
@if (session('success_import'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success_import')}}", "success");
    });
</script>
@endif
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(function() {

        var table = $('.data-table').DataTable({
            "scrollX": false,
            "scrollY": false,
            processing: true,
            serverSide: true,
            ajax: "{{ route('view-users') }}",
            "aoColumnDefs": [

                {
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                },
                {
                    "sWidth": "25%",
                    "aTargets": [1]
                },
                {
                    "aTargets": [2]
                },
                {
                    "aTargets": [6],
                    "className": "text-center"
                }

            ],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'full_name',
                    name: 'full_name'
                },
                {
                    data: 'role_user',
                    name: 'role_user'
                },
                {
                    data: 'username',
                    name: 'username'
                },
                {
                    data: 'unit_kerja',
                    name: 'unit_kerja'
                },
                {
                    data: 'status_aktif',
                    name: 'status_aktif'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });

    });


    $(document).on("click", '.aktif_user', function(e) {
        var id = $("#aktif_user").val();
        var status = 0;
        $.ajax({
            type: 'GET',
            url: '{{ route("update-status-user") }}',
            data: {
                id: id,
                status: status
            },
            success: function(response) {
                location.reload();

            }
        });
    });


    $(document).on("click", '.nonaktif_user', function(e) {
        var id = $("#nonaktif_user").val();
        var status = 1;
        $.ajax({
            type: 'GET',
            url: '{{ route("update-status-user") }}',
            data: {
                id: id,
                status: status
            },
            success: function(response) {
                location.reload();

            }
        });

    });

    function validasi_import() {
        var fileExtensions = [".xls", ".xlsx", ".csv"];

        var uraian_import = document.forms["myform_import"]["uraian_import"].value;

        if (uraian_import.length > 0) {
            var validation = false;
            for (var j = 0; j < fileExtensions.length; j++) {
                var Extention = fileExtensions[j];
                if (uraian_import.substr(uraian_import.length - Extention.length, Extention.length).toLowerCase() == Extention.toLowerCase()) {
                    validation = true;
                    break;
                }
            }

            if (!validation) {
                swal({
                    title: "Peringatan!",
                    text: "Format file tidak didukung",
                    type: "warning",
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false,
                });
                return false;
            }
        }
        return true;
    }
</script>
@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
@endsection