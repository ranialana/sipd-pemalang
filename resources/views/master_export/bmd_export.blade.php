<!DOCTYPE html>
<html lang="en">

<head>
    <title>Master BMD</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <table width="100%" border='1' cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Satuan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($bmd as $index=>$row)
            <tr>
                <?php
                $space = '';
                for ($i = 0; $i < $row['space']; $i++) {
                    $space = $space . '&nbsp;';
                }
                ?>
                <td style="text-align: center;">{{$index+1}}</td>
                <td>{!!$space!!}{{$row['bmd_description']}}</td>
                <td>{{$row['satuan']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>