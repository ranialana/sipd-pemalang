<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Master SPM</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <table width="100%" border='1' cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Uraian</th>
                    <th>Satuan</th>
                </tr>
            </thead>
            <tbody>
                @foreach($spm as $index=>$row)
                <tr>
                    <td style="text-align: center;">{{$index+1}}</td>
                    <td>{{$row->spm_description}}</td>
                    <td>{{$row->unit->unit}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
