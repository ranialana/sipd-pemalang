<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Master Kelurahan</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <table width="100%" border='1' cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Kecamatan</th>
                    <th>Nama Kelurahan</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($kelurahan as $index=>$row)
                <tr>
                    <td style="text-align: center;">{{$index+1}}</td>
                    <td>{{$row->kec->nama}}</td>
                    <td>{{$row->nama}}</td>
                    <td style="text-align: center;">
                        <?php
                        if($row->status === 1){
                            echo "Aktif";
                        }
                        else {
                            echo "Tidak Aktif";
                        }
                        ?>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
