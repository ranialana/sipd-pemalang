<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Master Users</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <table width="100%" border='1' cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Role</th>
                    <th>Username</th>
                    <th>Unit Kerja</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                @foreach($user as $index=>$row)
                <tr>
                    <td style="text-align: center;">{{$index+1}}</td>
                    <td>{{$row['nama']}}</td>
                    <td>{{$row['role']}}</td>
                    <td>{{$row['username']}}</td>
                    <td>{{$row['unit_kerja']}}</td>
                    <td style="text-align: center;">{{$row['status']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
