<!DOCTYPE html>
<html lang="en">

<head>
    <title>Master Bidang</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <table width="100%" border='1' cellspacing="0">
        <thead>
            <tr>
                <th>No</th>
                <th>Uraian</th>
                <th>Satuan</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($area as $index=>$row)
            <tr>
                <td style="text-align: center;">{{$index+1}}</td>
                <td>{{$row['area_name']}}</td>
                <td style="text-align: center;">{{$row['satuan']}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>