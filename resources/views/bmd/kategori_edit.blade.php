@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('master-bmd') }}">
                        Master Barang Milik Daerah
                    </a>
                </li>
                <li>
                    <a href="{{ route('kategori-bmd') }}">
                        Kategori Barang Milik Daerah
                    </a>
                </li>
                <li class="active">
                    Ubah Kategori Barang Milik Daerah
                </li>
            </ol>
            <div class="page-header">
                <h1>Ubah Kategori Barang Milik Daerah <small>(BMD)</small></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('save-bmd') }}" id="myform" onSubmit="return validasi()">
                        @csrf
                        <input type="hidden" id="id" name="id" placeholder="Name" class="form-control" value="{{$bmd->id}}">
                        <div class="form-group">
                            <label for="form-field-22">
                                Uraian
                            </label>
                            <input type="text" class="form-control" name="uraian" placeholder="Uraian" required id="uraian" value="{{$bmd->bmd_description}}">
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Satuan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select satuan" style="width: 100%;" id="satuan" name="satuan" required>
                                <option value=""></option>
                                @foreach($unit as $row)
                                <option value="{{$row->id}}" @if($row->id === $bmd->satuan_id) selected @endif>{{$row->unit}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="form-field-select-3">
                                Tahun Aktif 
                            </label>
                            @if(count($tahun)>0)
                                <div class="checkbox">
                                    @foreach($tahun as $list)
                                        <?php
                                            $tahunID = $list->id;
                                            $checked = null;
                                            foreach ($bmd_tahun as $bmd_thn)
                                            {
                                                if ($tahunID == $bmd_thn->id_tahun)
                                                {
                                                    $checked = ' checked=""';
                                                    break;
                                                }
                                            }
                                        ?>
                                        <label>
                                            <input type="checkbox" value="{{$list->id}}" name="tahun[]" {{$checked}}>
                                            {{$list->tahun}}
                                        </label>
                                            &#8197;&#8197;&#8197;&#8197;&#8197;&#8197;
                                    @endforeach
                                </div>
                            @else
                                <div class="checkbox">
                                    Tidak ada data!
                                </div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Unit Kerja Pengampu
                            </label>
                            <select id="form-field-select-3" class="form-control search-select pengampu" style="width: 100%;" name="type_user" required>
                                <option value="0" @if($bmd->type_user=== 0) selected @endif>Perangkat Daerah</option>
                                <option value="1" @if($bmd->type_user=== 1) selected @endif>Kecamatan</option>
                                <option value="2" @if($bmd->type_user=== 2) selected @endif>Kelurahan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-default">Reset</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                </div>
            </div>
            </form>

        </div>
    </div>
</div>
</div>
</div>
<script src="//code.jquery.com/jquery.js"></script>
<script>
    function validasi() {
        var full_name = document.forms["myform"]["full_name"].value;
        var username = document.forms["myform"]["username"].value;
        var role = document.forms["myform"]["role"].value;

        if (full_name == null || full_name == "") {
            swal({
                title: "Peringatan!",
                text: "Nama user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (username == null || username == "") {
            swal({
                title: "Peringatan!",
                text: "Username tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };

        if (role == null || role == "") {
            swal({
                title: "Peringatan!",
                text: "Peran user tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data Master BMD berhasil diubah!", "success");
    });
</script>
@endif
@endsection