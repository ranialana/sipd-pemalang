@extends('layouts/master_vw')
@section('content')


<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li>
                    <a href="{{ route('master-bmd') }}">
                        Master Barang Milik Daerah
                    </a>
                </li>
                <li class="active">
                    Kategori Barang Milik Daerah
                </li>
            </ol>
            <div class="page-header">
                <h1> Kategori Barang Milik Daerah <small>(BMD)</small></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-12">
                            <div class="col-md-12" align="right">
                                <div class="form-inline">
                                    <!-- <div class="form-group">
                                        <select id="master" class="form-control search-select" style="width: 200px;" id="" name="parent">
                                            <option value=""></option>
                                            @foreach($master as $row)
                                            <option value="{{$row->id}}">{{$row->data_description}}</option>
                                            @endforeach
                                        </select>
                                    </div> -->
                                    <!-- @foreach($master as $row)
                                    <fieldset class="form-group">

                                        <a class="btn btn-success btn-sm" href="{{route('add-datadukung')}}">
                                            {{$row->data_description}}
                                        </a>
                                    </fieldset>
                                    @endforeach -->
                                    <fieldset class="form-group">

                                        <a class="btn btn-success" href="{{route('add-kategori-bmd')}}">
                                            Tambah Kategori
                                        </a>
                                    </fieldset>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered data-table">
                            <thead>
                                <tr>
                                    <th class="text-center">No</th>
                                    <th>Uraian</th>
                                    <th>Satuan</th>
                                    <th width="100px" class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>



                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
    </div>



</div>
</div>

</div>


<script src="//code.jquery.com/jquery.js"></script>

<script type="text/javascript">
    $(function() {

        var table = $('.data-table').DataTable({
            "scrollX": false,
            "scrollY": false,
            processing: true,
            serverSide: true,
            pageLength: 10,
            ajax: {
                url: "{{ route('kategori-bmd') }}",
                data: function(d){
                    d.master = $('#master').val(),
                    d.search = $('input[type="search"]').val()
                }
            },
            "aoColumnDefs": [

                {
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                },
                {
                    "aTargets": [1]
                },
                {
                    "aTargets": [2]
                },
                {
                    "sWidth": "25%",
                    "aTargets": [3],
                    "className": "text-center"
                }
            ],
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'bmd_description',
                    name: 'bmd_description'
                },
                {
                    data: 'satuan',
                    name: 'satuan'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: false,
                    searchable: false
                },
            ]
        });
        $('#master').change(function() {
            table.draw();
        });
        $('input[type="search"]').change(function() {
            table.draw();
        });
    });



    function validasi() {
        var uraian = document.forms["myform"]["uraian"].value;
        if (uraian == null || uraian == "") {
            swal({
                title: "Peringatan!",
                text: "Master Data Dukung tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }


    function validasi_edit() {
        var uraian_edit = document.forms["myform_edit"]["uraian_edit"].value;
        if (uraian_edit == null || uraian_edit == "") {
            swal({
                title: "Peringatan!",
                text: "Nama SKPD tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
            return false;
        };
    }


    function validasi_import() {
        var fileExtensions = [".xls", ".xlsx", ".csv"];

        var uraian_import = document.forms["myform_import"]["uraian_import"].value;

        if (uraian_import.length > 0) {
            var validation = false;
            for (var j = 0; j < fileExtensions.length; j++) {
                var Extention = fileExtensions[j];
                if (uraian_import.substr(uraian_import.length - Extention.length, Extention.length).toLowerCase() == Extention.toLowerCase()) {
                    validation = true;
                    break;
                }
            }

            if (!validation) {
                swal({
                    title: "Peringatan!",
                    text: "Format file tidak didukung",
                    type: "warning",
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false,
                });
                return false;
            }
        }
        return true;
    }

    $(document).on("click", '.edit_button', function(e) {
        var id = $(this).data('id');
        var business_name = $(this).data('business_name');
        var check_sub = $(this).data('check_sub');
        var role = $(this).data('role');
        var satuan_id = $(this).data('satuan');

        $(".role").val(role);


        $(".id").val(id);
        $(".business_name").val(business_name);
        if (check_sub === 1) {
            $("#myModalEdit .cek_sub").prop('checked', true);
            $('.search-select').val(satuan_id);
            $('.search-select').select2().trigger('change');
        }

    });
</script>
@if (session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data Master BMD berhasil disimpan!", "success");
    });
</script>
@endif

@if (session('success_delete'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "Data Master BMD berhasil dihapus!", "success");
    });
</script>
@endif

@if (session('success_import'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success_import')}}", "success");
    });
</script>
@endif
<script>
    $(document).ready(function() {
        $('body').on('click', '.hapus_bmd', function() {

            var delete_url = $(this).attr('data-url');

            swal({
                title: "Apakah Anda Yakin Akan Menghapus Kategori BMD?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus !",
                cancelButtonText: "Batalkan",
                closeOnConfirm: false
            }, function() {

                window.location.href = delete_url;
            });

            return false;
        });
    });
</script>
@endsection