@extends('layouts/master_vw')
@section('content')
<div class="container">
    <!-- start: PAGE HEADER -->
    <div class="row">
        <div class="col-sm-12">
            <!-- start: STYLE SELECTOR BOX -->
            <ol class="breadcrumb">
                <li>
                    <i class="clip-home-3"></i>
                    <a href="{{ route('dashboard') }}">
                        eDatabase SIPD
                    </a>
                </li>
                <li class="active">
                    Sustainable Development Goals
                </li>
            </ol>
            <div class="page-header">
                <h1> Sustainable Development Goals <small>(SDGs)</small> </h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

            <!-- start: RESPONSIVE TABLE PANEL -->
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-external-link-square"></i> eDatabase SIPD
                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-12">


                            <div class="col-md-7">
                            <div class="form-inline">
                                    @if(auth()->user()->role === '0' || auth()->user()->role === '1')
                                    <input type="hidden" id="dari_request" value="{{$a}}">
                                    @endif
                                    @if(auth()->user()->role === '2')
                                    <input type="hidden" id="dari_request" value="{{$e}}">
                                    @endif
                                    <input type="hidden" id="sampai_request" value="{{$e}}">
                                    <input type="hidden" id="kecamatan_request" value="{{$kec}}">
                                    <input type="hidden" id="kelurahan_request" value="">
                                    <div class="form-group">
                                        <select id="form-field-select-3" class="form-control search-select dari" style="width: 80px;" placeholder="Pilih Tahun" name="satuan" required>
                                            @if(auth()->user()->role === '0' || auth()->user()->role === '1')
                                            @foreach($thn as $tahun)
                                            <option value="{{$tahun->tahun}}" {{ $tahun->tahun == $a ? 'selected' : ''}}>{{$tahun->tahun}}</option>
                                            @endforeach
                                            @endif
                                            @if(auth()->user()->role === '2')
                                            @foreach($thn as $tahun)
                                            <option value="{{$tahun->tahun}}" {{ $tahun->tahun == $e ? 'selected' : ''}}>{{$tahun->tahun}}</option>
                                            @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select id="form-field-select-3" class="form-control search-select sampai" style="width: 80px;" placeholder="Pilih Tahun" required>
                                            @foreach($thn as $tahun)
                                            @if($tahun->tahun <= $e) <option value="{{$tahun->tahun}}" {{ $tahun->tahun == $e ? 'selected' : ''}} disabled="true">{{$tahun->tahun}}</option>
                                                @endif
                                                @if($tahun->tahun > $e) <option value="{{$tahun->tahun}}" {{ $tahun->tahun == $e ? 'selected' : ''}} disabled="false">{{$tahun->tahun}}</option>
                                                @endif
                                                @endforeach
                                        </select>
                                    </div>
                                    @if(auth()->user()->role === '0')
                                    <div class="form-group">
                                        <select id="kec" class="form-control search-select kecamatan" placeholder="Pilih Tahun" required style="width: 150px;">
                                            <option value="Kabupaten" selected="selected">Kab. Pemalang</option>
                                            @foreach($kecamatan as $kec)
                                            <option value="{{$kec->id}}">{{$kec->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control search-select4 kelurahan" name="kelurahan" id="kel" style="width: 150px;">
                                            <option value="">Kelurahan/Desa</option>
                                            @foreach($kelurahan as $kel)
                                            <option value="" class="{{$kel->id_kec}}"></option>
                                            @endforeach
                                            @foreach($kelurahan as $kel)
                                            <option value="{{$kel->id}}" class="{{$kel->id_kec}}">{{$kel->nama}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endif
                                    @if(auth()->user()->role === '1' || auth()->user()->role === '2')
                                    @if(auth()->user()->type_user === 0)
                                    @endif
                                    @if(auth()->user()->type_user === 1)
                                    <div class="form-group">
                                        <select id="form-field-select-3" class="form-control search-select kecamatan" placeholder="Pilih Tahun" required>
                                            <option value="Kabupaten">Kab. Pemalang</option>
                                            <option value="{{$kecamatan->id}}" selected="selected">{{$kecamatan->nama}}</option>
                                        </select>
                                    </div>
                                    @endif
                                    @if(auth()->user()->type_user === 2)
                                    <div class="form-group">
                                        <select id="kec" class="form-control search-select kecamatan" placeholder="Pilih Tahun" required>
                                            <option value="Kabupaten">Kab. Pemalang</option>
                                            <option value="{{$kecamatan->id}}" selected="selected">{{$kecamatan->nama}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <select class="form-control search-select4 kelurahan" name="type_user" id="kel" style="width: 150px;">
                                            <option value="" class="{{$kelurahan->id_kec}}"></option>
                                            <option value="{{$kelurahan->id}}" class="{{$kelurahan->id_kec}}" selected="selected">{{$kelurahan->nama}}</option>
                                        </select>
                                    </div>
                                    @endif
                                    @endif
                                    <button type="submit" id="refresh" class="btn btn-sm btn-primary"><i class="fa fa-spinner"></i> Refresh</button>
                                </div>
                            </div>
                            <div class="col-md-5" align="right">

                                <fieldset class="form-group">
                                    <!-- <a class="btn btn-primary" data-toggle="modal" href='#myModalImport' style="display: none;">Import Excel</a> -->
                                    <a id="pdf" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
                                    <a id="excel" type="button" class="btn btn-success">Export Excel</a>
                                    @if(auth()->user()->role === '0' || auth()->user()->role === '1')
                                    <a type="button" class="btn btn-warning ajax_delete">Validasi Final</a>
                                    @endif
                                </fieldset>

                            </div>

                        </div>
                    </div>


                    <div class="table-responsive">

                        <table class="table table-bordered table-hover data-table">
                            <thead>
                                <tr>

                                    <th style="width: 5%;text-align: center;">No</th>
                                    <th>Uraian</th>
                                    <th>Satuan</th>
                                    @foreach($thn as $tahun)
                                    <th>{{$tahun->tahun}}</th>
                                    @endforeach
                                    <th style="text-align: center;">Aksi</th>

                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- end: RESPONSIVE TABLE PANEL -->
        </div>
    </div>




</div>
</div>

</div>





<div class="modal fade" id="myModal1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Tambah data SDGs</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('save-sdgs')}}" id="myform" onSubmit="return validasi()">
                        @csrf
                        <div class="form-group">
                            <label for="form-field-22">
                                Uraian
                            </label>
                            <input type="text" class="form-control" name="uraian" placeholder="Uraian" required id="uraian">
                        </div>
                        <div class="form-group">
                            <label for="form-field-select-3">
                                Satuan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select" style="width: 100%;" id="satuan" name="satuan" required>
                                <option value=""></option>
                                @foreach($unit as $row)
                                <option value="{{$row->id}}">{{$row->unit}}</option>
                                @endforeach
                            </select>
                        </div>




                </div>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                    Tutup
                </button>
                <button class="btn btn-default" type="submit">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>




<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title">Ubah data SDGs</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form method="post" action="{{route('save-sdgs')}}" id="myform" onSubmit="return validasi_edit()">
                        @csrf
                        <div class="form-group">
                            <label for="form-field-22">
                                Uraian
                            </label>
                            <input type="text" class="form-control sdgs_description" name="uraian" placeholder="Uraian" id="uraian_edit" required>
                            <input type="hidden" class="form-control id" name="id">
                        </div>
                        <div class="form-group" id="select">
                            <label for="form-field-22">
                                Satuan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select" style="width: 100%;" id="satuan_edit" name="satuan" required>
                                <option value=""></option>
                                @foreach($unit as $row)
                                <option value="{{$row->id}}">{{$row->unit}}</option>
                                @endforeach
                            </select>

                        </div>




                </div>
            </div>
            <div class="modal-footer">
                <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                    Tutup
                </button>
                <button class="btn btn-default" type="submit">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
@if(auth()->user()->role === '0')
<div class="modal fade" id="myModalImport" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Import Excel</h4>
            </div>
            <div class="modal-body">
                <div class="panel-body">
                    <form action="{{route('import-sdgs')}}" method="POST" id="myform_import" role="form" enctype="multipart/form-data" onSubmit="return validasi_import()">
                        @csrf
                        <div class="form-group" id="select">
                            <label for="form-field-22">
                                Pilih Kecamatan
                            </label>
                            <select id="form-field-select-3" class="form-control search-select" placeholder="Pilih Tahun" required style="width: 100%;" name="kec">
                                @foreach($kecamatan as $kec)
                                <option value="{{$kec->id}}">{{$kec->nama}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>
                                Pilih File Excel
                            </label>
                            <input type="file" class="file" data-show-preview="false" id="uraian_import" placeholder="Import Excel" name="uraian_import">
                            <a href="{{route('template-sdgs')}}"><i class="clip-download"></i> Download Template Excel</a>
                        </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
<div class="modal fade" id="edit">

</div>
<div class="modal fade" id="history">

</div>
<script src="//code.jquery.com/jquery.js"></script>

<script>
    $(function() {
        var columns = [{
                data: 'DT_RowIndex',
                name: 'DT_RowIndex'
            },
            {
                data: 'sdgs_description',
                name: 'sdgs_description'
            },
            {
                data: 'satuan',
                name: 'satuan'
            }
        ];

        <?php foreach ($thn as $tahun) {
        ?>
            columns.push({
                data: 'tahun{{ $tahun->tahun }}',
                name: '{{ $tahun->tahun }}'
            });
        <?php
        }; ?>
        columns.push({
            data: 'action',
            name: 'action',
            orderable: false,
            searchable: false
        });

        var table = $('.data-table').DataTable({
            "scrollX": false,
            "scrollY": false,
            processing: true,
            serverSide: true,
            pageLength: 100,
            columnDefs: [{
                searchable: true,
            }],
            ajax: {
                url: "{{route('view-sdgs')}}",
                data: function(d) {
                    d.dari = $('#dari_request').val(),
                        d.sampai = $('#sampai_request').val(),
                        d.kec = $('#kecamatan_request').val(),
                        d.kel = $('#kelurahan_request').val(),
                        d.search = $('input[type="search"]').val()
                }
            },
            "aoColumnDefs": [

                {
                    "sWidth": "5%",
                    "aTargets": [0],
                    "className": "text-center"
                },
                {
                    "aTargets": [1]
                },
                {
                    "sWidth": "10%",
                    "aTargets": [columns.length - 1],
                    "className": "text-center"
                }
            ],
            columns: columns,
        });
        $('input[type="search"]').change(function() {
            table.draw();
        });

        $('.dari').change(function() {
            var a = $('.dari').val();
            <?php foreach ($thn as $tahun) { ?>
                var b = '{{$tahun->tahun}}';
                if (parseInt(b) < parseInt(a)) {
                    $('.sampai option[value="' + b + '"]').attr('disabled', true);
                } else {
                    $('.sampai option[value="' + b + '"]').attr('disabled', false);
                }

            <?php }; ?>
            $('.sampai').select2();
        });

        $('#refresh').on('click', function() {
            function inArray(a, b) {
                var length = b.length;
                for (var i = 0; i < length; i++) {
                    if (b[i] == a) {
                        return true;
                    }
                }
                return false;
            }
            var dari = $('.dari').val();
            var sampai = $('.sampai').val();
            var kecamatan = $('.kecamatan').val();
            var kelurahan = $('.kelurahan').val();

            var selisih = sampai - dari;
            if (selisih === 0) {
                $('#dari_request').val(dari);
                $('#sampai_request').val(sampai);
                $('#kecamatan_request').val(kecamatan);
                $('#kelurahan_request').val(kelurahan);

                var total_column = parseInt('{{$column}}');

                for (var i = 3; i < total_column; i++) {
                    var col = table.settings().init().columns;

                    var name = col[i].name;
                    if (name === dari) {
                        table.column(i).visible(true, true);
                    } else {
                        table.column(i).visible(false, false);
                    }
                }
                table.columns.adjust().draw();
            } else if (selisih > 0) {
                if (selisih > 4) {
                    swal("Gagal!", "Selisih tahun tidak boleh lebih dari 5 tahun!", "error");
                } else {
                    $('#dari_request').val(dari);
                    $('#sampai_request').val(sampai);
                    $('#kecamatan_request').val(kecamatan);
                    $('#kelurahan_request').val(kelurahan);

                    var array_tahun = [];
                    var a = parseInt(dari);
                    for (var i = 0; i <= selisih; i++) {
                        array_tahun.push(a + i);
                    }

                    var total_column = parseInt('{{$column}}');

                    for (var i = 3; i < total_column; i++) {
                        var col = table.settings().init().columns;
                        var name = col[i].name;
                        if (inArray(name, array_tahun)) {
                            table.column(i).visible(true, true);
                        } else {
                            table.column(i).visible(false, false);
                        }
                    }
                    table.columns.adjust().draw();
                }


            } else {
                swal("Gagal!", "Tahun akhir harus lebih besar dari Tahun awal" + sampai + "!", "error");
            }
            table.draw();
        });
        // console.log(table.column().count());
        var total_column = parseInt('{{$column}}');

        for (var i = 3; i < total_column; i++) {
            var col = table.settings().init().columns;

            var name = col[i].name;
            <?php
            if (auth()->user()->role === '0' || auth()->user()->role === '1') {
            ?>
                if (name === '{{$a}}' || name === '{{$b}}' || name === '{{$c}}' || name === '{{$d}}' || name === '{{$e}}') {
                    table.column(i).visible(true, true);
                } else {
                    table.column(i).visible(false, false);
                }
            <?php
            } else {
            ?>
                if (name === '{{$e}}') {
                    table.column(i).visible(true, true);
                } else {
                    table.column(i).visible(false, false);
                }
            <?php
            }
            ?>
        }
        table.columns.adjust().draw();
    });

    function validasi() {
        var uraian = document.forms['myform']['uraian'].value;
        var satuan = document.forms['myform']['satuan'].value;

        if (uraian == null || uraian == '') {
            swal({
                title: "Peringatan!",
                text: "Uraian tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
        if (satuan == null || satuan == '') {
            swal({
                title: "Peringatan!",
                text: "Satuan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
    }

    function validasi_edit() {
        var uraian_edit = document.forms['myform_edit']['uraian_edit'].value;
        var satuan_edit = document.forms['myform_edit']['satuan_edit'].value;

        if (uraian_edit == null || uraian_edit == '') {
            swal({
                title: "Peringatan!",
                text: "Uraian tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
        if (satuan_edit == null || satuan_edit == '') {
            swal({
                title: "Peringatan!",
                text: "Satuan tidak boleh kosong",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
        }
    }

    $(document).on("click", '.edit_button', function(e) {
        var id = $(this).data('id');
        var sdgs_description = $(this).data('sdgs_description');
        var satuan_id = $(this).data('satuan');

        var change = $('#satuan_edit').select2();

        $(".id").val(id);
        $(".sdgs_description").val(sdgs_description);
        $('.search-select').val(satuan_id);
        $('.search-select').select2().trigger('change');

    });

    // Modal Edit
    function edit(id) {
        var id = id;
        var dari = $(this).data('dari');
        var sampai = $(this).data('sampai');
        var kec = $(this).data('kecamatan');
        var url = "{{url('sdgs/edit/:id')}}".replace(':id', id);
        $.ajax({
            url: url,
            type: "GET",
            data: {
                id: id,
                dari: dari,
                sampai: sampai,
                kec: kec
            },
            success: function(ajaxData) {
                $("#edit").html(ajaxData);
                $("#edit").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    }

    $(document).ready(function() {
        $(document).on("click", ".edit", function() {
            var m = $(this).attr("id");
            var d = $(this).data('dari');
            var s = $(this).data('sampai');
            var k = $(this).data('kecamatan');
            var kl = $(this).data('kelurahan');
            var link = "{{url('sdgs/edit/:id')}}".replace(':id', m);
            $.ajax({
                url: link,
                type: "GET",
                data: {
                    id: m,
                    dari: d,
                    sampai: s,
                    kec: k,
                    kel: kl
                },
                success: function(ajaxData) {
                    $("#edit").html(ajaxData);
                    $("#edit").modal('show', {
                        backdrop: 'true'
                    });
                }
            });
        });
    });

    // Modal History
    function edit(id) {
        var id = id;
        var dari = $(this).data('dari');
        var sampai = $(this).data('sampai');
        var kec = $(this).data('kecamatan');
        var kel = $(this).data('kelurahan');
        var url = "{{url('sdgs/history/:id')}}".replace(':id', id);
        $.ajax({
            url: url,
            type: "GET",
            data: {
                id: id,
                dari: dari,
                sampai: sampai,
                kec: kec,
                kel: kel
            },
            success: function(ajaxData) {
                $("#history").html(ajaxData);
                $("#history").modal('show', {
                    backdrop: 'true'
                });
            }
        });
    }

    $(document).ready(function() {
        $(document).on("click", ".history", function() {
            var m = $(this).attr("id");
            var d = $(this).data('dari');
            var s = $(this).data('sampai');
            var k = $(this).data('kecamatan');
            var link = "{{url('sdgs/history/:id')}}".replace(':id', m);
            $.ajax({
                url: link,
                type: "GET",
                data: {
                    id: m,
                    dari: d,
                    sampai: s,
                    kec: k
                },
                success: function(ajaxData) {
                    $("#history").html(ajaxData);
                    $("#history").modal('show', {
                        backdrop: 'true'
                    });
                }
            });
        });
    });

    $(document).ready(function() {
        $(".dari").select2();
        <?php
        if (auth()->user()->role === '0' || auth()->user()->role === '1') {
        ?>
            $(".dari").val("{{$a}}").trigger("change");
        <?php
        } else {
        ?>
            $(".dari").val("{{$e}}").trigger("change");
        <?php
        }
        if (auth()->user()->role === '0') {
        ?>
            $('#kecamatan_request').val('Kabupaten');
            $('#kelurahan_request').val('');
            $(".sampai").select2();
            $(".sampai").val("{{$e}}").trigger("change");
            $(".kecamatan").select2();
            $(".kecamatan").val("Kabupaten").trigger("change");
            <?php
        } else {
            if (auth()->user()->type_user === 0) {
            } elseif (auth()->user()->type_user === 1) {
            ?>
                $('#kecamatan_request').val('<?php echo auth()->user()->id_kec; ?>');
                $('#kelurahan_request').val('');
                $(".sampai").select2();
                $(".sampai").val("{{$e}}").trigger("change");
                $(".kecamatan").select2();
                $(".kecamatan").val('<?php echo auth()->user()->id_kec; ?>').trigger("change");
            <?php
            } else {
            ?>
                $('#kecamatan_request').val('<?php echo auth()->user()->id_kec; ?>');
                $('#kelurahan_request').val('<?php echo auth()->user()->id_kel; ?>');
                $(".sampai").select2();
                $(".sampai").val("{{$e}}").trigger("change");
                $(".kecamatan").select2();
                $(".kecamatan").val("<?php echo auth()->user()->id_kec; ?>").trigger("change");
                $(".kelurahan").select2();
                $(".kelurahan").val("<?php echo auth()->user()->id_kel; ?>").trigger("change");
        <?php
            }
        }
        ?>

    });

    $(document).ready(function() {
        $(document).on('click', "#excel", function() {
            var dari = $('#dari_request').val();
            var sampai = $('#sampai_request').val();
            var kec = $('#kecamatan_request').val();
            var kel = $('#kelurahan_request').val();

            window.location.href = "{{url('/export-excel-sdgs')}}?dari=" + dari + "&sampai=" + sampai + "&kec=" + kec + "&kel=" + kel;
        });
    });

    $(document).ready(function() {
        $(document).on('click', "#pdf", function() {
            var dari = $('#dari_request').val();
            var sampai = $('#sampai_request').val();
            var kec = $('#kecamatan_request').val();
            var kel = $('#kelurahan_request').val();

            window.open("{{url('/export-pdf-sdgs')}}?dari=" + dari + "&sampai=" + sampai + "&kec=" + kec + "&kel=" + kel, "_blank");
        });
    });
</script>

@if(session('success'))
<script>
    $(document).ready(function() {

        swal("Berhasil!", "{{session('success')}}", "success");
    });
</script>
@endif
<script>
    $(document).ready(function() {
        $('body').on('click', '.hapus_sdgs', function() {

            var delete_url = $(this).attr('data-url');

            swal({
                title: "Hapus SDGs?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Hapus !",
                cancelButtonText: "Batalkan",
                closeOnConfirm: false
            }, function() {

                window.location.href = delete_url;
            });

            return false;
        });
    });

    function validasi_import() {
        var fileExtensions = [".xlsx", ".csv"];

        var uraian_import = document.forms["myform_import"]["uraian_import"].value;

        if (uraian_import.length > 0) {
            var validation = false;
            for (var j = 0; j < fileExtensions.length; j++) {
                var Extention = fileExtensions[j];
                if (uraian_import.substr(uraian_import.length - Extention.length, Extention.length).toLowerCase() == Extention.toLowerCase()) {
                    validation = true;
                    break;
                }
            }

            if (!validation) {
                swal({
                    title: "Peringatan!",
                    text: "Format file tidak didukung",
                    type: "warning",
                    confirmButtonClass: 'btn btn-warning',
                    buttonsStyling: false,
                });
                return false;
            }
        }
        return true;
    }
</script>

<script>
    $(document).ready(function() {
        $('body').on('click', '.target_validasi', function() {

            var validasi_url = $(this).attr('data-url');

            swal({
                    title: "Peringatan!",
                    text: "Anda yakin akan validasi  data ini",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ya!",
                    cancelButtonText: "Batalkan!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        window.location.href = validasi_url;
                    } else {
                        swal("Batalkan", "Data ini batal divalidasi :)", "error");
                    }
                });

        });
    });
</script>
<script>
    $(".ajax_delete").on("click", function() {

        swal({
                title: "Peringatan!",
                text: "Anda yakin akan validasi final data ini",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya!",
                cancelButtonText: "Batalkan!",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location.href = "{{route('validasi-all-sdgs')}}";
                } else {
                    swal("Batalkan", "Data batal divalidasi :)", "error");
                }
            });


    });
</script>
@endsection