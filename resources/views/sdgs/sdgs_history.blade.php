<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Riwayat</h4>
        </div>
        <div class="modal-body">
            <table>
                <tr>
                    <td style="width: 20%;">Nama</td>
                    <td style="width: 5%;">:</td>
                    <td> {{$sdgs->sdgs_description}}</td>
                </tr>
                <tr>
                    <td style="width: 20%;">Satuan</td>
                    <td style="width: 5%;">:</td>
                    <td> {{$sdgs->unit->unit}}</td>
                </tr>
            </table>
            <br>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Tahun</th>
                        <th>Value</th>
                        <th>Tanggal Pengisian</th>
                        <th>Nama User</th>
                    </tr>
                </thead>
                <tbody>
                    @if(count($sdgstrans) > 0)
                        @foreach($sdgstrans as $trans)
                        <tr>
                            <td>{{$trans->tahun->tahun}}</td>
                            <td>
                                <?php
                                if ($trans->value !== "") {
                                    $val = str_replace(".", "", $trans->value);
                                    if (is_numeric($val)) {
                                        if (strpos($trans->value, ".") === true) {
                                            $decimal = strlen(substr($trans->value, strrpos($trans->value, '.' )+1));
                                            $value = number_format($trans->value,$decimal,',','.');
                                        }
                                        else {
                                            $value = number_format($trans->value,0,'','.');
                                        }
                                    }
                                    else {
                                        $value = $trans->value;
                                    }
                                }
                                else {
                                    $value = "-";
                                }
                                echo $value;
                                ?>
                            </td>
                            <td>{{ Carbon\Carbon::parse($trans->tgl_pengisian)->format('d-M-Y H:i:s') }}</td>
                            <td>{{$trans->user->username}}</td>
                        </tr>
                        @endforeach
                    @else
                    <tr>
                        <td colspan="4" style="text-align: center;">Tidak Ada Riwayat</td>
                    </tr>
                    @endif
                </tbody>
            </table>

        </div>
        <div class="modal-footer">
            <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                Tutup
            </button>
        </div>
    </div>
</div>