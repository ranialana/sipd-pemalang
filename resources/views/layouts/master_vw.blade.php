<!DOCTYPE html>

<html class="no-js">
<!--<![endif]-->

<head>
    <title>{{ Request::is('dashboard') ? 'Dashboard' : '' }}{{ Request::is('view-edatabase') || Request::is('add-edatabase') || Request::is('change-edatabase/*') ? 'eDatabase' : '' }}{{ Request::is('view-kor') ? 'KOR' : '' }}{{ Request::is('view-spm') ? 'SPM' : '' }}{{ Request::is('view-sdgs') ? 'SDGs' : '' }}{{ Request::is('view-datadukung') ? 'Data Dukung' : '' }}{{ Request::is('view-bmd') ? 'BMD' : '' }}{{ Request::is('view-iku') || Request::is('add-iku') || Request::is('change-iku/*') ? 'Indikator Kinerja Urusan' : '' }}{{ Request::is('view-business') || Request::is('detail-business/*') || Request::is('sub-area*') || Request::is('view-indicators*') || Request::is('view-sub-indicators*') || Request::is('parent-sub/*') ? 'Master Urusan' : '' }}{{ Request::is('view-skpd') ? 'SKPD' : '' }}{{ Request::is('view-users') || Request::is('add-user') || Request::is('change-user/*') ? 'User' : '' }} {{Request::is('master-kor') || Request::is('add-kor') || Request::is('change-master-kor/*') ? 'Master KOR' : ''}} {{Request::is('master-spm') || Request::is('add-spm') || Request::is('change-master-spm/*') ? 'Master SPM' : ''}}{{Request::is('master-sdgs') || Request::is('add-sdgs') || Request::is('change-master-sdgs/*') ? 'Master SDGs' : ''}}{{Request::is('master-datadukung') || Request::is('add-datadukung') || Request::is('change-master-datadukung/*') ? 'Master Data Dukung' : ''}}{{Request::is('master-bmd') || Request::is('add-bmd') || Request::is('change-master-bmd/*') ? 'Master BMD' : ''}}{{Request::is('bmd/kategori') || Request::is('change-kategori-bmd/*') || Request::is('bmd/kategori/add') ? 'Kategori BMD' : ''}}{{Request::is('datadukung/kategori') || Request::is('change-kategori-datadukung/*') || Request::is('datadukung/kategori/add') ? 'Kategori Data Dukung' : ''}}{{Request::is('master-kecamatan') || Request::is('change-kecamatan/*') || Request::is('add-kecamatan') ? 'Master Kecamatan' : ''}}{{Request::is('master-kelurahan') || Request::is('change-kelurahan/*') || Request::is('add-kelurahan') ? 'Master Kelurahan' : ''}} - eDatabase SIPD Kabupaten Pemalang </title>
    <link rel="shortcut icon" href="favicon.ico" />
    <!-- start: META -->
    <meta charset="utf-8" />
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta content="Responsive Admin Template build with Twitter Bootstrap and jQuery" name="description" />
    <meta content="ClipTheme" name="author" />


    <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700|Raleway:400,100,200,300,500,600,700,800,900/" />


    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/bootstrap/dist/css/bootstrap.min.css') !!}">
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/font-awesome/css/font-awesome.min.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('assets/fonts/clip-font.min.css') !!}" />


    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/sweetalert/dist/sweetalert.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/iCheck/skins/all.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('bower_components/perfect-scrollbar/css/perfect-scrollbar.min.css') !!}" />

    <link type="text/css" rel="stylesheet" href="{!! asset('assets/css/main.min.css') !!}" />
    <link type="text/css" rel="stylesheet" href="{!! asset('assets/css/main-responsive.min.css') !!}" />
    <link type="text/css" rel="stylesheet" media="print" href="{!! asset('assets/css/print.min.css') !!}" />
    <link type="text/css" rel="stylesheet" id="skin_color" href="{!! asset('assets/css/theme/light.min.css') !!}" />
    <!-- end: MAIN CSS -->
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link href="{!! asset('bower_components/select2/dist/css/select2.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('assets/plugin/bootstrap-timepicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/summernote/dist/summernote.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/bootstrap-fileinput/css/fileinput.min.css') !!}" rel="stylesheet" />
    <link href="{!! asset('bower_components/datatables/media/css/dataTables.bootstrap.min.css') !!}" rel="stylesheet" />
</head>

<body>

    <!-- start: HEADER -->
    <div class="navbar navbar-inverse navbar-fixed-top">
        <!-- start: TOP NAVIGATION CONTAINER -->
        <div class="container">
            <div class="navbar-header">
                <!-- start: RESPONSIVE MENU TOGGLER -->
                <button data-target=".navbar-collapse" data-toggle="collapse" class="navbar-toggle" type="button">
                    <span class="clip-list-2"></span>
                </button>
                <!-- end: RESPONSIVE MENU TOGGLER -->
                <!-- start: LOGO -->
                <a class="navbar-brand" href="index.html">
                    eDatabase SIPD
                </a>
                <!-- end: LOGO -->
            </div>
            <div class="navbar-tools">
                <!-- start: TOP NAVIGATION MENU -->
                <ul class="nav navbar-right">

                    <li class="dropdown current-user">
                        <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                            <span class="username">{{auth()->user()->username}}</span>
                            <i class="clip-chevron-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a data-toggle="modal" href='#modal-id'>
                                    <i class="clip-user-2"></i> &nbsp;Ubah Password
                                </a>
                            </li>

                            <li>
                                <a href="{{route('logout')}}">
                                    <i class="clip-exit"></i> &nbsp;Keluar
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
                <!-- end: TOP NAVIGATION MENU -->
            </div>
        </div>
        <!-- end: TOP NAVIGATION CONTAINER -->
    </div>
    <div class="main-container">
        <div class="navbar-content">
            <!-- start: SIDEBAR -->
            <div class="main-navigation navbar-collapse collapse">
                <!-- start: MAIN MENU TOGGLER BUTTON -->
                <div class="navigation-toggler">
                    <i class="clip-chevron-left"></i>
                    <i class="clip-chevron-right"></i>
                </div>
                <!-- end: MAIN MENU TOGGLER BUTTON -->
                <!-- start: MAIN NAVIGATION MENU -->
                <ul class="main-navigation-menu">
                    <li class="{{ Request::is('dashboard') ? 'active' : '' }}">
                        <a href="{{route('dashboard')}}">
                            <i class="clip-home-3"></i>
                            <span class="title"> Dashboard </span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('view-edatabase') || Request::is('add-edatabase') ? 'active' : '' }}">
                        <a href="{{route('view-edatabase')}}">
                            <i class="clip-database"></i>
                            <span class="title">eDatabase</span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('view-kor') ? 'active' : '' }}">
                        <a href="{{ route('view-kor') }}">
                            <i class="clip-stats"></i>
                            <span class="title">KOR</span><span class="selected"></span>
                        </a>
                    </li>

                    <li class="{{ Request::is('view-spm') ? 'active' : '' }}">
                        <a href="{{route('view-spm')}}">
                            <i class="clip-health"></i>
                            <span class="title">SPM</span><span class="selected"></span>
                        </a>
                    </li>

                    <li class="{{ Request::is('view-sdgs') ? 'active' : '' }}">
                        <a href="{{route('view-sdgs')}}">
                            <i class="clip-transfer"></i>
                            <span class="title">SDGs</span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('view-datadukung') ? 'active' : '' }}">
                        <a href="{{route('view-datadukung')}}">
                            <i class="clip-data"></i>
                            <span class="title">Data Dukung</span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('view-bmd') ? 'active' : '' }}">
                        <a href="{{route('view-bmd')}}">
                            <i class="clip-stack-2"></i>
                            <span class="title">BMD</span><span class="selected"></span>
                        </a>
                    </li>
                    <li class="{{ Request::is('view-iku') || Request::is('add-iku') || Request::is('change-iku/*') ? 'active' : '' }}">
                        <a href="{{route('view-iku')}}">
                            <i class="clip-rating-2"></i>
                            <span class="title">Indikator Kinerja Urusan</span><span class="selected"></span>
                        </a>
                    </li>
                    <!-- <li>
                        <a href="bps.html">
                            <i class="clip-banknote"></i>
                            <span class="title">Data BPS</span><span class="selected"></span>
                        </a>
                    </li> -->


                    @if(auth()->user()->role === '0')
                    <li class="{{ Request::is('view-business') || Request::is('detail-business/*') || Request::is('sub-area*') || Request::is('view-indicators*') || Request::is('view-sub-indicators*') || Request::is('parent-sub/*') || Request::is('view-skpd') || Request::is('view-users') || Request::is('add-user') || Request::is('change-user/*') || Request::is('master-kor') || Request::is('add-kor') || Request::is('change-master-kor/*') || Request::is('master-spm') || Request::is('add-spm') || Request::is('change-master-spm/*') || Request::is('master-sdgs') || Request::is('add-sdgs') || Request::is('change-master-sdgs/*') || Request::is('master-datadukung') || Request::is('add-datadukung') || Request::is('change-master-datadukung/*') || Request::is('master-bmd') || Request::is('add-bmd') || Request::is('change-master-bmd/*') || Request::is('datadukung/kategori') || Request::is('change-kategori-datadukung/*') || Request::is('datadukung/kategori/add')  || Request::is('bmd/kategori') || Request::is('change-kategori-bmd/*') || Request::is('bmd/kategori/add') || Request::is('master-kecamatan') || Request::is('change-kecamatan/*') || Request::is('add-kecamatan') || Request::is('master-kelurahan') || Request::is('change-kelurahan/*') || Request::is('add-kelurahan') ? 'active open' : '' }}">
                        <a href="javascript:void(0)">
                            <i class="clip-cog-2"></i>
                            <span class="title"> Master </span><i class="icon-arrow"></i>
                            <span class="selected"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="{{ Request::is('master-kor') || Request::is('add-kor') || Request::is('change-master-kor/*') ? 'active open' : '' }}">
                                <a href="{{ route('master-kor') }}">
                                    <i class="clip-stats"></i> <span class="title"> Master KOR </span>

                                </a>
                            </li>
                            <li class="{{ Request::is('master-spm') || Request::is('add-spm') || Request::is('change-master-spm/*') ? 'active open' : '' }} ? 'active' : '' }}">
                                <a href="{{route('master-spm')}}">
                                    <i class="clip-health"></i>
                                    <span class="title">Master SPM</span><span class="selected"></span>
                                </a>
                            </li>

                            <li class="{{ Request::is('master-sdgs') || Request::is('add-sdgs') || Request::is('change-master-sdgs/*') ? 'active open' : '' }} ? 'active' : '' }}">
                                <a href="{{route('master-sdgs')}}">
                                    <i class="clip-transfer"></i>
                                    <span class="title">Master SDGs</span><span class="selected"></span>
                                </a>
                            </li>
                            <li class="{{ Request::is('master-datadukung') || Request::is('add-datadukung') || Request::is('change-master-datadukung/*') || Request::is('datadukung/kategori') || Request::is('change-kategori-datadukung/*') || Request::is('datadukung/kategori/add') ? 'active open' : '' }} ? 'active' : '' }}">
                                <a href="{{route('master-datadukung')}}">
                                    <i class="clip-data"></i>
                                    <span class="title">Master Data Dukung</span><span class="selected"></span>
                                </a>
                            </li>
                            <li class="{{ Request::is('master-bmd') || Request::is('add-bmd') || Request::is('change-master-bmd/*') || Request::is('bmd/kategori') || Request::is('change-kategori-bmd/*') || Request::is('bmd/kategori/add') ? 'active open' : '' }} ? 'active' : '' }}">
                                <a href="{{route('master-bmd')}}">
                                    <i class="clip-stack-2"></i>
                                    <span class="title">Master BMD</span><span class="selected"></span>
                                </a>
                            </li>
                            <li class="{{ Request::is('view-business') || Request::is('detail-business/*') || Request::is('sub-area*') || Request::is('view-indicators*') || Request::is('view-sub-indicators*') || Request::is('parent-sub/*') ? 'active open' : '' }}">
                                <a href="{{ route('view-business') }}">
                                    <i class="clip-list-2"></i> <span class="title"> Urusan </span>

                                </a>
                            </li>
                            <li class="{{ Request::is('view-skpd') ? 'active open' : '' }}">
                                <a href="{{ route('view-skpd') }}">
                                    <i class="clip-spinner-4"></i> <span class="title"> SKPD </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('master-kecamatan') || Request::is('add-kecamatan') || Request::is('change-kecamatan/*') ? 'active open' : '' }}">
                                <a href="{{ route('master-kecamatan') }}">
                                    <i class="clip-spinner-5"></i> <span class="title"> Kecamatan </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('master-kelurahan') || Request::is('add-kelurahan') || Request::is('change-kelurahan/*') ? 'active open' : '' }}">
                                <a href="{{ route('master-kelurahan') }}">
                                    <i class="clip-spinner"></i> <span class="title"> Kelurahan </span>
                                </a>
                            </li>
                            <li class="{{ Request::is('view-users') || Request::is('add-user') || Request::is('change-user/*') ? 'active open' : '' }}">
                                <a href="{{ route('view-users') }}">
                                    <i class="clip-user-3"></i> <span class="title"> User </span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
            </div>
        </div>


        <div class="main-content">
            <!-- start: PANEL CONFIGURATION MODAL FORM -->
            <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title">Panel Configuration</h4>
                        </div>
                        <div class="modal-body">
                            Here will be a configuration form
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Close
                            </button>
                            <button type="button" class="btn btn-primary">
                                Save changes
                            </button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
            <!-- end: SPANEL CONFIGURATION MODAL FORM -->

            @yield('content')






            <div class="footer clearfix">
                <div class="footer-inner">
                    <script>
                        // document.write(new Date().getFullYear())
                    </script>2020 &copy; Pemerintah Kabupeten Pemalang.
                </div>
                <div class="footer-items">
                    <span class="go-top"><i class="clip-chevron-up"></i></span>
                </div>
            </div>


            <div class="modal fade" id="modal-id">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="{{route('change-password')}}" method="POST" role="form" id="form">
                        @csrf
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Ubah Password</h4>
                            </div>
                            <div class="modal-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="hidden" name="id" value="{{auth()->user()->id}}">
                                        <div class="form-group">
                                            <label class="control-label">
                                                Nama Lengkap
                                            </label>
                                            <input type="text" class="form-control" id="firstname" name="full_name" value="{{auth()->user()->full_name}}" required>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">
                                                Password
                                            </label>
                                            <input type="password" placeholder="Isi jika akan diganti" class="form-control" name="password" id="password" >
                                        </div>
                                        <!-- <div class="form-group">
                                            <label class="control-label">
                                                Confirm Password
                                            </label>
                                            <input type="password" placeholder="password" class="form-control" id="password_again" name="password_again">
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                                <button type="Submit" class="btn btn-primary">Ubah</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <script type="text/javascript" src="{!! asset('bower_components/jquery/dist/jquery.min.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/sweetalert/dist/sweetalert.min.js') !!}"></script>


            <!--<![endif]-->
            <script type="text/javascript" src="{!! asset('bower_components/jquery-ui/jquery-ui.min.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/bootstrap/dist/js/bootstrap.min.js') !!}"></script>

            <script type="text/javascript" src="{!! asset('bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/blockUI/jquery.blockUI.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/iCheck/icheck.min.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js') !!}"></script>
            <script type="text/javascript" src="{!! asset('bower_components/jquery.cookie/jquery.cookie.js') !!}"></script>

            <script type="text/javascript" src="{!! asset('assets/js/min/main.min.js') !!}"></script>
            <script src="{!! asset('bower_components/datatables/media/js/jquery.dataTables.min.js') !!}"></script>
            <script src="{!! asset('bower_components/datatables/media/js/dataTables.bootstrap.js') !!}"></script>
            <!-- end: MAIN JAVASCRIPTS -->
            <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
            <script src="{!! asset('bower_components/moment/min/moment.min.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js') !!}"></script>
            <script src="{!! asset('bower_components/autosize/dist/autosize.min.js') !!}"></script>
            <script src="{!! asset('bower_components/select2/dist/js/select2.min.js') !!}"></script>
            <script src="{!! asset('bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js') !!}"></script>
            <script src="{!! asset('bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-daterangepicker/daterangepicker.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js') !!}"></script>
            <script src="{!! asset('bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') !!}"></script>
            <script src="{!! asset('bower_components/jquery.tagsinput/src/jquery.tagsinput.js') !!}"></script>
            <script src="{!! asset('bower_components/summernote/dist/summernote.min.js') !!}"></script>
            <script src="{!! asset('bower_components/ckeditor/ckeditor.js') !!}"></script>
            <script src="{!! asset('bower_components/ckeditor/adapters/jquery.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js') !!}"></script>
            <script src="{!! asset('bower_components/bootstrap-fileinput/js/fileinput.min.js') !!}"></script>
            <script src="{!! asset('assets/js/form-elements.js') !!}"></script>

            <script type="text/javascript" src="http://www.appelsiini.net/projects/chained/jquery.chained.js?v=0.9.10"></script>
            <script>
                jQuery(document).ready(function() {
                    Main.init();
                    FormElements.init();
                });
            </script>
            <script>
                $("#kel").chained("#kec");

                $('#bidang').chained('#urusan');
                $("#sub_bidang").chained("#bidang");
                $("#indikator").chained("#sub_bidang");
                $("#subindikator").chained("#indikator");

                $("#tipe_user").chained("#role");
                $(document).ready(function() {
                    var type = $('#tipe_user').val();
                    if (type === '0') {
                        $('#skpd_uk').show();
                        $('#kecamatan').hide();
                        $('#kelurahan').hide();
                    } else if (type === '1') {
                        $('#skpd_uk').hide();
                        $('#kecamatan').show();
                        $('#kelurahan').hide();
                    } else if (type === '2') {
                        $('#skpd_uk').hide();
                        $('#kecamatan').hide();
                        $('#kelurahan').show();
                    } else {
                        $('#skpd_uk').hide();
                        $('#kecamatan').hide();
                        $('#kelurahan').hide();
                    }
                });

                $('#tipe_user').on('change', function(e) {
                    var type = $('#tipe_user').val();
                    if (type === '0') {
                        $('#skpd_uk').show();
                        $('#kecamatan').hide();
                        $('#kelurahan').hide();
                    } else if (type === '1') {
                        $('#skpd_uk').hide();
                        $('#kecamatan').show();
                        $('#kelurahan').hide();
                    } else if (type === '2') {
                        $('#skpd_uk').hide();
                        $('#kecamatan').hide();
                        $('#kelurahan').show();
                    } else {
                        $('#skpd_uk').hide();
                        $('#kecamatan').hide();
                        $('#kelurahan').hide();
                    }
                });

                $('#urusan').on('change', function(e) {
                    var unit = $(this).find(':selected').data('satuan');
                    if (unit > 0) {
                        $('.satuan').val(unit);
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val(unit);
                    } else {
                        $('.satuan').val('');
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val('');
                    }
                });
                $('#bidang').on('change', function(e) {
                    var unit = $(this).find(':selected').data('satuan');
                    if (unit > 0) {
                        $('.satuan').val(unit);
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val(unit);
                    } else {
                        $('.satuan').val('');
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val('');
                    }
                });
                $('#sub_bidang').on('change', function(e) {
                    var unit = $(this).find(':selected').data('satuan');
                    if (unit > 0) {
                        $('.satuan').val(unit);
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val(unit);
                    } else {
                        $('.satuan').val('');
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val('');

                    }
                });
                $('#indikator').on('change', function(e) {
                    var unit = $(this).find(':selected').data('satuan');
                    if (unit > 0) {
                        $('.satuan').val(unit);
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val(unit);
                    } else {
                        $('.satuan').val('');
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val('');
                    }
                });
                $('#subindikator').on('change', function(e) {
                    var unit = $(this).find(':selected').data('satuan');
                    if (unit > 0) {
                        $('.satuan').val(unit);
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val(unit);
                    } else {
                        $('.satuan').val('');
                        $('.satuan').select2().trigger('change');
                        $('#satuan').val('');
                    }
                });
            </script>
            </script>
            @if(session('success_password'))
            <script>
                swal('Berhasil!', "{{session('success_password')}}", 'success');
            </script>
            @endif
</body>

</html>