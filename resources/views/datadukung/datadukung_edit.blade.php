<div class="modal-dialog">
    <div class="modal-content">
    <form method="post" action="{{route('update-datadukung')}}" id="myform" onSubmit="return validasi_edit()">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title">Edit Data Data Dukung</h4>
        </div>
        <div class="modal-body">
            <div class="panel-body">
                <table>
                    <tr>
                        <td style="width: 20%;">Nama</td>
                        <td style="width: 5%;">:</td>
                        <td> {{$datadukung->data_description}}</td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">Satuan</td>
                        <td style="width: 5%;">:</td>
                        <td> {{$datadukung->unit->unit}}</td>
                    </tr>
                </table>
                <br>
                    @csrf
                    <input type="hidden" name="id" value="{{$id_parent}}">
                    <input type="hidden" name="id_child" value="{{$id_child}}">
                    <input type="hidden" class="form-control id" name="dari" value="{{$dari}}">
                    <input type="hidden" class="form-control id" name="sampai" value="{{$sampai}}">
                    <input type="hidden" class="form-control id" name="id_kec" value="{{$id_kec}}">
                    @foreach($input as $data)
                    <div class="form-group">
                        <label for="form-field-22">
                            {{$data['tahun']}}
                        </label>
                        <input type="hidden" class="form-control id" name="id_tahun[]" value="{{$data['id_tahun']}}">
                        <input type="text" class="form-control datadukung_name" name="val[]" placeholder="Nilai {{$data['tahun']}}" id="uraian_edit" value="{{$data['val']}}">
                        
                    </div>
                    @endforeach
            </div>
        </div>
        <div class="modal-footer">
            <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
                Tutup
            </button>
            <button class="btn btn-default" type="submit">
                    Simpan
                </button>

        </div>
    </form>  
    </div>
</div>