@extends('layouts/master_vw')
@section('content')


<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <!-- start: STYLE SELECTOR BOX -->
      <ol class="breadcrumb">
        <li>
          <i class="clip-home-3"></i>
          <a href="{{ route('dashboard') }}">
            eDatabase SIPD
          </a>
        </li>
        <li class="active">
          Satuan Kerja Perangkat Daerah (SKPD)
        </li>
      </ol>
      <div class="page-header">
        <h1>  Satuan Kerja Perangkat Daerah (SKPD) </h1>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <i class="fa fa-external-link-square"></i> eDatabase SIPD
        </div>
        <div class="panel-body">

          <div class="row">
            <div class="col-12">


            <div class="col-md-6">
            </div>
            <div class="col-md-6" align="right">

              <fieldset class="form-group">
              <a href="{{route('export-pdf-skpd')}}" type="button" class="btn btn-primary" target="_blank">Export PDF</a>
              <a href="{{route('export-excel-skpd')}}" type="button" class="btn btn-success">Export Excel</a>
              <a class="btn btn-primary" data-toggle="modal" href='#myModalImport'>Import Excel</a>
              <a data-toggle="modal" class="btn btn-success" role="button" href="#myModal1">
                Tambah Data
              </a>
            </fieldset>

          </div>

        </div>
      </div>

      <div class="table-responsive">   
        <table class="table table-bordered data-table">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th>Name</th>
              <th width="100px" class="text-center">Action</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>
  <!-- end: RESPONSIVE TABLE PANEL -->
  </div>
</div>




</div>
</div>

</div>


<div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">Tambah SKPD</h4>
      </div>
      <div class="modal-body">
        <div class="panel-body">
          <form method="post" action="{{ route('save-skpd') }}" id="myform" onSubmit="return validasi()">
            @csrf
            <div class="form-group">
              <label for="form-field-22">
                Nama SKPD
              </label>
              <input type="text" class="form-control" placeholder="Nama SKPD" id="uraian" name="uraian">
            </div> 
          </div>
        </div>
        <div class="modal-footer">
          <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
           Tutup 
         </button>
         <button class="btn btn-default" type="submit">
           Simpan
         </button>
       </div>
     </div>
   </div>
 </form>
</div>


<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title">Ubah SKPD</h4>
      </div>
      <div class="modal-body">
        <div class="panel-body">
          <form method="post" action="{{ route('save-skpd') }}" id="myform_edit" onSubmit="return validasi_edit()">
          @csrf
          <div class="form-group">
            <label for="form-field-22">
              Nama SKPD
            </label>
            <input type="text" class="form-control skpd_name" placeholder="Nama SKPD" id="uraian_edit" name="uraian">
            <input type="hidden" class="form-control id" placeholder="Nama SKPD"  name="id">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button aria-hidden="true" data-dismiss="modal" class="btn btn-default">
          Tutup 
        </button>
        <button class="btn btn-default" type="submit">
          Simpan
        </button>
      </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalImport" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Import Excel</h4>
      </div>
      <div class="modal-body">
        <div class="panel-body">
          <form action="{{route('import-skpd')}}" method="POST" id="myform_import" role="form" enctype="multipart/form-data" onSubmit="return validasi_import()">
            @csrf
            <div class="form-group">
              <div class="col-sm-12">
                <label>
                  Pilih File Excel
                </label>
                <input type="file" class="file" data-show-preview="false" id="uraian_import" placeholder="Import Excel" name="uraian_import">
                <a href="{{route('template-skpd')}}"><i class="clip-download"></i> Download Template Excel</a>
              </div>
            </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="//code.jquery.com/jquery.js"></script>

<script type="text/javascript">
  $(function () {
    
    var table = $('.data-table').DataTable({
      "scrollX": false,
      "scrollY": false,
      processing: true,
      serverSide: true,
      ajax: "{{ route('view-skpd') }}",
      "aoColumnDefs": [

      { "sWidth": "5%", "aTargets": [ 0 ],"className": "text-center"},
      { "aTargets": [ 1 ] },
      { "sWidth": "25%", "aTargets": [ 2 ] ,"className": "text-center"}
      ],
      columns: [
      { data: 'DT_RowIndex', name:'DT_RowIndex'},
      { data: 'skpd_name', name: 'skpd_name' },
      {data: 'action', name: 'action', orderable: false, searchable: false},
      ]
    });
    
  });



function validasi()
{
    var uraian      =document.forms["myform"]["uraian"].value;
    if (uraian==null || uraian=="")
    {
     swal({
      title: "Peringatan!",
      text: "Nama SKPD tidak boleh kosong",
      type: "warning",
      confirmButtonClass: 'btn btn-warning',
      buttonsStyling: false,
    });
     return false;
   };
 }


function validasi_edit()
{
  var uraian_edit      =document.forms["myform_edit"]["uraian_edit"].value;
  if (uraian_edit==null || uraian_edit=="")
  {
   swal({
    title: "Peringatan!",
    text: "Nama SKPD tidak boleh kosong",
    type: "warning",
    confirmButtonClass: 'btn btn-warning',
    buttonsStyling: false,
  });
   return false;
 };
}

function validasi_import()
{
	var fileExtensions = [".xls", ".xlsx", ".csv"];  

	var uraian_import = document.forms["myform_import"]["uraian_import"].value;

	if (uraian_import.length > 0) {
		var validation = false;
		for (var j = 0; j < fileExtensions.length; j++) {
			var Extention = fileExtensions[j];
			if (uraian_import.substr(uraian_import.length - Extention.length, Extention.length).toLowerCase() == Extention.toLowerCase()) {
				validation = true;
				break;
			}
		}
		
		if (!validation) {
			swal({
                title: "Peringatan!",
                text: "Format file tidak didukung",
                type: "warning",
                confirmButtonClass: 'btn btn-warning',
                buttonsStyling: false,
            });
			return false;
		}
	}
	return true;
}


$(document).on( "click", '.edit_button',function(e) {
  var id = $(this).data('id');
  var skpd_name = $(this).data('skpd_name');
  

  $(".id").val(id);
  $(".skpd_name").val(skpd_name);
  
});

</script>
@if (session('success'))
<script>

  $(document).ready(function(){
    
    swal("Berhasil!", "Data SKPD berhasil disimpan!", "success");
  });
</script>                                    
@endif  

@if (session('success_delete'))
<script>

  $(document).ready(function(){
    
    swal("Berhasil!", "Data SKPD berhasil dihapus!", "success");
  });
</script>                                    
@endif 
@if (session('success_import'))
<script>

$(document).ready(function(){
  
  swal("Berhasil!", "{{session('success_import')}}", "success");
});
</script>                                    
@endif
<script>
  $(document).ready(function(){
   $('body').on('click', '.hapus_skpd', function () {
    
    var delete_url = $(this).attr('data-url');

    swal({
      title: "Hapus SKPD?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Hapus !",
      cancelButtonText: "Batalkan",
      closeOnConfirm: false     
    }, function(){
     
      window.location.href = delete_url;
    });

    return false;
  });
 });
</script> 
@endsection
