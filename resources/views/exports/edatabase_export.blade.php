<!DOCTYPE html>
<html lang="en">

<head>
    <title>eDatabase</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/style.css" rel="stylesheet">
</head>

<body>
    <table width="100%" border='1' cellspacing="0">
        <thead>
            <tr>
                <th>Urusan/Bidang</th>
                <th>Satuan</th>
                <th>Nilai</th>
            </tr>
        </thead>
        <tbody>
            @foreach($edatabase as $index=>$row)
            <tr>
                <?php
                $space = '';
                for($i=0; $i<$row['space']; $i++){
                    $space = $space.'&nbsp;';
                }
                $val = str_replace(".", "", $row['value']);
                if (is_numeric($val)) {
                    if (strpos($row['value'], ".") === true) {
                        $decimal = strlen(substr($row['value'], strrpos($row['value'], '.') + 1));
                        $value = number_format($row['value'], $decimal, ',', '.');
                    } else {
                        $value = number_format($row['value'], 0, '', '.');
                    }
                } else if ($row['value'] === "") {
                    $value = '-';
                } else {
                    $value = $row['value'];
                }
                ?>
                <td>{!!$space!!}{{$row['number']}}. {{$row['uraian']}}</td>
                <td style="text-align: center;">{{$row['satuan']}}</td>
                <td style="text-align: center;">{{$value}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>