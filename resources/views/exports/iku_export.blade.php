<!DOCTYPE html>
<html lang="en">
    <head>
        <title>IKU</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <table width="100%" border='1' cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Uraian</th>
                    <th>Urusan</th>
                    <th>Satuan</th>
                    <th>Target <?php echo date('Y')+1; ?></th>
                </tr>
            </thead>
            <tbody>
                @foreach($iku as $index=>$row)
                <tr>
                    <td style="text-align: center;">{{$index+1}}</td>
                    <td>{{$row['iku_description']}}</td>
                    <td>{{$row['urusan']}}</td>
                    <td style="text-align: center;">{{$row['satuan']}}</td>
                    <td style="text-align: center;">{{$row['target']}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
