<!DOCTYPE html>
<html lang="en">
    <head>
        <title>SPM</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/style.css" rel="stylesheet">
    </head>
    <body>
        <table width="100%" border='1' cellspacing="0">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Uraian</th>
                    <th>Satuan</th>
                    @foreach($data['tahun'] as $thn)
                    <th>{{$thn->tahun}}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($data['spm'] as $index=>$row)
                <tr>
                    <td style="text-align: center;">{{$index+1}}</td>
                    <td>{{$row['uraian']}}</td>
                    <td style="text-align: center;">{{$row['satuan']}}</td>
                    @foreach($data['tahun'] as $thn)
                    <td>{{$row[$thn->tahun]}}</td>
                    @endforeach
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>
