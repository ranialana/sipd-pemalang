<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class edatabaseModel extends Model
{
    protected $table = 'edatabase';

    protected $fillable = [
        'id', 'business_id', 'area_id', 'sub_area_id', 'indicator_id', 'sub_indicator_id', 'unit_id', 'value', 'validate', 'validate_final'
    ];

    public function getBusiness()
    {
        return $this->belongsTo(businessModel::class, 'business_id');
    }


    public function getArea()
    {

        return $this->belongsTo(areaModel::class, 'area_id');
    }

    public function getSubArea()
    {

        return $this->belongsTo(subAreaModel::class, 'sub_area_id');
    }

    public function getIndicator()
    {

        return $this->belongsTo(indicatorModel::class, 'indicator_id');
    }

    public function getSubIndicator()
    {
        
        return $this->belongsTo(subIndicatorModel::class, 'sub_indicator_id');
    }
}
