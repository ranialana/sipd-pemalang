<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subIndicatorModel extends Model
{
    protected $table = 'sub_indicators';

    protected $fillable = [
        'id', 'business_id', 'indicator_id', 'area_id', 'sub_area_id', 'sub_indicator_name', 'unit_id', 'check_sub', 'role', 'parent_id', 'created_at', 'updated_at', 'active',
    ];

    public function child()
    {

        return $this->hasMany(subIndicatorModel::class, 'parent_id', 'id');
    }

    public function parent()
    {

        return $this->belongsTo(subIndicatorModel::class, 'parent_id');
    }

    // public function parent()
    // {
    //     return $this->hasMany(subIndicatorModel::class, 'parent_id');
    // }
 
    // public function childCategories()
    // {
    //     return $this->hasMany(subIndicatorMOdel::class, 'parent_id')->with('parent');
    // }

    public function getBusiness()
    {

        return $this->belongsTo(businessModel::class, 'business_id');
    }


    public function getArea()
    {

        return $this->belongsTo(areaModel::class, 'area_id');
    }

    public function getsubArea()
    {

        return $this->belongsTo(subAreaModel::class, 'sub_area_id');
    }

    public function getIndicator()
    {

        return $this->belongsTo(indicatorModel::class, 'indicator_id');
    }

    public function geteDatabase()
    {

        return $this->hasMany(edatabaseModel::class, 'sub_indicator_id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
