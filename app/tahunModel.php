<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tahunModel extends Model
{
    protected $table = "tahun";

    protected $fillable = ['tahun'];

    public function kortrans()
    {
        return $this->hasMany(kortransModel::class, 'id_tahun', 'id');
    }

    public function spmtrans()
    {
        return $this->hasMany(spmtransModel::class, 'id_tahun', 'id');
    }

    public function sdgstrans()
    {
        return $this->hasMany(sdgstransModel::class, 'id_tahun', 'id');
    }

    public function bmdtrans()
    {
        return $this->hasMany(bmdtransModel::class, 'id_tahun', 'id');
    }
}
