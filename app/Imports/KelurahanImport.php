<?php

namespace App\Imports;

use App\kecamatanModel;
use App\kelurahanModel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class KelurahanImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        foreach($collection as $index => $row){
            if($index > 0){
                $kecamatan = kecamatanModel::where('nama', $row[0])->first();
                if(count($kecamatan)>0){
                    $kel = new kelurahanModel;
                    $kel->id_kec = $kecamatan->id;
                    $kel->nama = $row[1];
                    $kel->save();
                }
            }
        }
    }
}
