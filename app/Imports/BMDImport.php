<?php

namespace App\Imports;

use App\bmdModel;
use App\unitsModel;
use App\bmdtransModel;
use App\tahunModel;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class BMDImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function  __construct($id_kec)
    {
        $this->id_kec = $id_kec;
    }
    public function collection(Collection $collection)
    {
        $rows = [];
        $tahun = array();
        $value = array();
        $user = Auth::user();

        foreach ($collection as $index => $items) {
            $rows = count($items);

            if ($items[0] === 'No.') {
                $tahun = $items->slice(0);
            } else if ($items[0] != 'No.' && $items[0] != "") {
                $value = $items->slice(0, count($tahun) - 1);
                $val = array();
                for ($i = 0; $i < count($value); $i++) {
                    if ($i  > 5) {
                        $val[] = array('value' => $value[$i], 'tahun' => $tahun[$i]);
                    } else if ($i === 2) {
                        $uraian = $value[$i];
                    } else if ($i === 5) {
                        $satuan = $value[$i];
                    } else {
                    }
                }
                if ($uraian != "") {
                    $cek_bmd = bmdModel::where('bmd_description', $uraian)->first();
                    if ($cek_bmd) {
                        
                        foreach ($val as $trans) {
                            $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                            if ($thn) {
                                $cek_bmdtrans = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn->id)->where('id_kec', $this->id_kec)->where('status', 'aktif')->first();
                                if ($cek_bmdtrans) {
                                    if ($trans['value'] != null) {
                                        if(strval($trans['value']) === strval($cek_bmdtrans->value)) {
                                            // jika value sama maka tidak ada action
                                        }else {
                                            $cek_bmdtrans->status = 'non-aktif';
                                            $cek_bmdtrans->save();

                                            $bmdtrans = new bmdtransModel;
                                            $bmdtrans->id = $bmdtrans->id;
                                            $bmdtrans->id_kec = $this->id_kec;
                                            $bmdtrans->id_kel = 0;
                                            $bmdtrans->user_id = $user->id;
                                            $bmdtrans->id_bmd = $cek_bmd->id;
                                            $bmdtrans->id_tahun = $thn->id;
                                            $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $bmdtrans->value = $trans['value'];
                                            $bmdtrans->status = 'aktif';
                                            $bmdtrans->save();

                                            // cek bmd trans kabupaten
                                            $cek_kab = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_bmd' => $cek_bmd->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    bmdtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_bmd' => $cek_bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                } else {
                                    if ($trans['value'] != null) {
                                        $bmdtrans = new bmdtransModel;
                                        $bmdtrans->id = $bmdtrans->id;
                                        $bmdtrans->id_kec = $this->id_kec;
                                        $bmdtrans->id_kel = 0;
                                        $bmdtrans->user_id = $user->id;
                                        $bmdtrans->id_bmd = $cek_bmd->id;
                                        $bmdtrans->id_tahun = $thn->id;
                                        $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $bmdtrans->value = $trans['value'];
                                        $bmdtrans->status = 'aktif';
                                        $bmdtrans->save();

                                        // cek bmd trans kabupaten
                                        $cek_kab = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_bmd' => $cek_bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_bmd' => $cek_bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                            else {
                                $thn_baru = new tahunModel;
                                $thn_baru->tahun = $trans['tahun'];
                                $thn_baru->save();
                                $cek_bmdtrans = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_kec', $this->id_kec)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();
                                if ($cek_bmdtrans) {
                                    if ($trans['value'] != null) {
                                        if(strval($trans['value']) === strval($cek_bmdtrans->value)) {
                                            // jika value sama maka tidak ada action
                                        }else {
                                            $cek_bmdtrans->status = 'non-aktif';
                                            $cek_bmdtrans->save();

                                            $bmdtrans = new bmdtransModel;
                                            $bmdtrans->id = $bmdtrans->id;
                                            $bmdtrans->id_kec = $this->id_kec;
                                            $bmdtrans->id_kel = 0;
                                            $bmdtrans->user_id = $user->id;
                                            $bmdtrans->id_bmd = $cek_bmd->id;
                                            $bmdtrans->id_tahun = $thn_baru->id;
                                            $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $bmdtrans->value = $trans['value'];
                                            $bmdtrans->status = 'aktif';
                                            $bmdtrans->save();

                                            // cek bmd trans kabupaten
                                            $cek_kab = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_bmd' => $cek_bmd->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    bmdtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_bmd' => $cek_bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                } else {
                                    if ($trans['value'] != null) {
                                        $bmdtrans = new bmdtransModel;
                                        $bmdtrans->id = $bmdtrans->id;
                                        $bmdtrans->id_kec = $this->id_kec;
                                        $bmdtrans->id_kel = 0;
                                        $bmdtrans->user_id = $user->id;
                                        $bmdtrans->id_bmd = $cek_bmd->id;
                                        $bmdtrans->id_tahun = $thn_baru->id;
                                        $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $bmdtrans->value = $trans['value'];
                                        $bmdtrans->status = 'aktif';
                                        $bmdtrans->save();

                                        // cek bmd trans kabupaten
                                        $cek_kab = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_bmd' => $cek_bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = bmdtransModel::where('id_bmd', $cek_bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_bmd' => $cek_bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                            
                        }

                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $cek_bmd->unit_id = $unit->id;
                            $cek_bmd->save();
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $cek_bmd->unit_id = $add_unit->id;
                            $cek_bmd->save();
                        }
                    } else {
                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $bmd = new bmdModel;
                            $bmd->bmd_description = $uraian;
                            $bmd->satuan_id = $unit->id;
                            $bmd->value = 0;
                            $bmd->parent = 0;
                            $bmd->validate = 0;
                            $bmd->validate_final = 0;
                            $bmd->save();

                            foreach ($val as $trans) {
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    if ($trans['value'] != null) {
                                        $bmdtrans = new bmdtransModel;
                                        $bmdtrans->id = $bmdtrans->id;
                                        $bmdtrans->id_kec = $this->id_kec;
                                        $bmdtrans->id_kel = 0;
                                        $bmdtrans->user_id = $user->id;
                                        $bmdtrans->id_bmd = $bmd->id;
                                        $bmdtrans->id_tahun = $thn->id;
                                        $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $bmdtrans->value = $trans['value'];
                                        $bmdtrans->status = 'aktif';
                                        $bmdtrans->save();

                                        // cek bmd trans kabupaten
                                        $cek_kab = bmdtransModel::where('id_bmd', $bmd->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_bmd' => $bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_bmd' => $bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();

                                    $bmdtrans = new bmdtransModel;
                                    $bmdtrans->id = $bmdtrans->id;
                                    $bmdtrans->id_kec = $this->id_kec;
                                    $bmdtrans->id_kel = 0;
                                    $bmdtrans->user_id = $user->id;
                                    $bmdtrans->id_bmd = $bmd->id;
                                    $bmdtrans->id_tahun = $thn_baru->id;
                                    $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                    $bmdtrans->value = $trans['value'];
                                    $bmdtrans->status = 'aktif';
                                    $bmdtrans->save();

                                    // cek bmd trans kabupaten
                                    $cek_kab = bmdtransModel::where('id_bmd', $bmd->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                        if ($cek_kab->value === $sum) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        }
                                        else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_bmd' => $bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );
                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                    else{
                                        // buat baru
                                        $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                        $data_kab = array(
                                            'id_bmd' => $bmd->id,
                                            'id_kec' => 0,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn_baru->id,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum,
                                            'status' => 'aktif'
                                        );

                                        bmdtransModel::create($data_kab);
                                    }
                                }
                                
                            }
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $bmd = new bmdModel;
                            $bmd->id = $bmd->id;
                            $bmd->bmd_description = $uraian;
                            $bmd->satuan_id = $add_unit->id;
                            $bmd->value = 0;
                            $bmd->parent = 0;
                            $bmd->validate = 0;
                            $bmd->validate_final = 0;
                            $bmd->save();

                            foreach ($val as $trans) {
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    if ($trans['value'] != null) {
                                        $bmdtrans = new bmdtransModel;
                                        $bmdtrans->id = $bmdtrans->id;
                                        $bmdtrans->id_kec = $this->id_kec;
                                        $bmdtrans->id_kel = 0;
                                        $bmdtrans->user_id = $user->id;
                                        $bmdtrans->id_bmd = $bmd->id;
                                        $bmdtrans->id_tahun = $thn->id;
                                        $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $bmdtrans->value = $trans['value'];
                                        $bmdtrans->status = 'aktif';
                                        $bmdtrans->save();

                                        // cek bmd trans kabupaten
                                        $cek_kab = bmdtransModel::where('id_bmd', $bmd->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_bmd' => $bmd->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                bmdtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_bmd' => $bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();

                                    $bmdtrans = new bmdtransModel;
                                    $bmdtrans->id = $bmdtrans->id;
                                    $bmdtrans->id_kec = $this->id_kec;
                                    $bmdtrans->id_kel = 0;
                                    $bmdtrans->user_id = $user->id;
                                    $bmdtrans->id_bmd = $bmd->id;
                                    $bmdtrans->id_tahun = $thn_baru->id;
                                    $bmdtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                    $bmdtrans->value = $trans['value'];
                                    $bmdtrans->status = 'aktif';
                                    $bmdtrans->save();

                                    // cek bmd trans kabupaten
                                    $cek_kab = bmdtransModel::where('id_bmd', $bmd->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                        if ($cek_kab->value === $sum) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        }
                                        else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_bmd' => $bmd->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );
                                            bmdtransModel::create($data_kab);
                                        }
                                    }
                                    else{
                                        // buat baru
                                        $sum = bmdtransModel::where('id_bmd', $bmd->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                        $data_kab = array(
                                            'id_bmd' => $bmd->id,
                                            'id_kec' => 0,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn_baru->id,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum,
                                            'status' => 'aktif'
                                        );

                                        spmtransModel::create($data_kab);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }
}
