<?php

namespace App\Imports;

use App\datadukungModel;
use App\unitsModel;
use App\datadukungtransModel;
use App\tahunModel;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class datadukungImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function  __construct($id_kec)
    {
        $this->id_kec = $id_kec;
    }
    public function collection(Collection $collection)
    {
        $rows = [];
        $tahun = array();
        $value = array();
        $user = Auth::user();

        foreach ($collection as $index => $items) {
            $rows = count($items);

            if ($items[0] === 'No.') {
                $tahun = $items->slice(0);
            } else if ($items[0] != 'No.' && $items[0] != "") {
                $value = $items->slice(0, count($tahun) - 1);
                $val = array();
                for ($i = 0; $i < count($value); $i++) {
                    if ($i  > 5) {
                        $val[] = array('value' => $value[$i], 'tahun' => $tahun[$i]);
                    } else if ($i === 2) {
                        $uraian = $value[$i];
                    } else if ($i === 5) {
                        $satuan = $value[$i];
                    } else {
                    }
                }
                if ($uraian != "") {
                    $cek_datadukung = datadukungModel::where('data_description', $uraian)->first();
                    if ($cek_datadukung) {
                        
                        foreach ($val as $trans) {
                            if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    $cek_datadukungtrans = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn->id)->where('id_kec', $this->id_kec)->where('status', 'aktif')->first();
                                    if ($cek_datadukungtrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_datadukungtrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_datadukungtrans->status = 'non-aktif';
                                                $cek_datadukungtrans->save();

                                                $datadukungtrans = new datadukungtransModel;
                                                $datadukungtrans->id = $datadukungtrans->id;
                                                $datadukungtrans->id_kec = $this->id_kec;
                                                $datadukungtrans->id_kel = 0;
                                                $datadukungtrans->user_id = $user->id;
                                                $datadukungtrans->id_data = $cek_datadukung->id;
                                                $datadukungtrans->id_tahun = $thn->id;
                                                $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $datadukungtrans->value = $trans['value'];
                                                $datadukungtrans->status = 'aktif';
                                                $datadukungtrans->save();

                                                // cek datadukung trans kabupaten
                                                $cek_kab = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_data' => $cek_datadukung->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        datadukungtransModel::create($data_kab);
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_data' => $cek_datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    datadukungtransModel::create($data_kab);
                                                }
                                                $cek_datadukung->validate = 0;
                                                $cek_datadukung->validate_final = 0;
                                                $cek_datadukung->save();
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $datadukungtrans = new datadukungtransModel;
                                            $datadukungtrans->id = $datadukungtrans->id;
                                            $datadukungtrans->id_kec = $this->id_kec;
                                            $datadukungtrans->id_kel = 0;
                                            $datadukungtrans->user_id = $user->id;
                                            $datadukungtrans->id_data = $cek_datadukung->id;
                                            $datadukungtrans->id_tahun = $thn->id;
                                            $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $datadukungtrans->value = $trans['value'];
                                            $datadukungtrans->status = 'aktif';
                                            $datadukungtrans->save();

                                            // cek datadukung trans kabupaten
                                            $cek_kab = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_data' => $cek_datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    datadukungtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_data' => $cek_datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                datadukungtransModel::create($data_kab);
                                            }
                                            $cek_datadukung->validate = 0;
                                            $cek_datadukung->validate_final = 0;
                                            $cek_datadukung->save();

                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();
                                    $cek_datadukungtrans = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_kec', $this->id_kec)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();
                                    if ($cek_datadukungtrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_datadukungtrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_datadukungtrans->status = 'non-aktif';
                                                $cek_datadukungtrans->save();

                                                $datadukungtrans = new datadukungtransModel;
                                                $datadukungtrans->id = $datadukungtrans->id;
                                                $datadukungtrans->id_kec = $this->id_kec;
                                                $datadukungtrans->id_kel = 0;
                                                $datadukungtrans->user_id = $user->id;
                                                $datadukungtrans->id_data = $cek_datadukung->id;
                                                $datadukungtrans->id_tahun = $thn_baru->id;
                                                $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $datadukungtrans->value = $trans['value'];
                                                $datadukungtrans->status = 'aktif';
                                                $datadukungtrans->save();

                                                // cek datadukung trans kabupaten
                                                $cek_kab = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_data' => $cek_datadukung->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn_baru->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        datadukungtransModel::create($data_kab);

                                                        $cek_datadukung->validate = 0;
                                                        $cek_datadukung->validate_final = 0;
                                                        $cek_datadukung->save();
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_data' => $cek_datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    datadukungtransModel::create($data_kab);

                                                    $cek_datadukung->validate = 0;
                                                    $cek_datadukung->validate_final = 0;
                                                    $cek_datadukung->save();
                                                }
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $datadukungtrans = new datadukungtransModel;
                                            $datadukungtrans->id = $datadukungtrans->id;
                                            $datadukungtrans->id_kec = $this->id_kec;
                                            $datadukungtrans->id_kel = 0;
                                            $datadukungtrans->user_id = $user->id;
                                            $datadukungtrans->id_data = $cek_datadukung->id;
                                            $datadukungtrans->id_tahun = $thn_baru->id;
                                            $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $datadukungtrans->value = $trans['value'];
                                            $datadukungtrans->status = 'aktif';
                                            $datadukungtrans->save();

                                            // cek datadukung trans kabupaten
                                            $cek_kab = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_data' => $cek_datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    datadukungtransModel::create($data_kab);

                                                    $cek_datadukung->validate = 0;
                                                    $cek_datadukung->validate_final = 0;
                                                    $cek_datadukung->save();
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = datadukungtransModel::where('id_data', $cek_datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_data' => $cek_datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                datadukungtransModel::create($data_kab);

                                                $cek_datadukung->validate = 0;
                                                $cek_datadukung->validate_final = 0;
                                                $cek_datadukung->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $cek_datadukung->unit_id = $unit->id;
                            $cek_datadukung->save();
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $cek_datadukung->unit_id = $add_unit->id;
                            $cek_datadukung->save();
                        }
                    } else {
                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $datadukung = new datadukungModel;
                            $datadukung->id = $datadukung->id;
                            $datadukung->data_description = $uraian;
                            $datadukung->unit_id = $unit->id;
                            $datadukung->value = 0;
                            $datadukung->parent = 0;
                            $datadukung->validate = 0;
                            $datadukung->validate_final = 0;
                            $datadukung->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $datadukungtrans = new datadukungtransModel;
                                            $datadukungtrans->id = $datadukungtrans->id;
                                            $datadukungtrans->id_kec = $this->id_kec;
                                            $datadukungtrans->id_kel = 0;
                                            $datadukungtrans->user_id = $user->id;
                                            $datadukungtrans->id_data = $datadukung->id;
                                            $datadukungtrans->id_tahun = $thn->id;
                                            $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $datadukungtrans->value = $trans['value'];
                                            $datadukungtrans->status = 'aktif';
                                            $datadukungtrans->save();

                                            // cek datadukung trans kabupaten
                                            $cek_kab = datadukungtransModel::where('id_data', $datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_data' => $datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    datadukungtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_data' => $datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                datadukungtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $datadukungtrans = new datadukungtransModel;
                                        $datadukungtrans->id = $datadukungtrans->id;
                                        $datadukungtrans->id_kec = $this->id_kec;
                                        $datadukungtrans->id_kel = 0;
                                        $datadukungtrans->user_id = $user->id;
                                        $datadukungtrans->id_data = $datadukung->id;
                                        $datadukungtrans->id_tahun = $thn_baru->id;
                                        $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $datadukungtrans->value = $trans['value'];
                                        $datadukungtrans->status = 'aktif';
                                        $datadukungtrans->save();

                                        // cek datadukung trans kabupaten
                                        $cek_kab = datadukungtransModel::where('id_data', $datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_data' => $datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                datadukungtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_data' => $datadukung->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            datadukungtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $datadukung = new datadukungModel;
                            $datadukung->id = $datadukung->id;
                            $datadukung->data_description = $uraian;
                            $datadukung->unit_id = $add_unit->id;
                            $datadukung->value = 0;
                            $datadukung->validate = 0;
                            $datadukung->validate_final = 0;
                            $datadukung->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $datadukungtrans = new datadukungtransModel;
                                            $datadukungtrans->id = $datadukungtrans->id;
                                            $datadukungtrans->id_kec = $this->id_kec;
                                            $datadukungtrans->id_kel = 0;
                                            $datadukungtrans->user_id = $user->id;
                                            $datadukungtrans->id_data = $datadukung->id;
                                            $datadukungtrans->id_tahun = $thn->id;
                                            $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $datadukungtrans->value = $trans['value'];
                                            $datadukungtrans->status = 'aktif';
                                            $datadukungtrans->save();

                                            // cek datadukung trans kabupaten
                                            $cek_kab = datadukungtransModel::where('id_data', $datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_data' => $datadukung->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    datadukungtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_data' => $datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                datadukungtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $datadukungtrans = new datadukungtransModel;
                                        $datadukungtrans->id = $datadukungtrans->id;
                                        $datadukungtrans->id_kec = $this->id_kec;
                                        $datadukungtrans->id_kel = 0;
                                        $datadukungtrans->user_id = $user->id;
                                        $datadukungtrans->id_data = $datadukung->id;
                                        $datadukungtrans->id_tahun = $thn_baru->id;
                                        $datadukungtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $datadukungtrans->value = $trans['value'];
                                        $datadukungtrans->status = 'aktif';
                                        $datadukungtrans->save();

                                        // cek datadukung trans kabupaten
                                        $cek_kab = datadukungtransModel::where('id_data', $datadukung->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_data' => $datadukung->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                datadukungtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = datadukungtransModel::where('id_data', $datadukung->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_data' => $datadukung->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            datadukungtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }
}
