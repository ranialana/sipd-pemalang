<?php

namespace App\Imports;

use App\korModel;
use App\unitsModel;
use App\kortransModel;
use App\tahunModel;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class KORImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function  __construct($id_kec)
    {
        $this->id_kec = $id_kec;
    }
    public function collection(Collection $collection)
    {
        $rows = [];
        $tahun = array();
        $value = array();
        $user = Auth::user();

        foreach ($collection as $index => $items) {
            $rows = count($items);

            if ($items[0] === 'No.') {
                $tahun = $items->slice(0);
            } else if ($items[0] != 'No.' && $items[0] != "") {
                $value = $items->slice(0, count($tahun) - 1);
                $val = array();
                for ($i = 0; $i < count($value); $i++) {
                    if ($i  > 5) {
                        $val[] = array('value' => $value[$i], 'tahun' => $tahun[$i]);
                    } else if ($i === 2) {
                        $uraian = $value[$i];
                    } else if ($i === 5) {
                        $satuan = $value[$i];
                    } else {
                    }
                }
                if ($uraian != "") {
                    $cek_kor = korModel::where('kor_name', $uraian)->first();
                    if ($cek_kor) {
                        
                        foreach ($val as $trans) {
                            if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    $cek_kortrans = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn->id)->where('id_kec', $this->id_kec)->where('status', 'aktif')->first();
                                    if ($cek_kortrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_kortrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_kortrans->status = 'non-aktif';
                                                $cek_kortrans->save();

                                                $kortrans = new kortransModel;
                                                $kortrans->id = $kortrans->id;
                                                $kortrans->id_kec = $this->id_kec;
                                                $kortrans->id_kel = 0;
                                                $kortrans->user_id = $user->id;
                                                $kortrans->id_kor = $cek_kor->id;
                                                $kortrans->id_tahun = $thn->id;
                                                $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $kortrans->value = $trans['value'];
                                                $kortrans->status = 'aktif';
                                                $kortrans->save();

                                                // cek kor trans kabupaten
                                                $cek_kab = kortransModel::where('id_kor', $cek_kor->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_kor' => $cek_kor->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        kortransModel::create($data_kab);
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_kor' => $cek_kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    kortransModel::create($data_kab);
                                                }
                                                $cek_kor->validate = 0;
                                                $cek_kor->validate_final = 0;
                                                $cek_kor->save();
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $kortrans = new kortransModel;
                                            $kortrans->id = $kortrans->id;
                                            $kortrans->id_kec = $this->id_kec;
                                            $kortrans->id_kel = 0;
                                            $kortrans->user_id = $user->id;
                                            $kortrans->id_kor = $cek_kor->id;
                                            $kortrans->id_tahun = $thn->id;
                                            $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $kortrans->value = $trans['value'];
                                            $kortrans->status = 'aktif';
                                            $kortrans->save();

                                            // cek kor trans kabupaten
                                            $cek_kab = kortransModel::where('id_kor', $cek_kor->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_kor' => $cek_kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    kortransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_kor' => $cek_kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                kortransModel::create($data_kab);
                                            }
                                            $cek_kor->validate = 0;
                                            $cek_kor->validate_final = 0;
                                            $cek_kor->save();

                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();
                                    $cek_kortrans = kortransModel::where('id_kor', $cek_kor->id)->where('id_kec', $this->id_kec)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();
                                    if ($cek_kortrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_kortrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_kortrans->status = 'non-aktif';
                                                $cek_kortrans->save();

                                                $kortrans = new kortransModel;
                                                $kortrans->id = $kortrans->id;
                                                $kortrans->id_kec = $this->id_kec;
                                                $kortrans->id_kel = 0;
                                                $kortrans->user_id = $user->id;
                                                $kortrans->id_kor = $cek_kor->id;
                                                $kortrans->id_tahun = $thn_baru->id;
                                                $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $kortrans->value = $trans['value'];
                                                $kortrans->status = 'aktif';
                                                $kortrans->save();

                                                // cek kor trans kabupaten
                                                $cek_kab = kortransModel::where('id_kor', $cek_kor->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_kor' => $cek_kor->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn_baru->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        kortransModel::create($data_kab);

                                                        $cek_kor->validate = 0;
                                                        $cek_kor->validate_final = 0;
                                                        $cek_kor->save();
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_kor' => $cek_kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    kortransModel::create($data_kab);

                                                    $cek_kor->validate = 0;
                                                    $cek_kor->validate_final = 0;
                                                    $cek_kor->save();
                                                }
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $kortrans = new kortransModel;
                                            $kortrans->id = $kortrans->id;
                                            $kortrans->id_kec = $this->id_kec;
                                            $kortrans->id_kel = 0;
                                            $kortrans->user_id = $user->id;
                                            $kortrans->id_kor = $cek_kor->id;
                                            $kortrans->id_tahun = $thn_baru->id;
                                            $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $kortrans->value = $trans['value'];
                                            $kortrans->status = 'aktif';
                                            $kortrans->save();

                                            // cek kor trans kabupaten
                                            $cek_kab = kortransModel::where('id_kor', $cek_kor->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_kor' => $cek_kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    kortransModel::create($data_kab);

                                                    $cek_kor->validate = 0;
                                                    $cek_kor->validate_final = 0;
                                                    $cek_kor->save();
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = kortransModel::where('id_kor', $cek_kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_kor' => $cek_kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                kortransModel::create($data_kab);

                                                $cek_kor->validate = 0;
                                                $cek_kor->validate_final = 0;
                                                $cek_kor->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $cek_kor->unit_id = $unit->id;
                            $cek_kor->save();
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $cek_kor->unit_id = $add_unit->id;
                            $cek_kor->save();
                        }
                    } else {
                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $kor = new korModel;
                            $kor->id = $kor->id;
                            $kor->kor_name = $uraian;
                            $kor->unit_id = $unit->id;
                            $kor->value = 0;
                            $kor->validate = 0;
                            $kor->validate_final = 0;
                            $kor->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $kortrans = new kortransModel;
                                            $kortrans->id = $kortrans->id;
                                            $kortrans->id_kec = $this->id_kec;
                                            $kortrans->id_kel = 0;
                                            $kortrans->user_id = $user->id;
                                            $kortrans->id_kor = $kor->id;
                                            $kortrans->id_tahun = $thn->id;
                                            $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $kortrans->value = $trans['value'];
                                            $kortrans->status = 'aktif';
                                            $kortrans->save();

                                            // cek kor trans kabupaten
                                            $cek_kab = kortransModel::where('id_kor', $kor->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_kor' => $kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    kortransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_kor' => $kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                kortransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $kortrans = new kortransModel;
                                        $kortrans->id = $kortrans->id;
                                        $kortrans->id_kec = $this->id_kec;
                                        $kortrans->id_kel = 0;
                                        $kortrans->user_id = $user->id;
                                        $kortrans->id_kor = $kor->id;
                                        $kortrans->id_tahun = $thn_baru->id;
                                        $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $kortrans->value = $trans['value'];
                                        $kortrans->status = 'aktif';
                                        $kortrans->save();

                                        // cek kor trans kabupaten
                                        $cek_kab = kortransModel::where('id_kor', $kor->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_kor' => $kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                kortransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_kor' => $kor->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            kortransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $kor = new korModel;
                            $kor->id = $kor->id;
                            $kor->kor_name = $uraian;
                            $kor->unit_id = $add_unit->id;
                            $kor->value = 0;
                            $kor->validate = 0;
                            $kor->validate_final = 0;
                            $kor->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $kortrans = new kortransModel;
                                            $kortrans->id = $kortrans->id;
                                            $kortrans->id_kec = $this->id_kec;
                                            $kortrans->id_kel = 0;
                                            $kortrans->user_id = $user->id;
                                            $kortrans->id_kor = $kor->id;
                                            $kortrans->id_tahun = $thn->id;
                                            $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $kortrans->value = $trans['value'];
                                            $kortrans->status = 'aktif';
                                            $kortrans->save();

                                            // cek kor trans kabupaten
                                            $cek_kab = kortransModel::where('id_kor', $kor->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_kor' => $kor->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    kortransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_kor' => $kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                kortransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $kortrans = new kortransModel;
                                        $kortrans->id = $kortrans->id;
                                        $kortrans->id_kec = $this->id_kec;
                                        $kortrans->id_kel = 0;
                                        $kortrans->user_id = $user->id;
                                        $kortrans->id_kor = $kor->id;
                                        $kortrans->id_tahun = $thn_baru->id;
                                        $kortrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $kortrans->value = $trans['value'];
                                        $kortrans->status = 'aktif';
                                        $kortrans->save();

                                        // cek kor trans kabupaten
                                        $cek_kab = kortransModel::where('id_kor', $kor->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_kor' => $kor->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                kortransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = kortransModel::where('id_kor', $kor->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_kor' => $kor->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            kortransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }
}
