<?php

namespace App\Imports;

use App\sdgsModel;
use App\unitsModel;
use App\sdgstransModel;
use App\tahunModel;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SDGsImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function  __construct($id_kec)
    {
        $this->id_kec = $id_kec;
    }
    public function collection(Collection $collection)
    {
        $rows = [];
        $tahun = array();
        $value = array();
        $user = Auth::user();

        foreach ($collection as $index => $items) {
            $rows = count($items);

            if ($items[0] === 'No.') {
                $tahun = $items->slice(0);
            } else if ($items[0] != 'No.' && $items[0] != "") {
                $value = $items->slice(0, count($tahun) - 1);
                $val = array();
                for ($i = 0; $i < count($value); $i++) {
                    if ($i  > 5) {
                        $val[] = array('value' => $value[$i], 'tahun' => $tahun[$i]);
                    } else if ($i === 2) {
                        $uraian = $value[$i];
                    } else if ($i === 5) {
                        $satuan = $value[$i];
                    } else {
                    }
                }
                if ($uraian != "") {
                    $cek_sdgs = sdgsModel::where('sdgs_description', $uraian)->first();
                    if ($cek_sdgs) {
                        
                        foreach ($val as $trans) {
                            if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    $cek_sdgstrans = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn->id)->where('id_kec', $this->id_kec)->where('status', 'aktif')->first();
                                    if ($cek_sdgstrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_sdgstrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_sdgstrans->status = 'non-aktif';
                                                $cek_sdgstrans->save();

                                                $sdgstrans = new sdgstransModel;
                                                $sdgstrans->id = $sdgstrans->id;
                                                $sdgstrans->id_kec = $this->id_kec;
                                                $sdgstrans->id_kel = 0;
                                                $sdgstrans->user_id = $user->id;
                                                $sdgstrans->id_sdgs = $cek_sdgs->id;
                                                $sdgstrans->id_tahun = $thn->id;
                                                $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $sdgstrans->value = $trans['value'];
                                                $sdgstrans->status = 'aktif';
                                                $sdgstrans->save();

                                                // cek sdgs trans kabupaten
                                                $cek_kab = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_sdgs' => $cek_sdgs->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        sdgstransModel::create($data_kab);
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_sdgs' => $cek_sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    sdgstransModel::create($data_kab);
                                                }
                                                $cek_sdgs->validate = 0;
                                                $cek_sdgs->validate_final = 0;
                                                $cek_sdgs->save();
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $sdgstrans = new sdgstransModel;
                                            $sdgstrans->id = $sdgstrans->id;
                                            $sdgstrans->id_kec = $this->id_kec;
                                            $sdgstrans->id_kel = 0;
                                            $sdgstrans->user_id = $user->id;
                                            $sdgstrans->id_sdgs = $cek_sdgs->id;
                                            $sdgstrans->id_tahun = $thn->id;
                                            $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $sdgstrans->value = $trans['value'];
                                            $sdgstrans->status = 'aktif';
                                            $sdgstrans->save();

                                            // cek sdgs trans kabupaten
                                            $cek_kab = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_sdgs' => $cek_sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    sdgstransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_sdgs' => $cek_sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                sdgstransModel::create($data_kab);
                                            }
                                            $cek_sdgs->validate = 0;
                                            $cek_sdgs->validate_final = 0;
                                            $cek_sdgs->save();

                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();
                                    $cek_sdgstrans = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_kec', $this->id_kec)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();
                                    if ($cek_sdgstrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_sdgstrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_sdgstrans->status = 'non-aktif';
                                                $cek_sdgstrans->save();

                                                $sdgstrans = new sdgstransModel;
                                                $sdgstrans->id = $sdgstrans->id;
                                                $sdgstrans->id_kec = $this->id_kec;
                                                $sdgstrans->id_kel = 0;
                                                $sdgstrans->user_id = $user->id;
                                                $sdgstrans->id_sdgs = $cek_sdgs->id;
                                                $sdgstrans->id_tahun = $thn_baru->id;
                                                $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $sdgstrans->value = $trans['value'];
                                                $sdgstrans->status = 'aktif';
                                                $sdgstrans->save();

                                                // cek sdgs trans kabupaten
                                                $cek_kab = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_sdgs' => $cek_sdgs->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn_baru->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        sdgstransModel::create($data_kab);

                                                        $cek_sdgs->validate = 0;
                                                        $cek_sdgs->validate_final = 0;
                                                        $cek_sdgs->save();
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_sdgs' => $cek_sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    sdgstransModel::create($data_kab);

                                                    $cek_sdgs->validate = 0;
                                                    $cek_sdgs->validate_final = 0;
                                                    $cek_sdgs->save();
                                                }
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $sdgstrans = new sdgstransModel;
                                            $sdgstrans->id = $sdgstrans->id;
                                            $sdgstrans->id_kec = $this->id_kec;
                                            $sdgstrans->id_kel = 0;
                                            $sdgstrans->user_id = $user->id;
                                            $sdgstrans->id_sdgs = $cek_sdgs->id;
                                            $sdgstrans->id_tahun = $thn_baru->id;
                                            $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $sdgstrans->value = $trans['value'];
                                            $sdgstrans->status = 'aktif';
                                            $sdgstrans->save();

                                            // cek sdgs trans kabupaten
                                            $cek_kab = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_sdgs' => $cek_sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    sdgstransModel::create($data_kab);

                                                    $cek_sdgs->validate = 0;
                                                    $cek_sdgs->validate_final = 0;
                                                    $cek_sdgs->save();
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = sdgstransModel::where('id_sdgs', $cek_sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_sdgs' => $cek_sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                sdgstransModel::create($data_kab);

                                                $cek_sdgs->validate = 0;
                                                $cek_sdgs->validate_final = 0;
                                                $cek_sdgs->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $cek_sdgs->unit_id = $unit->id;
                            $cek_sdgs->save();
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $cek_sdgs->unit_id = $add_unit->id;
                            $cek_sdgs->save();
                        }
                    } else {
                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $sdgs = new sdgsModel;
                            $sdgs->id = $sdgs->id;
                            $sdgs->sdgs_description = $uraian;
                            $sdgs->unit_id = $unit->id;
                            $sdgs->value = 0;
                            $sdgs->validate = 0;
                            $sdgs->validate_final = 0;
                            $sdgs->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $sdgstrans = new sdgstransModel;
                                            $sdgstrans->id = $sdgstrans->id;
                                            $sdgstrans->id_kec = $this->id_kec;
                                            $sdgstrans->id_kel = 0;
                                            $sdgstrans->user_id = $user->id;
                                            $sdgstrans->id_sdgs = $sdgs->id;
                                            $sdgstrans->id_tahun = $thn->id;
                                            $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $sdgstrans->value = $trans['value'];
                                            $sdgstrans->status = 'aktif';
                                            $sdgstrans->save();

                                            // cek sdgs trans kabupaten
                                            $cek_kab = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_sdgs' => $sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    sdgstransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_sdgs' => $sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                sdgstransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $sdgstrans = new sdgstransModel;
                                        $sdgstrans->id = $sdgstrans->id;
                                        $sdgstrans->id_kec = $this->id_kec;
                                        $sdgstrans->id_kel = 0;
                                        $sdgstrans->user_id = $user->id;
                                        $sdgstrans->id_sdgs = $sdgs->id;
                                        $sdgstrans->id_tahun = $thn_baru->id;
                                        $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $sdgstrans->value = $trans['value'];
                                        $sdgstrans->status = 'aktif';
                                        $sdgstrans->save();

                                        // cek sdgs trans kabupaten
                                        $cek_kab = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_sdgs' => $sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                sdgstransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_sdgs' => $sdgs->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            sdgstransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $sdgs = new sdgsModel;
                            $sdgs->id = $sdgs->id;
                            $sdgs->sdgs_description = $uraian;
                            $sdgs->unit_id = $add_unit->id;
                            $sdgs->value = 0;
                            $sdgs->validate = 0;
                            $sdgs->validate_final = 0;
                            $sdgs->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $sdgstrans = new sdgstransModel;
                                            $sdgstrans->id = $sdgstrans->id;
                                            $sdgstrans->id_kec = $this->id_kec;
                                            $sdgstrans->id_kel = 0;
                                            $sdgstrans->user_id = $user->id;
                                            $sdgstrans->id_sdgs = $sdgs->id;
                                            $sdgstrans->id_tahun = $thn->id;
                                            $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $sdgstrans->value = $trans['value'];
                                            $sdgstrans->status = 'aktif';
                                            $sdgstrans->save();

                                            // cek sdgs trans kabupaten
                                            $cek_kab = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_sdgs' => $sdgs->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    sdgstransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_sdgs' => $sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                sdgstransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $sdgstrans = new sdgstransModel;
                                        $sdgstrans->id = $sdgstrans->id;
                                        $sdgstrans->id_kec = $this->id_kec;
                                        $sdgstrans->id_kel = 0;
                                        $sdgstrans->user_id = $user->id;
                                        $sdgstrans->id_sdgs = $sdgs->id;
                                        $sdgstrans->id_tahun = $thn_baru->id;
                                        $sdgstrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $sdgstrans->value = $trans['value'];
                                        $sdgstrans->status = 'aktif';
                                        $sdgstrans->save();

                                        // cek sdgs trans kabupaten
                                        $cek_kab = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_sdgs' => $sdgs->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                sdgstransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = sdgstransModel::where('id_sdgs', $sdgs->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_sdgs' => $sdgs->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            sdgstransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }
}
