<?php

namespace App\Imports;

use App\spmModel;
use App\unitsModel;
use App\spmtransModel;
use App\tahunModel;
use Auth;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class SPMImport implements ToCollection
{
    /**
     * @param Collection $collection
     */
    public function  __construct($id_kec)
    {
        $this->id_kec = $id_kec;
    }
    public function collection(Collection $collection)
    {
        $rows = [];
        $tahun = array();
        $value = array();
        $user = Auth::user();

        foreach ($collection as $index => $items) {
            $rows = count($items);

            if ($items[0] === 'No.') {
                $tahun = $items->slice(0);
            } else if ($items[0] != 'No.' && $items[0] != "") {
                $value = $items->slice(0, count($tahun) - 1);
                $val = array();
                for ($i = 0; $i < count($value); $i++) {
                    if ($i  > 5) {
                        $val[] = array('value' => $value[$i], 'tahun' => $tahun[$i]);
                    } else if ($i === 2) {
                        $uraian = $value[$i];
                    } else if ($i === 5) {
                        $satuan = $value[$i];
                    } else {
                    }
                }
                if ($uraian != "") {
                    $cek_spm = spmModel::where('spm_description', $uraian)->first();
                    if ($cek_spm) {
                        
                        foreach ($val as $trans) {
                            if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                if ($thn) {
                                    $cek_spmtrans = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn->id)->where('id_kec', $this->id_kec)->where('status', 'aktif')->first();
                                    if ($cek_spmtrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_spmtrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_spmtrans->status = 'non-aktif';
                                                $cek_spmtrans->save();

                                                $spmtrans = new spmtransModel;
                                                $spmtrans->id = $spmtrans->id;
                                                $spmtrans->id_kec = $this->id_kec;
                                                $spmtrans->id_kel = 0;
                                                $spmtrans->user_id = $user->id;
                                                $spmtrans->id_spm = $cek_spm->id;
                                                $spmtrans->id_tahun = $thn->id;
                                                $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $spmtrans->value = $trans['value'];
                                                $spmtrans->status = 'aktif';
                                                $spmtrans->save();

                                                // cek spm trans kabupaten
                                                $cek_kab = spmtransModel::where('id_spm', $cek_spm->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_spm' => $cek_spm->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        spmtransModel::create($data_kab);
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_spm' => $cek_spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    spmtransModel::create($data_kab);
                                                }
                                                $cek_spm->validate = 0;
                                                $cek_spm->validate_final = 0;
                                                $cek_spm->save();
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $spmtrans = new spmtransModel;
                                            $spmtrans->id = $spmtrans->id;
                                            $spmtrans->id_kec = $this->id_kec;
                                            $spmtrans->id_kel = 0;
                                            $spmtrans->user_id = $user->id;
                                            $spmtrans->id_spm = $cek_spm->id;
                                            $spmtrans->id_tahun = $thn->id;
                                            $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $spmtrans->value = $trans['value'];
                                            $spmtrans->status = 'aktif';
                                            $spmtrans->save();

                                            // cek spm trans kabupaten
                                            $cek_kab = spmtransModel::where('id_spm', $cek_spm->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_spm' => $cek_spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    spmtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_spm' => $cek_spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                spmtransModel::create($data_kab);
                                            }
                                            $cek_spm->validate = 0;
                                            $cek_spm->validate_final = 0;
                                            $cek_spm->save();

                                        }
                                    }
                                }
                                else {
                                    $thn_baru = new tahunModel;
                                    $thn_baru->tahun = $trans['tahun'];
                                    $thn_baru->save();
                                    $cek_spmtrans = spmtransModel::where('id_spm', $cek_spm->id)->where('id_kec', $this->id_kec)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();
                                    if ($cek_spmtrans) {
                                        if ($trans['value'] != null) {
                                            if(strval($trans['value']) === strval($cek_spmtrans->value)) {
                                                // jika value sama maka tidak ada action
                                            }else {
                                                $cek_spmtrans->status = 'non-aktif';
                                                $cek_spmtrans->save();

                                                $spmtrans = new spmtransModel;
                                                $spmtrans->id = $spmtrans->id;
                                                $spmtrans->id_kec = $this->id_kec;
                                                $spmtrans->id_kel = 0;
                                                $spmtrans->user_id = $user->id;
                                                $spmtrans->id_spm = $cek_spm->id;
                                                $spmtrans->id_tahun = $thn_baru->id;
                                                $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                                $spmtrans->value = $trans['value'];
                                                $spmtrans->status = 'aktif';
                                                $spmtrans->save();

                                                // cek spm trans kabupaten
                                                $cek_kab = spmtransModel::where('id_spm', $cek_spm->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                                if (count((array)$cek_kab) > 0) {
                                                    // jika data kabupaten ada maka update status + create
                                                    $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                    if ($cek_kab->value === $sum) {
                                                        // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                    }
                                                    else {
                                                        $cek_kab->status = 'non-aktif';
                                                        $cek_kab->save();

                                                        $data_kab = array(
                                                            'id_spm' => $cek_spm->id,
                                                            'id_kec' => 0,
                                                            'id_kel' => 0,
                                                            'user_id' => $user->id,
                                                            'id_tahun' => $thn_baru->id,
                                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                            'value' => $sum,
                                                            'status' => 'aktif'
                                                        );
                                                        spmtransModel::create($data_kab);

                                                        $cek_spm->validate = 0;
                                                        $cek_spm->validate_final = 0;
                                                        $cek_spm->save();
                                                    }
                                                }
                                                else{
                                                    // buat baru
                                                    $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                    $data_kab = array(
                                                        'id_spm' => $cek_spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );

                                                    spmtransModel::create($data_kab);

                                                    $cek_spm->validate = 0;
                                                    $cek_spm->validate_final = 0;
                                                    $cek_spm->save();
                                                }
                                            }
                                        }
                                    } else {
                                        if ($trans['value'] != null) {
                                            $spmtrans = new spmtransModel;
                                            $spmtrans->id = $spmtrans->id;
                                            $spmtrans->id_kec = $this->id_kec;
                                            $spmtrans->id_kel = 0;
                                            $spmtrans->user_id = $user->id;
                                            $spmtrans->id_spm = $cek_spm->id;
                                            $spmtrans->id_tahun = $thn_baru->id;
                                            $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $spmtrans->value = $trans['value'];
                                            $spmtrans->status = 'aktif';
                                            $spmtrans->save();

                                            // cek spm trans kabupaten
                                            $cek_kab = spmtransModel::where('id_spm', $cek_spm->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_spm' => $cek_spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn_baru->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    spmtransModel::create($data_kab);

                                                    $cek_spm->validate = 0;
                                                    $cek_spm->validate_final = 0;
                                                    $cek_spm->save();
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = spmtransModel::where('id_spm', $cek_spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_spm' => $cek_spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                spmtransModel::create($data_kab);

                                                $cek_spm->validate = 0;
                                                $cek_spm->validate_final = 0;
                                                $cek_spm->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $cek_spm->unit_id = $unit->id;
                            $cek_spm->save();
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $cek_spm->unit_id = $add_unit->id;
                            $cek_spm->save();
                        }
                    } else {
                        $unit = unitsModel::where('unit', $satuan)->first();
                        if ($unit) {
                            $spm = new spmModel;
                            $spm->id = $spm->id;
                            $spm->spm_description = $uraian;
                            $spm->unit_id = $unit->id;
                            $spm->value = 0;
                            $spm->validate = 0;
                            $spm->validate_final = 0;
                            $spm->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $spmtrans = new spmtransModel;
                                            $spmtrans->id = $spmtrans->id;
                                            $spmtrans->id_kec = $this->id_kec;
                                            $spmtrans->id_kel = 0;
                                            $spmtrans->user_id = $user->id;
                                            $spmtrans->id_spm = $spm->id;
                                            $spmtrans->id_tahun = $thn->id;
                                            $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $spmtrans->value = $trans['value'];
                                            $spmtrans->status = 'aktif';
                                            $spmtrans->save();

                                            // cek spm trans kabupaten
                                            $cek_kab = spmtransModel::where('id_spm', $spm->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_spm' => $spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    spmtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_spm' => $spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                spmtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $spmtrans = new spmtransModel;
                                        $spmtrans->id = $spmtrans->id;
                                        $spmtrans->id_kec = $this->id_kec;
                                        $spmtrans->id_kel = 0;
                                        $spmtrans->user_id = $user->id;
                                        $spmtrans->id_spm = $spm->id;
                                        $spmtrans->id_tahun = $thn_baru->id;
                                        $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $spmtrans->value = $trans['value'];
                                        $spmtrans->status = 'aktif';
                                        $spmtrans->save();

                                        // cek spm trans kabupaten
                                        $cek_kab = spmtransModel::where('id_spm', $spm->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_spm' => $spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                spmtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_spm' => $spm->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            spmtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        } else {
                            $add_unit = new unitsModel;
                            $add_unit->id = $add_unit->id;
                            $add_unit->unit = $satuan;
                            $add_unit->save();

                            $spm = new spmModel;
                            $spm->id = $spm->id;
                            $spm->spm_description = $uraian;
                            $spm->unit_id = $add_unit->id;
                            $spm->value = 0;
                            $spm->validate = 0;
                            $spm->validate_final = 0;
                            $spm->save();

                            foreach ($val as $trans) {
                                if($trans['tahun'] != null && strlen($trans['tahun']) >= 4 && ctype_digit($trans['tahun'])){
                                    $thn = tahunModel::where('tahun', $trans['tahun'])->first();
                                    if ($thn) {
                                        if ($trans['value'] != null) {
                                            $spmtrans = new spmtransModel;
                                            $spmtrans->id = $spmtrans->id;
                                            $spmtrans->id_kec = $this->id_kec;
                                            $spmtrans->id_kel = 0;
                                            $spmtrans->user_id = $user->id;
                                            $spmtrans->id_spm = $spm->id;
                                            $spmtrans->id_tahun = $thn->id;
                                            $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                            $spmtrans->value = $trans['value'];
                                            $spmtrans->status = 'aktif';
                                            $spmtrans->save();

                                            // cek spm trans kabupaten
                                            $cek_kab = spmtransModel::where('id_spm', $spm->id)->where('id_kec', '0')->where('id_tahun', $thn->id)->where('status', 'aktif')->first();

                                            if (count((array)$cek_kab) > 0) {
                                                // jika data kabupaten ada maka update status + create
                                                $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                                if ($cek_kab->value === $sum) {
                                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                                }
                                                else {
                                                    $cek_kab->status = 'non-aktif';
                                                    $cek_kab->save();

                                                    $data_kab = array(
                                                        'id_spm' => $spm->id,
                                                        'id_kec' => 0,
                                                        'id_kel' => 0,
                                                        'user_id' => $user->id,
                                                        'id_tahun' => $thn->id,
                                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                        'value' => $sum,
                                                        'status' => 'aktif'
                                                    );
                                                    spmtransModel::create($data_kab);
                                                }
                                            }
                                            else{
                                                // buat baru
                                                $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                                $data_kab = array(
                                                    'id_spm' => $spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );

                                                spmtransModel::create($data_kab);
                                            }
                                        }
                                    }
                                    else {
                                        $thn_baru = new tahunModel;
                                        $thn_baru->tahun = $trans['tahun'];
                                        $thn_baru->save();

                                        $spmtrans = new spmtransModel;
                                        $spmtrans->id = $spmtrans->id;
                                        $spmtrans->id_kec = $this->id_kec;
                                        $spmtrans->id_kel = 0;
                                        $spmtrans->user_id = $user->id;
                                        $spmtrans->id_spm = $spm->id;
                                        $spmtrans->id_tahun = $thn_baru->id;
                                        $spmtrans->tgl_pengisian = date('Y-m-d H:i:s');
                                        $spmtrans->value = $trans['value'];
                                        $spmtrans->status = 'aktif';
                                        $spmtrans->save();

                                        // cek spm trans kabupaten
                                        $cek_kab = spmtransModel::where('id_spm', $spm->id)->where('id_kec', '0')->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->first();

                                        if (count((array)$cek_kab) > 0) {
                                            // jika data kabupaten ada maka update status + create
                                            $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                            if ($cek_kab->value === $sum) {
                                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                            }
                                            else {
                                                $cek_kab->status = 'non-aktif';
                                                $cek_kab->save();

                                                $data_kab = array(
                                                    'id_spm' => $spm->id,
                                                    'id_kec' => 0,
                                                    'id_kel' => 0,
                                                    'user_id' => $user->id,
                                                    'id_tahun' => $thn_baru->id,
                                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                    'value' => $sum,
                                                    'status' => 'aktif'
                                                );
                                                spmtransModel::create($data_kab);
                                            }
                                        }
                                        else{
                                            // buat baru
                                            $sum = spmtransModel::where('id_spm', $spm->id)->where('id_tahun', $thn_baru->id)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                                            $data_kab = array(
                                                'id_spm' => $spm->id,
                                                'id_kec' => 0,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn_baru->id,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum,
                                                'status' => 'aktif'
                                            );

                                            spmtransModel::create($data_kab);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
            }
        }
    }
}
