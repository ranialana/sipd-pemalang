<?php

namespace App\Imports;

use App\userModel;
use App\skpdUser;
use App\skpdModel;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class UsersImport implements ToCollection
{
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
      foreach($collection as $key => $row){
        if ($key > 0) {
          $users = userModel::where('email', $row[1])->where('username', $row[0])->get();
          if (count($users) == 0) {
              $user = new userModel;
              $user->id = $user->id;
              $user->username = $row[0];
              $user->email = $row[1];
              $user->active = 0;
              $user->password = bcrypt('pemalang');
              $user->role = $row[2];
              $user->full_name = $row[3];
              $user->save();
            
            if($row[4] != '') {
              $array = explode(',', $row[4]);
              $user_id = $user->id;
              foreach($array as $data) {
                $skpd = skpdModel::where('skpd_name', $data)->first();
                if ($skpd) {
                  $skpd_user = new skpdUser;
                  $skpd_user->id = $skpd_user->id;
                  $skpd_user->user_id = $user_id;
                  $skpd_user->skpd_id = $skpd->id;
                  $skpd_user->active = 0;
                  $skpd_user->save(); 
                }
                
              }
            }
          }
        }
        
      }
        
    }
}
