<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sdgsModel extends Model
{
    protected $table = 'sdgs';
    protected $fillable = ['id', 'sdgs_description', 'unit_id', 'value', 'type_user', 'validate', 'validate_final'];

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }

    public function sdgs_trans()
    {
        return $this->hasMany(sdgstransModel::class, 'id_sdgs', 'id');
    }
}
