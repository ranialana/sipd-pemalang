<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kecamatanModel extends Model
{
    protected $table = "kecamatan";
    protected $fillable = ['nama', 'status'];

    public function kel()
    {
        return $this->hasMany(kelurahannModel::class, 'id_kec', 'id');
    }
}
