<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bmdtransModel extends Model
{
    protected $table = 'bmd_trans';

    protected $fillable = ['user_id', 'id_bmd', 'id_bmd_child', 'id_kec', 'id_kel', 'id_tahun', 'value', 'tgl_pengisian', 'check_active', 'status'];

    public function bmd()
    {
        return $this->belongsTo(bmdModel::class, 'id_bmd');
    } 
    public function user()
    {
        return $this->belongsTo(userModel::class, 'user_id');
    }

    public function tahun()
    {
        return $this->belongsTo(tahunModel::class, 'id_tahun');
    }
}
