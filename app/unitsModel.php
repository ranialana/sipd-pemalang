<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class unitsModel extends Model
{
    protected $table = 'units';
    protected $fillable = ['unit'];

    public function kor()
    {
        return $this->hasMany(korModel::class, 'unit_id', 'id');
    }

    public function spm()
    {
        return $this->hasMany(spmModel::class, 'unit_id', 'id');
    }

    public function sdgs()
    {
        return $this->hasMany(sdgsModel::class, 'unit_id', 'id');
    }

    public function data_dukung()
    {
        return $this->hasMany(datadukungModel::class, 'unit_id', 'id');
    }

    public function data_dukung_child()
    {
        return $this->hasMany(datadukungchildModel::class, 'unit_id', 'id');
    }

    public function bmd()
    {
        return $this->hasMany(bmdModel::class, 'satuan_id', 'id');
    }

    public function bmd_child()
    {
        return $this->hasMany(bmdchildModel::class, 'unit_id', 'id');
    }

    public function business()
    {
        return $this->hasMany(businessModel::class, 'unit_id', 'id');
    }

    public function area()
    {
        return $this->hasMany(areaModel::class, 'unit_id', 'id');
    }

    public function subArea()
    {
        return $this->hasMany(subAreaModel::class, 'unit_id', 'id');
    }

    public function indicator()
    {
        return $this->hasMany(indicatorModel::class, 'unit_id', 'id');
    }

    public function subIndicator()
    {
        return $this->hasMany(subIndicatorModel::class, 'unit_id', 'id');
    }
}
