<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bmdModel extends Model
{
    protected $table = 'bmd';
    protected $fillable = ['id', 'bmd_description', 'satuan_id', 'value', 'validate', 'validate_final'];

    public function child()
    {
        return $this->hasMany(bmdchildModel::class, 'id_bmd', 'id');
    }
    
    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'satuan_id');
    }

    public function bmd_trans()
    {
        return $this->hasMany(bmdtransModel::class, 'id_bmd', 'id');
    }
}
