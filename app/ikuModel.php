<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ikuModel extends Model
{
    protected $table = 'iku';

    protected $fillable = ['id', 'iku_description', 'unit_id', 'value', 'target', 'validate', 'validate_final'];
}
