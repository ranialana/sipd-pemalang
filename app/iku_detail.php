<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class iku_detail extends Model
{
    protected $table = "iku_detail";

    protected $fillable  = ['iku_id', 'business_id'];

    public function getBusiness()
    {
    	return $this->hasOne('App\businessModel','id','business_id');
    }
}
