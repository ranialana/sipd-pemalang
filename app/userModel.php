<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\skpdModel;

class userModel extends Authenticatable
{
   protected $table = 'users';
   protected $fillable = [
      'id', 'created_at', 'updated_at', 'active', 'username', 'email', 'password', 'role', 'type_user', 'id_kec', 'id_kel', 'full_name',
   ];

   public function kor_trans()
   {
      return $this->hasMany(kortransModel::class, 'user_id', 'id');
   }
   /**/

   public function getSkpdUser()
   {
      return $this->hasMany(skpdUser::class, 'user_id', 'id');
   }

   /* */
}
