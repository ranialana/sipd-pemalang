<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datadukungchildModel extends Model
{
    protected $table = "data_support_child";
    protected $fillable = ['id', 'id_data', 'data_description', 'unit_id', 'type_user', 'value', 'validate', 'validate_final'];

    public function parent()
    {
        return $this->belongsTo(datadukungModel::class, 'id_data');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
