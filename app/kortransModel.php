<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kortransModel extends Model
{
    protected $table = 'kor_trans';

    protected $fillable = ['user_id', 'id_kor', 'id_kec', 'id_kel', 'id_tahun', 'value', 'check_active', 'status'];

    public function kor()
    {
        return $this->belongsTo(korModel::class, 'id_kor');
    }
    

    public function user()
    {
        return $this->belongsTo(userModel::class, 'user_id');
    }

    public function tahun()
    {
        return $this->belongsTo(tahunModel::class, 'id_tahun');
    }
}
