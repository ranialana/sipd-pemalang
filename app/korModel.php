<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class korModel extends Model
{
    protected $table = 'kor';
    protected $fillable = ['id', 'kor_name', 'unit_id', 'value', 'type_user', 'validate', 'validate_final'];

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }

    public function kor_trans()
    {
        return $this->hasMany(kortransModel::class, 'id_kor', 'id');
    }
}
