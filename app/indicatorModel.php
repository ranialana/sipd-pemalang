<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indicatorModel extends Model
{
    protected $table = 'indicators';

    protected $fillable = [
        'id', 'business_id', 'indicator_name', 'area_id', 'sub_area_id', 'check_sub', 'role', 'unit_id', 'created_at', 'updated_at', 'active',
    ];

    public function getBusiness()
    {

        return $this->belongsTo(businessMode::class, 'business_id');
    }


    public function getArea()
    {

        return $this->belongsTo(areaModel::class, 'area_id');
    }

    public function getsubArea()
    {

        return $this->belongsTo(subAreaModel::class, 'sub_area_id');
    }


    public function getSubIndicator()
    {

        return $this->hasMany(subIndicatorModel::class, 'indicator_id');
    }

    public function geteDatabase()
    {

        return $this->hasMany(edatabaseModel::class, 'business_id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
