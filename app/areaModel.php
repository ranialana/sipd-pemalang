<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class areaModel extends Model
{
    protected $table = 'areas';

    protected $fillable = [
        'id', 'business_id', 'area_name', 'check_sub', 'role', 'unit_id', 'created_at', 'updated_at', 'active',
    ];


    public function getBusiness()
    {

        return $this->belongsTo(businessModel::class, 'business_id');
    }


    public function getSubArea()
    {

        return $this->hasMany(subAreaModel::class, 'area_id');
    }

    public function getIndicator()
    {

        return $this->hasMany(indicatorModel::class, 'sub_area_id');
    }

    public function getSubIndicator()
    {

        return $this->hasMany(subIndicatorModel::class, 'area_id');
    }

    public function geteDatabase()
    {

        return $this->hasMany(edatabaseModel::class, 'business_id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
