<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class skpdUser extends Model
{

   use SoftDeletes;
   protected $table = 'users_skpd';
   protected $fillable = [
      'id', 'created_at', 'updated_at', 'user_id', 'skpd_id',
   ];

   public function User()
   {
      return $this->belongsTo(userModel::class, 'user_id');
   }

   public function getSkpd()
   {
      return $this->hasOne('App\skpdModel', 'id', 'skpd_id');
   }
}
