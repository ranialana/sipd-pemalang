<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spmtransModel extends Model
{
    protected $table = 'spm_trans';

    protected $fillable = ['user_id', 'id_spm', 'id_kec', 'id_kel', 'id_tahun', 'value', 'check_active', 'status'];

    public function spm()
    {
        return $this->belongsTo(spmModel::class, 'id_spm');
    }
    

    public function user()
    {
        return $this->belongsTo(userModel::class, 'user_id');
    }

    public function tahun()
    {
        return $this->belongsTo(tahunModel::class, 'id_tahun');
    }
}
