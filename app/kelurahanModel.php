<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelurahanModel extends Model
{
    protected $table = "kelurahan";
    protected $fillable = ['id_kec', 'nama', 'status'];

    public function kec()
    {
        return $this->belongsTo(kecamatanModel::class, 'id_kec');
    }
}
