<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\kelurahanModel;
use App\kecamatanModel;
use App\Imports\KelurahanImport;
use App\Exports\KelurahanExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use File;
use PDF;

use Illuminate\Http\Request;


class KelurahanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = kelurahanModel::orderBy('id_kec', 'ASC')->orderBy('nama', 'ASC')->get();
            $stat = "";

            return Datatables::of($data)
                ->addColumn('id_kecamatan', function ($row) {
                    $kecamatan = kecamatanModel::find($row->id_kec);

                    return $kecamatan->nama;
                })         
                ->addColumn('status', function ($row) {
                    if($row->status == 1)
                    {
                        $stat = "Aktif";
                    }else
                    {
                        $stat = "Tidak Aktif";
                    }

                    return $stat;
                })
                ->addColumn('action', function ($row) {

                    $btn = '<a href="' . url("change-kelurahan/" . $row->id) . '"><span class="glyphicon glyphicon-pencil"></span></a>';

                    $btn = $btn . ' <a href="#" class="hapus_kelurahan" data-url="' . url("delete-kelurahan/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';

                    return $btn;
                })
                ->addIndexColumn()
                ->make(true);
        }
        return view('kelurahan.master_kelurahan');
    }

    public function change_kelurahan($id)
    {
        $kecamatan = kecamatanModel::all();
        $kelurahan = kelurahanModel::find($id);
        
        return view('kelurahan.kelurahan_edit', ['kecamatan' => $kecamatan, 'kelurahan' => $kelurahan]);
    }


    public function update_status_kelurahan(request $request)
    {
        $id_kelurahan   = $request->id;

        $form_data = array(
                                'id_kec'    =>  $request->kecamatan,
                                'nama'      =>  $request->nama,
                                'status'    =>  $request->status
                            );

        kelurahanModel::whereId($id_kelurahan)->update($form_data);

        return redirect('/master-kelurahan')->with('success', 'Data Kelurahan Berhasil Diubah!');
    }

    public function add_kelurahan()
    {
        $kecamatan = kecamatanModel::all();

        return view('kelurahan.kelurahan_add', ['kecamatan' => $kecamatan]);
    }

    public function save_kelurahan(Request $request)
    {
        $form_data = array(
                                'id_kec'    =>  $request->kecamatan,
                                'nama'      =>  $request->nama,
                                'status'    =>  $request->status
                            );     

        kelurahanModel::create($form_data);

        return redirect('/master-kelurahan')->with('success', 'Data Kelurahan Berhasil Ditambahkan!');
    }

    public function delete_kelurahan($id)
    {
        kelurahanModel::whereId($id)->delete();

        return back()->with('success', 'Data Master Kelurahan berhasil dihapus!');
    }

    public function import_kelurahan(Request $request)
    {
        $nama_file = rand().$request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new KelurahanImport, storage_path('app/public/tmp_import/'.$nama_file));
        
        Storage::delete('public/tmp_import/'.$nama_file);

        return back()->with('success_import','File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/Kelurahan.xlsx');
    }

    public function excel()
    {
        return Excel::download(new KelurahanExport, 'Kelurahan.xlsx');
    }

    public function pdf()
    {
        $kelurahan = kelurahanModel::orderBy('id_kec', 'ASC')->orderBy('nama', 'ASC')->get();

        view()->share('kelurahan', $kelurahan);
        $pdf = PDF::loadView('master_export.kelurahan_export', $kelurahan);

        return $pdf->stream('Kelurahan.pdf');
    }
}
