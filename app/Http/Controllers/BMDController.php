<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\bmdModel;
use \App\bmdtransModel;
use \App\bmdchildModel;
use \App\unitsModel;
use \App\tahunModel;
use \App\kecamatanModel;
use \App\kelurahanModel;
use App\Imports\bmdImport;
use App\Exports\BMDExport;
use App\Exports\MasterBMDExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;
use Auth;

class BMDController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $thn = tahunModel::all();
            if (auth()->user()->role === '0') {
                $data1 = bmdModel::all();
            } else {
                if (auth()->user()->type_user === 0) {
                    $data1 = bmdModel::where('type_user', 0)->get();
                } elseif (auth()->user()->type_user === 1) {
                    $data1 = bmdModel::where('type_user', 1)->get();
                } else {
                    $data1 = bmdModel::where('type_user', 2)->get();
                }
            }

            foreach ($data1 as $dd) {
                $data[] = array('id' => $dd->id, 'bmd_description' => $dd->bmd_description, 'space' => 0, 'satuan_id' => $dd->satuan_id, 'value' => $dd->value, 'type_user' => $dd->type_user, 'validate' => $dd->validate, 'validate_final' => $dd->validate_final, 'created_at' => $dd->creatd_at, 'updated_at' => $dd->updated_at, 'master' => $dd->id);
                if (count($dd->child)) {
                    $data = parent_bmd($dd->child, $data, 0, $dd->id);
                }
            }
            if (count($data1) > 0) {
            } else {
                $data = array();
            }
            $table = Datatables::of($data);
            if ($request->get('kec') === "Kabupaten") {
                $table->editColumn('data', function ($row) {
                    $space = '';
                    for ($i = 0; $i < $row['space']; $i++) {
                        $space = $space . '&nbsp;';
                    }
                    return $space . $row['bmd_description'];
                });
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row['satuan_id']);
                    return $unit->unit;
                });
                foreach ($thn as $a) {
                    $th = $a->id;
                    $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                        if ($row['space'] === 0) {
                            $id_parent = $row['id'];
                            $id_child = 0;
                        } else {
                            $id_parent = $row['master'];
                            $id_child = $row['id'];
                        }
                        if ($row['type_user'] === 0) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row['type_user'] === 1) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$bmdtrans) > 0) {
                            $val = str_replace(".", "", $bmdtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($bmdtrans->value, ".") === true) {
                                    $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                    $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($bmdtrans->value, 0, '', '.');
                                }
                            } else if ($bmdtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $bmdtrans->value;
                            }
                        } else {
                            $value = '-';
                        }

                        return $value;
                    });
                }

                $table->addColumn('action', function ($row) use ($request) {
                    if ($row['space'] === 0) {
                        $id_parent = $row['id'];
                        $id_child = 0;
                    } else {
                        $id_parent = $row['master'];
                        $id_child = $row['id'];
                    }
                    $btn = '<a class="history" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';
                    if ($row['type_user'] === 0) {
                        if (auth()->user()->role === '1') {
                            if ($row['validate'] === 0 && $row['validate_final'] === 0) {
                                if ($row['space'] === 0) {
                                    $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-bmd/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                } else {
                                    $btn = $btn . ' <a href="#" class="target_validasi_child" data-url="' . url("validasi-bmd-child/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                }
                            } else if ($row['validate'] === 1 && $row['validate_final'] === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif (auth()->user()->role === '2' || auth()->user()->role === '0') {
                            if ($row['space'] === 0) {
                                $id_parent = $row['id'];
                                $id_child = 0;
                            } else {
                                $id_parent = $row['master'];
                                $id_child = $row['id'];
                            }
                            $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                        } else {
                        }
                    }
                    // if ($row['validate'] === 0 && $row['validate_final'] === 0) {
                    //     $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-bmd/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                    // } else if ($row['validate'] === 1 && $row['validate_final'] === 0) {
                    //     $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                    // } else {
                    //     $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                    // }
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['bmd_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['data', 'action']);

                return $table->make(true);
            } else {
                $table->editColumn('data', function ($row) {
                    $space = '';
                    for ($i = 0; $i < $row['space']; $i++) {
                        $space = $space . '&nbsp;';
                    }
                    return $space . $row['bmd_description'];
                });
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row['satuan_id']);
                    return $unit->unit;
                });
                if ($request->get('kel')) {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row['space'] === 0) {
                                $id_parent = $row['id'];
                                $id_child = 0;
                            } else {
                                $id_parent = $row['master'];
                                $id_child = $row['id'];
                            }
                            if ($row['type_user'] === 0) {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row['type_user'] === 1) {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$bmdtrans) > 0) {
                                $val = str_replace(".", "", $bmdtrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($bmdtrans->value, ".") === true) {
                                        $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                        $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($bmdtrans->value, 0, '', '.');
                                    }
                                } else if ($bmdtrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $bmdtrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                } else {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row['space'] === 0) {
                                $id_parent = $row['id'];
                                $id_child = 0;
                            } else {
                                $id_parent = $row['master'];
                                $id_child = $row['id'];
                            }
                            if ($row['type_user'] === 0) {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row['type_user'] === 1) {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$bmdtrans) > 0) {
                                $val = str_replace(".", "", $bmdtrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($bmdtrans->value, ".") === true) {
                                        $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                        $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($bmdtrans->value, 0, '', '.');
                                    }
                                } else if ($bmdtrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $bmdtrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                }


                $table->addColumn('action', function ($row) use ($request) {
                    if ($row['space'] === 0) {
                        $id_parent = $row['id'];
                        $id_child = 0;
                    } else {
                        $id_parent = $row['master'];
                        $id_child = $row['id'];
                    }
                    $btn = '<a class="history" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '"  data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';


                    if (auth()->user()->role === '1') {

                        if ($row['type_user'] === 0) {
                            if ($row['validate'] === 0 && $row['validate_final'] === 0) {
                                if ($row['space'] === 0) {
                                    $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-bmd/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                } else {
                                    $btn = $btn . ' <a href="#" class="target_validasi_child" data-url="' . url("validasi-bmd-child/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                }
                            } else if ($row['validate'] === 1 && $row['validate_final'] === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif ($row['type_user'] === 1) {
                            if ($row['validate'] === 0 && $row['validate_final'] === 0) {
                                if ($row['space'] === 0) {
                                    $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-bmd/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                } else {
                                    $btn = $btn . ' <a href="#" class="target_validasi_child" data-url="' . url("validasi-bmd-child/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                }
                            } else if ($row['validate'] === 1 && $row['validate_final'] === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } else {
                            if ($row['validate'] === 0 && $row['validate_final'] === 0) {
                                if ($row['space'] === 0) {
                                    $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-bmd/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                } else {
                                    $btn = $btn . ' <a href="#" class="target_validasi_child" data-url="' . url("validasi-bmd-child/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                                }
                            } else if ($row['validate'] === 1 && $row['validate_final'] === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        }
                    } else {
                        if ($row['type_user'] === 0) {
                            if ($request->kel) {
                            } else {
                                if ($request->get('kec')) {
                                } else {
                                    if ($row['space'] === 0) {
                                        $id_parent = $row['id'];
                                        $id_child = 0;
                                    } else {
                                        $id_parent = $row['master'];
                                        $id_child = $row['id'];
                                    }
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } elseif ($row['type_user'] === 1) {
                            if ($request->get('kec') != 0) {
                                if ($request->kel) {
                                } else {
                                    if ($row['space'] === 0) {
                                        $id_parent = $row['id'];
                                        $id_child = 0;
                                    } else {
                                        $id_parent = $row['master'];
                                        $id_child = $row['id'];
                                    }
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } else {
                            if ($request->kel) {
                                if ($row['space'] === 0) {
                                    $id_parent = $row['id'];
                                    $id_child = 0;
                                } else {
                                    $id_parent = $row['master'];
                                    $id_child = $row['id'];
                                }
                                $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $id_parent . '" data-child-support="' . $id_child . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                            }
                        }
                    }
                    // $btn = $btn . ' <a href="#myModalEdit" data-toggle="modal" data-id="' . $row['id'] . '" data-bmd_description="' . $row['bmd_description'] . '" data-satuan="' . $row['satuan_id'] . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    // $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row['id'] . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';

                    // $btn = $btn . ' <a href="#" class="hapus_bmd" data-url="' . url("delete-bmd/" . $row['id']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['bmd_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['data', 'action']);

                return $table->make(true);
            }
        }
        $unit = unitsModel::all();
        $thn = tahunModel::all();

        if (auth()->user()->role === '0') {
            $kecamatan = kecamatanModel::all();
            $kelurahan = kelurahanModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $kecamatan = kecamatanModel::all();
                $kelurahan = kelurahanModel::all();
            } elseif (auth()->user()->type_user === 1) {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::all();
            } else {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::find(auth()->user()->id_kel);
            }
        }




        $a = date('Y') - 4;
        $b = date('Y') - 3;
        $c = date('Y') - 2;
        $d = date('Y') - 1;
        $e = date('Y');

        $column = 2 + count($thn) + 1;

        $kec = "Kabupaten";
        $kel = '';

        return view('bmd.bmd_vw', ['unit' => $unit, 'thn' => $thn, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan, 'kec' => $kec, 'kel' => $kel, 'column' => $column, 'a' => $a, 'b' => $b, 'c' => $c, 'd' => $d, 'e' => $e]);
    }

    public function save_bmd(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;
        if ($request->parent) {
            $parent = $request->parent;

            $form_data = array(
                'id_bmd' => $parent,
                'bmd_description' => $request->uraian,
                'satuan_id' => $request->satuan,
                'value' => 0,
                'type_user' => $request->type_user,
                'validate' => 0,
                'validate_final' => 0
            );

            if ($request->type_user === '0') {
                // insert data untuk val kabupaten untuk user type Perangkat Daerah
                $id_kec = null;
                $id_kel = null;
            } elseif ($request->type_user === '1') {
                // insert data untuk val kabupaten untuk user type Kecamatan
                $id_kec = 0;
                $id_kel = null;
            } else {
                // insert data untuk val kabupaten untuk user type Kelurahan
                $id_kec = null;
                $id_kel = 0;
            }

            if ($id) {
                $bmd = bmdchildModel::find($id);
                $tahun = $request->tahun;
                // Cek type_user bmd Perangkat Daerah
                if ($bmd->type_user === 0) {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        $check_bmdtrans_tahun->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    }
                    // =
                    elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();

                        if ($request->tahun) {

                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => null,
                                    'id_kel' => 0,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $bmd->id,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    bmdtransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }

                // Cek type_user bmd Kecamatan
                elseif ($bmd->type_user === 1) {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    // foreach ($tahun as $thn) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $bmd->id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    // }

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();

                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                            // jika user type kelurahan insert bmd trans untuk sum kecamatan
                            if ($request->type_user === '2') {
                                $kecamatan = kecamatanModel::all();
                                foreach ($kecamatan as $kec) {
                                    foreach ($tahun as $thn) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_bmd' => $parent,
                                            'id_bmd_child' => $bmd->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        bmdtransModel::create($data_sum_kec);
                                    }
                                }
                            }
                        }
                    }
                }

                // Cek type_user bmd Kelurahan
                else {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $bmd->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $bmd->id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                        $kecamatan = kecamatanModel::all();
                                        foreach ($kecamatan as $kec) {
                                            $data_sum_kec = array(
                                                'user_id' => $user->id,
                                                'id_bmd' => $parent,
                                                'id_bmd_child' => $bmd->id,
                                                'id_kec' => $kec->id,
                                                'id_kel' => null,
                                                'id_tahun' => $array_edit[$i],
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => '',
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_sum_kec);
                                        }
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                    bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    // ===========================
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                        $kecamatan = kecamatanModel::all();
                                        foreach ($kecamatan as $kec) {
                                            $data_sum_kec = array(
                                                'user_id' => $user->id,
                                                'id_bmd' => $parent,
                                                'id_bmd_child' => $bmd->id,
                                                'id_kec' => $kec->id,
                                                'id_kel' => null,
                                                'id_tahun' => $del,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => '',
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_sum_kec);
                                        }
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    }
                }

                bmdchildModel::whereid($id)->update($form_data);

                return redirect('/master-bmd')->with('success', 'Data Master Data Dukung berhasil diubah!');
            } else {
                $bmd = bmdchildModel::create($form_data);
                $tahun = $request->tahun_id;
                if ($request->tahun_id) {
                    if (count($tahun) > 0) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_bmd' => $parent,
                                'id_bmd_child' => $bmd->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => 1,
                                'status' => 'aktif'
                            );

                            bmdtransModel::create($data);
                        }
                        // jika user type kelurahan insert bmd trans untuk sum kecamatan
                        if ($request->type_user === '2') {
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $bmd->id,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    bmdtransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }


                return redirect('/master-bmd')->with('success', 'Data Master bmd berhasil ditambah!');
            }
        } else {
            $form_data = array(
                'bmd_description' => $request->uraian,
                'satuan_id' => $request->satuan,
                'value' => 0,
                'type_user' => $request->type_user,
                //'parent' => $parent,
                'validate' => 0,
                'validate_final' => 0
            );

            if ($request->type_user === '0') {
                // insert data untuk val kabupaten untuk user type Perangkat Daerah
                $id_kec = null;
                $id_kel = null;
            } elseif ($request->type_user === '1') {
                // insert data untuk val kabupaten untuk user type Kecamatan
                $id_kec = 0;
                $id_kel = null;
            } else {
                // insert data untuk val kabupaten untuk user type Kelurahan
                $id_kec = null;
                $id_kel = 0;
            }

            if ($id) {
                $bmd = bmdModel::find($id);
                $tahun = $request->tahun;
                // Cek type_user bmd Perangkat Daerah
                if ($bmd->type_user === 0) {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        $check_bmdtrans_tahun->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    } elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();

                        if ($request->tahun) {

                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => null,
                                    'id_kel' => 0,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $bmd->id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    bmdtransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }

                // Cek type_user bmd Kecamatan
                elseif ($bmd->type_user === 1) {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', 0)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    // foreach ($tahun as $thn) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $bmd->id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    // }

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $id)->where('id_kec', $id_kec)->where('id_bmd_child', 0)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', 0)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();

                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                            // jika user type kelurahan insert bmd trans untuk sum kecamatan
                            if ($request->type_user === '2') {
                                $kecamatan = kecamatanModel::all();
                                foreach ($kecamatan as $kec) {
                                    foreach ($tahun as $thn) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_bmd' => $bmd->id,
                                            'id_bmd_child' => 0,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        bmdtransModel::create($data_sum_kec);
                                    }
                                }
                            }
                        }
                    }
                }

                // Cek type_user bmd Kelurahan
                else {
                    // Cek Request type_user Perangkat Daerah
                    if ($request->type_user === '0') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', null)->where('id_kel', 0)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } elseif ($request->type_user === '1') {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', null)->where('id_kel', 0)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                        if ($request->tahun) {
                            foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_bmd' => $bmd->id,
                                    'id_bmd_child' => 0,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                bmdtransModel::create($data);
                            }
                        }
                    } else {
                        $bmd_trans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                        if (count($bmd_trans) > 0) {
                            foreach ($bmd_trans as $value) {
                                $array_tahun[] = $value->id_tahun;
                            }
                        } else {
                            $array_tahun = array();
                        }
                        if ($request->tahun) {
                            if (count($tahun) >= count($array_tahun)) {
                                $array_same = array();
                                $array_edit = array();
                                for ($y = 0; $y < count($tahun); $y++) {

                                    if (in_array($tahun[$y], $array_tahun) == true) {
                                        $array_same[] = $tahun[$y];
                                    } else {
                                        $array_edit[] = $tahun[$y];
                                    }
                                }
                                $array_delete = array_diff($array_tahun, $array_same);
                                // Add Tahun Berbeda bmd Trans
                                for ($i = 0; $i < count($array_edit); $i++) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $bmd->id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $array_edit[$i],
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                    if (count($check_bmdtrans) > 0) {
                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => '1']);
                                    } else {
                                        bmdtransModel::create($data);
                                        $kecamatan = kecamatanModel::all();
                                        foreach ($kecamatan as $kec) {
                                            $data_sum_kec = array(
                                                'user_id' => $user->id,
                                                'id_bmd' => $bmd->id,
                                                'id_bmd_child' => 0,
                                                'id_kec' => $kec->id,
                                                'id_kel' => null,
                                                'id_tahun' => $array_edit[$i],
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => '',
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_sum_kec);
                                        }
                                    }
                                }
                                foreach ($array_delete as $key => $del) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                    bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                                }
                            } else {
                                $array_same = array();
                                $array_delete = array();
                                for ($y = 0; $y < count($array_tahun); $y++) {

                                    if (in_array($array_tahun[$y], $tahun) == true) {
                                        $array_same[] = $array_tahun[$y];
                                    } else {
                                        $array_delete[] = $array_tahun[$y];
                                    }
                                }
                                $array_edit = array_diff($tahun, $array_same);
                                // echo dd($array_delete);
                                foreach ($array_edit as $key => $del) {
                                    $data = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $id_kec,
                                        'id_kel' => $id_kel,
                                        'id_tahun' => $del,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                    if (count($check_bmdtrans_tahun) > 0) {
                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));

                                        bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => '1'));
                                    } else {
                                        bmdtransModel::create($data);
                                        $kecamatan = kecamatanModel::all();
                                        foreach ($kecamatan as $kec) {
                                            $data_sum_kec = array(
                                                'user_id' => $user->id,
                                                'id_bmd' => $bmd->id,
                                                'id_bmd_child' => 0,
                                                'id_kec' => $kec->id,
                                                'id_kel' => null,
                                                'id_tahun' => $del,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => '',
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );

                                            bmdtransModel::create($data_sum_kec);
                                        }
                                    }
                                }
                                for ($i = 0; $i < count($array_delete); $i++) {
                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                    $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                                }
                            }
                        } else {

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                            $check_bmdtrans_tahun = bmdtransModel::where('id_bmd', $id)->where('id_bmd_child', 0)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                        }
                    }
                }

                bmdModel::whereid($id)->update($form_data);

                return redirect('/bmd/kategori')->with('success', 'Data Master Data Dukung berhasil diubah!');
            } else {
                $bmd = bmdModel::create($form_data);
                $tahun = $request->tahun_id;
                if ($request->tahun_id) {
                    if (count($tahun) > 0) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_bmd' => $bmd->id,
                                'id_bmd_child' => 0,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => 1,
                                'status' => 'aktif'
                            );

                            bmdtransModel::create($data);
                        }
                        // jika user type kelurahan insert bmd trans untuk sum kecamatan
                        if ($request->type_user === '2') {
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_bmd' => $bmd->id,
                                        'id_bmd_child' => 0,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    bmdtransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }


                return redirect('/bmd/kategori')->with('success', 'Data Master bmd berhasil ditambah!');
            }
        }


        return back()->with('success', 'Data Master Data Dukung berhasil disimpan!');
    }

    public function edit(Request $request)
    {
        if (intval($request->get('child')) === 0) {
            $bmd = bmdModel::find($request->get('id'));
        } else {
            $bmd = bmdchildModel::find($request->get('child'));
        }
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();

        $tahun = tahunModel::whereBetween('id', [$dari->id, $sampai->id])->get();

        foreach ($tahun as $thn) {
            // cek pegampu
            if ($request->get('child') === 0) {
                $id_parent = $request->get('id');
                $id_child = 0;
            } else {
                $id_parent = $request->get('id');
                $id_child = $request->get('child');
            }

            if ($bmd->type_user === 0) {
                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_tahun', $thn->id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
            } elseif ($bmd->type_user === 1) {
                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            } else {
                $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            }


            if (count((array)$bmdtrans) > 0) {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => $bmdtrans->value);
            } else {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => '');
            }
        }

        return view('bmd.bmd_edit', ['id_parent' => $request->get('id'), 'id_child' => $request->get('child'), 'id_kec' => $request->get('kec'), 'bmd' => $bmd, 'input' => $input, 'dari' => $dari->id, 'id_kel' => $request->get('kel'), 'sampai' => $sampai->id]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $id_kec = $request->id_kec;

        $id_tahun = $request->id_tahun;
        $value = $request->val;



        if (intval($request->id_child) === 0) {
            $parent = $request->id;
            $child = 0;
            // cek type_user atau pengampu bmd
            $bmd = bmdModel::find($request->id);
        } else {
            $parent = $request->id;
            $child = $request->id_child;
            $bmd = bmdchildModel::find($request->id_child);
        }
        // jika pengampu Perangkat Daerah
        if ($bmd->type_user === 0) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $bmdtrans = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel bmdtrans
                    if (count((array)$bmdtrans) > 0) {
                        if ($bmdtrans->value === $value[$index]) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update bmdtrans jika value input berbeda
                            $bmdtrans->status = 'non-aktif';
                            $bmdtrans->save();
                            bmdtransModel::create($form_data);

                            $bmd = bmdModel::find($request->id);
                            $bmd->validate = 0;
                            $bmd->validate_final = 0;
                            $bmd->save();
                        }
                    }
                } else {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $bmdtrans = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel bmdtrans
                    if (count((array)$bmdtrans) > 0) {
                        $cek_val = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update bmdtrans jika value input berbeda
                            $bmdtrans->status = 'non-aktif';
                            $bmdtrans->save();
                            bmdtransModel::create($form_data);
                        }
                    }
                }
            }
        }
        // jika pengampu Kecamatan
        elseif ($bmd->type_user === 1) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $bmdtrans_kab = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', 0)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel bmdtrans
                    if (count((array)$bmdtrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $bmdtrans_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$bmdtrans_kec) > 0) {
                            // echo $bmdtrans_kec->value;
                            if ($bmdtrans_kec->value === $value[$index]) {
                                // Untuk value yg sama tidak diberi aksi update
                            } else {
                                // Update bmdtrans jika value input berbeda
                                $bmdtrans_kec->status = 'non-aktif';
                                $bmdtrans_kec->save();
                                bmdtransModel::create($form_data);

                                // jika data kabupaten ada maka update status + create
                                $sum = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                if ($bmdtrans_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $bmdtrans_kab->status = 'non-aktif';
                                    $bmdtrans_kab->save();

                                    $data_kab = array(
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $child,
                                        'id_kec' => 0,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => 1,
                                        'status' => 'aktif'
                                    );
                                    bmdtransModel::create($data_kab);
                                    if ($child === 0) {
                                        $bmd = bmdModel::find($parent);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    } else {
                                        $bmd = bmdchildModel::find($child);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }
                                }
                                // }

                                if ($child === 0) {
                                    $bmd = bmdModel::find($parent);
                                    $bmd->validate = 0;
                                    $bmd->validate_final = 0;
                                    $bmd->save();
                                } else {
                                    $bmd = bmdchildModel::find($child);
                                    $bmd->validate = 0;
                                    $bmd->validate_final = 0;
                                    $bmd->save();
                                }
                            }
                            // echo dd($bmdtrans_kec);
                        } else {
                            bmdtransModel::create($form_data);

                            $sum = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                            if ($bmdtrans_kab->value === $sum) {
                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                            } else {
                                $bmdtrans_kab->status = 'non-aktif';
                                $bmdtrans_kab->save();

                                $data_kab = array(
                                    'id_bmd' => $parent,
                                    'id_bmd_child' => $child,
                                    'id_kec' => 0,
                                    'id_kel' => null,
                                    'user_id' => $user->id,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => $sum,
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );
                                bmdtransModel::create($data_kab);

                                if ($child === 0) {
                                    $bmd = bmdModel::find($parent);
                                    $bmd->validate = 0;
                                    $bmd->validate_final = 0;
                                    $bmd->save();
                                } else {
                                    $bmd = bmdchildModel::find($child);
                                    $bmd->validate = 0;
                                    $bmd->validate_final = 0;
                                    $bmd->save();
                                }
                            }
                        }
                    }
                } else {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $bmdtrans = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', $id_kec)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel bmdtrans
                    if (count((array)$bmdtrans) > 0) {
                        $cek_val = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update bmdtrans jika value input berbeda
                            $bmdtrans->status = 'non-aktif';
                            $bmdtrans->save();
                            bmdtransModel::create($form_data);

                            // cek bmd trans kabupaten
                            $cek_kab = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', '0')->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kab) > 0) {
                                // jika data kabupaten ada maka update status + create
                                $sum = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                if ($cek_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kab->status = 'non-aktif';
                                    $cek_kab->save();

                                    $data_kab = array(
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $child,
                                        'id_kec' => 0,
                                        'id_kel' => 0,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    bmdtransModel::create($data_kab);

                                    if ($child === 0) {
                                        $bmd = bmdModel::find($parent);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    } else {
                                        $bmd = bmdchildModel::find($child);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }
                                }
                            }
                            // else {
                            //     // buat baru
                            //     $sum = bmdtransModel::where('id_bmd', $id)->where('id_tahun', $thn)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                            //     $data_kab = array(
                            //         'id_bmd' => $id,
                            //         'id_kec' => 0,
                            //         'id_kel' => 0,
                            //         'user_id' => $user->id,
                            //         'id_tahun' => $thn,
                            //         'tgl_pengisian' => date('Y-m-d H:i:s'),
                            //         'value' => $sum,
                            //         'status' => 'aktif'
                            //     );

                            //     bmdtransModel::create($data_kab);

                            //     $bmd = bmdModel::find($id);
                            //     $bmd->validate = 0;
                            //     $bmd->validate_final = 0;
                            //     $bmd->save();
                            // }
                        }
                    }
                }
            }
        }
        // jika pengampu Kelurahan
        else {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $bmdtrans_kab = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel bmdtrans
                    if (count((array)$bmdtrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $bmdtrans_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$bmdtrans_kec) > 0) {
                            // cek data kel per id_kel
                            $bmdtrans_kel = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$bmdtrans_kel) > 0) {
                                // echo $bmdtrans_kel->value;
                                if ($bmdtrans_kel->value === $value[$index]) {
                                    // jika value sama maka tidak ada aksi
                                } else {
                                    // Update bmdtrans jika value input berbeda
                                    $bmdtrans_kel->status = 'non-aktif';
                                    $bmdtrans_kel->save();
                                    bmdtransModel::create($form_data);
                                    //=======================================================================
                                    $sum_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                    if ($bmdtrans_kec->value === $sum_kec) {
                                        // jika sama tidak ada update status+create

                                    } else {
                                        $bmdtrans_kec->status = 'non-aktif';
                                        $bmdtrans_kec->save();

                                        $data_kec = array(
                                            'id_bmd' => $parent,
                                            'id_bmd_child' => $child,
                                            'id_kec' => $id_kec,
                                            'id_kel' => null,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kec,
                                            'check_active' => 1,
                                            'status' => 'aktif'
                                        );
                                        bmdtransModel::create($data_kec);

                                        if ($child === 0) {
                                            $bmd = bmdModel::find($parent);
                                            $bmd->validate = 0;
                                            $bmd->validate_final = 0;
                                            $bmd->save();
                                        } else {
                                            $bmd = bmdchildModel::find($child);
                                            $bmd->validate = 0;
                                            $bmd->validate_final = 0;
                                            $bmd->save();
                                        }

                                        // sum data kelurahan
                                        $sum_kab = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                        if ($bmdtrans_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $bmdtrans_kab->status = 'non-aktif';
                                            $bmdtrans_kab->save();
                                            bmdtransModel::create($form_data);

                                            $data_kab = array(
                                                'id_bmd' => $parent,
                                                'id_bmd_child' => $child,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => 1,
                                                'status' => 'aktif'
                                            );
                                            bmdtransModel::create($data_kab);

                                            if ($child === 0) {
                                                $bmd = bmdModel::find($parent);
                                                $bmd->validate = 0;
                                                $bmd->validate_final = 0;
                                                $bmd->save();
                                            } else {
                                                $bmd = bmdchildModel::find($child);
                                                $bmd->validate = 0;
                                                $bmd->validate_final = 0;
                                                $bmd->save();
                                            }
                                        }
                                    }

                                    // }

                                    if ($child === 0) {
                                        $bmd = bmdModel::find($parent);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    } else {
                                        $bmd = bmdchildModel::find($child);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }
                                }
                            } else {
                                // create data kelurahan
                                bmdtransModel::create($form_data);

                                $sum_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                if ($bmdtrans_kec->value === $sum_kec) {
                                } else {
                                    $bmdtrans_kec->status = "non-aktif";
                                    $bmdtrans_kec->save();

                                    $data_kec = array(
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $child,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    bmdtransModel::create($data_kec);

                                    if ($child === 0) {
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    } else {
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }

                                    $sum_kab = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');

                                    if ($bmdtrans_kab->value === $sum_kab) {
                                    } else {
                                        $bmdtrans_kab->status = "non-aktif";
                                        $bmdtrans_kab->save();

                                        $data_kab = array(
                                            'id_bmd' => $parent,
                                            'id_bmd_child' => $child,
                                            'id_kec' => null,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kab,
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );
                                        bmdtransModel::create($data_kab);

                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }
                                }
                            }
                            // echo dd($bmdtrans_kec);
                        }
                    }
                } else {
                    $form_data = array(
                        'id_bmd' => $parent,
                        'id_bmd_child' => $child,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $bmdtrans = bmdtransModel::where('id_tahun', $thn)->where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel bmdtrans
                    if (count((array)$bmdtrans) > 0) {
                        if ($bmdtrans->value === '') {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update bmdtrans jika value input berbeda
                            $bmdtrans->status = 'non-aktif';
                            $bmdtrans->save();
                            bmdtransModel::create($form_data);

                            // cek bmdtrans kecamatan
                            $cek_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', $id_kec)->where('id_kel', null)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kec) > 0) {
                                // jika data kecamatan ada maka update status + create
                                $sum_kec = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');
                                if ($cek_kec->value === $sum_kec) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kec->status = 'non-aktif';
                                    $cek_kec->save();

                                    $data_kec = array(
                                        'id_bmd' => $parent,
                                        'id_bmd_child' => $child,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    bmdtransModel::create($data_kec);

                                    if ($child === 0) {
                                        $bmd = bmdModel::find($parent);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    } else {
                                        $bmd = bmdchildModel::find($child);
                                        $bmd->validate = 0;
                                        $bmd->validate_final = 0;
                                        $bmd->save();
                                    }

                                    // cek bmd trans kabupaten
                                    $cek_kab = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum_kab = bmdtransModel::where('id_bmd', $parent)->where('id_bmd_child', $child)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->where('id_kel', null)->sum('value');
                                        if ($cek_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_bmd' => $parent,
                                                'id_bmd_child' => $child,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );
                                            bmdtransModel::create($data_kab);

                                            if ($child === 0) {
                                                $bmd = bmdModel::find($parent);
                                                $bmd->validate = 0;
                                                $bmd->validate_final = 0;
                                                $bmd->save();
                                            } else {
                                                $bmd = bmdchildModel::find($child);
                                                $bmd->validate = 0;
                                                $bmd->validate_final = 0;
                                                $bmd->save();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return back()->with('success', 'Data bmd berhasil disimpan!');
    }

    public function history(Request $request)
    {
        $bmd = bmdModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();
        $bmdtrans = bmdtransModel::where('id_bmd', $request->get('id'))->where('id_kec', $request->get('kec'))->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();

        return view('bmd.bmd_history', ['bmd' => $bmd, 'bmdtrans' => $bmdtrans]);
    }

    public function delete_bmd($id)
    {
        $bmd = bmdchildModel::find($id);
        bmdtransModel::where('id_bmd', $bmd->id_bmd)->where('id_bmd_child', $id)->delete();
        bmdchildModel::whereId($id)->delete();

        return back()->with('success', 'Data Master Data Dukung berhasil dihapus!');
    }

    public function validasi($id)
    {
        $bmd = bmdModel::find($id);
        $bmd->validate = 1;
        $bmd->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_child($id)
    {
        $bmd = bmdchildModel::find($id);
        $bmd->validate = 1;
        $bmd->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_all()
    {
        bmdModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));
        bmdchildModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));
        return back()->with('success', 'Data berhasil divalidasi.');
    }

    public function import_bmd(Request $request)
    {
        $id_kec = $request->kec;
        $nama_file = rand() . $request->file('uraian_import')->getCLientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new bmdImport($id_kec), storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);

        return back()->with('success', 'File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/bmd.xlsx');
    }

    public function excel(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data1 = bmdModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data1 = bmdModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data1 = bmdModel::where('type_user', 1)->get();
            } else {
                $data1 = bmdModel::where('type_user', 2)->get();
            }
        }

        foreach ($data1 as $dd) {
            $data[] = array('id' => $dd->id, 'bmd_description' => $dd->bmd_description, 'space' => 0, 'satuan_id' => $dd->satuan_id, 'value' => $dd->value, 'type_user' => $dd->type_user, 'validate' => $dd->validate, 'validate_final' => $dd->validate_final, 'created_at' => $dd->creatd_at, 'updated_at' => $dd->updated_at, 'master' => $dd->id);
            if (count($dd->child)) {
                $data = parent_bmd($dd->child, $data, 0, $dd->id);
            }
        }
        if (count($data1) > 0) {
        } else {
            $data = array();
        }

        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row['satuan_id']);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row['space'] === 0) {
                        $id_parent = $row['id'];
                        $id_child = 0;
                    } else {
                        $id_parent = $row['master'];
                        $id_child = $row['id'];
                    }
                    if ($row['type_user'] === 0) {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row['type_user'] === 1) {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$bmdtrans) > 0) {
                        $val = str_replace(".", "", $bmdtrans->value);
                        if (is_numeric($val)) {
                            if (strpos($bmdtrans->value, ".") === true) {
                                $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                $value = number_format($bmdtrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($bmdtrans->value, 0, '', '.');
                            }
                        } else if ($bmdtrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $bmdtrans->value;
                        }
                    } else {
                        $value = '-';
                    }

                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row['bmd_description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                $bmd[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row['satuan_id']);

                if ($request->kel) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;

                        if ($row['space'] === 0) {
                            $id_parent = $row['id'];
                            $id_child = 0;
                        } else {
                            $id_parent = $row['master'];
                            $id_child = $row['id'];
                        }
                        if ($row['type_user'] === 0) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row['type_user'] === 1) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$bmdtrans) > 0) {
                            $val = str_replace(".", "", $bmdtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($bmdtrans->value, ".") === true) {
                                    $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                    $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($bmdtrans->value, 0, '', '.');
                                }
                            } else if ($bmdtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $bmdtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row['description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                    $bmd[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row['space'] === 0) {
                            $id_parent = $row['id'];
                            $id_child = 0;
                        } else {
                            $id_parent = $row['master'];
                            $id_child = $row['id'];
                        }
                        if ($row['type_user'] === 0) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row['type_user'] === 1) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$bmdtrans) > 0) {
                            $val = str_replace(".", "", $bmdtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($bmdtrans->value, ".") === true) {
                                    $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                    $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($bmdtrans->value, 0, '', '.');
                                }
                            } else if ($bmdtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $bmdtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row['bmd_description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                    $bmd[] = $array_data + $tahun;
                }
            }
        }

        $data_bmd = array('bmd' => $bmd, 'tahun' => $thn);

        return Excel::download(new BMDExport($data_bmd), 'BMD.xlsx');
    }

    public function pdf(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data1 = bmdModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data1 = bmdModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data1 = bmdModel::where('type_user', 1)->get();
            } else {
                $data1 = bmdModel::where('type_user', 2)->get();
            }
        }

        foreach ($data1 as $dd) {
            $data[] = array('id' => $dd->id, 'bmd_description' => $dd->bmd_description, 'space' => 0, 'satuan_id' => $dd->satuan_id, 'value' => $dd->value, 'type_user' => $dd->type_user, 'validate' => $dd->validate, 'validate_final' => $dd->validate_final, 'created_at' => $dd->creatd_at, 'updated_at' => $dd->updated_at, 'master' => $dd->id);
            if (count($dd->child)) {
                $data = parent_bmd($dd->child, $data, 0, $dd->id);
            }
        }
        if (count($data1) > 0) {
        } else {
            $data = array();
        }

        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row['satuan_id']);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row['space'] === 0) {
                        $id_parent = $row['id'];
                        $id_child = 0;
                    } else {
                        $id_parent = $row['master'];
                        $id_child = $row['id'];
                    }
                    if ($row['type_user'] === 0) {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row['type_user'] === 1) {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$bmdtrans) > 0) {
                        $val = str_replace(".", "", $bmdtrans->value);
                        if (is_numeric($val)) {
                            if (strpos($bmdtrans->value, ".") === true) {
                                $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                $value = number_format($bmdtrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($bmdtrans->value, 0, '', '.');
                            }
                        } else if ($bmdtrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $bmdtrans->value;
                        }
                    } else {
                        $value = '-';
                    }

                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row['bmd_description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                $bmd[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row['satuan_id']);

                if ($request->kel) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;

                        if ($row['space'] === 0) {
                            $id_parent = $row['id'];
                            $id_child = 0;
                        } else {
                            $id_parent = $row['master'];
                            $id_child = $row['id'];
                        }
                        if ($row['type_user'] === 0) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row['type_user'] === 1) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$bmdtrans) > 0) {
                            $val = str_replace(".", "", $bmdtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($bmdtrans->value, ".") === true) {
                                    $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                    $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($bmdtrans->value, 0, '', '.');
                                }
                            } else if ($bmdtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $bmdtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row['description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                    $bmd[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row['space'] === 0) {
                            $id_parent = $row['id'];
                            $id_child = 0;
                        } else {
                            $id_parent = $row['master'];
                            $id_child = $row['id'];
                        }
                        if ($row['type_user'] === 0) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row['type_user'] === 1) {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $bmdtrans = bmdtransModel::where('id_bmd', $id_parent)->where('id_bmd_child', $id_child)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$bmdtrans) > 0) {
                            $val = str_replace(".", "", $bmdtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($bmdtrans->value, ".") === true) {
                                    $decimal = strlen(substr($bmdtrans->value, strrpos($bmdtrans->value, '.') + 1));
                                    $value = number_format($bmdtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($bmdtrans->value, 0, '', '.');
                                }
                            } else if ($bmdtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $bmdtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row['bmd_description'], 'satuan' => $unit->unit, 'space' => $row['space']);
                    $bmd[] = $array_data + $tahun;
                }
            }
        }

        $data_bmd = array('bmd' => $bmd, 'tahun' => $thn);

        view()->share('data', $data_bmd);
        $pdf = PDF::loadView('exports.bmd_export', $data_bmd);

        return $pdf->stream('BMD.pdf');
    }

    public function excel_master()
    {
        return Excel::download(new MasterBMDExport, 'MasterBMD.xlsx');
    }

    public function pdf_master()
    {
        $bmd_data = bmdModel::all();
        foreach ($bmd_data as $dd) {
            $data[] = array('id' => $dd->id, 'bmd_description' => $dd->bmd_description, 'space' => 0, 'satuan_id' => $dd->satuan_id, 'value' => $dd->value, 'type_user' => $dd->type_user, 'validate' => $dd->validate, 'validate_final' => $dd->validate_final, 'created_at' => $dd->creatd_at, 'updated_at' => $dd->updated_at, 'master' => $dd->id);
            if (count($dd->child)) {
                $data = parent_bmd($dd->child, $data, 0, $dd->id);
            }
        }
        if (count($bmd_data) > 0) {
            foreach ($data as $row) {
                if ($row['satuan_id']) {
                    if ($row['satuan_id'] > 0) {
                        $units = \App\unitsModel::find($row['satuan_id']);
                        $unit = $units->unit;
                    } else {
                        $unit = '';
                    }
                } else {
                    $unit = '';
                }
                $bmd[] = array('id' => $row['id'], 'bmd_description' => $row['bmd_description'], 'space' => $row['space'], 'satuan' => $unit, 'value' => $row['value'], 'type_user' => $row['type_user'], 'validate' => $row['validate'], 'validate_final' => $row['validate_final'], 'created_at' => $row['created_at'], 'updated_at' => $row['updated_at'], 'master' => $row['master']);
            }
        } else {
            $bmd = array();
        }

        view()->share('bmd', $bmd);
        $pdf = PDF::loadView('master_export.bmd_export', $bmd);

        return $pdf->stream('MasterBMD.pdf');
    }

    public function master_bmd(Request $request)
    {
        // where('parent', 0)->get()
        $data_dukung = bmdModel::all();

        foreach ($data_dukung as $dd) {
            $data[] = array('id' => $dd->id, 'bmd_description' => $dd->bmd_description, 'space' => 0, 'satuan_id' => $dd->satuan_id, 'value' => $dd->value, 'type_user' => $dd->type_user, 'validate' => $dd->validate, 'validate_final' => $dd->validate_final, 'created_at' => $dd->creatd_at, 'updated_at' => $dd->updated_at, 'master' => $dd->id);
            if (count($dd->child)) {
                $data = parent_bmd($dd->child, $data, 0, $dd->id);
            }
        }
        if (count($data_dukung) > 0) {
        } else {
            $data = array();
        }

        if ($request->ajax()) {
            $data2 = bmdModel::latest()->get();
            $data2 = $data2->toArray();

            return Datatables::of($data)
                ->editColumn('data', function ($row) {
                    $space = '';
                    for ($i = 0; $i < $row['space']; $i++) {
                        $space = $space . '&nbsp;';
                    }
                    return $space . $row['bmd_description'];
                })
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row['satuan_id']);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    if ($row['space'] === 0) {
                        $btn = '';
                    } else {
                        $btn = ' <a href="' . url("change-master-bmd/" . $row['id']) . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                        $btn = $btn . ' <a href="#" class="hapus_bmd" data-url="' . url("delete-bmd/" . $row['id']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    }

                    return $btn;
                })
                ->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['bmd_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }

                    // if (!empty($request->get('master'))) {
                    //     $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                    //         if ($row['master'] == $request->get('master')) {

                    //             return true;
                    //         }

                    //         return false;
                    //     });
                    // }
                })
                ->addIndexColumn()
                ->rawColumns(['data', 'action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        $d = bmdModel::all();
        $d = $d->toArray();
        $master = bmdModel::all();
        return view('bmd.master_bmd', ['unit' => $unit, 'data' => $data, 'd' => $d, 'master' => $master]);
    }

    public function add_bmd()
    {
        $data = bmdModel::all();
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        return view('bmd.bmd_add', ['data' => $data, 'tahun' => $tahun, 'unit' => $unit]);
    }

    public function change_bmd($id)
    {
        $parent = bmdModel::all();
        $bmd = bmdchildModel::find($id);
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        if ($bmd->type_user === 0) {
            $bmd_tahun = bmdtransModel::where('id_bmd', $bmd->id_bmd)->where('id_bmd_child', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', null)->get();
        } elseif ($bmd->type_user === 1) {
            $bmd_tahun = bmdtransModel::where('id_bmd', $bmd->id_bmd)->where('id_bmd_child', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', 0)->where('id_kel', null)->get();
        } else {
            $bmd_tahun = bmdtransModel::where('id_bmd', $bmd->id_bmd)->where('id_bmd_child', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', 0)->get();
        }
        // echo dd($bmd_tahun);
        return view('bmd.master_bmd_edit', ['parent' => $parent, 'bmd' => $bmd, 'unit' => $unit, 'tahun' => $tahun, 'bmd_tahun' => $bmd_tahun]);
    }

    public function kategori(Request $request)
    {
        // where('parent', 0)->get()

        if ($request->ajax()) {
            $data = bmdModel::all();
            return Datatables::of($data)
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row['satuan_id']);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . url("change-kategori-bmd/" . $row['id']) . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_bmd" data-url="' . url("delete-bmd-kategori/" . $row['id']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['bmd_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        $d = bmdModel::all();
        $d = $d->toArray();
        $master = bmdModel::all();
        return view('bmd.kategori', ['unit' => $unit, 'd' => $d, 'master' => $master]);
    }

    public function kategori_add()
    {
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        return view('bmd.kategori_add', ['tahun' => $tahun, 'unit' => $unit]);
    }

    public function change_kategori($id)
    {
        $bmd = bmdModel::find($id);
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        if ($bmd->type_user === 0) {
            $bmd_tahun = bmdtransModel::where('id_bmd', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', null)->get();
        } elseif ($bmd->type_user === 1) {
            $bmd_tahun = bmdtransModel::where('id_bmd', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', 0)->where('id_kel', null)->get();
        } else {
            $bmd_tahun = bmdtransModel::where('id_bmd', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', 0)->get();
        }
        // echo dd($bmd_tahun);
        return view('bmd.kategori_edit', ['bmd' => $bmd, 'unit' => $unit, 'tahun' => $tahun, 'bmd_tahun' => $bmd_tahun]);
    }

    public function delete_kategori($id)
    {
        bmdtransModel::where('id_bmd', $id)->delete();
        bmdchildModel::where('id_bmd', $id)->delete();
        bmdModel::whereId($id)->delete();

        return back()->with('success', 'Data Kategori Data Dukung berhasil dihapus!');
    }
}
