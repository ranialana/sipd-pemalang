<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\edatabaseModel;
use App\businessModel;
use App\areaModel;
use App\subAreaModel;
use App\indicatorModel;
use App\subIndicatorModel;
use App\unitsModel;
use App\tahunModel;
use App\Exports\eDatabaseExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;
use Auth;

class eDatabaseController extends Controller
{
    public function index2(Request $request)
    {
        $data = edatabaseModel::all();
        $data = $data->groupBy(['business_id', 'area_id', 'sub_area_id', 'indicator_id']);
        // echo dd($data->toArray());
        $array_si = array();
        $data2 = subIndicatorModel::where('id', 4)->with('parent')->get();
        // $data2 = edb($data, auth()->user());
        // Get Data User
        $user = \App\userModel::find(auth()->user()->id);
        // Get SKPD
        $skpd_user = \App\skpdUser::where('user_id', auth()->user()->id)->get();

        if (count($skpd_user) > 0) {
            foreach ($skpd_user as $su) {
                $get_skpd = \App\skpdModel::find($su->skpd_id);

                $skpd[] = array('id' => $get_skpd->id, 'skpd_name' => $get_skpd->skpd_name, 'active' => $get_skpd->active);
            }
        } else {
            $skpd = 0;
        }

        // Get Kecamatan
        $kec = \App\kecamatanModel::where('id', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kec) > 0) {
            $kecamatan[] = array('id' => $kec->id, 'nama' => $kec->nama, 'status' => $kec->status);
        } else {
            $kecamatan = 0;
        }

        // Get Kelurahan
        $kel = \App\kelurahanModel::where('id', auth()->user()->id_kel)->where('id_kec', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kel) > 0) {
            $kelurahan[] = array('id' => $kel->id, 'id_kec' => $kel->id_kec, 'nama' => $kel->nama, 'status' => $kel->status);
        } else {
            $kelurahan = 0;
        }
        $data = edb($data, $user, $skpd, $kecamatan, $kelurahan);

        // echo dd($data);
        if ($data) {
            $data = $data;
        } else {
            $data = array();
        }
        // echo dd($data);
        // echo dd(edb($data, $user, $skpd, $kecamatan, $kelurahan));
        if ($request->ajax()) {
            $thn = tahunModel::all();

            // $data = edb($data, $user, $skpd, $kecamatan, $kelurahan);
            $table = Datatables::of($data);
            $table->addColumn('master_data', function ($row) use ($data) {
                $dd = '';
                foreach ($data as $d) {
                    $dd = $dd . $d['uraian'];
                }
                return $dd;
            });
            $table->editColumn('urusan', function ($row) use ($data) {
                $space = '';
                for ($i = 0; $i < $row['space']; $i++) {
                    $space = $space . '&nbsp;';
                }
                return $space . $row['number'] . '. ' . $row['uraian'] . ' - ' . $row['space'];
            });
            $table->addColumn('satuan', function ($row) {
                if ($row['satuan'] > 0) {
                    $units = unitsModel::find($row['satuan']);
                    $unit = $units->unit;
                } else {
                    $unit = '';
                }
                return $unit;
            });
            $table->addColumn('value', function ($row) {
                // $unit = unitsModel::find($row->unit_id);
                // return $unit->unit;
                return $row['value'];
            });
            $table->addColumn('action', function ($row) use ($request) {
                if ($row['action'] === 0) {
                    $btn = '';
                } else {
                    $btn = '';
                    if (auth()->user()->role === '0' || auth()->user()->role === '2') {
                        $btn = $btn . '<a href="' . url('change-edatabase/' . $row['id'] . '') . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    } else {
                        $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-edatabase/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                    }
                }

                return $btn;
            });
            $table->filter(function ($instance) use ($request) {

                if (!empty($request->get('search'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        if (Str::contains(Str::lower($row['uraian']), Str::lower($request->get('search')))) {

                            return true;
                        } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                            return true;
                        }


                        return false;
                    });
                }
                if (!empty($request->get('urusan'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        if ($row['master'] == $request->get('urusan')) {

                            return true;
                        }


                        return false;
                    });
                }
            });
            $table->rawColumns(['urusan', 'action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
        $edatabase = edatabaseModel::all();
        $array_business = array();
        // foreach ($edatabase as $db) {
        //     $result[$db->business_id] = null;
        // }
        // $value = array_keys($result);

        $urusan = businessModel::all();
        $area = areaModel::all();
        $sub_area = subAreaModel::all();
        $indicator = indicatorModel::all();
        $sub_indicator = subIndicatorModel::all();
        $thn = tahunModel::all();
        return view('edatabase.edatabase_vw', ['urusan' => $urusan, 'area' => $area, 'sub_area' => $sub_area, 'indicator' => $indicator, 'sub_indicator' => $sub_indicator, 'thn' => $thn, 'edatabase' => $edatabase]);
    }

    public function index(Request $request)
    {
        $getrole = edatabaseModel::all();
        $getrole = $getrole->groupBy(['business_id', 'area_id', 'sub_area_id', 'indicator_id']);

        // Get Data User
        $user = \App\userModel::find(auth()->user()->id);
        // Get SKPD
        $skpd_user = \App\skpdUser::where('user_id', auth()->user()->id)->get();

        if (count($skpd_user) > 0) {
            foreach ($skpd_user as $su) {
                $get_skpd = \App\skpdModel::find($su->skpd_id);

                $skpd[] = array('id' => $get_skpd->id, 'skpd_name' => $get_skpd->skpd_name, 'active' => $get_skpd->active);
            }
        } else {
            $skpd = 0;
        }

        // Get Kecamatan
        $kec = \App\kecamatanModel::where('id', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kec) > 0) {
            $kecamatan[] = array('id' => $kec->id, 'nama' => $kec->nama, 'status' => $kec->status);
        } else {
            $kecamatan = 0;
        }
        $kecamatan = single_array($kecamatan);

        // Get Kelurahan
        $kel = \App\kelurahanModel::where('id', auth()->user()->id_kel)->where('id_kec', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kel) > 0) {
            $kelurahan[] = array('id' => $kel->id, 'id_kec' => $kel->id_kec, 'nama' => $kel->nama, 'status' => $kel->status);
        } else {
            $kelurahan = 0;
        }
        $kelurahan = single_array($kelurahan);

        $data = edatabaseModel::all();

        $array_parent[] = array();
        $business_edb = array();
        $area_edb = array();
        $subarea_edb = array();
        $indicator_edb = array();
        $subindicator_edb = array();
        $subindicator_parent = array();
        foreach ($data as $edb) {
            if ($edb->business_id != null) {
                if (in_array($edb->business_id, $business_edb)) {
                    if ($edb->area_id != null) {
                        if (in_array($edb->area_id, $area_edb)) {
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $area_edb[] = $edb->area_id;
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubIndicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $business_edb[] = $edb->business_id;
                    if ($edb->area_id != null) {
                        if (in_array($edb->area_id, $area_edb)) {
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);

                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $area_edb[] = $edb->area_id;
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $urusan = businessModel::all();
        $array_business = array();

        $role_edb_all = role_edb($getrole, $user, $skpd, $kec, $kel);

        $role = array();

        // Get Role Business
        $role_urusan = $role_edb_all['data_business'];

        for ($ib = 0; $ib < count($business_edb); $ib++) {
            $get_role_business = array();
            for ($u = 0; $u < count($role_urusan); $u++) {
                if ($role_urusan[$u]['id'] === $business_edb[$ib]) {
                    for ($ur = 0; $ur < count($role_urusan[$u]['role']); $ur++) {
                        if (in_array($role_urusan[$u]['role'][$ur], $get_role_business)) {
                        } else {
                            $get_role_business[] = $role_urusan[$u]['role'][$ur];
                        }
                    }
                }
            }
            $data_business[$business_edb[$ib]] = $get_role_business;
        }
        $role['data_business'] = $data_business;

        // Get Role Area
        $role_bidang = $role_edb_all['data_area'];

        for ($ia = 0; $ia < count($area_edb); $ia++) {
            $get_role_area = array();
            for ($b = 0; $b < count($role_bidang); $b++) {
                if ($role_bidang[$b]['id'] === $area_edb[$ia]) {
                    for ($br = 0; $br < count($role_bidang[$b]['role']); $br++) {
                        if (in_array($role_bidang[$b]['role'][$br], $get_role_area)) {
                        } else {
                            $get_role_area[] = $role_bidang[$b]['role'][$br];
                        }
                    }
                }
            }
            $data_area[$area_edb[$ia]] = $get_role_area;
        }
        $role['data_area'] = $data_area;

        // Get Role SubArea
        $role_subbidang = $role_edb_all['data_subarea'];

        for ($isa = 0; $isa < count($subarea_edb); $isa++) {
            $get_role_subarea = array();
            for ($sb = 0; $sb < count($role_subbidang); $sb++) {
                if ($role_subbidang[$sb]['id'] === $subarea_edb[$isa]) {
                    for ($sbr = 0; $sbr < count($role_subbidang[$sb]['role']); $sbr++) {
                        if (in_array($role_subbidang[$sb]['role'][$sbr], $get_role_subarea)) {
                        } else {
                            $get_role_subarea[] = $role_subbidang[$sb]['role'][$sbr];
                        }
                    }
                }
            }
            $data_subarea[$subarea_edb[$isa]] = $get_role_subarea;
        }
        $role['data_subarea'] = $data_subarea;

        // Get Role Indicator
        $role_indikator = $role_edb_all['data_indicator'];

        for ($ii = 0; $ii < count($indicator_edb); $ii++) {
            $get_role_indicator = array();
            for ($sb = 0; $sb < count($role_indikator); $sb++) {
                if ($role_indikator[$sb]['id'] === $indicator_edb[$ii]) {
                    for ($sbr = 0; $sbr < count($role_indikator[$sb]['role']); $sbr++) {
                        if (in_array($role_indikator[$sb]['role'][$sbr], $get_role_indicator)) {
                        } else {
                            $get_role_indicator[] = $role_indikator[$sb]['role'][$sbr];
                        }
                    }
                }
            }
            $data_indicator[$indicator_edb[$ii]] = $get_role_indicator;
        }
        $role['data_indicator'] = $data_indicator;

        $role['data_si'] = $role_edb_all['data_si'];

        $parent_si = array();
        for ($si_parent = 0; $si_parent < count($subindicator_parent); $si_parent++) {
            if (in_array($subindicator_parent[$si_parent], $subindicator_edb)) {
            } else {
                if (in_array($subindicator_parent[$si_parent], $parent_si)) {
                } else {
                    $parent_si[] = $subindicator_parent[$si_parent];
                }
            }
        }


        for ($p_si = 0; $p_si < count($parent_si); $p_si++) {
            $sub_parent = subIndicatorModel::find($parent_si[$p_si]);

            if (count($sub_parent->child) > 0) {
                $role_parent = array();
                $child_parent = array();
                $get_role_parent = parent_si_role_edb($sub_parent->child, $parent_si, $subindicator_edb, $role_parent);

                $data_parent[$parent_si[$p_si]] = $get_role_parent;
            } else {
                $data_parent[$parent_si[$p_si]] = array($sub_parent->role);
            }
        }
        if ($user->role === '0') {
            foreach ($urusan as $u => $business) {
                if (in_array($business->id, $business_edb)) {
                    $array_business[] = $business->id;
                    $business_role = single_array($role['data_business'][$business->id]);

                    if (count($business->getArea) > 0) {

                        $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        $array_area = array();
                        foreach ($business->getArea as $a => $area) {

                            if (in_array($area->id, $area_edb)) {
                                $array_area[] = $area->id;
                                $area_role = single_array($role['data_area'][$area->id]);

                                if (count($area->getSubArea) > 0) {

                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);

                                    $array_subarea = array();
                                    foreach ($area->getSubArea as $sa => $subarea) {

                                        if (in_array($subarea->id, $subarea_edb)) {
                                            $array_subarea[] = $subarea->id;
                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                            if (count($subarea->getIndicator) > 0) {

                                                $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);

                                                $array_indicator = array();
                                                foreach ($subarea->getIndicator as $in => $indicator) {

                                                    if (in_array($indicator->id, $indicator_edb)) {
                                                        $array_indicator[] = $indicator->id;
                                                        $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                        if (count($indicator->getSubIndicator) > 0) {

                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);

                                                            $array_si = array();
                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {

                                                                if ($subindicator->parent_id === null) {
                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                        $value = $edb->value;
                                                                        $satuan = $edb->unit_id;
                                                                        $action = 1;
                                                                        $array_si[] = $subindicator->id;
                                                                        $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                    } else {

                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                            $array_si[] = $subindicator->id;
                                                                            $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                            if (count($subindicator->child) > 0) {
                                                                                $data_edb = parent_si_edb($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si']);
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                }
                                                            }
                                                        } else {
                                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                        }
                                                    }
                                                }
                                            } else {
                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                            }
                                        }
                                    }
                                } else {
                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                }
                            }
                        }
                    } else {
                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                        $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                    }
                } else {
                }
            }
        } else {
            if ($user->type_user === 0) {
                $skpd_name = array();
                if (is_array($skpd)) {
                    foreach ($skpd as $sk) {
                        $skpd_name[] = $sk['skpd_name'];
                    }
                } else {
                    $skpd_name = 0;
                }

                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {
                        $array_business[] = $business->id;
                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            for ($r_business = 0; $r_business < count($business_role); $r_business++) {
                                if (in_array($business_role[$r_business], $skpd_name)) {
                                    $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                    $array_area = array();

                                    break;
                                }
                            }

                            foreach ($business->getArea as $a => $area) {

                                if (in_array($area->id, $area_edb)) {
                                    $array_area[] = $area->id;
                                    $area_role = single_array($role['data_area'][$area->id]);

                                    if (count($area->getSubArea) > 0) {
                                        for ($r_area = 0; $r_area < count($area_role); $r_area++) {
                                            if (in_array($area_role[$r_area], $skpd_name)) {
                                                $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);

                                                $array_subarea = array();
                                                break;
                                            }
                                        }

                                        foreach ($area->getSubArea as $sa => $subarea) {

                                            if (in_array($subarea->id, $subarea_edb)) {
                                                $array_subarea[] = $subarea->id;
                                                $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                if (count($subarea->getIndicator) > 0) {
                                                    for ($r_subarea = 0; $r_subarea < count($subarea_role); $r_subarea++) {
                                                        if (in_array($subarea_role[$r_subarea], $skpd_name)) {
                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);

                                                            $array_indicator = array();
                                                            break;
                                                        }
                                                    }

                                                    foreach ($subarea->getIndicator as $in => $indicator) {

                                                        if (in_array($indicator->id, $indicator_edb)) {
                                                            $array_indicator[] = $indicator->id;
                                                            $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                            if (count($indicator->getSubIndicator) > 0) {
                                                                for ($r_indicator = 0; $r_indicator < count($indicator_role); $r_indicator++) {
                                                                    if (in_array($indicator_role[$r_indicator], $skpd_name)) {
                                                                        $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);

                                                                        $array_si = array();
                                                                        break;
                                                                    }
                                                                }

                                                                foreach ($indicator->getSubIndicator as $sin => $subindicator) {

                                                                    if ($subindicator->parent_id === null) {
                                                                        if (in_array($subindicator->id, $subindicator_edb)) {
                                                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                            $value = $edb->value;
                                                                            $satuan = $edb->unit_id;
                                                                            $action = 1;
                                                                            $array_si[] = $subindicator->id;

                                                                            for ($r_si = 0; $r_si < count($role['data_si'][$subindicator->id]); $r_si++) {
                                                                                if (in_array($role['data_si'][$subindicator->id][$r_si], $skpd_name)) {
                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {

                                                                            if (in_array($subindicator->id, $parent_si)) {
                                                                                $array_si[] = $subindicator->id;

                                                                                for ($r_si = 0; $r_si < count($data_parent[$subindicator->id]); $r_si++) {
                                                                                    if (in_array($data_parent[$subindicator->id][$r_si], $skpd_name)) {
                                                                                        $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                        if (count($subindicator->child) > 0) {
                                                                                            $data_edb = parent_si_edb_pd($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $skpd_name);
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                }
                                            }
                                        }
                                    } else {
                                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                        $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            } elseif ($user->type_user === 1) {
                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {

                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            if (in_array("Kecamatan", $business_role)) {
                                $array_business[] = $business->id;
                                $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                $array_area = array();

                                foreach ($business->getArea as $a => $area) {

                                    if (in_array($area->id, $area_edb)) {

                                        $area_role = single_array($role['data_area'][$area->id]);

                                        if (count($area->getSubArea) > 0) {
                                            if (in_array("Kecamatan", $area_role)) {
                                                $array_area[] = $area->id;
                                                if ($area->getBusiness->role === "Kecamatan" && $area->getBusiness->check_sub === 0) {
                                                    if ($area->area_name === $kecamatan['nama']) {
                                                        $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    }
                                                } else {
                                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    $array_subarea = array();
                                                    foreach ($area->getSubArea as $sa => $subarea) {

                                                        if (in_array($subarea->id, $subarea_edb)) {

                                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                            if (count($subarea->getIndicator) > 0) {
                                                                if (in_array("Kecamatan", $subarea_role)) {
                                                                    $array_subarea[] = $subarea->id;
                                                                    if ($subarea->getArea->role === "Kecamatan" && $subarea->getArea->check_sub === 0) {
                                                                        if ($subarea->subarea_name === $kecamatan['nama']) {
                                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        }
                                                                    } else {
                                                                        $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        $array_indicator = array();
                                                                        foreach ($subarea->getIndicator as $in => $indicator) {

                                                                            if (in_array($indicator->id, $indicator_edb)) {

                                                                                $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                                                if (count($indicator->getSubIndicator) > 0) {
                                                                                    if (in_array("Kecamatan", $indicator_role)) {
                                                                                        $array_indicator[] = $indicator->id;
                                                                                        if ($indicator->getSubArea->role === "Kecamatan" && $indicator->getSubArea->check_sub === 0) {
                                                                                            if ($indicator->indicator_name === $kecamatan['nama']) {
                                                                                                $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);
                                                                                            }
                                                                                        } else {
                                                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);


                                                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {
                                                                                                $array_si = array();
                                                                                                if ($subindicator->parent_id === null) {
                                                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                                                        if (in_array("Kecamatan", $role['data_si'][$subindicator->id])) {

                                                                                                            if ($subindicator->getIndicator->role === "Kecamatan" && $subindicator->getIndicator->check_sub === 0) {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();
                                                                                                                if ($subindicator->sub_indicator_name === $kecamatan['nama']) {
                                                                                                                    $value = $edb->value;
                                                                                                                    $satuan = $edb->unit_id;
                                                                                                                    $action = 1;
                                                                                                                    $array_si[] = $subindicator->id;
                                                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                }
                                                                                                            } else {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                                                                $value = $edb->value;
                                                                                                                $satuan = $edb->unit_id;
                                                                                                                $action = 1;

                                                                                                                $array_si[] = $subindicator->id;

                                                                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                            }
                                                                                                        }
                                                                                                    } else {

                                                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                                                            if (in_array("Kecamatan", $data_parent[$subindicator->id])) {
                                                                                                                $array_si[] = $subindicator->id;
                                                                                                                $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                if (count($subindicator->child) > 0) {
                                                                                                                    $data_edb = parent_si_edb_kecamatan($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $kecamatan['nama']);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                        }
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            } else {
                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {

                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            if (in_array("Kelurahan", $business_role)) {
                                $array_business[] = $business->id;
                                $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                $array_area = array();

                                foreach ($business->getArea as $a => $area) {

                                    if (in_array($area->id, $area_edb)) {

                                        $area_role = single_array($role['data_area'][$area->id]);

                                        if (count($area->getSubArea) > 0) {
                                            if (in_array("Kelurahan", $area_role)) {
                                                $array_area[] = $area->id;
                                                if ($area->getBusiness->role === "Kelurahan" && $area->getBusiness->check_sub === 0) {
                                                    if ($area->area_name === $kelurahan['nama']) {
                                                        $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    }
                                                } else {
                                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    $array_subarea = array();
                                                    foreach ($area->getSubArea as $sa => $subarea) {

                                                        if (in_array($subarea->id, $subarea_edb)) {

                                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                            if (count($subarea->getIndicator) > 0) {
                                                                if (in_array("Kelurahan", $subarea_role)) {
                                                                    $array_subarea[] = $subarea->id;
                                                                    if ($subarea->getArea->role === "Kelurahan" && $subarea->getArea->check_sub === 0) {
                                                                        if ($subarea->subarea_name === $kelurahan['nama']) {
                                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        }
                                                                    } else {
                                                                        $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        $array_indicator = array();
                                                                        foreach ($subarea->getIndicator as $in => $indicator) {

                                                                            if (in_array($indicator->id, $indicator_edb)) {

                                                                                $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                                                if (count($indicator->getSubIndicator) > 0) {
                                                                                    if (in_array("Kelurahan", $indicator_role)) {
                                                                                        $array_indicator[] = $indicator->id;
                                                                                        if ($indicator->getSubArea->role === "Kelurahan" && $indicator->getSubArea->check_sub === 0) {
                                                                                            if ($indicator->indicator_name === $kelurahan['nama']) {
                                                                                                $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);
                                                                                            }
                                                                                        } else {
                                                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);


                                                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {
                                                                                                $array_si = array();
                                                                                                if ($subindicator->parent_id === null) {
                                                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                                                        if (in_array("Kelurahan", $role['data_si'][$subindicator->id])) {

                                                                                                            if ($subindicator->getIndicator->role === "Kelurahan" && $subindicator->getIndicator->check_sub === 0) {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();
                                                                                                                if ($subindicator->sub_indicator_name === $kelurahan['nama']) {
                                                                                                                    $value = $edb->value;
                                                                                                                    $satuan = $edb->unit_id;
                                                                                                                    $action = 1;
                                                                                                                    $array_si[] = $subindicator->id;
                                                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                }
                                                                                                            } else {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                                                                $value = $edb->value;
                                                                                                                $satuan = $edb->unit_id;
                                                                                                                $action = 1;

                                                                                                                $array_si[] = $subindicator->id;

                                                                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                            }
                                                                                                        }
                                                                                                    } else {

                                                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                                                            if (in_array("Kelurahan", $data_parent[$subindicator->id])) {
                                                                                                                $array_si[] = $subindicator->id;
                                                                                                                $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                if (count($subindicator->child) > 0) {
                                                                                                                    $data_edb = parent_si_edb_kelurahan($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $kelurahan['nama']);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                        }
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            }
        }

        $array_si = array();
        $data2 = subIndicatorModel::where('id', 4)->with('parent')->get();
        if ($data) {
            $data = $data;
        } else {
            $data = array();
        }

        if(empty($data_edb)){
            $data_edb = array();
        }
        if ($request->ajax()) {
            $thn = tahunModel::all();

            $table = Datatables::of($data_edb);
            $table->addColumn('master_data', function ($row) use ($data) {
                $dd = '';
                foreach ($data as $d) {
                    $dd = $dd . $d['uraian'];
                }
                return $dd;
            });
            $table->editColumn('urusan', function ($row) use ($data) {
                $space = '';
                for ($i = 0; $i < $row['space']; $i++) {
                    $space = $space . '&nbsp;';
                }
                //  . "<b>" . implode("|", $row['role']) . "</b>"
                return $space . $row['number'] . '. ' . $row['uraian'];
            });
            $table->addColumn('satuan', function ($row) {
                if ($row['satuan']) {
                    if ($row['satuan'] > 0) {
                        $units = unitsModel::find($row['satuan']);
                        $unit = $units->unit;
                    } else {
                        $unit = '';
                    }
                } else {
                    $unit = '';
                }

                return $unit;
            });
            $table->addColumn('value', function ($row) {
                $val = str_replace(".", "", $row['value']);
                if (is_numeric($val)) {
                    if (strpos($row['value'], ".") === true) {
                        $decimal = strlen(substr($row['value'], strrpos($row['value'], '.') + 1));
                        $value = number_format($row['value'], $decimal, ',', '.');
                    } else {
                        $value = number_format($row['value'], 0, '', '.');
                    }
                } else if ($row['value'] === "") {
                    $value = '-';
                } else {
                    $value = $row['value'];
                }
                return $value;
            });
            $table->addColumn('action', function ($row) use ($request) {
                if ($row['action'] === 0) {
                    $btn = '';
                } else {
                    $btn = '';
                    if (auth()->user()->role === '0' || auth()->user()->role === '2') {
                        $btn = $btn . '<a href="' . url('change-edatabase/' . $row['id'] . '') . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    } else {
                        $edb = edatabaseModel::find($row['id']);
                        if ($edb->validate === 0 && $edb->validate_final === 0) {
                            $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-edb/" . $row['id']) . '"><span class="clip-notification"></span></a>';
                        } else if ($edb->validate === 1 && $edb->validate_final === 0) {
                            $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                        } else {
                            $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                        }
                    }
                }

                return $btn;
            });
            $table->filter(function ($instance) use ($request) {

                if (!empty($request->get('search'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        if (Str::contains(Str::lower($row['uraian']), Str::lower($request->get('search')))) {

                            return true;
                        } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                            return true;
                        }


                        return false;
                    });
                }
                if (!empty($request->get('urusan'))) {
                    $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                        if ($row['master'] == $request->get('urusan')) {

                            return true;
                        }


                        return false;
                    });
                }
            });
            $table->rawColumns(['urusan', 'action']);
            $table->addIndexColumn();
            return $table->make(true);
        }
        $edatabase = edatabaseModel::all();
        $array_business = array();

        $urusan = businessModel::all();
        $area = areaModel::all();
        $sub_area = subAreaModel::all();
        $indicator = indicatorModel::all();
        $sub_indicator = subIndicatorModel::all();
        $thn = tahunModel::all();
        return view('edatabase.edatabase_vw', ['urusan' => $urusan, 'area' => $area, 'sub_area' => $sub_area, 'indicator' => $indicator, 'sub_indicator' => $sub_indicator, 'thn' => $thn, 'edatabase' => $edatabase]);
    }

    public function add_edatabase()
    {
        $urusan = businessModel::where('active', 0)->get();
        $area = areaModel::where('active', 0)->get();
        $sub_area = subAreaModel::where('active', 0)->get();
        $indicator = indicatorModel::where('active', 0)->get();
        $sub_indicator = subIndicatorModel::where('active', 0)->where('parent_id', null)->get();

        foreach ($sub_indicator as $sub) {
            if (count($sub->child)) {
                $status = 0;
            } else {
                $status = 1;
            }
            $data[] = array('id' => $sub->id, 'sub_indicator_name' => $sub->sub_indicator_name, 'space' => 0, 'unit_id' => $sub->unit_id, 'indicator_id' => $sub->indicator_id, 'status' => $status);
            if (count($sub->child)) {
                $data = parent_si($sub->child, $data, 0, $sub->id);
            }
        }

        $unit = unitsModel::all();

        return view('edatabase.addeDatabase_vw', ['urusan' => $urusan, 'area' => $area, 'sub_area' => $sub_area, 'indicator' => $indicator, 'sub_indicator' => $data, 'unit' => $unit]);
    }

    public function save_edatabase(Request $request)
    {
        if ($request->id) {
            $data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_id' => $request->indicator_id,
                'sub_indicator_id' => $request->sub_indicator_id,
                'unit_id' => $request->satuan,
                'value' => $request->value,
                'validate' => 0,
                'validate_final' => 0
            );
            edatabaseModel::whereId($request->id)->update($data);
        } else {
            if ($request->business_id) {
                $business_id = $request->business_id;
            } else {
                $business_id = 0;
            }
            if ($request->area_id) {
                $area_id = $request->area_id;
            } else {
                $area_id = 0;
            }
            if ($request->sub_area_id) {
                $sub_area_id = $request->sub_area_id;
            } else {
                $sub_area_id = 0;
            }
            if ($request->indicator_id) {
                $indicator_id = $request->indicator_id;
            } else {
                $indicator_id = 0;
            }
            if ($request->sub_indicator_id) {
                $subindicator_id = $request->sub_indicator_id;
            } else {
                $subindicator_id = 0;
            }
            $data = array(
                'id' => $request->id,
                'business_id' => $business_id,
                'area_id' => $area_id,
                'sub_area_id' => $sub_area_id,
                'indicator_id' => $indicator_id,
                'sub_indicator_id' => $subindicator_id,
                'unit_id' => $request->satuan,
                'value' => $request->value,
                'validate' => 0,
                'validate_final' => 0
            );
            $edb = edatabaseModel::where('business_id', $request->business_id)->where('area_id', $request->area_id)->where('sub_area_id', $sub_area_id)->where('indicator_id', $request->indicator_id)->where('sub_indicator_id', $request->sub_indicator_id)->first();
            if (count((array)$edb) > 0) {
                if ($request->value === $edb->value) {
                } else {
                    return back()->with('success', $request->satuan);
                    $edb->id = $request->id;
                    $edb->business_id = $request->business_id;
                    $edb->area_id = $request->area_id;
                    $edb->sub_area_id = $request->sub_area_id;
                    $edb->indicator_id = $request->indicator_id;
                    $edb->sub_indicator_id = $request->sub_indicator_id;
                    $edb->unit_id = $request->satuan;
                    $edb->value = $request->value;
                    $edb->validate = 0;
                    $edb->validate_final = 0;
                    $edb->save();
                }
            } else {
                $edatabase = new edatabaseModel;
                $edatabase->id = $request->id;
                $edatabase->business_id = $request->business_id;
                $edatabase->area_id = $request->area_id;
                $edatabase->sub_area_id = $request->sub_area_id;
                $edatabase->indicator_id = $request->indicator_id;
                $edatabase->sub_indicator_id = $request->sub_indicator_id;
                $edatabase->unit_id = $request->satuan;
                $edatabase->value = $request->value;
                $edatabase->validate = 0;
                $edatabase->validate_final = 0;
                $edatabase->save();
            }
        }

        return back()->with('success', 'Data eDatabase berhasil disimpan!');
    }

    public function change_edatabase($id)
    {
        $edatabase = edatabaseModel::find($id);
        $urusan = businessModel::where('active', 0)->get();
        $area = areaModel::where('active', 0)->get();
        $sub_area = subAreaModel::where('active', 0)->get();
        $indicator = indicatorModel::where('active', 0)->get();
        $sub_indicator = subIndicatorModel::where('active', 0)->where('parent_id', null)->get();

        foreach ($sub_indicator as $sub) {
            $data[] = array('id' => $sub->id, 'sub_indicator_name' => $sub->sub_indicator_name, 'space' => 0, 'unit_id' => $sub->unit_id, 'indicator_id' => $sub->indicator_id);
            if (count($sub->child)) {
                $data = parent_si($sub->child, $data, 0, $sub->id);
            }
        }

        $unit = unitsModel::all();

        return view('edatabase.editeDatabase', ['edatabase' => $edatabase, 'urusan' => $urusan, 'area' => $area, 'sub_area' => $sub_area, 'indicator' => $indicator, 'sub_indicator' => $data, 'unit' => $unit]);
    }

    public function validasi($id)
    {
        $edb = edatabaseModel::find($id);
        $edb->validate = 1;
        $edb->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_all()
    {
        edatabaseModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));

        return back()->with('success', 'Data berhasil divalidasi.');
    }

    public function excel()
    {
        return Excel::download(new eDatabaseExport, 'eDatabase.xlsx');
    }

    public function pdf()
    {
        $getrole = edatabaseModel::all();
        $getrole = $getrole->groupBy(['business_id', 'area_id', 'sub_area_id', 'indicator_id']);

        // Get Data User
        $user = \App\userModel::find(auth()->user()->id);
        // Get SKPD
        $skpd_user = \App\skpdUser::where('user_id', auth()->user()->id)->get();

        if (count($skpd_user) > 0) {
            foreach ($skpd_user as $su) {
                $get_skpd = \App\skpdModel::find($su->skpd_id);

                $skpd[] = array('id' => $get_skpd->id, 'skpd_name' => $get_skpd->skpd_name, 'active' => $get_skpd->active);
            }
        } else {
            $skpd = 0;
        }

        // Get Kecamatan
        $kec = \App\kecamatanModel::where('id', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kec) > 0) {
            $kecamatan[] = array('id' => $kec->id, 'nama' => $kec->nama, 'status' => $kec->status);
        } else {
            $kecamatan = 0;
        }
        $kecamatan = single_array($kecamatan);

        // Get Kelurahan
        $kel = \App\kelurahanModel::where('id', auth()->user()->id_kel)->where('id_kec', auth()->user()->id_kec)->where('status', 1)->first();

        if (count((array)$kel) > 0) {
            $kelurahan[] = array('id' => $kel->id, 'id_kec' => $kel->id_kec, 'nama' => $kel->nama, 'status' => $kel->status);
        } else {
            $kelurahan = 0;
        }
        $kelurahan = single_array($kelurahan);

        $data = edatabaseModel::all();

        $array_parent[] = array();
        $business_edb = array();
        $area_edb = array();
        $subarea_edb = array();
        $indicator_edb = array();
        $subindicator_edb = array();
        $subindicator_parent = array();
        foreach ($data as $edb) {
            if ($edb->business_id != null) {
                if (in_array($edb->business_id, $business_edb)) {
                    if ($edb->area_id != null) {
                        if (in_array($edb->area_id, $area_edb)) {
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $area_edb[] = $edb->area_id;
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubIndicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else {
                    $business_edb[] = $edb->business_id;
                    if ($edb->area_id != null) {
                        if (in_array($edb->area_id, $area_edb)) {
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);

                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        } else {
                            $area_edb[] = $edb->area_id;
                            if ($edb->sub_area_id != null) {
                                if (in_array($edb->sub_area_id, $subarea_edb)) {
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    $subarea_edb[] = $edb->sub_area_id;
                                    if ($edb->indicator_id != null) {
                                        if (in_array($edb->indicator_id, $indicator_edb)) {
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $indicator_edb[] = $edb->indicator_id;
                                            if ($edb->sub_indicator_id != null) {
                                                if (in_array($edb->sub_indicator_id, $subindicator_edb)) {
                                                } else {
                                                    $subindicator_edb[] = $edb->sub_indicator_id;
                                                    if ($edb->getSubindicator->parent) {

                                                        $parent = getparent_si(
                                                            $edb->getSubIndicator->parent,
                                                            $array_parent
                                                        );
                                                        $parent = single_array($parent);
                                                        for ($p = 0; $p < count($parent); $p++) {
                                                            if (in_array($parent[$p], $subindicator_edb)) {
                                                            } else {
                                                                $subindicator_parent[] = $parent[$p];
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $urusan = businessModel::all();
        $array_business = array();

        $role_edb_all = role_edb($getrole, $user, $skpd, $kec, $kel);

        $role = array();

        // Get Role Business
        $role_urusan = $role_edb_all['data_business'];

        for ($ib = 0; $ib < count($business_edb); $ib++) {
            $get_role_business = array();
            for ($u = 0; $u < count($role_urusan); $u++) {
                if ($role_urusan[$u]['id'] === $business_edb[$ib]) {
                    for ($ur = 0; $ur < count($role_urusan[$u]['role']); $ur++) {
                        if (in_array($role_urusan[$u]['role'][$ur], $get_role_business)) {
                        } else {
                            $get_role_business[] = $role_urusan[$u]['role'][$ur];
                        }
                    }
                }
            }
            $data_business[$business_edb[$ib]] = $get_role_business;
        }
        $role['data_business'] = $data_business;

        // Get Role Area
        $role_bidang = $role_edb_all['data_area'];

        for ($ia = 0; $ia < count($area_edb); $ia++) {
            $get_role_area = array();
            for ($b = 0; $b < count($role_bidang); $b++) {
                if ($role_bidang[$b]['id'] === $area_edb[$ia]) {
                    for ($br = 0; $br < count($role_bidang[$b]['role']); $br++) {
                        if (in_array($role_bidang[$b]['role'][$br], $get_role_area)) {
                        } else {
                            $get_role_area[] = $role_bidang[$b]['role'][$br];
                        }
                    }
                }
            }
            $data_area[$area_edb[$ia]] = $get_role_area;
        }
        $role['data_area'] = $data_area;

        // Get Role SubArea
        $role_subbidang = $role_edb_all['data_subarea'];

        for ($isa = 0; $isa < count($subarea_edb); $isa++) {
            $get_role_subarea = array();
            for ($sb = 0; $sb < count($role_subbidang); $sb++) {
                if ($role_subbidang[$sb]['id'] === $subarea_edb[$isa]) {
                    for ($sbr = 0; $sbr < count($role_subbidang[$sb]['role']); $sbr++) {
                        if (in_array($role_subbidang[$sb]['role'][$sbr], $get_role_subarea)) {
                        } else {
                            $get_role_subarea[] = $role_subbidang[$sb]['role'][$sbr];
                        }
                    }
                }
            }
            $data_subarea[$subarea_edb[$isa]] = $get_role_subarea;
        }
        $role['data_subarea'] = $data_subarea;

        // Get Role Indicator
        $role_indikator = $role_edb_all['data_indicator'];

        for ($ii = 0; $ii < count($indicator_edb); $ii++) {
            $get_role_indicator = array();
            for ($sb = 0; $sb < count($role_indikator); $sb++) {
                if ($role_indikator[$sb]['id'] === $indicator_edb[$ii]) {
                    for ($sbr = 0; $sbr < count($role_indikator[$sb]['role']); $sbr++) {
                        if (in_array($role_indikator[$sb]['role'][$sbr], $get_role_indicator)) {
                        } else {
                            $get_role_indicator[] = $role_indikator[$sb]['role'][$sbr];
                        }
                    }
                }
            }
            $data_indicator[$indicator_edb[$ii]] = $get_role_indicator;
        }
        $role['data_indicator'] = $data_indicator;

        $role['data_si'] = $role_edb_all['data_si'];

        $parent_si = array();
        for ($si_parent = 0; $si_parent < count($subindicator_parent); $si_parent++) {
            if (in_array($subindicator_parent[$si_parent], $subindicator_edb)) {
            } else {
                if (in_array($subindicator_parent[$si_parent], $parent_si)) {
                } else {
                    $parent_si[] = $subindicator_parent[$si_parent];
                }
            }
        }


        for ($p_si = 0; $p_si < count($parent_si); $p_si++) {
            $sub_parent = subIndicatorModel::find($parent_si[$p_si]);

            if (count($sub_parent->child) > 0) {
                $role_parent = array();
                $child_parent = array();
                $get_role_parent = parent_si_role_edb($sub_parent->child, $parent_si, $subindicator_edb, $role_parent);

                $data_parent[$parent_si[$p_si]] = $get_role_parent;
            } else {
                $data_parent[$parent_si[$p_si]] = array($sub_parent->role);
            }
        }
        if ($user->role === '0') {
            foreach ($urusan as $u => $business) {
                if (in_array($business->id, $business_edb)) {
                    $array_business[] = $business->id;
                    $business_role = single_array($role['data_business'][$business->id]);

                    if (count($business->getArea) > 0) {

                        $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        $array_area = array();
                        foreach ($business->getArea as $a => $area) {

                            if (in_array($area->id, $area_edb)) {
                                $array_area[] = $area->id;
                                $area_role = single_array($role['data_area'][$area->id]);

                                if (count($area->getSubArea) > 0) {

                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);

                                    $array_subarea = array();
                                    foreach ($area->getSubArea as $sa => $subarea) {

                                        if (in_array($subarea->id, $subarea_edb)) {
                                            $array_subarea[] = $subarea->id;
                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                            if (count($subarea->getIndicator) > 0) {

                                                $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);

                                                $array_indicator = array();
                                                foreach ($subarea->getIndicator as $in => $indicator) {

                                                    if (in_array($indicator->id, $indicator_edb)) {
                                                        $array_indicator[] = $indicator->id;
                                                        $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                        if (count($indicator->getSubIndicator) > 0) {

                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);

                                                            $array_si = array();
                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {

                                                                if ($subindicator->parent_id === null) {
                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                        $value = $edb->value;
                                                                        $satuan = $edb->unit_id;
                                                                        $action = 1;
                                                                        $array_si[] = $subindicator->id;
                                                                        $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                    } else {

                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                            $array_si[] = $subindicator->id;
                                                                            $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                            if (count($subindicator->child) > 0) {
                                                                                $data_edb = parent_si_edb($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si']);
                                                                            }
                                                                        }
                                                                    }
                                                                } else {
                                                                }
                                                            }
                                                        } else {
                                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                        }
                                                    }
                                                }
                                            } else {
                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                            }
                                        }
                                    }
                                } else {
                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                }
                            }
                        }
                    } else {
                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                        $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                    }
                } else {
                }
            }
        } else {
            if ($user->type_user === 0) {
                $skpd_name = array();
                if (is_array($skpd)) {
                    foreach ($skpd as $sk) {
                        $skpd_name[] = $sk['skpd_name'];
                    }
                } else {
                    $skpd_name = 0;
                }

                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {
                        $array_business[] = $business->id;
                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            for ($r_business = 0; $r_business < count($business_role); $r_business++) {
                                if (in_array($business_role[$r_business], $skpd_name)) {
                                    $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                    $array_area = array();

                                    break;
                                }
                            }

                            foreach ($business->getArea as $a => $area) {

                                if (in_array($area->id, $area_edb)) {
                                    $array_area[] = $area->id;
                                    $area_role = single_array($role['data_area'][$area->id]);

                                    if (count($area->getSubArea) > 0) {
                                        for ($r_area = 0; $r_area < count($area_role); $r_area++) {
                                            if (in_array($area_role[$r_area], $skpd_name)) {
                                                $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);

                                                $array_subarea = array();
                                                break;
                                            }
                                        }

                                        foreach ($area->getSubArea as $sa => $subarea) {

                                            if (in_array($subarea->id, $subarea_edb)) {
                                                $array_subarea[] = $subarea->id;
                                                $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                if (count($subarea->getIndicator) > 0) {
                                                    for ($r_subarea = 0; $r_subarea < count($subarea_role); $r_subarea++) {
                                                        if (in_array($subarea_role[$r_subarea], $skpd_name)) {
                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);

                                                            $array_indicator = array();
                                                            break;
                                                        }
                                                    }

                                                    foreach ($subarea->getIndicator as $in => $indicator) {

                                                        if (in_array($indicator->id, $indicator_edb)) {
                                                            $array_indicator[] = $indicator->id;
                                                            $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                            if (count($indicator->getSubIndicator) > 0) {
                                                                for ($r_indicator = 0; $r_indicator < count($indicator_role); $r_indicator++) {
                                                                    if (in_array($indicator_role[$r_indicator], $skpd_name)) {
                                                                        $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);

                                                                        $array_si = array();
                                                                        break;
                                                                    }
                                                                }

                                                                foreach ($indicator->getSubIndicator as $sin => $subindicator) {

                                                                    if ($subindicator->parent_id === null) {
                                                                        if (in_array($subindicator->id, $subindicator_edb)) {
                                                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                            $value = $edb->value;
                                                                            $satuan = $edb->unit_id;
                                                                            $action = 1;
                                                                            $array_si[] = $subindicator->id;

                                                                            for ($r_si = 0; $r_si < count($role['data_si'][$subindicator->id]); $r_si++) {
                                                                                if (in_array($role['data_si'][$subindicator->id][$r_si], $skpd_name)) {
                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                    break;
                                                                                }
                                                                            }
                                                                        } else {

                                                                            if (in_array($subindicator->id, $parent_si)) {
                                                                                $array_si[] = $subindicator->id;

                                                                                for ($r_si = 0; $r_si < count($data_parent[$subindicator->id]); $r_si++) {
                                                                                    if (in_array($data_parent[$subindicator->id][$r_si], $skpd_name)) {
                                                                                        $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                        if (count($subindicator->child) > 0) {
                                                                                            $data_edb = parent_si_edb_pd($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $skpd_name);
                                                                                        }
                                                                                        break;
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    } else {
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                }
                                            }
                                        }
                                    } else {
                                        $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                        $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            } elseif ($user->type_user === 1) {
                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {

                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            if (in_array("Kecamatan", $business_role)) {
                                $array_business[] = $business->id;
                                $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                $array_area = array();

                                foreach ($business->getArea as $a => $area) {

                                    if (in_array($area->id, $area_edb)) {

                                        $area_role = single_array($role['data_area'][$area->id]);

                                        if (count($area->getSubArea) > 0) {
                                            if (in_array("Kecamatan", $area_role)) {
                                                $array_area[] = $area->id;
                                                if ($area->getBusiness->role === "Kecamatan" && $area->getBusiness->check_sub === 0) {
                                                    if ($area->area_name === $kecamatan['nama']) {
                                                        $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    }
                                                } else {
                                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    $array_subarea = array();
                                                    foreach ($area->getSubArea as $sa => $subarea) {

                                                        if (in_array($subarea->id, $subarea_edb)) {

                                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                            if (count($subarea->getIndicator) > 0) {
                                                                if (in_array("Kecamatan", $subarea_role)) {
                                                                    $array_subarea[] = $subarea->id;
                                                                    if ($subarea->getArea->role === "Kecamatan" && $subarea->getArea->check_sub === 0) {
                                                                        if ($subarea->subarea_name === $kecamatan['nama']) {
                                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        }
                                                                    } else {
                                                                        $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        $array_indicator = array();
                                                                        foreach ($subarea->getIndicator as $in => $indicator) {

                                                                            if (in_array($indicator->id, $indicator_edb)) {

                                                                                $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                                                if (count($indicator->getSubIndicator) > 0) {
                                                                                    if (in_array("Kecamatan", $indicator_role)) {
                                                                                        $array_indicator[] = $indicator->id;
                                                                                        if ($indicator->getSubArea->role === "Kecamatan" && $indicator->getSubArea->check_sub === 0) {
                                                                                            if ($indicator->indicator_name === $kecamatan['nama']) {
                                                                                                $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);
                                                                                            }
                                                                                        } else {
                                                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);


                                                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {
                                                                                                $array_si = array();
                                                                                                if ($subindicator->parent_id === null) {
                                                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                                                        if (in_array("Kecamatan", $role['data_si'][$subindicator->id])) {

                                                                                                            if ($subindicator->getIndicator->role === "Kecamatan" && $subindicator->getIndicator->check_sub === 0) {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();
                                                                                                                if ($subindicator->sub_indicator_name === $kecamatan['nama']) {
                                                                                                                    $value = $edb->value;
                                                                                                                    $satuan = $edb->unit_id;
                                                                                                                    $action = 1;
                                                                                                                    $array_si[] = $subindicator->id;
                                                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                }
                                                                                                            } else {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                                                                $value = $edb->value;
                                                                                                                $satuan = $edb->unit_id;
                                                                                                                $action = 1;

                                                                                                                $array_si[] = $subindicator->id;

                                                                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                            }
                                                                                                        }
                                                                                                    } else {

                                                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                                                            if (in_array("Kecamatan", $data_parent[$subindicator->id])) {
                                                                                                                $array_si[] = $subindicator->id;
                                                                                                                $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                if (count($subindicator->child) > 0) {
                                                                                                                    $data_edb = parent_si_edb_kecamatan($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $kecamatan['nama']);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                        }
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            } else {
                foreach ($urusan as $u => $business) {
                    if (in_array($business->id, $business_edb)) {

                        $business_role = single_array($role['data_business'][$business->id]);

                        if (count($business->getArea) > 0) {
                            if (in_array("Kelurahan", $business_role)) {
                                $array_business[] = $business->id;
                                $data_edb[] = array('uraian' => $business->business_name, 'role' => $business_role, 'check_sub' => $business->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                                $array_area = array();

                                foreach ($business->getArea as $a => $area) {

                                    if (in_array($area->id, $area_edb)) {

                                        $area_role = single_array($role['data_area'][$area->id]);

                                        if (count($area->getSubArea) > 0) {
                                            if (in_array("Kelurahan", $area_role)) {
                                                $array_area[] = $area->id;
                                                if ($area->getBusiness->role === "Kelurahan" && $area->getBusiness->check_sub === 0) {
                                                    if ($area->area_name === $kelurahan['nama']) {
                                                        $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    }
                                                } else {
                                                    $data_edb[] = array('uraian' => $area->area_name, 'role' => $area_role, 'check_sub' => $area->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                                    $array_subarea = array();
                                                    foreach ($area->getSubArea as $sa => $subarea) {

                                                        if (in_array($subarea->id, $subarea_edb)) {

                                                            $subarea_role = single_array($role['data_subarea'][$subarea->id]);

                                                            if (count($subarea->getIndicator) > 0) {
                                                                if (in_array("Kelurahan", $subarea_role)) {
                                                                    $array_subarea[] = $subarea->id;
                                                                    if ($subarea->getArea->role === "Kelurahan" && $subarea->getArea->check_sub === 0) {
                                                                        if ($subarea->subarea_name === $kelurahan['nama']) {
                                                                            $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        }
                                                                    } else {
                                                                        $data_edb[] = array('uraian' => $subarea->subarea_name, 'role' => $subarea_role, 'check_sub' => $subarea->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                        $array_indicator = array();
                                                                        foreach ($subarea->getIndicator as $in => $indicator) {

                                                                            if (in_array($indicator->id, $indicator_edb)) {

                                                                                $indicator_role = single_array($role['data_indicator'][$indicator->id]);

                                                                                if (count($indicator->getSubIndicator) > 0) {
                                                                                    if (in_array("Kelurahan", $indicator_role)) {
                                                                                        $array_indicator[] = $indicator->id;
                                                                                        if ($indicator->getSubArea->role === "Kelurahan" && $indicator->getSubArea->check_sub === 0) {
                                                                                            if ($indicator->indicator_name === $kelurahan['nama']) {
                                                                                                $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);
                                                                                            }
                                                                                        } else {
                                                                                            $data_edb[] = array('uraian' => $indicator->indicator_name, 'role' => $indicator_role, 'check_sub' => $indicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_indicator), 'space' => 12);


                                                                                            foreach ($indicator->getSubIndicator as $sin => $subindicator) {
                                                                                                $array_si = array();
                                                                                                if ($subindicator->parent_id === null) {
                                                                                                    if (in_array($subindicator->id, $subindicator_edb)) {
                                                                                                        if (in_array("Kelurahan", $role['data_si'][$subindicator->id])) {

                                                                                                            if ($subindicator->getIndicator->role === "Kelurahan" && $subindicator->getIndicator->check_sub === 0) {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();
                                                                                                                if ($subindicator->sub_indicator_name === $kelurahan['nama']) {
                                                                                                                    $value = $edb->value;
                                                                                                                    $satuan = $edb->unit_id;
                                                                                                                    $action = 1;
                                                                                                                    $array_si[] = $subindicator->id;
                                                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                }
                                                                                                            } else {
                                                                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', $subindicator->id)->first();

                                                                                                                $value = $edb->value;
                                                                                                                $satuan = $edb->unit_id;
                                                                                                                $action = 1;

                                                                                                                $array_si[] = $subindicator->id;

                                                                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subindicator->sub_indicator_name, 'role' => $role['data_si'][$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => $value, 'satuan' => $satuan, 'action' => $action, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                            }
                                                                                                        }
                                                                                                    } else {

                                                                                                        if (in_array($subindicator->id, $parent_si)) {
                                                                                                            if (in_array("Kelurahan", $data_parent[$subindicator->id])) {
                                                                                                                $array_si[] = $subindicator->id;
                                                                                                                $data_edb[] = array('uraian' => $subindicator->sub_indicator_name, 'role' => $data_parent[$subindicator->id], 'check_sub' => $subindicator->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $business->id, 'number' => count($array_si), 'space' => 16);
                                                                                                                if (count($subindicator->child) > 0) {
                                                                                                                    $data_edb = parent_si_edb_kelurahan($subindicator->child, $data_edb, 16, $business->id, $subindicator_edb, $parent_si, $subindicator->role, $subindicator->id, $data_parent, $role['data_si'], $kelurahan['nama']);
                                                                                                                }
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                } else {
                                                                                                }
                                                                                            }
                                                                                        }
                                                                                    }
                                                                                } else {
                                                                                    $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', $indicator->id)->where('sub_indicator_id', null)->first();

                                                                                    $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', $subarea->id)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                                                $data_edb[] = array('id' => $edb->id, 'uraian' => $subarea->subarea_name, 'role' => $subarea->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_subarea), 'space' => 8);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', $area->id)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                                            $data_edb[] = array('id' => $edb->id, 'uraian' => $area->area_name, 'role' => $area->role, 'check_sub' => $area->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => roman_numbered(count($array_area)), 'space' => 4);
                                        }
                                    }
                                }
                            }
                        } else {
                            $edb = edatabaseModel::where('business_id', $business->id)->where('area_id', null)->where('sub_area_id', null)->where('indicator_id', null)->where('sub_indicator_id', null)->first();

                            $data_edb[] = array('id' => $edb->id, 'uraian' => $business->business_name, 'role' => $business->role, 'check_sub' => $business->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $business->id, 'number' => count($array_business), 'space' => 0);
                        }
                    } else {
                    }
                }
            }
        }

        foreach ($data_edb as $row) {
            if ($row['satuan']) {
                if ($row['satuan'] > 0) {
                    $units = \App\unitsModel::find($row['satuan']);
                    $unit = $units->unit;
                } else {
                    $unit = '';
                }
            } else {
                $unit = '';
            }
            $edatabase[] = array('number' => $row['number'], 'uraian' => $row['uraian'], 'satuan' => $unit, 'value' => $row['value'], 'space' => $row['space']);
        }

        view()->share('edatabase', $edatabase);
        $pdf = PDF::loadView('exports.edatabase_export', $edatabase);

        return $pdf->stream('eDatabase.pdf');
    }
}
