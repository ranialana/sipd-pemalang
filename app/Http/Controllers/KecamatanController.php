<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\kecamatanModel;
use DataTables;
use App\Imports\KecamatanImport;
use App\Exports\KecamatanExport;
use Maatwebsite\Excel\Facades\Excel;
use File;
use PDF;

use Illuminate\Http\Request;


class KecamatanController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = kecamatanModel::latest()->get();
            $stat = "";

            return Datatables::of($data)            
                ->addColumn('status', function ($row) {
                    if($row->status == 1)
                    {
                        $stat = "Aktif";
                    }else
                    {
                        $stat = "Tidak Aktif";
                    }

                    return $stat;
                })
                ->addColumn('action', function ($row) {

                    $btn = '<a href="' . url("change-kecamatan/" . $row->id) . '"><span class="glyphicon glyphicon-pencil"></span></a>';

                    $btn = $btn . ' <a href="#" class="hapus_kecamatan" data-url="' . url("delete-kecamatan/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';

                    return $btn;
                })
                ->addIndexColumn()
                ->make(true);
        }
        return view('kecamatan.master_kecamatan');
    }

    public function change_kecamatan($id)
    {
        $kecamatan = kecamatanModel::find($id);
        
        return view('kecamatan.kecamatan_edit', ['kecamatan' => $kecamatan]);
    }


    public function update_status_kecamatan(request $request)
    {
        $id_kecamatan   = $request->id;

        $form_data = array(
                                'nama'        =>  $request->nama,
                                'status'            =>  $request->status
                            );

        kecamatanModel::whereId($id_kecamatan)->update($form_data);

        return redirect('/master-kecamatan')->with('success', 'Data Kecamatan Berhasil Diubah!');
    }

    public function add_kecamatan()
    {

        return view('kecamatan.kecamatan_add');
    }

    public function save_kecamatan(Request $request)
    {
        $form_data = array(
                                'nama'        =>  $request->nama,
                                'status'            =>  $request->status
                            );     

        kecamatanModel::create($form_data);

        return redirect('/master-kecamatan')->with('success', 'Data Kecamatan Berhasil Ditambahkan!');
    }

    public function delete_kecamatan($id)
    {
        kecamatanModel::whereId($id)->delete();

        return back()->with('success', 'Data Master Kecamatan berhasil dihapus!');
    }

    public function import_kecamatan(Request $request)
    {
        $nama_file = rand().$request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new KecamatanImport, storage_path('app/public/tmp_import/'.$nama_file));
        
        Storage::delete('public/tmp_import/'.$nama_file);

        return back()->with('success_import','File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/Kelurahan.xlsx');
    }

    public function excel()
    {
        return Excel::download(new KecamatanExport, 'Kecamatan.xlsx');
    }

    public function pdf()
    {
        $kecamatan = kecamatanModel::all();

        view()->share('kecamatan', $kecamatan);
        $pdf = PDF::loadView('master_export.kecamatan_export', $kecamatan);

        return $pdf->stream('Kecamatan.pdf');
    }
}
