<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\spmModel;
use \App\spmtransModel;
use \App\unitsModel;
use \App\tahunModel;
use \App\kecamatanModel;
use \App\kelurahanModel;
use App\Imports\SPMImport;
use App\Exports\SPMExport;
use App\Exports\MasterSPMExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;
use Auth;
use Illuminate\Filesystem\Filesystem;

class SPMController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $thn = tahunModel::all();
            if (auth()->user()->role === '0') {
                $data = spmModel::all();
            } else {
                if (auth()->user()->type_user === 0) {
                    $data = spmModel::where('type_user', 0)->get();
                } elseif (auth()->user()->type_user === 1) {
                    $data = spmModel::where('type_user', 1)->get();
                } else {
                    $data = spmModel::where('type_user', 2)->get();
                }
            }

            $table = Datatables::of($data);
            if ($request->get('kec') === "Kabupaten") {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                foreach ($thn as $a) {
                    $th = $a->id;
                    $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                        if ($row->type_user === 0) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$spmtrans) > 0) {
                            $val = str_replace(".", "", $spmtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($spmtrans->value, ".") === true) {
                                    $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                    $value = number_format($spmtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($spmtrans->value, 0, '', '.');
                                }
                            } else if ($spmtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $spmtrans->value;
                            }
                        } else {
                            $value = '-';
                        }

                        return $value;
                    });
                }

                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';
                    if ($row->type_user === 0) {
                        if (auth()->user()->role === '1') {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-spm/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif (auth()->user()->role === '2' || auth()->user()->role === '0') {
                            $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                        } else {
                        }
                    }
                    // if ($row->validate === 0 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-spm/" . $row->id) . '"><span class="clip-notification"></span></a>';
                    // } else if ($row->validate === 1 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                    // } else {
                    //     $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                    // }
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['spm_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            } else {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                if ($request->get('kel')) {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$spmtrans) > 0) {
                                $val = str_replace(".", "", $spmtrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($spmtrans->value, ".") === true) {
                                        $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                        $value = number_format($spmtrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($spmtrans->value, 0, '', '.');
                                    }
                                } else if ($spmtrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $spmtrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                } else {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$spmtrans) > 0) {
                                $val = str_replace(".", "", $spmtrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($spmtrans->value, ".") === true) {
                                        $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                        $value = number_format($spmtrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($spmtrans->value, 0, '', '.');
                                    }
                                } else if ($spmtrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $spmtrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                }


                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '"  data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';


                    if (auth()->user()->role === '1') {
                        if ($row->type_user === 0) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-spm/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif ($row->type_user === 1) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-spm/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } else {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-spm/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        }
                    } else {
                        if ($row->type_user === 0) {
                            if ($request->kel) {
                            } else {
                                if ($request->get('kec')) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } elseif ($row->type_user === 1) {
                            if ($request->get('kec') != 0) {
                                if ($request->kel) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } else {
                            if ($request->kel) {
                                $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                            }
                        }
                    }
                    // $btn = $btn . ' <a href="#myModalEdit" data-toggle="modal" data-id="' . $row->id . '" data-spm_description="' . $row->spm_description . '" data-satuan="' . $row->unit_id . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    // $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';

                    // $btn = $btn . ' <a href="#" class="hapus_spm" data-url="' . url("delete-spm/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['spm_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            }
        }
        $unit = unitsModel::all();
        $thn = tahunModel::all();

        if (auth()->user()->role === '0') {
            $kecamatan = kecamatanModel::all();
            $kelurahan = kelurahanModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $kecamatan = kecamatanModel::all();
                $kelurahan = kelurahanModel::all();
            } elseif (auth()->user()->type_user === 1) {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::all();
            } else {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::find(auth()->user()->id_kel);
            }
        }




        $a = date('Y') - 4;
        $b = date('Y') - 3;
        $c = date('Y') - 2;
        $d = date('Y') - 1;
        $e = date('Y');

        $column = 2 + count($thn) + 1;

        $kec = "Kabupaten";
        $kel = '';

        return view('spm.spm_vw', ['unit' => $unit, 'thn' => $thn, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan, 'kec' => $kec, 'kel' => $kel, 'column' => $column, 'a' => $a, 'b' => $b, 'c' => $c, 'd' => $d, 'e' => $e]);
    }

    public function save_spm(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;

        $form_data = array(
            'spm_description' => $request->uraian,
            'unit_id' => $request->satuan,
            'value' => 0,
            'type_user' => $request->type_user,
            'validate' => 0,
            'validate_final' => 0
        );

        if ($request->type_user === '0') {
            // insert data untuk val kabupaten untuk user type Perangkat Daerah
            $id_kec = null;
            $id_kel = null;
        } elseif ($request->type_user === '1') {
            // insert data untuk val kabupaten untuk user type Kecamatan
            $id_kec = 0;
            $id_kel = null;
        } else {
            // insert data untuk val kabupaten untuk user type Kelurahan
            $id_kec = null;
            $id_kel = 0;
        }

        if ($id) {
            $spm = spmModel::find($id);
            $tahun = $request->tahun;
            // Cek type_user spm Perangkat Daerah
            if ($spm->type_user === 0) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($spm_trans) > 0) {
                        foreach ($spm_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda spm Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_spmtrans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_spmtrans) > 0) {
                                    spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    spmtransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('id_tahun', $del)->update(array('check_active' => 0));

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_spmtrans_tahun) > 0) {
                                    $check_spmtrans_tahun->update(array('check_active' => 1));
                                } else {
                                    spmtransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } elseif ($request->type_user === '1') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                    }
                } else {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();

                    if ($request->tahun) {

                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_kec' => null,
                                'id_kel' => 0,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $spm->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                spmtransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }

            // Cek type_user spm Kecamatan
            elseif ($spm->type_user === 1) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_spm' => $spm->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($spm_trans) > 0) {
                        foreach ($spm_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda spm Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                // foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $spm->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );
                                echo $array_edit[$i] . "</br>";
                                // }

                                $check_spmtrans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_spmtrans) > 0) {
                                    spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    spmtransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_spmtrans_tahun) > 0) {
                                    spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    spmtransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } else {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();

                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_spm' => $spm->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                        // jika user type kelurahan insert spm trans untuk sum kecamatan
                        if ($request->type_user === '2') {
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_spm' => $spm->id,
                                        'id_spm' => $spm->id,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    spmtransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }
            }

            // Cek type_user spm Kelurahan
            else {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_spm' => $spm->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_spm' => $spm->id,
                                'id_spm' => $spm->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            spmtransModel::create($data);
                        }
                    }
                } else {
                    $spm_trans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($spm_trans) > 0) {
                        foreach ($spm_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda spm Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $spm->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                $check_spmtrans = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_spmtrans) > 0) {
                                    spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    spmtransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_spm' => $spm->id,
                                            'id_spm' => $spm->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $array_edit[$i],
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        spmtransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_spmtrans_tahun) > 0) {
                                    spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    spmtransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_spm' => $spm->id,
                                            'id_spm' => $spm->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $del,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        spmtransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_spmtrans_tahun = spmtransModel::where('id_spm', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                }
            }

            spmModel::whereid($id)->update($form_data);

            return redirect('/master-spm')->with('success', 'Data Master spm berhasil diubah!');
        } else {
            $spm = spmModel::create($form_data);
            $tahun = $request->tahun_id;
            if ($request->tahun_id) {
                if (count($tahun) > 0) {
                    foreach ($tahun as $thn) {
                        $data = array(
                            'user_id' => $user->id,
                            'id_spm' => $spm->id,
                            'id_spm' => $spm->id,
                            'id_kec' => $id_kec,
                            'id_kel' => $id_kel,
                            'id_tahun' => $thn,
                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                            'value' => '',
                            'check_active' => '1',
                            'status' => 'aktif'
                        );

                        spmtransModel::create($data);
                    }
                    // jika user type kelurahan insert spm trans untuk sum kecamatan
                    if ($request->type_user === '2') {
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_spm' => $spm->id,
                                    'id_spm' => $spm->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                spmtransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }


            return redirect('/master-spm')->with('success', 'Data Master spm berhasil ditambah!');
        }

        return redirect('/master-spm')->with('success', 'Data Master spm berhasil disimpan!');
    }

    public function edit(Request $request)
    {
        $spm = spmModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();

        $tahun = tahunModel::whereBetween('id', [$dari->id, $sampai->id])->get();
        // if($request->get('kec') != ''){
        //     $kec = $request->kec;
        // }
        // else {
        //     $kec = null;
        // }
        foreach ($tahun as $thn) {
            // cek pegampu
            if ($spm->type_user === 0) {
                $spmtrans = spmtransModel::where('id_spm', $request->get('id'))->where('id_tahun', $thn->id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
            } elseif ($spm->type_user === 1) {
                $spmtrans = spmtransModel::where('id_spm', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            } else {
                $spmtrans = spmtransModel::where('id_spm', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            }


            if (count((array)$spmtrans) > 0) {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => $spmtrans->value);
            } else {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => '');
            }
        }


        return view('spm.spm_edit', ['id_kec' => $request->get('kec'), 'spm' => $spm, 'input' => $input, 'dari' => $dari->id, 'id_kel' => $request->get('kel'), 'sampai' => $sampai->id]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;
        $id_kec = $request->id_kec;

        $id_tahun = $request->id_tahun;
        $value = $request->val;

        // cek type_user atau pengampu spm
        $spm = spmModel::find($id);

        // jika pengampu Perangkat Daerah
        if ($spm->type_user === 0) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $spmtrans = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel spmtrans
                    if (count((array)$spmtrans) > 0) {
                        if ($spmtrans->value === $value[$index]) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update spmtrans jika value input berbeda
                            $spmtrans->status = 'non-aktif';
                            $spmtrans->save();
                            spmtransModel::create($form_data);

                            $spm = spmModel::find($id);
                            $spm->validate = 0;
                            $spm->validate_final = 0;
                            $spm->save();
                        }
                    }
                } else {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $spmtrans = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel spmtrans
                    if (count((array)$spmtrans) > 0) {
                        $cek_val = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update spmtrans jika value input berbeda
                            $spmtrans->status = 'non-aktif';
                            $spmtrans->save();
                            spmtransModel::create($form_data);
                        }
                    }
                }
            }
        }
        // jika pengampu Kecamatan
        elseif ($spm->type_user === 1) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $spmtrans_kab = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', 0)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel spmtrans
                    if (count((array)$spmtrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $spmtrans_kec = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$spmtrans_kec) > 0) {
                            // echo $spmtrans_kec->value;
                            if ($spmtrans_kec->value === $value[$index]) {
                                // Untuk value yg sama tidak diberi aksi update
                            } else {
                                // Update spmtrans jika value input berbeda
                                $spmtrans_kec->status = 'non-aktif';
                                $spmtrans_kec->save();
                                spmtransModel::create($form_data);

                                // jika data kabupaten ada maka update status + create
                                $sum = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                if ($spmtrans_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $spmtrans_kab->status = 'non-aktif';
                                    $spmtrans_kab->save();

                                    $data_kab = array(
                                        'id_spm' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => 1,
                                        'status' => 'aktif'
                                    );
                                    spmtransModel::create($data_kab);

                                    $spm = spmModel::find($id);
                                    $spm->validate = 0;
                                    $spm->validate_final = 0;
                                    $spm->save();
                                }
                                // }

                                $spm = spmModel::find($id);
                                $spm->validate = 0;
                                $spm->validate_final = 0;
                                $spm->save();
                            }
                            // echo dd($spmtrans_kec);
                        } else {
                            spmtransModel::create($form_data);

                            $sum = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                            if ($spmtrans_kab->value === $sum) {
                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                            } else {
                                $spmtrans_kab->status = 'non-aktif';
                                $spmtrans_kab->save();

                                $data_kab = array(
                                    'id_spm' => $id,
                                    'id_kec' => 0,
                                    'id_kel' => null,
                                    'user_id' => $user->id,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => $sum,
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );
                                spmtransModel::create($data_kab);

                                $spm = spmModel::find($id);
                                $spm->validate = 0;
                                $spm->validate_final = 0;
                                $spm->save();
                            }
                        }
                    }
                } else {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $spmtrans = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', $id_kec)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel spmtrans
                    if (count((array)$spmtrans) > 0) {
                        $cek_val = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update spmtrans jika value input berbeda
                            $spmtrans->status = 'non-aktif';
                            $spmtrans->save();
                            spmtransModel::create($form_data);

                            // cek spm trans kabupaten
                            $cek_kab = spmtransModel::where('id_spm', $id)->where('id_kec', '0')->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kab) > 0) {
                                // jika data kabupaten ada maka update status + create
                                $sum = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                if ($cek_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kab->status = 'non-aktif';
                                    $cek_kab->save();

                                    $data_kab = array(
                                        'id_spm' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => 0,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    spmtransModel::create($data_kab);

                                    $spm = spmModel::find($id);
                                    $spm->validate = 0;
                                    $spm->validate_final = 0;
                                    $spm->save();
                                }
                            }
                            // else {
                            //     // buat baru
                            //     $sum = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                            //     $data_kab = array(
                            //         'id_spm' => $id,
                            //         'id_kec' => 0,
                            //         'id_kel' => 0,
                            //         'user_id' => $user->id,
                            //         'id_tahun' => $thn,
                            //         'tgl_pengisian' => date('Y-m-d H:i:s'),
                            //         'value' => $sum,
                            //         'status' => 'aktif'
                            //     );

                            //     spmtransModel::create($data_kab);

                            //     $spm = spmModel::find($id);
                            //     $spm->validate = 0;
                            //     $spm->validate_final = 0;
                            //     $spm->save();
                            // }
                        }
                    }
                }
            }
        }
        // jika pengampu Kelurahan
        else {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $spmtrans_kab = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel spmtrans
                    if (count((array)$spmtrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $spmtrans_kec = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$spmtrans_kec) > 0) {
                            // cek data kel per id_kel
                            $spmtrans_kel = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$spmtrans_kel) > 0) {
                                // echo $spmtrans_kel->value;
                                if ($spmtrans_kel->value === $value[$index]) {
                                    // jika value sama maka tidak ada aksi
                                } else {
                                    // Update spmtrans jika value input berbeda
                                    $spmtrans_kel->status = 'non-aktif';
                                    $spmtrans_kel->save();
                                    spmtransModel::create($form_data);
                                    //=======================================================================
                                    $sum_kec = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                    if ($spmtrans_kec->value === $sum_kec) {
                                        // jika sama tidak ada update status+create

                                    } else {
                                        $spmtrans_kec->status = 'non-aktif';
                                        $spmtrans_kec->save();

                                        $data_kec = array(
                                            'id_spm' => $id,
                                            'id_kec' => $id_kec,
                                            'id_kel' => null,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kec,
                                            'check_active' => 1,
                                            'status' => 'aktif'
                                        );
                                        spmtransModel::create($data_kec);

                                        $spm = spmModel::find($id);
                                        $spm->validate = 0;
                                        $spm->validate_final = 0;
                                        $spm->save();

                                        // sum data kelurahan
                                        $sum_kab = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                        if ($spmtrans_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $spmtrans_kab->status = 'non-aktif';
                                            $spmtrans_kab->save();
                                            spmtransModel::create($form_data);

                                            $data_kab = array(
                                                'id_spm' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => 1,
                                                'status' => 'aktif'
                                            );
                                            spmtransModel::create($data_kab);

                                            $spm = spmModel::find($id);
                                            $spm->validate = 0;
                                            $spm->validate_final = 0;
                                            $spm->save();
                                        }
                                    }

                                    // }

                                    $spm = spmModel::find($id);
                                    $spm->validate = 0;
                                    $spm->validate_final = 0;
                                    $spm->save();
                                }
                            } else {
                                // create data kelurahan
                                spmtransModel::create($form_data);

                                $sum_kec = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                if ($spmtrans_kec->value === $sum_kec) {
                                } else {
                                    $spmtrans_kec->status = "non-aktif";
                                    $spmtrans_kec->save();

                                    $data_kec = array(
                                        'id_spm' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    spmtransModel::create($data_kec);

                                    $spm->validate = 0;
                                    $spm->validate_final = 0;
                                    $spm->save();

                                    $sum_kab = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');

                                    if ($spmtrans_kab->value === $sum_kab) {
                                    } else {
                                        $spmtrans_kab->status = "non-aktif";
                                        $spmtrans_kab->save();

                                        $data_kab = array(
                                            'id_spm' => $id,
                                            'id_kec' => null,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kab,
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );
                                        spmtransModel::create($data_kab);

                                        $spm->validate = 0;
                                        $spm->validate_final = 0;
                                        $spm->save();
                                    }
                                }
                            }
                            // echo dd($spmtrans_kec);
                        }
                    }
                } else {
                    $form_data = array(
                        'id_spm' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $spmtrans = spmtransModel::where('id_tahun', $thn)->where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel spmtrans
                    if (count((array)$spmtrans) > 0) {
                        if ($spmtrans->value === '') {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update spmtrans jika value input berbeda
                            $spmtrans->status = 'non-aktif';
                            $spmtrans->save();
                            spmtransModel::create($form_data);

                            // cek spmtrans kecamatan
                            $cek_kec = spmtransModel::where('id_spm', $id)->where('id_kec', $id_kec)->where('id_kel', null)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kec) > 0) {
                                // jika data kecamatan ada maka update status + create
                                $sum_kec = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');
                                if ($cek_kec->value === $sum_kec) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kec->status = 'non-aktif';
                                    $cek_kec->save();

                                    $data_kec = array(
                                        'id_spm' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    spmtransModel::create($data_kec);

                                    $spm = spmModel::find($id);
                                    $spm->validate = 0;
                                    $spm->validate_final = 0;
                                    $spm->save();

                                    // cek spm trans kabupaten
                                    $cek_kab = spmtransModel::where('id_spm', $id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum_kab = spmtransModel::where('id_spm', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->where('id_kel', null)->sum('value');
                                        if ($cek_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_spm' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );
                                            spmtransModel::create($data_kab);

                                            $spm = spmModel::find($id);
                                            $spm->validate = 0;
                                            $spm->validate_final = 0;
                                            $spm->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return back()->with('success', 'Data spm berhasil disimpan!');
    }

    public function history(Request $request)
    {
        $spm = spmModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();
        $spmtrans = spmtransModel::where('id_spm', $request->get('id'))->where('id_kec', $request->get('kec'))->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();

        return view('spm.spm_history', ['spm' => $spm, 'spmtrans' => $spmtrans]);
    }

    public function delete_spm($id)
    {
        spmtransModel::where('id_spm', $id)->delete();
        spmModel::whereId($id)->delete();

        return back()->with('success', 'Data Master SPM berhasil dihapus!');
    }

    public function validasi($id)
    {
        $spm = spmModel::find($id);
        $spm->validate = 1;
        $spm->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_all()
    {
        spmModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));

        return back()->with('success', 'Data berhasil divalidasi.');
    }

    public function import_spm(Request $request)
    {
        $id_kec = $request->kec;
        $nama_file = rand() . $request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new SPMImport($id_kec), storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);
        // Storage::deleteDirectory('public/tmp_import');

        return back()->with('success', 'File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/SPM.xlsx');
    }

    public function excel(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = spmModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = spmModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = spmModel::where('type_user', 1)->get();
            } else {
                $data = spmModel::where('type_user', 2)->get();
            }
        }
        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row->type_user === 0) {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row->type_user === 1) {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$spmtrans) > 0) {
                        $val = str_replace(".", "", $spmtrans->value);
                        if (is_numeric($val)) {
                            if (strpos($spmtrans->value, ".") === true) {
                                $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                $value = number_format($spmtrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($spmtrans->value, 0, '', '.');
                            }
                        } else if ($spmtrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $spmtrans->value;
                        }
                    } else {
                        $value = '-';
                    }
                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                $spm[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$spmtrans) > 0) {
                            $val = str_replace(".", "", $spmtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($spmtrans->value, ".") === true) {
                                    $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                    $value = number_format($spmtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($spmtrans->value, 0, '', '.');
                                }
                            } else if ($spmtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $spmtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                    $spm[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$spmtrans) > 0) {
                            $val = str_replace(".", "", $spmtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($spmtrans->value, ".") === true) {
                                    $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                    $value = number_format($spmtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($spmtrans->value, 0, '', '.');
                                }
                            } else if ($spmtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $spmtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                    $spm[] = $array_data + $tahun;
                }
            }
        }

        $data = array('spm' => $spm, 'tahun' => $thn);

        return Excel::download(new SPMExport($data), 'SPM.xlsx');
    }

    public function pdf(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = spmModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = spmModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = spmModel::where('type_user', 1)->get();
            } else {
                $data = spmModel::where('type_user', 2)->get();
            }
        }
        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row->type_user === 0) {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row->type_user === 1) {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$spmtrans) > 0) {
                        $val = str_replace(".", "", $spmtrans->value);
                        if (is_numeric($val)) {
                            if (strpos($spmtrans->value, ".") === true) {
                                $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                $value = number_format($spmtrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($spmtrans->value, 0, '', '.');
                            }
                        } else if ($spmtrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $spmtrans->value;
                        }
                    } else {
                        $value = '-';
                    }
                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                $spm[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$spmtrans) > 0) {
                            $val = str_replace(".", "", $spmtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($spmtrans->value, ".") === true) {
                                    $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                    $value = number_format($spmtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($spmtrans->value, 0, '', '.');
                                }
                            } else if ($spmtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $spmtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                    $spm[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $spmtrans = spmtransModel::where('id_spm', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$spmtrans) > 0) {
                            $val = str_replace(".", "", $spmtrans->value);
                            if (is_numeric($val)) {
                                if (strpos($spmtrans->value, ".") === true) {
                                    $decimal = strlen(substr($spmtrans->value, strrpos($spmtrans->value, '.') + 1));
                                    $value = number_format($spmtrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($spmtrans->value, 0, '', '.');
                                }
                            } else if ($spmtrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $spmtrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->spm_description, 'satuan' => $unit->unit);
                    $spm[] = $array_data + $tahun;
                }
            }
        }

        $data = array('spm' => $spm, 'tahun' => $thn);

        view()->share('data', $data);
        $pdf = PDF::loadView('exports.spm_export', $data);

        return $pdf->stream('SPM.pdf');
    }

    public function excel_master()
    {
        return Excel::download(new MasterSPMExport, 'MasterSPM.xlsx');
    }

    public function pdf_master(Request $request)
    {
        $spm = spmModel::all();

        view()->share('spm', $spm);
        $pdf = PDF::loadView('master_export.spm_export', $spm);

        return $pdf->stream('MasterSPM.pdf');
    }

    public function master_spm(Request $request)
    {
        if ($request->ajax()) {
            $data = spmModel::latest()->get();

            return Datatables::of($data)
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . url("change-master-spm/" . $row->id) . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_spm" data-url="' . url("delete-spm/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        return view('spm.master_spm', ['unit' => $unit]);
    }

    public function add_spm()
    {
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        return view('spm.spm_add', ['tahun' => $tahun, 'unit' => $unit]);
    }

    public function change_spm($id)
    {
        $spm = spmModel::find($id);
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        if ($spm->type_user === 0) {
            $spm_tahun = spmtransModel::where('id_spm', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', null)->get();
        } elseif ($spm->type_user === 1) {
            $spm_tahun = spmtransModel::where('id_spm', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', 0)->where('id_kel', null)->get();
        } else {
            $spm_tahun = spmtransModel::where('id_spm', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', 0)->get();
        }
        // echo dd($spm_tahun);
        return view('spm.master_spm_edit', ['spm' => $spm, 'unit' => $unit, 'tahun' => $tahun, 'spm_tahun' => $spm_tahun]);
    }
}
