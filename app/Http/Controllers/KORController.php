<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\korModel;
use \App\kortransModel;
use \App\unitsModel;
use \App\tahunModel;
use \App\kecamatanModel;
use \App\kelurahanModel;
use App\Imports\KORImport;
use App\Exports\KORExport;
use App\Exports\MasterKORExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;
use Auth;

class KORController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $thn = tahunModel::all();
            if (auth()->user()->role === '0') {
                $data = korModel::all();
            } else {
                if (auth()->user()->type_user === 0) {
                    $data = korModel::where('type_user', 0)->get();
                } elseif (auth()->user()->type_user === 1) {
                    $data = korModel::where('type_user', 1)->get();
                } else {
                    $data = korModel::where('type_user', 2)->get();
                }
            }

            $table = Datatables::of($data);
            if ($request->get('kec') === "Kabupaten") {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                foreach ($thn as $a) {
                    $th = $a->id;
                    $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                        if ($row->type_user === 0) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$kortrans) > 0) {
                            $val = str_replace(".", "", $kortrans->value);
                            if (is_numeric($val)) {
                                if (strpos($kortrans->value, ".") === true) {
                                    $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                    $value = number_format($kortrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($kortrans->value, 0, '', '.');
                                }
                            } else if ($kortrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $kortrans->value;
                            }
                        } else {
                            $value = '-';
                        }

                        return $value;
                    });
                }

                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';
                    if ($row->type_user === 0) {
                        if (auth()->user()->role === '1') {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-kor/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif (auth()->user()->role === '2' || auth()->user()->role === '0') {
                            $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                        } else {
                        }
                    }
                    // if ($row->validate === 0 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-kor/" . $row->id) . '"><span class="clip-notification"></span></a>';
                    // } else if ($row->validate === 1 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                    // } else {
                    //     $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                    // }
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['kor_name']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            } else {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                if ($request->get('kel')) {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                } else {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                }


                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '"  data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';


                    if (auth()->user()->role === '1') {
                        if ($row->type_user === 0) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-kor/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif ($row->type_user === 1) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-kor/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } else {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-kor/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        }
                    } else {
                        if ($row->type_user === 0) {
                            if ($request->kel) {
                            } else {
                                if ($request->get('kec')) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } elseif ($row->type_user === 1) {
                            if ($request->get('kec') != 0) {
                                if ($request->kel) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } else {
                            if ($request->kel) {
                                $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                            }
                        }
                    }
                    // $btn = $btn . ' <a href="#myModalEdit" data-toggle="modal" data-id="' . $row->id . '" data-kor_name="' . $row->kor_name . '" data-satuan="' . $row->unit_id . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    // $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';

                    // $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-kor/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['kor_name']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            }
        }
        $unit = unitsModel::all();
        $thn = tahunModel::all();

        if (auth()->user()->role === '0') {
            $kecamatan = kecamatanModel::all();
            $kelurahan = kelurahanModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $kecamatan = kecamatanModel::all();
                $kelurahan = kelurahanModel::all();
            } elseif (auth()->user()->type_user === 1) {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::all();
            } else {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::find(auth()->user()->id_kel);
            }
        }




        $a = date('Y') - 4;
        $b = date('Y') - 3;
        $c = date('Y') - 2;
        $d = date('Y') - 1;
        $e = date('Y');

        $column = 2 + count($thn) + 1;

        $kec = "Kabupaten";
        $kel = '';

        return view('kor.kor_vw', ['unit' => $unit, 'thn' => $thn, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan, 'kec' => $kec, 'kel' => $kel, 'column' => $column, 'a' => $a, 'b' => $b, 'c' => $c, 'd' => $d, 'e' => $e]);
    }

    public function save_kor(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;

        $form_data = array(
            'kor_name' => $request->uraian,
            'unit_id' => $request->satuan,
            'value' => 0,
            'type_user' => $request->type_user,
            'validate' => 0,
            'validate_final' => 0
        );

        if ($request->type_user === '0') {
            // insert data untuk val kabupaten untuk user type Perangkat Daerah
            $id_kec = null;
            $id_kel = null;
        } elseif ($request->type_user === '1') {
            // insert data untuk val kabupaten untuk user type Kecamatan
            $id_kec = 0;
            $id_kel = null;
        } else {
            // insert data untuk val kabupaten untuk user type Kelurahan
            $id_kec = null;
            $id_kel = 0;
        }

        if ($id) {
            $kor = korModel::find($id);
            $tahun = $request->tahun;
            // Cek type_user KOR Perangkat Daerah
            if ($kor->type_user === 0) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($kor_trans) > 0) {
                        foreach ($kor_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda Kor Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_kortrans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_kortrans) > 0) {
                                    kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    kortransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('id_tahun', $del)->update(array('check_active' => 0));

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_kortrans_tahun) > 0) {
                                    $check_kortrans_tahun->update(array('check_active' => 1));
                                } else {
                                    kortransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } elseif ($request->type_user === '1') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                    }
                } else {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();

                    if ($request->tahun) {

                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kec' => null,
                                'id_kel' => 0,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $kor->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                kortransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }

            // Cek type_user KOR Kecamatan
            elseif ($kor->type_user === 1) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kor' => $kor->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($kor_trans) > 0) {
                        foreach ($kor_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda Kor Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                // foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $kor->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );
                                echo $array_edit[$i] . "</br>";
                                // }

                                $check_kortrans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_kortrans) > 0) {
                                    kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    kortransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_kortrans_tahun) > 0) {
                                    kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    kortransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } else {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();

                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kor' => $kor->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                        // jika user type kelurahan insert kor trans untuk sum kecamatan
                        if ($request->type_user === '2') {
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_kor' => $kor->id,
                                        'id_kor' => $kor->id,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    kortransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }
            }

            // Cek type_user KOR Kelurahan
            else {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kor' => $kor->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_kor' => $kor->id,
                                'id_kor' => $kor->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            kortransModel::create($data);
                        }
                    }
                } else {
                    $kor_trans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($kor_trans) > 0) {
                        foreach ($kor_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda Kor Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $kor->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                $check_kortrans = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_kortrans) > 0) {
                                    kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    kortransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_kor' => $kor->id,
                                            'id_kor' => $kor->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $array_edit[$i],
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        kortransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_kortrans_tahun) > 0) {
                                    kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    kortransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_kor' => $kor->id,
                                            'id_kor' => $kor->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $del,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        kortransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_kortrans_tahun = kortransModel::where('id_kor', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                }
            }

            korModel::whereid($id)->update($form_data);

            return redirect('/master-kor')->with('success', 'Data Master KOR berhasil diubah!');
        } else {
            $kor = korModel::create($form_data);
            $tahun = $request->tahun_id;
            if ($request->tahun_id) {
                if (count($tahun) > 0) {
                    foreach ($tahun as $thn) {
                        $data = array(
                            'user_id' => $user->id,
                            'id_kor' => $kor->id,
                            'id_kor' => $kor->id,
                            'id_kec' => $id_kec,
                            'id_kel' => $id_kel,
                            'id_tahun' => $thn,
                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                            'value' => '',
                            'check_active' => '1',
                            'status' => 'aktif'
                        );

                        kortransModel::create($data);
                    }
                    // jika user type kelurahan insert kor trans untuk sum kecamatan
                    if ($request->type_user === '2') {
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_kor' => $kor->id,
                                    'id_kor' => $kor->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                kortransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }


            return redirect('/master-kor')->with('success', 'Data Master KOR berhasil ditambah!');
        }

        return redirect('/master-kor')->with('success', 'Data Master KOR berhasil disimpan!');
    }

    public function edit(Request $request)
    {
        $kor = korModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();

        $tahun = tahunModel::whereBetween('id', [$dari->id, $sampai->id])->get();
        // if($request->get('kec') != ''){
        //     $kec = $request->kec;
        // }
        // else {
        //     $kec = null;
        // }
        foreach ($tahun as $thn) {
            // cek pegampu
            if ($kor->type_user === 0) {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_tahun', $thn->id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
            } elseif ($kor->type_user === 1) {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            } else {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            }


            if (count((array)$kortrans) > 0) {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => $kortrans->value);
            } else {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => '');
            }
        }


        return view('kor.kor_edit', ['id_kec' => $request->get('kec'), 'kor' => $kor, 'input' => $input, 'dari' => $dari->id, 'id_kel' => $request->get('kel'), 'sampai' => $sampai->id]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;
        $id_kec = $request->id_kec;

        $id_tahun = $request->id_tahun;
        $value = $request->val;

        // cek type_user atau pengampu kor
        $kor = korModel::find($id);

        // jika pengampu Perangkat Daerah
        if ($kor->type_user === 0) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $kortrans = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel kortrans
                    if (count((array)$kortrans) > 0) {
                        if ($kortrans->value === $value[$index]) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update kortrans jika value input berbeda
                            $kortrans->status = 'non-aktif';
                            $kortrans->save();
                            kortransModel::create($form_data);

                            $kor = korModel::find($id);
                            $kor->validate = 0;
                            $kor->validate_final = 0;
                            $kor->save();
                        }
                    }
                } else {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $kortrans = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel kortrans
                    if (count((array)$kortrans) > 0) {
                        $cek_val = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update kortrans jika value input berbeda
                            $kortrans->status = 'non-aktif';
                            $kortrans->save();
                            kortransModel::create($form_data);
                        }
                    }
                }
            }
        }
        // jika pengampu Kecamatan
        elseif ($kor->type_user === 1) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $kortrans_kab = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', 0)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel kortrans
                    if (count((array)$kortrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $kortrans_kec = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$kortrans_kec) > 0) {
                            // echo $kortrans_kec->value;
                            if ($kortrans_kec->value === $value[$index]) {
                                // Untuk value yg sama tidak diberi aksi update
                            } else {
                                // Update kortrans jika value input berbeda
                                $kortrans_kec->status = 'non-aktif';
                                $kortrans_kec->save();
                                kortransModel::create($form_data);

                                // jika data kabupaten ada maka update status + create
                                $sum = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                if ($kortrans_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $kortrans_kab->status = 'non-aktif';
                                    $kortrans_kab->save();

                                    $data_kab = array(
                                        'id_kor' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => 1,
                                        'status' => 'aktif'
                                    );
                                    kortransModel::create($data_kab);

                                    $kor = korModel::find($id);
                                    $kor->validate = 0;
                                    $kor->validate_final = 0;
                                    $kor->save();
                                }
                                // }

                                $kor = korModel::find($id);
                                $kor->validate = 0;
                                $kor->validate_final = 0;
                                $kor->save();
                            }
                            // echo dd($kortrans_kec);
                        } else {
                            kortransModel::create($form_data);

                            $sum = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                            if ($kortrans_kab->value === $sum) {
                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                            } else {
                                $kortrans_kab->status = 'non-aktif';
                                $kortrans_kab->save();

                                $data_kab = array(
                                    'id_kor' => $id,
                                    'id_kec' => 0,
                                    'id_kel' => null,
                                    'user_id' => $user->id,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => $sum,
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );
                                kortransModel::create($data_kab);

                                $kor = korModel::find($id);
                                $kor->validate = 0;
                                $kor->validate_final = 0;
                                $kor->save();
                            }
                        }
                    }
                } else {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $kortrans = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', $id_kec)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel kortrans
                    if (count((array)$kortrans) > 0) {
                        $cek_val = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update kortrans jika value input berbeda
                            $kortrans->status = 'non-aktif';
                            $kortrans->save();
                            kortransModel::create($form_data);

                            // cek kor trans kabupaten
                            $cek_kab = kortransModel::where('id_kor', $id)->where('id_kec', '0')->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kab) > 0) {
                                // jika data kabupaten ada maka update status + create
                                $sum = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                if ($cek_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kab->status = 'non-aktif';
                                    $cek_kab->save();

                                    $data_kab = array(
                                        'id_kor' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => 0,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    kortransModel::create($data_kab);

                                    $kor = korModel::find($id);
                                    $kor->validate = 0;
                                    $kor->validate_final = 0;
                                    $kor->save();
                                }
                            }
                            // else {
                            //     // buat baru
                            //     $sum = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                            //     $data_kab = array(
                            //         'id_kor' => $id,
                            //         'id_kec' => 0,
                            //         'id_kel' => 0,
                            //         'user_id' => $user->id,
                            //         'id_tahun' => $thn,
                            //         'tgl_pengisian' => date('Y-m-d H:i:s'),
                            //         'value' => $sum,
                            //         'status' => 'aktif'
                            //     );

                            //     kortransModel::create($data_kab);

                            //     $kor = korModel::find($id);
                            //     $kor->validate = 0;
                            //     $kor->validate_final = 0;
                            //     $kor->save();
                            // }
                        }
                    }
                }
            }
        }
        // jika pengampu Kelurahan
        else {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $kortrans_kab = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel kortrans
                    if (count((array)$kortrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $kortrans_kec = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$kortrans_kec) > 0) {
                            // cek data kel per id_kel
                            $kortrans_kel = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$kortrans_kel) > 0) {
                                // echo $kortrans_kel->value;
                                if ($kortrans_kel->value === $value[$index]) {
                                    // jika value sama maka tidak ada aksi
                                } else {
                                    // Update kortrans jika value input berbeda
                                    $kortrans_kel->status = 'non-aktif';
                                    $kortrans_kel->save();
                                    kortransModel::create($form_data);
                                    //=======================================================================
                                    $sum_kec = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                    if ($kortrans_kec->value === $sum_kec) {
                                        // jika sama tidak ada update status+create

                                    } else {
                                        $kortrans_kec->status = 'non-aktif';
                                        $kortrans_kec->save();

                                        $data_kec = array(
                                            'id_kor' => $id,
                                            'id_kec' => $id_kec,
                                            'id_kel' => null,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kec,
                                            'check_active' => 1,
                                            'status' => 'aktif'
                                        );
                                        kortransModel::create($data_kec);

                                        $kor = korModel::find($id);
                                        $kor->validate = 0;
                                        $kor->validate_final = 0;
                                        $kor->save();

                                        // sum data kelurahan
                                        $sum_kab = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                        if ($kortrans_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $kortrans_kab->status = 'non-aktif';
                                            $kortrans_kab->save();
                                            kortransModel::create($form_data);

                                            $data_kab = array(
                                                'id_kor' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => 1,
                                                'status' => 'aktif'
                                            );
                                            kortransModel::create($data_kab);

                                            $kor = korModel::find($id);
                                            $kor->validate = 0;
                                            $kor->validate_final = 0;
                                            $kor->save();
                                        }
                                    }

                                    // }

                                    $kor = korModel::find($id);
                                    $kor->validate = 0;
                                    $kor->validate_final = 0;
                                    $kor->save();
                                }
                            } else {
                                // create data kelurahan
                                kortransModel::create($form_data);

                                $sum_kec = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                if ($kortrans_kec->value === $sum_kec) {
                                } else {
                                    $kortrans_kec->status = "non-aktif";
                                    $kortrans_kec->save();

                                    $data_kec = array(
                                        'id_kor' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    kortransModel::create($data_kec);

                                    $kor->validate = 0;
                                    $kor->validate_final = 0;
                                    $kor->save();

                                    $sum_kab = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');

                                    if ($kortrans_kab->value === $sum_kab) {
                                    } else {
                                        $kortrans_kab->status = "non-aktif";
                                        $kortrans_kab->save();

                                        $data_kab = array(
                                            'id_kor' => $id,
                                            'id_kec' => null,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kab,
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );
                                        kortransModel::create($data_kab);

                                        $kor->validate = 0;
                                        $kor->validate_final = 0;
                                        $kor->save();
                                    }
                                }
                            }
                            // echo dd($kortrans_kec);
                        }
                    }
                } else {
                    $form_data = array(
                        'id_kor' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $kortrans = kortransModel::where('id_tahun', $thn)->where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel kortrans
                    if (count((array)$kortrans) > 0) {
                        if ($kortrans->value === '') {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update kortrans jika value input berbeda
                            $kortrans->status = 'non-aktif';
                            $kortrans->save();
                            kortransModel::create($form_data);

                            // cek kortrans kecamatan
                            $cek_kec = kortransModel::where('id_kor', $id)->where('id_kec', $id_kec)->where('id_kel', null)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kec) > 0) {
                                // jika data kecamatan ada maka update status + create
                                $sum_kec = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');
                                if ($cek_kec->value === $sum_kec) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kec->status = 'non-aktif';
                                    $cek_kec->save();

                                    $data_kec = array(
                                        'id_kor' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    kortransModel::create($data_kec);

                                    $kor = korModel::find($id);
                                    $kor->validate = 0;
                                    $kor->validate_final = 0;
                                    $kor->save();

                                    // cek kor trans kabupaten
                                    $cek_kab = kortransModel::where('id_kor', $id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum_kab = kortransModel::where('id_kor', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->where('id_kel', null)->sum('value');
                                        if ($cek_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_kor' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );
                                            kortransModel::create($data_kab);

                                            $kor = korModel::find($id);
                                            $kor->validate = 0;
                                            $kor->validate_final = 0;
                                            $kor->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return back()->with('success', 'Data KOR berhasil disimpan!');
    }

    public function history(Request $request)
    {
        $kor = korModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();
        if ($request->get('kec') === 'Kabupaten') {
            if ($kor->type_user === 0) {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
            } elseif ($kor->type_user === 1) {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', 0)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
            } else {
                $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
            }
        } else {
            if ($request->get('kel') != '0') {
                if ($kor->type_user === 0) {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                } elseif ($kor->type_user === 1) {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                } else {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                }
            } else {
                if ($kor->type_user === 0) {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                } elseif ($kor->type_user === 1) {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                } else {
                    $kortrans = kortransModel::where('id_kor', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();
                }
            }
        }

        // $kortrans = kortransModel::distinct()->where('id_kor', $id)->get(['value']);

        return view('kor.kor_history', ['kor' => $kor, 'kortrans' => $kortrans]);
    }

    public function delete_kor($id)
    {
        kortransModel::where('id_kor', $id)->delete();
        korModel::whereId($id)->delete();

        return back()->with('success', 'Data Master KOR berhasil dihapus!');
    }

    public function validasi($id)
    {
        $kor = korModel::find($id);
        $kor->validate = 1;
        $kor->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_all()
    {
        korModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));

        return back()->with('success', 'Data berhasil divalidasi.');
    }

    public function import_kor(Request $request)
    {
        $id_kec = $request->kec;
        $nama_file = $request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new KORImport($id_kec), storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);

        return back()->with('success', 'File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/KOR.xlsx');
    }

    public function excel(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = korModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = korModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = korModel::where('type_user', 1)->get();
            } else {
                $data = korModel::where('type_user', 2)->get();
            }
        }
        foreach($data as $row){
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                        if ($row->type_user === 0) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$kortrans) > 0) {
                            $val = str_replace(".", "", $kortrans->value);
                            if (is_numeric($val)) {
                                if (strpos($kortrans->value, ".") === true) {
                                    $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                    $value = number_format($kortrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($kortrans->value, 0, '', '.');
                                }
                            } else if ($kortrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $kortrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                $kor[] = $array_data + $tahun;
            } else {
                    $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }
                            $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                    $kor[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }
                            $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                    $kor[] = $array_data + $tahun;
                }
            }
        }

        $data = array('kor' => $kor, 'tahun' => $thn);

        return Excel::download(new KORExport($data), 'KOR.xlsx');
    }

    public function pdf(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = korModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = korModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = korModel::where('type_user', 1)->get();
            } else {
                $data = korModel::where('type_user', 2)->get();
            }
        }
        foreach($data as $row){
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                        if ($row->type_user === 0) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$kortrans) > 0) {
                            $val = str_replace(".", "", $kortrans->value);
                            if (is_numeric($val)) {
                                if (strpos($kortrans->value, ".") === true) {
                                    $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                    $value = number_format($kortrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($kortrans->value, 0, '', '.');
                                }
                            } else if ($kortrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $kortrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                $kor[] = $array_data + $tahun;
            } else {
                    $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }
                            $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                    $kor[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                            if ($row->type_user === 0) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $kortrans = kortransModel::where('id_kor', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$kortrans) > 0) {
                                $val = str_replace(".", "", $kortrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($kortrans->value, ".") === true) {
                                        $decimal = strlen(substr($kortrans->value, strrpos($kortrans->value, '.') + 1));
                                        $value = number_format($kortrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($kortrans->value, 0, '', '.');
                                    }
                                } else if ($kortrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $kortrans->value;
                                }
                            } else {
                                $value = '-';
                            }
                            $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->kor_name, 'satuan' => $unit->unit);
                    $kor[] = $array_data + $tahun;
                }
            }
        }

        $data = array('kor' => $kor, 'tahun' => $thn);

        view()->share('data', $data);
        $pdf = PDF::loadView('exports.kor_export', $data);

        return $pdf->stream('KOR.pdf');
    }

    public function excel_master()
    {
        return Excel::download(new MasterKORExport, 'MasterKOR.xlsx');
    }

    public function pdf_master(Request $request)
    {
        $kor = korModel::all();

        view()->share('kor', $kor);
        $pdf = PDF::loadView('master_export.kor_export', $kor);

        return $pdf->stream('MasterKOR.pdf');
    }

    public function master_kor(Request $request)
    {
        if ($request->ajax()) {
            $data = korModel::latest()->get();

            return Datatables::of($data)
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . url("change-master-kor/" . $row->id) . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-kor/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        return view('kor.master_kor', ['unit' => $unit]);
    }

    public function add_kor()
    {
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        return view('kor.kor_add', ['tahun' => $tahun, 'unit' => $unit]);
    }

    public function change_kor($id)
    {
        $kor = korModel::find($id);
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        if ($kor->type_user === 0) {
            $kor_tahun = kortransModel::where('id_kor', $id)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', null)->where('id_kel', null)->get();
        } elseif ($kor->type_user === 1) {
            $kor_tahun = kortransModel::where('id_kor', $id)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', 0)->where('id_kel', null)->get();
        } else {
            $kor_tahun = kortransModel::where('id_kor', $id)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', null)->where('id_kel', 0)->get();
        }
        // echo dd($kor_tahun);
        return view('kor.master_kor_edit', ['kor' => $kor, 'unit' => $unit, 'tahun' => $tahun, 'kor_tahun' => $kor_tahun]);
    }
}
