<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use \App\sdgsModel;
use \App\sdgstransModel;
use \App\unitsModel;
use \App\tahunModel;
use \App\kecamatanModel;
use \App\kelurahanModel;
use App\Imports\sdgsImport;
use App\Exports\SDGsExport;
use App\Exports\MasterSDGsExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;
use Auth;
use Illuminate\Filesystem\Filesystem;

class SDGsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $thn = tahunModel::all();
            if (auth()->user()->role === '0') {
                $data = sdgsModel::all();
            } else {
                if (auth()->user()->type_user === 0) {
                    $data = sdgsModel::where('type_user', 0)->get();
                } elseif (auth()->user()->type_user === 1) {
                    $data = sdgsModel::where('type_user', 1)->get();
                } else {
                    $data = sdgsModel::where('type_user', 2)->get();
                }
            }

            $table = Datatables::of($data);
            if ($request->get('kec') === "Kabupaten") {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                foreach ($thn as $a) {
                    $th = $a->id;
                    $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                        if ($row->type_user === 0) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }

                        if (count((array)$sdgstrans) > 0) {
                            $val = str_replace(".", "", $sdgstrans->value);
                            if (is_numeric($val)) {
                                if (strpos($sdgstrans->value, ".") === true) {
                                    $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                    $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($sdgstrans->value, 0, '', '.');
                                }
                            } else if ($sdgstrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $sdgstrans->value;
                            }
                        } else {
                            $value = '-';
                        }

                        return $value;
                    });
                }

                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';
                    if ($row->type_user === 0) {
                        if (auth()->user()->role === '1') {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-sdgs/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif (auth()->user()->role === '2' || auth()->user()->role === '0') {
                            $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                        } else {
                        }
                    }
                    // if ($row->validate === 0 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-sdgs/" . $row->id) . '"><span class="clip-notification"></span></a>';
                    // } else if ($row->validate === 1 && $row->validate_final === 0) {
                    //     $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                    // } else {
                    //     $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                    // }
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['sdgs_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            } else {
                $table->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                });
                if ($request->get('kel')) {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$sdgstrans) > 0) {
                                $val = str_replace(".", "", $sdgstrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($sdgstrans->value, ".") === true) {
                                        $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                        $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($sdgstrans->value, 0, '', '.');
                                    }
                                } else if ($sdgstrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $sdgstrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                } else {
                    foreach ($thn as $a) {
                        $th = $a->id;
                        $table->addColumn('tahun' . $a->tahun, function ($row) use ($th, $request) {
                            if ($row->type_user === 0) {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } elseif ($row->type_user === 1) {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            } else {
                                $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                            }
                            if (count((array)$sdgstrans) > 0) {
                                $val = str_replace(".", "", $sdgstrans->value);
                                if (is_numeric($val)) {
                                    if (strpos($sdgstrans->value, ".") === true) {
                                        $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                        $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                    } else {
                                        $value = number_format($sdgstrans->value, 0, '', '.');
                                    }
                                } else if ($sdgstrans->value === "") {
                                    $value = '-';
                                } else {
                                    $value = $sdgstrans->value;
                                }
                            } else {
                                $value = '-';
                            }

                            return $value;
                        });
                    }
                }


                $table->addColumn('action', function ($row) use ($request) {
                    $btn = '<a class="history" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '"  data-kecamatan="' . $request->get('kec') . '" href="#" data-target="history"><span class="glyphicon glyphicon-time"></span></a>';


                    if (auth()->user()->role === '1') {
                        if ($row->type_user === 0) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-sdgs/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } elseif ($row->type_user === 1) {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-sdgs/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        } else {
                            if ($row->validate === 0 && $row->validate_final === 0) {
                                $btn = $btn . ' <a href="#" class="target_validasi" data-url="' . url("validasi-sdgs/" . $row->id) . '"><span class="clip-notification"></span></a>';
                            } else if ($row->validate === 1 && $row->validate_final === 0) {
                                $btn = $btn . ' <span style="color: green;" class="clip-checkmark-2"></span>';
                            } else {
                                $btn = $btn . ' <label class="label label-success">Sudah Divalidasi</label>';
                            }
                        }
                    } else {
                        if ($row->type_user === 0) {
                            if ($request->kel) {
                            } else {
                                if ($request->get('kec')) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } elseif ($row->type_user === 1) {
                            if ($request->get('kec') != 0) {
                                if ($request->kel) {
                                } else {
                                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                                }
                            }
                        } else {
                            if ($request->kel) {
                                $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" data-kelurahan="' . $request->get('kel') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                            }
                        }
                    }
                    // $btn = $btn . ' <a href="#myModalEdit" data-toggle="modal" data-id="' . $row->id . '" data-sdgs_description="' . $row->sdgs_description . '" data-satuan="' . $row->unit_id . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    // $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" data-dari="' . $request->get('dari') . '" data-sampai="' . $request->get('sampai') . '" data-kecamatan="' . $request->get('kec') . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';

                    // $btn = $btn . ' <a href="#" class="hapus_sdgs" data-url="' . url("delete-sdgs/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                });
                $table->filter(function ($instance) use ($request) {

                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['sdgs_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                });
                $table->addIndexColumn();
                $table->rawColumns(['action']);

                return $table->make(true);
            }
        }
        $unit = unitsModel::all();
        $thn = tahunModel::all();

        if (auth()->user()->role === '0') {
            $kecamatan = kecamatanModel::all();
            $kelurahan = kelurahanModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $kecamatan = kecamatanModel::all();
                $kelurahan = kelurahanModel::all();
            } elseif (auth()->user()->type_user === 1) {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::all();
            } else {
                $kecamatan = kecamatanModel::find(auth()->user()->id_kec);
                $kelurahan = kelurahanModel::find(auth()->user()->id_kel);
            }
        }




        $a = date('Y') - 4;
        $b = date('Y') - 3;
        $c = date('Y') - 2;
        $d = date('Y') - 1;
        $e = date('Y');

        $column = 2 + count($thn) + 1;

        $kec = "Kabupaten";
        $kel = '';

        return view('sdgs.sdgs_vw', ['unit' => $unit, 'thn' => $thn, 'kecamatan' => $kecamatan, 'kelurahan' => $kelurahan, 'kec' => $kec, 'kel' => $kel, 'column' => $column, 'a' => $a, 'b' => $b, 'c' => $c, 'd' => $d, 'e' => $e]);
    }

    public function save_sdgs(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;

        $form_data = array(
            'sdgs_description' => $request->uraian,
            'unit_id' => $request->satuan,
            'value' => 0,
            'type_user' => $request->type_user,
            'validate' => 0,
            'validate_final' => 0
        );

        if ($request->type_user === '0') {
            // insert data untuk val kabupaten untuk user type Perangkat Daerah
            $id_kec = null;
            $id_kel = null;
        } elseif ($request->type_user === '1') {
            // insert data untuk val kabupaten untuk user type Kecamatan
            $id_kec = 0;
            $id_kel = null;
        } else {
            // insert data untuk val kabupaten untuk user type Kelurahan
            $id_kec = null;
            $id_kel = 0;
        }

        if ($id) {
            $sdgs = sdgsModel::find($id);
            $tahun = $request->tahun;
            // Cek type_user sdgs Perangkat Daerah
            if ($sdgs->type_user === 0) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($sdgs_trans) > 0) {
                        foreach ($sdgs_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda sdgs Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_sdgstrans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_sdgstrans) > 0) {
                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    sdgstransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('id_tahun', $del)->update(array('check_active' => 0));

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_sdgstrans_tahun) > 0) {
                                    $check_sdgstrans_tahun->update(array('check_active' => 1));
                                } else {
                                    sdgstransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } elseif ($request->type_user === '1') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                    }
                } else {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', null)->where('status', 'aktif')->delete();

                    if ($request->tahun) {

                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => null,
                                'id_kel' => 0,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $sdgs->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                sdgstransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }

            // Cek type_user sdgs Kecamatan
            elseif ($sdgs->type_user === 1) {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($sdgs_trans) > 0) {
                        foreach ($sdgs_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda sdgs Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                // foreach ($tahun as $thn) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $sdgs->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );
                                echo $array_edit[$i] . "</br>";
                                // }

                                $check_sdgstrans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_sdgstrans) > 0) {
                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    sdgstransModel::create($data);
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_sdgstrans_tahun) > 0) {
                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    sdgstransModel::create($data);
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                } else {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', 0)->where('id_kel', null)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->delete();

                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                        // jika user type kelurahan insert sdgs trans untuk sum kecamatan
                        if ($request->type_user === '2') {
                            $kecamatan = kecamatanModel::all();
                            foreach ($kecamatan as $kec) {
                                foreach ($tahun as $thn) {
                                    $data_sum_kec = array(
                                        'user_id' => $user->id,
                                        'id_sdgs' => $sdgs->id,
                                        'id_sdgs' => $sdgs->id,
                                        'id_kec' => $kec->id,
                                        'id_kel' => null,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => '',
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );

                                    sdgstransModel::create($data_sum_kec);
                                }
                            }
                        }
                    }
                }
            }

            // Cek type_user sdgs Kelurahan
            else {
                // Cek Request type_user Perangkat Daerah
                if ($request->type_user === '0') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                    }
                } elseif ($request->type_user === '1') {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', 0)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->delete();
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->delete();
                    if ($request->tahun) {
                        foreach ($tahun as $thn) {
                            $data = array(
                                'user_id' => $user->id,
                                'id_sdgs' => $sdgs->id,
                                'id_sdgs' => $sdgs->id,
                                'id_kec' => $id_kec,
                                'id_kel' => $id_kel,
                                'id_tahun' => $thn,
                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                'value' => '',
                                'check_active' => '1',
                                'status' => 'aktif'
                            );

                            sdgstransModel::create($data);
                        }
                    }
                } else {
                    $sdgs_trans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->get();
                    if (count($sdgs_trans) > 0) {
                        foreach ($sdgs_trans as $value) {
                            $array_tahun[] = $value->id_tahun;
                        }
                    } else {
                        $array_tahun = array();
                    }
                    if ($request->tahun) {
                        if (count($tahun) >= count($array_tahun)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($tahun); $y++) {

                                if (in_array($tahun[$y], $array_tahun) == true) {
                                    $array_same[] = $tahun[$y];
                                } else {
                                    $array_edit[] = $tahun[$y];
                                }
                            }
                            $array_delete = array_diff($array_tahun, $array_same);
                            // Add Tahun Berbeda sdgs Trans
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $sdgs->id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $array_edit[$i],
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                $check_sdgstrans = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->get();

                                if (count($check_sdgstrans) > 0) {
                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $array_edit[$i])->update(['check_active' => 1]);
                                } else {
                                    sdgstransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_sdgs' => $sdgs->id,
                                            'id_sdgs' => $sdgs->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $array_edit[$i],
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        sdgstransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            foreach ($array_delete as $key => $del) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));

                                sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 0));
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_tahun); $y++) {

                                if (in_array($array_tahun[$y], $tahun) == true) {
                                    $array_same[] = $array_tahun[$y];
                                } else {
                                    $array_delete[] = $array_tahun[$y];
                                }
                            }
                            $array_edit = array_diff($tahun, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $id,
                                    'id_kec' => $id_kec,
                                    'id_kel' => $id_kel,
                                    'id_tahun' => $del,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->get();

                                if (count($check_sdgstrans_tahun) > 0) {
                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));

                                    sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 0)->where('status', 'aktif')->where('id_tahun', $del)->update(array('check_active' => 1));
                                } else {
                                    sdgstransModel::create($data);
                                    $kecamatan = kecamatanModel::all();
                                    foreach ($kecamatan as $kec) {
                                        $data_sum_kec = array(
                                            'user_id' => $user->id,
                                            'id_sdgs' => $sdgs->id,
                                            'id_sdgs' => $sdgs->id,
                                            'id_kec' => $kec->id,
                                            'id_kel' => null,
                                            'id_tahun' => $del,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => '',
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );

                                        sdgstransModel::create($data_sum_kec);
                                    }
                                }
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));

                                $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', '!=', null)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $array_delete[$i])->update(array('check_active' => 0));
                            }
                        }
                    } else {

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $id_kel)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));

                        $check_sdgstrans_tahun = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->update(array('check_active' => 0));
                    }
                }
            }

            sdgsModel::whereid($id)->update($form_data);

            return redirect('/master-sdgs')->with('success', 'Data Master sdgs berhasil diubah!');
        } else {
            $sdgs = sdgsModel::create($form_data);
            $tahun = $request->tahun_id;
            if ($request->tahun_id) {
                if (count($tahun) > 0) {
                    foreach ($tahun as $thn) {
                        $data = array(
                            'user_id' => $user->id,
                            'id_sdgs' => $sdgs->id,
                            'id_sdgs' => $sdgs->id,
                            'id_kec' => $id_kec,
                            'id_kel' => $id_kel,
                            'id_tahun' => $thn,
                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                            'value' => '',
                            'check_active' => '1',
                            'status' => 'aktif'
                        );

                        sdgstransModel::create($data);
                    }
                    // jika user type kelurahan insert sdgs trans untuk sum kecamatan
                    if ($request->type_user === '2') {
                        $kecamatan = kecamatanModel::all();
                        foreach ($kecamatan as $kec) {
                            foreach ($tahun as $thn) {
                                $data_sum_kec = array(
                                    'user_id' => $user->id,
                                    'id_sdgs' => $sdgs->id,
                                    'id_sdgs' => $sdgs->id,
                                    'id_kec' => $kec->id,
                                    'id_kel' => null,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => '',
                                    'check_active' => '1',
                                    'status' => 'aktif'
                                );

                                sdgstransModel::create($data_sum_kec);
                            }
                        }
                    }
                }
            }


            return redirect('/master-sdgs')->with('success', 'Data Master sdgs berhasil ditambah!');
        }

        return redirect('/master-sdgs')->with('success', 'Data Master sdgs berhasil disimpan!');
    }

    public function edit(Request $request)
    {
        $sdgs = sdgsModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();

        $tahun = tahunModel::whereBetween('id', [$dari->id, $sampai->id])->get();
        // if($request->get('kec') != ''){
        //     $kec = $request->kec;
        // }
        // else {
        //     $kec = null;
        // }
        foreach ($tahun as $thn) {
            // cek pegampu
            if ($sdgs->type_user === 0) {
                $sdgstrans = sdgstransModel::where('id_sdgs', $request->get('id'))->where('id_tahun', $thn->id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
            } elseif ($sdgs->type_user === 1) {
                $sdgstrans = sdgstransModel::where('id_sdgs', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            } else {
                $sdgstrans = sdgstransModel::where('id_sdgs', $request->get('id'))->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('check_active', 1)->where('status', 'aktif')->where('id_tahun', $thn->id)->first();
            }


            if (count((array)$sdgstrans) > 0) {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => $sdgstrans->value);
            } else {
                $input[] = array('id_tahun' => $thn->id, 'tahun' => $thn->tahun, 'val' => '');
            }
        }


        return view('sdgs.sdgs_edit', ['id_kec' => $request->get('kec'), 'sdgs' => $sdgs, 'input' => $input, 'dari' => $dari->id, 'id_kel' => $request->get('kel'), 'sampai' => $sampai->id]);
    }

    public function update(Request $request)
    {
        $user = Auth::user();
        $id = $request->id;
        $id_kec = $request->id_kec;

        $id_tahun = $request->id_tahun;
        $value = $request->val;

        // cek type_user atau pengampu sdgs
        $sdgs = sdgsModel::find($id);

        // jika pengampu Perangkat Daerah
        if ($sdgs->type_user === 0) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $sdgstrans = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel sdgstrans
                    if (count((array)$sdgstrans) > 0) {
                        if ($sdgstrans->value === $value[$index]) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update sdgstrans jika value input berbeda
                            $sdgstrans->status = 'non-aktif';
                            $sdgstrans->save();
                            sdgstransModel::create($form_data);

                            $sdgs = sdgsModel::find($id);
                            $sdgs->validate = 0;
                            $sdgs->validate_final = 0;
                            $sdgs->save();
                        }
                    }
                } else {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => null,
                        'id_kel' => null,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $sdgstrans = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel sdgstrans
                    if (count((array)$sdgstrans) > 0) {
                        $cek_val = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update sdgstrans jika value input berbeda
                            $sdgstrans->status = 'non-aktif';
                            $sdgstrans->save();
                            sdgstransModel::create($form_data);
                        }
                    }
                }
            }
        }
        // jika pengampu Kecamatan
        elseif ($sdgs->type_user === 1) {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $sdgstrans_kab = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', 0)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel sdgstrans
                    if (count((array)$sdgstrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $sdgstrans_kec = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$sdgstrans_kec) > 0) {
                            // echo $sdgstrans_kec->value;
                            if ($sdgstrans_kec->value === $value[$index]) {
                                // Untuk value yg sama tidak diberi aksi update
                            } else {
                                // Update sdgstrans jika value input berbeda
                                $sdgstrans_kec->status = 'non-aktif';
                                $sdgstrans_kec->save();
                                sdgstransModel::create($form_data);

                                // jika data kabupaten ada maka update status + create
                                $sum = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                if ($sdgstrans_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $sdgstrans_kab->status = 'non-aktif';
                                    $sdgstrans_kab->save();

                                    $data_kab = array(
                                        'id_sdgs' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => 1,
                                        'status' => 'aktif'
                                    );
                                    sdgstransModel::create($data_kab);

                                    $sdgs = sdgsModel::find($id);
                                    $sdgs->validate = 0;
                                    $sdgs->validate_final = 0;
                                    $sdgs->save();
                                }
                                // }

                                $sdgs = sdgsModel::find($id);
                                $sdgs->validate = 0;
                                $sdgs->validate_final = 0;
                                $sdgs->save();
                            }
                            // echo dd($sdgstrans_kec);
                        } else {
                            sdgstransModel::create($form_data);

                            $sum = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                            if ($sdgstrans_kab->value === $sum) {
                                // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                            } else {
                                $sdgstrans_kab->status = 'non-aktif';
                                $sdgstrans_kab->save();

                                $data_kab = array(
                                    'id_sdgs' => $id,
                                    'id_kec' => 0,
                                    'id_kel' => null,
                                    'user_id' => $user->id,
                                    'id_tahun' => $thn,
                                    'tgl_pengisian' => date('Y-m-d H:i:s'),
                                    'value' => $sum,
                                    'check_active' => 1,
                                    'status' => 'aktif'
                                );
                                sdgstransModel::create($data_kab);

                                $sdgs = sdgsModel::find($id);
                                $sdgs->validate = 0;
                                $sdgs->validate_final = 0;
                                $sdgs->save();
                            }
                        }
                    }
                } else {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => 0,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $sdgstrans = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', $id_kec)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel sdgstrans
                    if (count((array)$sdgstrans) > 0) {
                        $cek_val = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', $id_kec)->where('value', "")->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$cek_val) > 0) {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update sdgstrans jika value input berbeda
                            $sdgstrans->status = 'non-aktif';
                            $sdgstrans->save();
                            sdgstransModel::create($form_data);

                            // cek sdgs trans kabupaten
                            $cek_kab = sdgstransModel::where('id_sdgs', $id)->where('id_kec', '0')->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kab) > 0) {
                                // jika data kabupaten ada maka update status + create
                                $sum = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');
                                if ($cek_kab->value === $sum) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kab->status = 'non-aktif';
                                    $cek_kab->save();

                                    $data_kab = array(
                                        'id_sdgs' => $id,
                                        'id_kec' => 0,
                                        'id_kel' => 0,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    sdgstransModel::create($data_kab);

                                    $sdgs = sdgsModel::find($id);
                                    $sdgs->validate = 0;
                                    $sdgs->validate_final = 0;
                                    $sdgs->save();
                                }
                            }
                            // else {
                            //     // buat baru
                            //     $sum = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('status', 'aktif')->where('id_kec', '!=', '0')->sum('value');

                            //     $data_kab = array(
                            //         'id_sdgs' => $id,
                            //         'id_kec' => 0,
                            //         'id_kel' => 0,
                            //         'user_id' => $user->id,
                            //         'id_tahun' => $thn,
                            //         'tgl_pengisian' => date('Y-m-d H:i:s'),
                            //         'value' => $sum,
                            //         'status' => 'aktif'
                            //     );

                            //     sdgstransModel::create($data_kab);

                            //     $sdgs = sdgsModel::find($id);
                            //     $sdgs->validate = 0;
                            //     $sdgs->validate_final = 0;
                            //     $sdgs->save();
                            // }
                        }
                    }
                }
            }
        }
        // jika pengampu Kelurahan
        else {
            foreach ($id_tahun as $index => $thn) {
                if ($value[$index] != "") {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => $value[$index],
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    // cek data tahun aktif by data kabupaten
                    $sdgstrans_kab = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', 0)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data tahun aktif ada di tabel sdgstrans
                    if (count((array)$sdgstrans_kab) > 0) {
                        // cek data kecamatan per id_kec
                        $sdgstrans_kec = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->first();
                        if (count((array)$sdgstrans_kec) > 0) {
                            // cek data kel per id_kel
                            $sdgstrans_kel = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$sdgstrans_kel) > 0) {
                                // echo $sdgstrans_kel->value;
                                if ($sdgstrans_kel->value === $value[$index]) {
                                    // jika value sama maka tidak ada aksi
                                } else {
                                    // Update sdgstrans jika value input berbeda
                                    $sdgstrans_kel->status = 'non-aktif';
                                    $sdgstrans_kel->save();
                                    sdgstransModel::create($form_data);
                                    //=======================================================================
                                    $sum_kec = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                    if ($sdgstrans_kec->value === $sum_kec) {
                                        // jika sama tidak ada update status+create

                                    } else {
                                        $sdgstrans_kec->status = 'non-aktif';
                                        $sdgstrans_kec->save();

                                        $data_kec = array(
                                            'id_sdgs' => $id,
                                            'id_kec' => $id_kec,
                                            'id_kel' => null,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kec,
                                            'check_active' => 1,
                                            'status' => 'aktif'
                                        );
                                        sdgstransModel::create($data_kec);

                                        $sdgs = sdgsModel::find($id);
                                        $sdgs->validate = 0;
                                        $sdgs->validate_final = 0;
                                        $sdgs->save();

                                        // sum data kelurahan
                                        $sum_kab = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('id_kec', '!=', null)->where('id_kel', null)->where('check_active', 1)->where('status', 'aktif')->sum('value');
                                        if ($sdgstrans_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $sdgstrans_kab->status = 'non-aktif';
                                            $sdgstrans_kab->save();
                                            sdgstransModel::create($form_data);

                                            $data_kab = array(
                                                'id_sdgs' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => 1,
                                                'status' => 'aktif'
                                            );
                                            sdgstransModel::create($data_kab);

                                            $sdgs = sdgsModel::find($id);
                                            $sdgs->validate = 0;
                                            $sdgs->validate_final = 0;
                                            $sdgs->save();
                                        }
                                    }

                                    // }

                                    $sdgs = sdgsModel::find($id);
                                    $sdgs->validate = 0;
                                    $sdgs->validate_final = 0;
                                    $sdgs->save();
                                }
                            } else {
                                // create data kelurahan
                                sdgstransModel::create($form_data);

                                $sum_kec = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', $id_kec)->where('id_kel', '!=', null)->sum('value');

                                if ($sdgstrans_kec->value === $sum_kec) {
                                } else {
                                    $sdgstrans_kec->status = "non-aktif";
                                    $sdgstrans_kec->save();

                                    $data_kec = array(
                                        'id_sdgs' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    sdgstransModel::create($data_kec);

                                    $sdgs->validate = 0;
                                    $sdgs->validate_final = 0;
                                    $sdgs->save();

                                    $sum_kab = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');

                                    if ($sdgstrans_kab->value === $sum_kab) {
                                    } else {
                                        $sdgstrans_kab->status = "non-aktif";
                                        $sdgstrans_kab->save();

                                        $data_kab = array(
                                            'id_sdgs' => $id,
                                            'id_kec' => null,
                                            'id_kel' => 0,
                                            'user_id' => $user->id,
                                            'id_tahun' => $thn,
                                            'tgl_pengisian' => date('Y-m-d H:i:s'),
                                            'value' => $sum_kab,
                                            'check_active' => '1',
                                            'status' => 'aktif'
                                        );
                                        sdgstransModel::create($data_kab);

                                        $sdgs->validate = 0;
                                        $sdgs->validate_final = 0;
                                        $sdgs->save();
                                    }
                                }
                            }
                            // echo dd($sdgstrans_kec);
                        }
                    }
                } else {
                    $form_data = array(
                        'id_sdgs' => $id,
                        'id_kec' => $id_kec,
                        'id_kel' => $request->id_kel,
                        'user_id' => $user->id,
                        'id_tahun' => $thn,
                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                        'value' => "",
                        'check_active' => '1',
                        'status' => 'aktif'
                    );
                    $sdgstrans = sdgstransModel::where('id_tahun', $thn)->where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', $request->id_kel)->where('check_active', 1)->where('status', 'aktif')->first();
                    // cek data di tabel sdgstrans
                    if (count((array)$sdgstrans) > 0) {
                        if ($sdgstrans->value === '') {
                            // Untuk value yg sama tidak diberi aksi update
                        } else {
                            // Update sdgstrans jika value input berbeda
                            $sdgstrans->status = 'non-aktif';
                            $sdgstrans->save();
                            sdgstransModel::create($form_data);

                            // cek sdgstrans kecamatan
                            $cek_kec = sdgstransModel::where('id_sdgs', $id)->where('id_kec', $id_kec)->where('id_kel', null)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                            if (count((array)$cek_kec) > 0) {
                                // jika data kecamatan ada maka update status + create
                                $sum_kec = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', null)->where('id_kel', null)->sum('value');
                                if ($cek_kec->value === $sum_kec) {
                                    // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                } else {
                                    $cek_kec->status = 'non-aktif';
                                    $cek_kec->save();

                                    $data_kec = array(
                                        'id_sdgs' => $id,
                                        'id_kec' => $id_kec,
                                        'id_kel' => null,
                                        'user_id' => $user->id,
                                        'id_tahun' => $thn,
                                        'tgl_pengisian' => date('Y-m-d H:i:s'),
                                        'value' => $sum_kec,
                                        'check_active' => '1',
                                        'status' => 'aktif'
                                    );
                                    sdgstransModel::create($data_kec);

                                    $sdgs = sdgsModel::find($id);
                                    $sdgs->validate = 0;
                                    $sdgs->validate_final = 0;
                                    $sdgs->save();

                                    // cek sdgs trans kabupaten
                                    $cek_kab = sdgstransModel::where('id_sdgs', $id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->first();

                                    if (count((array)$cek_kab) > 0) {
                                        // jika data kabupaten ada maka update status + create
                                        $sum_kab = sdgstransModel::where('id_sdgs', $id)->where('id_tahun', $thn)->where('check_active', 1)->where('status', 'aktif')->where('id_kec', '!=', '0')->where('id_kel', null)->sum('value');
                                        if ($cek_kab->value === $sum_kab) {
                                            // jika nilai sama dengan data sebelumnya maka tidak ada aksi
                                        } else {
                                            $cek_kab->status = 'non-aktif';
                                            $cek_kab->save();

                                            $data_kab = array(
                                                'id_sdgs' => $id,
                                                'id_kec' => null,
                                                'id_kel' => 0,
                                                'user_id' => $user->id,
                                                'id_tahun' => $thn,
                                                'tgl_pengisian' => date('Y-m-d H:i:s'),
                                                'value' => $sum_kab,
                                                'check_active' => '1',
                                                'status' => 'aktif'
                                            );
                                            sdgstransModel::create($data_kab);

                                            $sdgs = sdgsModel::find($id);
                                            $sdgs->validate = 0;
                                            $sdgs->validate_final = 0;
                                            $sdgs->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return back()->with('success', 'Data sdgs berhasil disimpan!');
    }

    public function history(Request $request)
    {
        $sdgs = sdgsModel::find($request->get('id'));
        $dari = tahunModel::where('tahun', $request->get('dari'))->first();
        $sampai = tahunModel::where('tahun', $request->get('sampai'))->first();
        $sdgstrans = sdgstransModel::where('id_sdgs', $request->get('id'))->where('id_kec', $request->get('kec'))->whereBetween('id_tahun', [$dari->id, $sampai->id])->orderBy('id_tahun', 'DESC')->get();

        return view('sdgs.sdgs_history', ['sdgs' => $sdgs, 'sdgstrans' => $sdgstrans]);
    }

    public function delete_sdgs($id)
    {
        sdgstransModel::where('id_sdgs', $id)->delete();
        sdgsModel::whereId($id)->delete();

        return back()->with('success', 'Data Master sdgs berhasil dihapus!');
    }

    public function validasi($id)
    {
        $sdgs = sdgsModel::find($id);
        $sdgs->validate = 1;
        $sdgs->save();

        return back()->with('success', 'Data ini berhasil divalidasi.');
    }

    public function validasi_all()
    {
        sdgsModel::where('validate_final', '0')->where('validate', '1')->update(array('validate_final' => 1));

        return back()->with('success', 'Data berhasil divalidasi.');
    }

    public function import_sdgs(Request $request)
    {
        $id_kec = $request->kec;
        $nama_file = rand() . $request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new sdgsImport($id_kec), storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);
        // Storage::deleteDirectory('public/tmp_import');

        return back()->with('success', 'File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/sdgs.xlsx');
    }

    public function excel(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = sdgsModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = sdgsModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = sdgsModel::where('type_user', 1)->get();
            } else {
                $data = sdgsModel::where('type_user', 2)->get();
            }
        }
        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row->type_user === 0) {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row->type_user === 1) {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$sdgstrans) > 0) {
                        $val = str_replace(".", "", $sdgstrans->value);
                        if (is_numeric($val)) {
                            if (strpos($sdgstrans->value, ".") === true) {
                                $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                $value = number_format($sdgstrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($sdgstrans->value, 0, '', '.');
                            }
                        } else if ($sdgstrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $sdgstrans->value;
                        }
                    } else {
                        $value = '-';
                    }
                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                $sdgs[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$sdgstrans) > 0) {
                            $val = str_replace(".", "", $sdgstrans->value);
                            if (is_numeric($val)) {
                                if (strpos($sdgstrans->value, ".") === true) {
                                    $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                    $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($sdgstrans->value, 0, '', '.');
                                }
                            } else if ($sdgstrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $sdgstrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                    $sdgs[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$sdgstrans) > 0) {
                            $val = str_replace(".", "", $sdgstrans->value);
                            if (is_numeric($val)) {
                                if (strpos($sdgstrans->value, ".") === true) {
                                    $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                    $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($sdgstrans->value, 0, '', '.');
                                }
                            } else if ($sdgstrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $sdgstrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                    $sdgs[] = $array_data + $tahun;
                }
            }
        }

        $data = array('sdgs' => $sdgs, 'tahun' => $thn);

        return Excel::download(new SDGsExport($data), 'SDGs.xlsx');
    }

    public function pdf(Request $request)
    {
        $thn = tahunModel::whereBetween('tahun', [$request->dari, $request->sampai])->get();
        if (auth()->user()->role === '0') {
            $data = sdgsModel::all();
        } else {
            if (auth()->user()->type_user === 0) {
                $data = sdgsModel::where('type_user', 0)->get();
            } elseif (auth()->user()->type_user === 1) {
                $data = sdgsModel::where('type_user', 1)->get();
            } else {
                $data = sdgsModel::where('type_user', 2)->get();
            }
        }
        foreach ($data as $row) {
            if ($request->kec === "Kabupaten") {
                $unit = unitsModel::find($row->unit_id);
                $tahun = array();
                foreach ($thn as $a) {
                    $th = $a->id;
                    if ($row->type_user === 0) {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } elseif ($row->type_user === 1) {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', 0)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    } else {
                        $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                    }

                    if (count((array)$sdgstrans) > 0) {
                        $val = str_replace(".", "", $sdgstrans->value);
                        if (is_numeric($val)) {
                            if (strpos($sdgstrans->value, ".") === true) {
                                $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                $value = number_format($sdgstrans->value, $decimal, ',', '.');
                            } else {
                                $value = number_format($sdgstrans->value, 0, '', '.');
                            }
                        } else if ($sdgstrans->value === "") {
                            $value = '-';
                        } else {
                            $value = $sdgstrans->value;
                        }
                    } else {
                        $value = '-';
                    }
                    $tahun[$a->tahun] = $value;
                }
                $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                $sdgs[] = $array_data + $tahun;
            } else {
                $unit = unitsModel::find($row->unit_id);
                if ($request->get('kel')) {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', $request->get('kel'))->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$sdgstrans) > 0) {
                            $val = str_replace(".", "", $sdgstrans->value);
                            if (is_numeric($val)) {
                                if (strpos($sdgstrans->value, ".") === true) {
                                    $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                    $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($sdgstrans->value, 0, '', '.');
                                }
                            } else if ($sdgstrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $sdgstrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                    $sdgs[] = $array_data + $tahun;
                } else {
                    $tahun = array();
                    foreach ($thn as $a) {
                        $th = $a->id;
                        if ($row->type_user === 0) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', null)->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } elseif ($row->type_user === 1) {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', 0)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        } else {
                            $sdgstrans = sdgstransModel::where('id_sdgs', $row->id)->where('id_kec', $request->get('kec'))->where('id_kel', null)->where('id_tahun', $th)->where('check_active', 1)->where('status', 'aktif')->first();
                        }
                        if (count((array)$sdgstrans) > 0) {
                            $val = str_replace(".", "", $sdgstrans->value);
                            if (is_numeric($val)) {
                                if (strpos($sdgstrans->value, ".") === true) {
                                    $decimal = strlen(substr($sdgstrans->value, strrpos($sdgstrans->value, '.') + 1));
                                    $value = number_format($sdgstrans->value, $decimal, ',', '.');
                                } else {
                                    $value = number_format($sdgstrans->value, 0, '', '.');
                                }
                            } else if ($sdgstrans->value === "") {
                                $value = '-';
                            } else {
                                $value = $sdgstrans->value;
                            }
                        } else {
                            $value = '-';
                        }
                        $tahun[$a->tahun] = $value;
                    }
                    $array_data = array('uraian' => $row->sdgs_description, 'satuan' => $unit->unit);
                    $sdgs[] = $array_data + $tahun;
                }
            }
        }

        $data = array('sdgs' => $sdgs, 'tahun' => $thn);

        view()->share('data', $data);
        $pdf = PDF::loadView('exports.sdgs_export', $data);

        return $pdf->stream('SDGs.pdf');
    }

    public function excel_master()
    {
        return Excel::download(new MasterSDGsExport, 'MasterSDGs.xlsx');
    }

    public function pdf_master(Request $request)
    {
        $sdgs = sdgsModel::all();

        view()->share('sdgs', $sdgs);
        $pdf = PDF::loadView('master_export.sdgs_export', $sdgs);

        return $pdf->stream('MasterSDGs.pdf');
    }

    public function master_sdgs(Request $request)
    {
        if ($request->ajax()) {
            $data = sdgsModel::latest()->get();

            return Datatables::of($data)
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row->unit_id);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    $btn = ' <a href="' . url("change-master-sdgs/" . $row->id) . '" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_sdgs" data-url="' . url("delete-sdgs/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        return view('sdgs.master_sdgs', ['unit' => $unit]);
    }

    public function add_sdgs()
    {
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        return view('sdgs.sdgs_add', ['tahun' => $tahun, 'unit' => $unit]);
    }

    public function change_sdgs($id)
    {
        $sdgs = sdgsModel::find($id);
        $unit = unitsModel::all();
        $tahun = tahunModel::orderBy('tahun', 'asc')->get();
        if ($sdgs->type_user === 0) {
            $sdgs_tahun = sdgstransModel::where('id_sdgs', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', null)->get();
        } elseif ($sdgs->type_user === 1) {
            $sdgs_tahun = sdgstransModel::where('id_sdgs', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', 0)->where('id_kel', null)->get();
        } else {
            $sdgs_tahun = sdgstransModel::where('id_sdgs', $id)->where('check_active', '1')->where('status', 'aktif')->where('id_kec', null)->where('id_kel', 0)->get();
        }
        // echo dd($sdgs_tahun);
        return view('sdgs.master_sdgs_edit', ['sdgs' => $sdgs, 'unit' => $unit, 'tahun' => $tahun, 'sdgs_tahun' => $sdgs_tahun]);
    }
}
