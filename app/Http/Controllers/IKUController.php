<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\ikuModel;
use App\unitsModel;
use App\businessModel;
use App\iku_detail;
use App\Imports\SPMImport;
use App\Exports\IKUExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Str;
use DataTables;
use File;
use PDF;

class IKUController extends Controller
{
    public function index(Request $request)
    {
        $data = ikuModel::latest()->get();
            // echo dd($data->toArray());
        if ($request->ajax()) {
            $data = ikuModel::latest()->get();
            // echo dd($data->toArray());
            return Datatables::of($data->toArray())
                ->addColumn('urusan', function ($row) {
                    $iku_detail = iku_detail::where('iku_id', $row['id'])->whereHas('getBusiness', function ($q) {
                        $q->where('active', '>=', '0');
                    })->get();

                    if (count($iku_detail) > 0) {

                        foreach ($iku_detail as $key) :

                            $detail_iku[] = $key->getBusiness->business_name;

                        endforeach;

                        $btn = $detail_iku;
                    } else {

                        $btn = '-';
                    }
                    return $btn;
                })
                ->addColumn('satuan', function ($row) {
                    $unit = unitsModel::find($row['unit_id']);
                    return $unit->unit;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<a href="' . url("change-iku/" . $row['id']) . '" data-toggle="modal"><span class="glyphicon glyphicon-pencil"></span></a>';

                    $btn = $btn . ' <a href="#" class="hapus_iku" data-url="' . url("delete-iku/" . $row['id']) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })
                ->filter(function ($instance) use ($request) {

                    if (!empty($request->get('urusan'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {
                            $urusan = implode(', ', $row['urusan']);
                            return Str::contains($urusan, $request->get('urusan')) ? true : false;
                        });
                    }


                    if (!empty($request->get('search'))) {
                        $instance->collection = $instance->collection->filter(function ($row) use ($request) {

                            if (Str::contains(Str::lower($row['iku_description']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['satuan']), Str::lower($request->get('search')))) {

                                return true;
                            } else if (Str::contains(Str::lower($row['target']), Str::lower($request->get('search')))) {

                                return true;
                            }


                            return false;
                        });
                    }
                })
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        $urusan = businessModel::all();
        $unit = unitsModel::all();

        return view('iku.iku_vw', ['urusan' => $urusan, 'unit' => $unit]);
    }

    public function add_iku()
    {
        $unit = unitsModel::all();
        $urusan = businessModel::all();

        return view('iku.addIKU_vw', ['unit' => $unit, 'urusan' => $urusan]);
    }

    public function save_iku(Request $request)
    {
        $id = $request->id;
        $form_data = array(
            'iku_description' => $request->uraian,
            'unit_id' => $request->satuan,
            'value' => 0,
            'target' => $request->target,
            'validate' => 0,
            'validate_final' => 0
        );

        if ($id) {
            $urusan = $request->urusan;

            $iku_detail = iku_detail::where('iku_id', $id)->get();

            if (count($iku_detail) > 0) {
                foreach ($iku_detail as $value) {
                    $array_urusan[] = $value->business_id;
                }
            } else {
                $array_urusan = array();
            }
            if ($request->urusan) {
                if (count($urusan) >= count($array_urusan)) {
                    $array_same = array();
                    $array_edit = array();
                    for ($y = 0; $y < count($urusan); $y++) {

                        if (in_array($urusan[$y], $array_urusan) == true) {
                            $array_same[] = $urusan[$y];
                        } else {
                            $array_edit[] = $urusan[$y];
                        }
                    }
                    $array_delete = array_diff($array_urusan, $array_same);
                    for ($i = 0; $i < count($array_edit); $i++) {
                        $data = array(
                            'iku_id'        =>  $id,
                            'business_id'        =>  $array_edit[$i]
                        );
                        iku_detail::create($data);
                    }
                    foreach ($array_delete as $key => $del) {
                        iku_detail::where('iku_id', $id)->where('business_id', $del)->delete();
                    }
                } else {
                    $array_same = array();
                    $array_delete = array();
                    for ($y = 0; $y < count($array_urusan); $y++) {

                        if (in_array($array_urusan[$y], $urusan) == true) {
                            $array_same[] = $array_urusan[$y];
                        } else {
                            $array_delete[] = $array_urusan[$y];
                        }
                    }
                    $array_edit = array_diff($urusan, $array_same);
                    // echo dd($array_delete);
                    foreach ($array_edit as $key => $del) {
                        $data = array(
                            'iku_id'        =>  $id,
                            'business_id'        =>  $del
                        );
                        iku_detail::create($data);
                    }
                    for ($i = 0; $i < count($array_delete); $i++) {
                        iku_detail::where('iku_id', $id)->where('business_id', $array_delete[$i])->delete();
                    }
                }
            } else {

                $delete_detail = iku_detail::where('iku_id', $id)->delete();
            }

            ikuModel::whereid($id)->update($form_data);

            return back()->with('success', 'Data IKU berhasil diubah!');
        } else {
            $business = $request->urusan;

            $iku = ikuModel::create($form_data);
            if (count($business) > 0) {
                foreach ($business as $urusan) {
                    $data = array(
                        'iku_id' => $iku->id,
                        'business_id' => $urusan
                    );

                    iku_detail::create($data);
                }
            }

            return back()->with('success', 'Data IKU berhasil ditambah!');
        }
    }

    public function change_iku($id)
    {
        $iku = ikuModel::find($id);
        $satuan = unitsModel::all();
        $urusan = businessModel::all();
        $detail_iku = iku_detail::where('iku_id', $id)->get();

        return view('iku.changeiku_vw', ['iku' => $iku, 'satuan' => $satuan, 'urusan' => $urusan, 'detail_iku' => $detail_iku]);
    }

    public function delete_iku($id)
    {
        iku_detail::where('iku_id', $id)->delete();
        ikuModel::whereId($id)->delete();

        return back()->with('success', 'Data IKU berhasil dihapus!');
    }

    public function excel()
    {
        return Excel::download(new IKUExport, 'IKU.xlsx');
    }

    public function pdf(Request $request)
    {
        $iku_data = ikuModel::latest()->get();

        if(count($iku_data)>0){
            foreach($iku_data as $row){
                $iku_detail = iku_detail::where('iku_id', $row['id'])->whereHas('getBusiness', function ($q) {
                    $q->where('active', '>=', '0');
                })->get();

                if (count($iku_detail) > 0) {

                    foreach ($iku_detail as $key){

                        $detail_iku[] = $key->getBusiness->business_name;

                    }

                    $urusan = implode(', ',$detail_iku);
                } else {

                    $urusan = '-';
                }

                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $iku[] = array('id' => $row->id, 'iku_description' => $row->iku_description, 'urusan' => $urusan, 'satuan' => $unit, 'target' => $row->target);
            }
        }
        else {
            $iku = array();
        }

        view()->share('iku', $iku);
        $pdf = PDF::loadView('exports.iku_export', $iku);

        return $pdf->stream('IKU.pdf');
    }
}
