<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\userModel;
use App\skpdModel;
use App\skpdUser;
use App\kecamatanModel;
use App\kelurahanModel;
use App\Imports\UsersImport;
use App\Exports\UserExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use File;
use PDF;

use Illuminate\Http\Request;


class UsersController extends Controller
{
    public function change_password(Request $request)
    {
        $user = userModel::find($request->id);
        $user->full_name = $request->full_name;
        if ($request->password) {
            $user->password = bcrypt($request->password);
        }
        $user->save();

        return back()->with('success_password', 'Nama lengkap dan password berhasil di ubah');
    }

    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = userModel::latest()->get();

            return Datatables::of($data)
                ->addColumn('role_user', function ($row) {
                    if ($row->role === '0') {
                        $role = 'Super Admin';
                    } elseif ($row->role === '1') {
                        $role = 'Penyelia';
                    } else {
                        $role = 'User';
                    }
                    return $role;
                })
                ->addColumn('unit_kerja', function ($row) {

                    $peran_user = skpdUser::where('user_id', $row->id)->whereHas('getSkpd', function ($q) {
                        $q->where('active', '>=', '0');
                    })->get();

                    if (count($peran_user) > 0) {

                        foreach ($peran_user as $key) :

                            $skpd_user[] = $key->getSkpd->skpd_name;

                        endforeach;

                        $btn = $skpd_user;
                    } else {

                        $btn = '-';
                    }
                    return $btn;
                })->addColumn('action', function ($row) {

                    $btn = '<a href="' . url("change-user/" . $row->id) . '"><span class="glyphicon glyphicon-pencil"></span></a>';
                    return $btn;
                })->addColumn('status_aktif', function ($row) {

                    if ($row->active == 0) {

                        $btn = '<label class="checkbox-inline">
                                <input type="checkbox"  value="' . $row->id . '" checked="checked" class="nonaktif_user" id="nonaktif_user">
                                    Aktif
                                </label>';
                    } else {

                        $btn = '<label class="checkbox-inline">
                                <input type="checkbox" value="' . $row->id . '" class="aktif_user"  id="aktif_user">
                                    Tidak Aktif
                                </label>';
                    }
                    return $btn;
                })

                ->rawColumns(['action', 'status_aktif'])
                ->addIndexColumn()
                ->make(true);
        }
        return view('user.users_vw');
    }

    public function update_status_user(request $request)
    {

        $id_user   = $request->id;
        $form_data = array(
            'active'        =>  $request->status
        );

        return userModel::whereId($id_user)->update($form_data);
    }

    public function change_user(Request $request)
    {

        $id_user   = $request->id;
        $data['user'] = userModel::where('id', $id_user)->get();
        $data['user_skpd'] = skpdUser::where('user_id', $id_user)->get();
        $data['skpd_list'] = skpdModel::get();
        $data['kecamatan'] = kecamatanModel::all();
        $data['kelurahan'] = kelurahanModel::all();
        return view('user.change_vw', $data);
    }

    public function add_user()
    {
        $data['skpd_list'] = skpdModel::get();
        $data['kecamatan'] = kecamatanModel::all();
        $data['kelurahan'] = kelurahanModel::all();

        return view('user.addUser_vw', $data);
    }

    public function save_user(Request $request)
    {

        $id_user = $request->id_user;

        if ($id_user) {
            // update user Super Admin
            if ($request->role === '0') {
                skpdUser::where('user_id', $id_user)->delete();
                $form_data = array(
                    'full_name'        =>  $request->full_name,
                    'email'            =>  $request->email,
                    'username'         =>  $request->username,
                    'role'             =>  $request->role,
                    'type_user' => null,
                    'id_kec' => null,
                    'id_kel' => null
                );
            }
            // update user Penyelia
            elseif ($request->role === '1') {
                // update user type Perangkat Daerah
                if ($request->type_user === '0') {
                    $skpd_id = $request->skpd_id;

                    $user_skpd = skpdUser::where('user_id', $id_user)->get();
                    if (count($user_skpd) > 0) {
                        foreach ($user_skpd as $value) {
                            $array_skpd[] = $value->skpd_id;
                        }
                    } else {
                        $array_skpd = array();
                    }

                    if ($request->skpd_id) {
                        if (count($skpd_id) >= count($array_skpd)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($skpd_id); $y++) {

                                if (in_array($skpd_id[$y], $array_skpd) == true) {
                                    $array_same[] = $skpd_id[$y];
                                } else {
                                    $array_edit[] = $skpd_id[$y];
                                }
                            }
                            $array_delete = array_diff($array_skpd, $array_same);
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id'        =>  $id_user,
                                    'skpd_id'        =>  $array_edit[$i]
                                );
                                skpdUser::create($data);
                            }
                            foreach ($array_delete as $key => $del) {
                                skpdUser::where('user_id', $id_user)->where('skpd_id', $del)->delete();
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_skpd); $y++) {

                                if (in_array($array_skpd[$y], $skpd_id) == true) {
                                    $array_same[] = $array_skpd[$y];
                                } else {
                                    $array_delete[] = $array_skpd[$y];
                                }
                            }
                            $array_edit = array_diff($skpd_id, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id'        =>  $id_user,
                                    'skpd_id'        =>  $del
                                );
                                skpdUser::create($data);
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                skpdUser::where('user_id', $id_user)->where('skpd_id', $array_delete[$i])->delete();
                            }
                        }
                    } else {

                        $delete_skpd = skpdUser::where('user_id', $id_user)->delete();
                    }
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => null,
                        'id_kel' => null
                    );
                }
                // update user type Kecamatan
                elseif ($request->type_user === '1') {
                    skpdUser::where('user_id', $id_user)->delete();
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => $request->kecamatan,
                        'id_kel' => null
                    );
                }
                // update user type Kelurahan
                else {
                    skpdUser::where('user_id', $id_user)->delete();
                    $kel = kelurahanModel::find($request->kelurahan);

                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => $kel->id_kec,
                        'id_kel' => $request->kelurahan
                    );
                }
            }
            // update user User
            else {
                // update user type Perangkat Daerah
                if ($request->type_user === '0') {
                    $skpd_id = $request->skpd_id;

                    $user_skpd = skpdUser::where('user_id', $id_user)->get();
                    if (count($user_skpd) > 0) {
                        foreach ($user_skpd as $value) {
                            $array_skpd[] = $value->skpd_id;
                        }
                    } else {
                        $array_skpd = array();
                    }



                    if ($request->skpd_id) {
                        if (count($skpd_id) >= count($array_skpd)) {
                            $array_same = array();
                            $array_edit = array();
                            for ($y = 0; $y < count($skpd_id); $y++) {

                                if (in_array($skpd_id[$y], $array_skpd) == true) {
                                    $array_same[] = $skpd_id[$y];
                                } else {
                                    $array_edit[] = $skpd_id[$y];
                                }
                            }
                            $array_delete = array_diff($array_skpd, $array_same);
                            for ($i = 0; $i < count($array_edit); $i++) {
                                $data = array(
                                    'user_id'        =>  $id_user,
                                    'skpd_id'        =>  $array_edit[$i]
                                );
                                skpdUser::create($data);
                            }
                            foreach ($array_delete as $key => $del) {
                                skpdUser::where('user_id', $id_user)->where('skpd_id', $del)->delete();
                            }
                        } else {
                            $array_same = array();
                            $array_delete = array();
                            for ($y = 0; $y < count($array_skpd); $y++) {

                                if (in_array($array_skpd[$y], $skpd_id) == true) {
                                    $array_same[] = $array_skpd[$y];
                                } else {
                                    $array_delete[] = $array_skpd[$y];
                                }
                            }
                            $array_edit = array_diff($skpd_id, $array_same);
                            // echo dd($array_delete);
                            foreach ($array_edit as $key => $del) {
                                $data = array(
                                    'user_id'        =>  $id_user,
                                    'skpd_id'        =>  $del
                                );
                                skpdUser::create($data);
                            }
                            for ($i = 0; $i < count($array_delete); $i++) {
                                skpdUser::where('user_id', $id_user)->where('skpd_id', $array_delete[$i])->delete();
                            }
                        }
                    } else {

                        $delete_skpd = skpdUser::where('user_id', $id_user)->delete();
                    }
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => null,
                        'id_kel' => null
                    );
                }
                // update user type Kecamatan
                elseif ($request->type_user === '1') {
                    skpdUser::where('user_id', $id_user)->delete();
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => $request->kecamatan,
                        'id_kel' => null
                    );
                }
                // update user type Kelurahan
                else {
                    skpdUser::where('user_id', $id_user)->delete();
                    $kel = kelurahanModel::find($request->kelurahan);

                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $request->type_user,
                        'id_kec' => $kel->id_kec,
                        'id_kel' => $request->kelurahan
                    );
                }
            }
            userModel::whereid($id_user)->update($form_data);


            if ($request->password) {

                $form_password = array(
                    'password' => bcrypt($request->password),
                );

                userModel::whereId($id_user)->update($form_password);
            }

            return redirect('/view-users')->with('success', 'Data user berhasil diubah!');
        } else {
            if ($request->role != '0') {
                $type = $request->type_user;
                if ($type === "0") {
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $type,
                        'id_kec' => null,
                        'id_kel' => null,
                        'password' => bcrypt($request->password)

                    );

                    $user = userModel::create($form_data);
                    $user_skpd = "";
                    if ($request->skpd_id) {

                        for ($i = 0; $i < count($request->skpd_id); $i++) {
                            $data = array(
                                'user_id'        =>  $user->id,
                                'skpd_id'        =>  $request->skpd_id[$i]
                            );
                            skpdUser::create($data);
                        }
                    }
                } elseif ($type === '1') {
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $type,
                        'id_kec' => $request->kecamatan,
                        'id_kel' => null,
                        'password' => bcrypt($request->password)

                    );

                    $user = userModel::create($form_data);
                } else {
                    $kelurahan = kelurahanModel::where('id', $request->kelurahan)->first();
                    $form_data = array(
                        'full_name'        =>  $request->full_name,
                        'email'            =>  $request->email,
                        'username'         =>  $request->username,
                        'role'             =>  $request->role,
                        'type_user' => $type,
                        'id_kec' => $kelurahan->id_kec,
                        'id_kel' => $kelurahan->id,
                        'password' => bcrypt($request->password)

                    );

                    $user = userModel::create($form_data);
                }
            } else {
                $type = null;

                $form_data = array(
                    'full_name'        =>  $request->full_name,
                    'email'            =>  $request->email,
                    'username'         =>  $request->username,
                    'role'             =>  $request->role,
                    'type_user' => $type,
                    'id_kec' => null,
                    'id_kel' => null,
                    'password' => bcrypt($request->password)

                );

                $user = userModel::create($form_data);
            }

            return redirect('/view-users')->with('success', 'Data user berhasil ditambah!');
        }
    }

    public function import_user(Request $request)
    {
        $nama_file = rand() . $request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new UsersImport, storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);

        return back()->with('success_import', 'File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/User.xlsx');
    }

    public function excel()
    {
        return Excel::download(new UserExport, 'Users.xlsx');
    }

    public function pdf()
    {
        $data = userModel::latest()->get();
        if (count($data) > 0) {
            foreach ($data as $row) {
                // get role
                if ($row->role === '0') {
                    $role = 'Super Admin';
                } elseif ($row->role === '1') {
                    $role = 'Penyelia';
                } else {
                    $role = 'User';
                }
                // get unit kerja
                $peran_user = skpdUser::where('user_id', $row->id)->whereHas('getSkpd', function ($q) {
                    $q->where('active', '>=', '0');
                })->get();

                if (count($peran_user) > 0) {

                    foreach ($peran_user as $key) {

                        $skpd_user[] = $key->getSkpd->skpd_name;
                    }

                    $skpd = implode(", ", $skpd_user);
                } else {

                    $skpd = '-';
                }
                //get status
                if ($row->active == 0) {
                    $status = "Aktif";
                } else {
                    $status = "Tidak Aktif";
                }

                $user[] = array('id' => $row->id, 'nama' => $row->full_name, 'role' => $role, 'username' => $row->username, 'unit_kerja' => $skpd, 'status' => $status);
            }
        } else {
            $user = array();
        }

        view()->share('user', $user);
        $pdf = PDF::loadView('master_export.user_export', $user);

        return $pdf->stream('Users.pdf');
    }
}
