<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\businessModel;
use App\Imports\BusinessImport;
use App\Imports\AreaImport;
use App\Imports\SubAreaImport;
use App\Imports\IndicatorImport;
use App\Imports\SubIndicatorImport;
use App\Imports\ParentSubImport;
use App\areaModel;
use App\subAreaModel;
use App\indicatorModel;
use App\subIndicatorModel;
use App\kecamatanModel;
use App\kelurahanModel;
use App\unitsModel;
use App\Exports\BusinessExport;
use App\Exports\AreaExport;
use App\Exports\SubAreaExport;
use App\Exports\IndicatorExport;
use App\Exports\SubIndicatorExport;
use App\Exports\ParentSubExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use File;
use PDF;

class BusinessController extends Controller
{
    public function index(Request $request)
    {

        if ($request->ajax()) {
            $data = businessModel::where('active', '0')->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("detail-business/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("detail-business/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("detail-business/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>'; 
                    $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-business/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();
        return view('business.business_vw', ['unit' => $unit, 'skpd' => $skpd]);
    }

    public function detail_business(Request $request)
    {

        $id = $request->id;
        $area_business = businessModel::where('id', $id)->first();
        $req['business_name'] = $area_business->business_name;
        $req['role'] = $area_business->role;
        $req['check_sub'] = $area_business->check_sub;

        if ($request->ajax()) {
            $data = areaModel::where('active', '0')->has('getBusiness')->where('business_id', $id)->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("sub-area?id=" . $row->id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("sub-area?id=" . $row->id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("sub-area?id=" . $row->id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>'; 
                    $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-area/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        $req['business_id_get'] = $id;
        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();
        return view('business.area_vw', $req, ['unit' => $unit, 'skpd' => $skpd]);
    }


    public function sub_area(Request $request)
    {

        $id = $request->id;
        $business_id = $request->business_id;

        $area_business = businessModel::where('id', $business_id)->first();
        $area         = areaModel::where('id', $id)->first();

        $req['business_name'] = $area_business->business_name;
        $req['area_name'] = $area->area_name;
        $req['role'] = $area->role;
        $req['check_sub'] = $area->check_sub;

        if ($request->ajax()) {
            $data = subAreaModel::where('active', '0')->has('getBusiness')->has('getArea')->where('area_id', $id)->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("view-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("view-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("view-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-sub-area/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        $req['business_id_get'] = $business_id;
        $req['area_id_get'] = $id;

        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();
        return view('business.sub_area_vw', $req, ['unit' => $unit, 'skpd' => $skpd]);
    }

    public function indicators(Request $request)
    {

        $id = $request->id;
        $area_id = $request->area_id;
        $business_id = $request->business_id;

        $area_business = businessModel::where('id', $business_id)->first();
        $area         = areaModel::where('id', $area_id)->first();
        $sub_area     = subAreaModel::where('id', $id)->first();

        $req['business_name'] = $area_business->business_name;
        $req['area_name'] = $area->area_name;
        $req['subarea_name'] = $sub_area->subarea_name;
        $req['role'] = $sub_area->role;
        $req['check_sub'] = $sub_area->check_sub;

        if ($request->ajax()) {
            $data = indicatorModel::where('active', '0')->where('sub_area_id', $id)->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("view-sub-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id . "&sub_area_id=" . $row->sub_area_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("view-sub-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id . "&sub_area_id=" . $row->sub_area_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("view-sub-indicators?id=" . $row->id . "&area_id=" . $row->area_id . "&business_id=" . $row->business_id . "&sub_area_id=" . $row->sub_area_id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_kor" data-url="' . url("delete-indicator/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        $req['business_id_get'] = $business_id;
        $req['area_id_get'] = $area_id;
        $req['sub_area_id_get'] = $id;
        

        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();

        return view('business.indicator_vw', $req, ['unit' => $unit, 'skpd' => $skpd]);
    }


    public function sub_indicators(Request $request)
    {

        $id = $request->id;
        $area_id = $request->area_id;
        $business_id = $request->business_id;
        $sub_area_id = $request->sub_area_id;

        $area_business = businessModel::where('id', $business_id)->first();
        $area         = areaModel::where('id', $area_id)->first();
        $sub_area     = subAreaModel::where('id', $sub_area_id)->first();
        $indicator    = indicatorModel::where('id', $id)->first();

        $req['business_name'] = $area_business->business_name;
        $req['area_name'] = $area->area_name;
        $req['subarea_name'] = $sub_area->subarea_name;
        $req['indicator_name'] = $indicator->indicator_name;
        $req['indicator_role'] = $indicator->indicator_role;
        $req['role'] = $indicator->role;
        $req['check_sub'] = $indicator->check_sub;

        if ($request->ajax()) {
            $data = subIndicatorModel::where('active', '0')->has('getBusiness')->has('getArea')->has('getsubArea')->has('getIndicator')->where('indicator_id', $id)->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_urusan" data-url="' . url("delete-sub-indicator/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();

        $req['business_id_get'] = $business_id;
        $req['area_id_get'] = $area_id;
        $req['sub_area_id_get'] = $sub_area_id;
        $req['indicator_id_get'] = $id;

        return view('business.sub_indicator_vw', $req, ['unit' => $unit, 'skpd' => $skpd]);
    }

    public function parent_sub(Request $request)
    {

        $id = $request->id;

        $subindicator = subIndicatorModel::find($id);

        $area_business = businessModel::where('id', $subindicator->business_id)->first();
        $area         = areaModel::where('id', $subindicator->area_id)->first();
        $sub_area     = subAreaModel::where('id', $subindicator->sub_area_id)->first();
        $indicator    = indicatorModel::where('id', $subindicator->indicator_id)->first();

        $req['business_name'] = $area_business->business_name;
        $req['area_name'] = $area->area_name;
        $req['subarea_name'] = $sub_area->subarea_name;
        $req['indicator_name'] = $indicator->indicator_name;
        $req['subindicator_name'] = $subindicator->sub_indicator_name;
        $req['role'] = $subindicator->role;
        $req['check_sub'] = $subindicator->check_sub;

        if ($request->ajax()) {
            $data = subIndicatorModel::where('active', '0')->has('getBusiness')->has('getArea')->has('getsubArea')->has('getIndicator')->where('active', 0)->where('parent_id', $id)->get();

            return Datatables::of($data)
                ->addColumn('action', function ($row) {
                    if ($row->check_sub === 1) {
                        $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                    } else {
                        if($row->role === "Kecamatan"){
                            $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        elseif($row->role === "Kelurahan"){
                            $btn = '<a href="' . url("parent-sub/" . $row->id) . '"><span class="glyphicon glyphicon-eye-open"></span></a>';
                        }
                        else {
                            $btn = '';
                        }
                    }
                    $btn = $btn . ' <a class="edit" data-toggle="modal" id="' . $row->id . '" href="#" data-target="edit"><span class="glyphicon glyphicon-pencil"></span></a>';
                    $btn = $btn . ' <a href="#" class="hapus_urusan" data-url="' . url("delete-sub-indicator/" . $row->id) . '"><span class="glyphicon glyphicon-trash"></span></a>';
                    return $btn;
                })->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }
        $unit = unitsModel::all();
        $skpd = \App\skpdModel::all();

        $req['business_id_get'] = $subindicator->business_id;
        $req['area_id_get'] = $subindicator->area_id;
        $req['sub_area_id_get'] = $subindicator->sub_area_id;
        $req['indicator_id_get'] = $subindicator->indicator_id;
        $req['sub_indicator_id_get'] = $id;

        if($subindicator->parent){
            $parent[] = array();
            $parent = parent_link($subindicator->parent, $parent, 1);
        }
        else {
            $parent = 0;
        }
        if(is_array($parent)){
            unset($parent[0]);
        }
        else {

        }
        // echo dd($parent);
        return view('business.parent_sub', $req, ['unit' => $unit, 'skpd' => $skpd, 'parent' => $parent, 'subindicator' => $subindicator]);
    }

    public function save_business(Request $request)
    {

        $id   = $request->id;
        if($request->check_sub){
            $check_sub = 1;
        }
        else {
            $check_sub = 0;
        }

        if($request->role === 'Kecamatan'){
            $role = "Kecamatan";
        }
        elseif($request->role === 'Kelurahan'){
            $role = "Kelurahan";
        }
        else {
            $role = $request->role;
        }

        if ($role === 'Kecamatan' && $check_sub === 1) {
            $form_data = array(
                'business_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $business = businessModel::find($id);
                if ($business->check_sub === 1 && empty($request->check_sub)) {
                    areaModel::where('business_id', $id)->update(['active' => 1]);
                    subAreaModel::where('business_id', $id)->update(['active' => 1]);
                    indicatorModel::where('business_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('business_id', $id)->update(['active' => 1]);
                }
                businessModel::whereId($id)->update($form_data);
            } else {
                businessModel::create($form_data);
            }
        }
        elseif ($role === 'Kecamatan' && $check_sub === 0) {
            $form_data = array(
                'business_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $business = businessModel::find($id);
                if ($business->check_sub === 1 && empty($request->check_sub)) {
                    areaModel::where('business_id', $id)->update(['active' => 1]);
                    subAreaModel::where('business_id', $id)->update(['active' => 1]);
                    indicatorModel::where('business_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('business_id', $id)->update(['active' => 1]);
                }
                businessModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $area = new areaModel;
                    $area->id = $area->id;
                    $area->business_id = $id;
                    $area->check_sub = 0;
                    $area->unit_id = $request->satuan;
                    $area->role = $role;
                    $area->area_name = $kec->nama;
                    $area->save();
                }
                
            } else {
                $business = businessModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $area = new areaModel;
                    $area->id = $area->id;
                    $area->business_id = $business->id;
                    $area->check_sub = 0;
                    $area->unit_id = $request->satuan;
                    $area->role = $role;
                    $area->area_name = $kec->nama;
                    $area->save();
                }
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 1) {
            $form_data = array(
                'business_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $business = businessModel::find($id);
                if ($business->check_sub === 1 && empty($request->check_sub)) {
                    areaModel::where('business_id', $id)->update(['active' => 1]);
                    subAreaModel::where('business_id', $id)->update(['active' => 1]);
                    indicatorModel::where('business_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('business_id', $id)->update(['active' => 1]);
                }
                businessModel::whereId($id)->update($form_data);
            } else {
                businessModel::create($form_data);
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 0) {
            $form_data = array(
                'business_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $business = businessModel::find($id);
                if ($business->check_sub === 1 && empty($request->check_sub)) {
                    areaModel::where('business_id', $id)->update(['active' => 1]);
                    subAreaModel::where('business_id', $id)->update(['active' => 1]);
                    indicatorModel::where('business_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('business_id', $id)->update(['active' => 1]);
                }
                businessModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $area = new areaModel;
                    $area->id = $area->id;
                    $area->business_id = $business->id;
                    $area->check_sub = 0;
                    $area->unit_id = '';
                    $area->role = "";
                    $area->area_name = $kec->nama;
                    $area->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $area = new areaModel;
                        $area->id = $area->id;
                        $area->business_id = $business->id;
                        $area->check_sub = 0;
                        $area->unit_id = $request->satuan;
                        $area->role = $role;
                        $area->area_name = $kel->nama;
                        $area->save();
                    }
                }
                
            } else {
                $business = businessModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $area = new areaModel;
                    $area->id = $area->id;
                    $area->business_id = $business->id;
                    $area->check_sub = 0;
                    $area->unit_id = $request->satuan;
                    $area->role = $role;
                    $area->area_name = $kec->nama;
                    $area->save();
                }
                foreach($kecamatan as $kec){
                    $area = new areaModel;
                    $area->id = $area->id;
                    $area->business_id = $business->id;
                    $area->check_sub = 0;
                    $area->unit_id = '';
                    $area->role = "";
                    $area->area_name = $kec->nama;
                    $area->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $area = new areaModel;
                        $area->id = $area->id;
                        $area->business_id = $business->id;
                        $area->check_sub = 0;
                        $area->unit_id = $request->satuan;
                        $area->role = $role;
                        $area->area_name = $kel->nama;
                        $area->save();
                    }
                }
            }
        }
        else {
            if (isset($request->check_sub)) {
                $form_data = array(
                    'business_name'        =>  $request->uraian,
                    'check_sub' => 1,
                    'role' => $request->role,
                    'unit_id' => ''
                );
            } else {
                $form_data = array(
                    'business_name'        =>  $request->uraian,
                    'check_sub' => 0,
                    'role' => $request->role,
                    'unit_id' => $request->satuan
                );
            }
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    areaModel::where('business_id', $id)->update(['active' => 1]);
                    subAreaModel::where('business_id', $id)->update(['active' => 1]);
                    indicatorModel::where('business_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('business_id', $id)->update(['active' => 1]);
                }
                businessModel::whereId($id)->update($form_data);
            } else {
                businessModel::create($form_data);
            }
        }
        
        return back()->with('success', 'Data Urusan berhasil disimpan!');
    }

    public function save_area(Request $request)
    {
        $id   = $request->id;
        if($request->check_sub){
            $check_sub = 1;
        }
        else {
            $check_sub = 0;
        }

        if($request->role === 'Kecamatan'){
            $role = "Kecamatan";
        }
        elseif($request->role === 'Kelurahan'){
            $role = "Kelurahan";
        }
        else {
            $role = $request->role;
        }

        if ($role === 'Kecamatan' && $check_sub === 1) {
            $form_data = array(
                'area_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    subAreaModel::where('area_id', $id)->update(['active' => 1]);
                    indicatorModel::where('area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('area_id', $id)->update(['active' => 1]);
                }
                areaModel::whereId($id)->update($form_data);
            } else {
                areaModel::create($form_data);
            }
        }
        elseif ($role === 'Kecamatan' && $check_sub === 0) {
            $form_data = array(
                'area_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    subAreaModel::where('area_id', $id)->update(['active' => 1]);
                    indicatorModel::where('area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('area_id', $id)->update(['active' => 1]);
                }
                areaModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subarea = new subAreaModel;
                    $subarea->id = $subarea->id;
                    $subarea->business_id = $request->business_id;
                    $subarea->area_id = $id;
                    $subarea->check_sub = 0;
                    $subarea->unit_id = $request->satuan;
                    $subarea->role = $role;
                    $subarea->subarea_name = $kec->nama;
                    $subarea->save();
                }
                
            } else {
                $area = areaModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subarea = new subAreaModel;
                    $subarea->id = $subarea->id;
                    $subarea->business_id = $request->business_id;
                    $subarea->area_id = $area->id;
                    $subarea->check_sub = 0;
                    $subarea->unit_id = $request->satuan;
                    $subarea->role = $role;
                    $subarea->subarea_name = $kec->nama;
                    $subarea->save();
                }
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 1) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    subAreaModel::where('area_id', $id)->update(['active' => 1]);
                    indicatorModel::where('area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('area_id', $id)->update(['active' => 1]);
                }
                areaModel::whereId($id)->update($form_data);
            } else {
                areaModel::create($form_data);
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 0) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    subAreaModel::where('area_id', $id)->update(['active' => 1]);
                    indicatorModel::where('area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('area_id', $id)->update(['active' => 1]);
                }
                areaModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subarea = new subAreaModel;
                    $subarea->id = $subarea->id;
                    $subarea->business_id = $request->business_id;
                    $subarea->area_id = $id;
                    $subarea->check_sub = 0;
                    $subarea->unit_id = '';
                    $subarea->role = "";
                    $subarea->subarea_name = $kec->nama;
                    $subarea->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subarea = new subAreaModel;
                        $subarea->id = $subarea->id;
                        $subarea->business_id = $request->business_id;
                        $subarea->area_id = $id;
                        $subarea->check_sub = 0;
                        $subarea->unit_id = $request->satuan;
                        $subarea->role = $role;
                        $subarea->subarea_name = $kel->nama;
                        $subarea->save();
                    }
                }
                
            } else {
                $area = areaModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subarea = new subAreaModel;
                    $subarea->id = $subarea->id;
                    $subarea->business_id = $request->business_id;
                    $subarea->area_id = $area->id;
                    $subarea->check_sub = 0;
                    $subarea->unit_id = $request->satuan;
                    $subarea->role = $role;
                    $subarea->subarea_name = $kec->nama;
                    $subarea->save();
                }
                foreach($kecamatan as $kec){
                    $subarea = new subAreaModel;
                    $subarea->id = $subarea->id;
                    $subarea->business_id = $request->business_id;
                    $subarea->area_id = $area->id;
                    $subarea->check_sub = 0;
                    $subarea->unit_id = '';
                    $subarea->role = "";
                    $subarea->subarea_name = $kec->nama;
                    $subarea->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subarea = new subAreaModel;
                        $subarea->id = $subarea->id;
                        $subarea->business_id = $request->business_id;
                        $subarea->area_id = $area->id;
                        $subarea->check_sub = 0;
                        $subarea->unit_id = $request->satuan;
                        $subarea->role = $role;
                        $subarea->subarea_name = $kel->nama;
                        $subarea->save();
                    }
                }
            }
        }
        else {
            if (isset($request->check_sub)) {
                $form_data = array(
                    'area_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'check_sub' => 1,
                    'role' => $request->role,
                    'unit_id' => ''
                );
            } else {
                $form_data = array(
                    'area_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'check_sub' => 0,
                    'role' => $request->role,
                    'unit_id' => $request->satuan
                );
            }
            if ($id) {
                $area = areaModel::find($id);
                if ($area->check_sub === 1 && empty($request->check_sub)) {
                    subAreaModel::where('area_id', $id)->update(['active' => 1]);
                    indicatorModel::where('area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('area_id', $id)->update(['active' => 1]);
                }
                areaModel::whereId($id)->update($form_data);
            } else {
                areaModel::create($form_data);
            }
        }
        
        return back()->with('success', 'Data bidang berhasil disimpan!');
    }


    public function save_sub_area(Request $request)
    {
        $id   = $request->id;
        if($request->check_sub){
            $check_sub = 1;
        }
        else {
            $check_sub = 0;
        }

        if($request->role === 'Kecamatan'){
            $role = "Kecamatan";
        }
        elseif($request->role === 'Kelurahan'){
            $role = "Kelurahan";
        }
        else {
            $role = $request->role;
        }

        if ($role === 'Kecamatan' && $check_sub === 1) {
            $form_data = array(
                'subarea_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $subarea = subAreaModel::find($id);
                if ($subarea->check_sub === 1 && empty($request->check_sub)) {
                    indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                }
                subAreaModel::whereId($id)->update($form_data);
            } else {
                subAreaModel::create($form_data);
            }
        }
        elseif ($role === 'Kecamatan' && $check_sub === 0) {
            $form_data = array(
                'subarea_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $subarea = subAreaModel::find($id);
                if ($subarea->check_sub === 1 && empty($request->check_sub)) {
                    indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                }
                subAreaModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $indicator = new indicatorModel;
                    $indicator->id = $indicator->id;
                    $indicator->business_id = $request->business_id;
                    $indicator->area_id = $request->area_id;
                    $indicator->sub_area_id = $id;
                    $indicator->check_sub = 0;
                    $indicator->unit_id = $request->satuan;
                    $indicator->role = $role;
                    $indicator->indicator_name = $kec->nama;
                    $indicator->save();
                }
                
            } else {
                $subarea = subAreaModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $indicator = new indicatorModel;
                    $indicator->id = $indicator->id;
                    $indicator->business_id = $request->business_id;
                    $indicator->area_id = $request->area_id;
                    $indicator->sub_area_id = $subarea->id;
                    $indicator->check_sub = 0;
                    $indicator->unit_id = $request->satuan;
                    $indicator->role = $role;
                    $indicator->indicator_name = $kec->nama;
                    $indicator->save();
                }
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 1) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'subarea_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $subarea = subAreaModel::find($id);
                if ($subarea->check_sub === 1 && empty($request->check_sub)) {
                    indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                }
                subAreaModel::whereId($id)->update($form_data);
            } else {
                subAreaModel::create($form_data);
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 0) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'subarea_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $subarea = subAreaModel::find($id);
                if ($subarea->check_sub === 1 && empty($request->check_sub)) {
                    indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                }
                subAreaModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $indicator = new indicatorModel;
                    $indicator->id = $indicator->id;
                    $indicator->business_id = $request->business_id;
                    $indicator->area_id = $request->area_id;
                    $indicator->sub_area_id = $id;
                    $indicator->check_sub = 0;
                    $indicator->unit_id = '';
                    $indicator->role = "";
                    $indicator->indicator_name = $kec->nama;
                    $indicator->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $indicator = new indicatorModel;
                        $indicator->id = $indicator->id;
                        $indicator->business_id = $request->business_id;
                        $indicator->area_id = $request->area_id;
                        $indicator->sub_area_id = $id;
                        $indicator->check_sub = 0;
                        $indicator->unit_id = $request->satuan;
                        $indicator->role = $role;
                        $indicator->indicator_name = $kel->nama;
                        $indicator->save();
                    }
                }
                
            } else {
                $subarea = subAreaModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $indicator = new indicatorModel;
                    $indicator->id = $indicator->id;
                    $indicator->business_id = $request->business_id;
                    $indicator->area_id = $request->area_id;
                    $indicator->sub_area_id = $subarea->id;
                    $indicator->check_sub = 0;
                    $indicator->unit_id = $request->satuan;
                    $indicator->role = $role;
                    $indicator->indicator_name = $kec->nama;
                    $indicator->save();
                }
                foreach($kecamatan as $kec){
                    $indicator = new indicatorModel;
                    $indicator->id = $indicator->id;
                    $indicator->business_id = $request->business_id;
                    $indicator->area_id = $request->area_id;
                    $indicator->sub_area_id = $subarea->id;
                    $indicator->check_sub = 0;
                    $indicator->unit_id = '';
                    $indicator->role = "";
                    $indicator->indicator_name = $kec->nama;
                    $indicator->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $indicator = new indicatorModel;
                        $indicator->id = $indicator->id;
                        $indicator->business_id = $request->business_id;
                        $indicator->area_id = $request->area_id;
                        $indicator->sub_area_id = $subarea->id;
                        $indicator->check_sub = 0;
                        $indicator->unit_id = $request->satuan;
                        $indicator->role = $role;
                        $indicator->indicator_name = $kel->nama;
                        $indicator->save();
                    }
                }
            }
        }
        else{
            if (isset($request->check_sub)) {
                $form_data = array(
                    'subarea_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'check_sub' => 1,
                    'role' => $request->role,
                    'unit_id' => ''
                );
            } else {
                $form_data = array(
                    'subarea_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'check_sub' => 0,
                    'role' => $request->role,
                    'unit_id' => $request->satuan
                );
            }
            if ($id) {
                $subarea = subAreaModel::find($id);
                if ($subarea->check_sub === 1 && empty($request->check_sub)) {
                    indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
                }
                subAreaModel::whereId($id)->update($form_data);
            } else {
                subAreaModel::create($form_data);
            }
        }
        
        return back()->with('success', 'Data sub bidang berhasil disimpan!');
        // ==============================================
        // $id   = $request->id;

        // if (isset($request->check_sub)) {
        //     $form_data = array(
        //         'subarea_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'check_sub' => 1,
        //         'role' => $request->role,
        //         'unit_id' => ''
        //     );
        // } else {
        //     $form_data = array(
        //         'subarea_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'check_sub' => 0,
        //         'role' => $request->role,
        //         'unit_id' => $request->satuan
        //     );
        // }


        // if ($id) {
        //     $subarea = subAreaModel::find($id);
        //     if ($subarea->check_sub === 1 && empty($request->check_sub)) {
        //         indicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
        //         subIndicatorModel::where('sub_area_id', $id)->update(['active' => 1]);
        //     }
        //     subAreaModel::whereId($id)->update($form_data);
        // } else {
        //     subAreaModel::create($form_data);
        // }
        // return back()->with('success', 'Data sub bidang berhasil disimpan!');
    }


    public function save_indicator(Request $request)
    {
        $id   = $request->id;
        if($request->check_sub){
            $check_sub = 1;
        }
        else {
            $check_sub = 0;
        }

        if($request->role === 'Kecamatan'){
            $role = "Kecamatan";
        }
        elseif($request->role === 'Kelurahan'){
            $role = "Kelurahan";
        }
        else {
            $role = $request->role;
        }

        if ($role === 'Kecamatan' && $check_sub === 1) {
            $form_data = array(
                'indicator_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                indicatorModel::whereId($id)->update($form_data);
            } else {
                indicatorModel::create($form_data);
            }
        }
        elseif ($role === 'Kecamatan' && $check_sub === 0) {
            $form_data = array(
                'indicator_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                indicatorModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator = new subIndicatorModel;
                    $subindicator->id = $subindicator->id;
                    $subindicator->business_id = $request->business_id;
                    $subindicator->area_id = $request->area_id;
                    $subindicator->sub_area_id = $request->sub_area_id;
                    $subindicator->indicator_id = $id;
                    $subindicator->check_sub = 0;
                    $subindicator->unit_id = $request->satuan;
                    $subindicator->role = $role;
                    $subindicator->sub_indicator_name = $kec->nama;
                    $subindicator->save();
                }
                
            } else {
                $indicator = indicatorModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator = new subIndicatorModel;
                    $subindicator->id = $subindicator->id;
                    $subindicator->business_id = $request->business_id;
                    $subindicator->area_id = $request->area_id;
                    $subindicator->sub_area_id = $request->sub_area_id;
                    $subindicator->indicator_id = $indicator->id;
                    $subindicator->check_sub = 0;
                    $subindicator->unit_id = $request->satuan;
                    $subindicator->role = $role;
                    $subindicator->sub_indicator_name = $kec->nama;
                    $subindicator->save();
                }
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 1) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                indicatorModel::whereId($id)->update($form_data);
            } else {
                indicatorModel::create($form_data);
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 0) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                indicatorModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator = new subIndicatorModel;
                    $subindicator->id = $subindicator->id;
                    $subindicator->business_id = $request->business_id;
                    $subindicator->area_id = $request->area_id;
                    $subindicator->sub_area_id = $request->sub_area_id;
                    $subindicator->indicator_id = $id;
                    $subindicator->check_sub = 0;
                    $subindicator->unit_id = '';
                    $subindicator->role = "";
                    $subindicator->sub_indicator_name = $kec->nama;
                    $subindicator->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subindicator = new subIndicatorModel;
                        $subindicator->id = $subindicator->id;
                        $subindicator->business_id = $request->business_id;
                        $subindicator->area_id = $request->area_id;
                        $subindicator->sub_area_id = $request->sub_area_id;
                        $subindicator->indicator_id = $id;
                        $subindicator->check_sub = 0;
                        $subindicator->unit_id = $request->satuan;
                        $subindicator->role = $role;
                        $subindicator->sub_indicator_name = $kel->nama;
                        $subindicator->save();
                    }
                }
                
            } else {
                $indicator = indicatorModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator = new subIndicatorModel;
                    $subindicator->id = $subindicator->id;
                    $subindicator->business_id = $request->business_id;
                    $subindicator->area_id = $request->area_id;
                    $subindicator->sub_area_id = $request->sub_area_id;
                    $subindicator->indicator_id = $indicator->id;
                    $subindicator->check_sub = 0;
                    $subindicator->unit_id = $request->satuan;
                    $subindicator->role = $role;
                    $subindicator->sub_indicator_name = $kec->nama;
                    $subindicator->save();
                }
                foreach($kecamatan as $kec){
                    $subindicator = new subIndicatorModel;
                    $subindicator->id = $subindicator->id;
                    $subindicator->business_id = $request->business_id;
                    $subindicator->area_id = $request->area_id;
                    $subindicator->sub_area_id = $request->sub_area_id;
                    $subindicator->indicator_id = $indicator->id;
                    $subindicator->check_sub = 0;
                    $subindicator->unit_id = '';
                    $subindicator->role = "";
                    $subindicator->sub_indicator_name = $kec->nama;
                    $subindicator->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subindicator = new subIndicatorModel;
                        $subindicator->id = $subindicator->id;
                        $subindicator->business_id = $request->business_id;
                        $subindicator->area_id = $request->area_id;
                        $subindicator->sub_area_id = $request->sub_area_id;
                        $subindicator->indicator_id = $indicator->id;
                        $subindicator->check_sub = 0;
                        $subindicator->unit_id = $request->satuan;
                        $subindicator->role = $role;
                        $subindicator->sub_indicator_name = $kel->nama;
                        $subindicator->save();
                    }
                }
            }
        }
        else {
            if (isset($request->check_sub)) {
                $form_data = array(
                    'indicator_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'sub_area_id'             =>  $request->sub_area_id,
                    'check_sub' => 1,
                    'role' => $request->role,
                    'unit_id' => 0
                );
            } else {
                $form_data = array(
                    'indicator_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'sub_area_id'             =>  $request->sub_area_id,
                    'check_sub' => 0,
                    'role' => $request->role,
                    'unit_id' => $request->satuan
                );
            }


            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                    subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                indicatorModel::whereId($id)->update($form_data);
            } else {
                indicatorModel::create($form_data);
            }
        }
        
        return back()->with('success', 'Data indicator berhasil disimpan!');
        // -=================================================================
        // $id   = $request->id;

        // if (isset($request->check_sub)) {
        //     $form_data = array(
        //         'indicator_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'sub_area_id'             =>  $request->sub_area_id,
        //         'check_sub' => 1,
        //         'role' => $request->role,
        //         'unit_id' => 0
        //     );
        // } else {
        //     $form_data = array(
        //         'indicator_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'sub_area_id'             =>  $request->sub_area_id,
        //         'check_sub' => 0,
        //         'role' => $request->role,
        //         'unit_id' => $request->satuan
        //     );
        // }


        // if ($id) {
        //     $indicator = indicatorModel::find($id);
        //     if ($indicator->check_sub === 1 && empty($request->check_sub)) {
        //         subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
        //         subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
        //     }
        //     indicatorModel::whereId($id)->update($form_data);
        // } else {
        //     indicatorModel::create($form_data);
        // }
        // return back()->with('success', 'Data indicator berhasil disimpan!');
    }


    public function save_sub_indicator(Request $request)
    {
        $id   = $request->id;
        if($request->check_sub){
            $check_sub = 1;
        }
        else {
            $check_sub = 0;
        }

        if($request->role === 'Kecamatan'){
            $role = "Kecamatan";
        }
        elseif($request->role === 'Kelurahan'){
            $role = "Kelurahan";
        }
        else {
            $role = $request->role;
        }

        if ($role === 'Kecamatan' && $check_sub === 1) {
            $form_data = array(
                'sub_indicator_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_id' => $request->indicator_id,
                'check_sub' => $check_sub,
                'parent_id' => $request->parent,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $indicator = indicatorModel::find($id);
                if ($indicator->check_sub === 1 && empty($request->check_sub)) {
                    // subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                subIndicatorModel::whereId($id)->update($form_data);
            } else {
                subIndicatorModel::create($form_data);
            }
        }
        elseif ($role === 'Kecamatan' && $check_sub === 0) {
            $form_data = array(
                'sub_indicator_name'        =>  $request->uraian,
                'business_id'      =>  $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_id' => $request->indicator_id,
                'check_sub' => $check_sub,
                'role' => $role,
                'parent_id' => $request->parent,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $subindicator = subIndicatorModel::find($id);
                if ($subindicator->check_sub === 1 && empty($request->check_sub)) {
                    subIndicatorModel::where('sub_indicator_id', $id)->update(['active' => 1]);
                }
                // ================================================================
                subIndicatorModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator2 = new subIndicatorModel;
                    $subindicator2->id = $subindicator2->id;
                    $subindicator2->business_id = $request->business_id;
                    $subindicator2->area_id = $request->area_id;
                    $subindicator2->sub_area_id = $request->sub_area_id;
                    $subindicator2->indicator_id = $request->indicator_id;
                    $subindicator2->check_sub = 0;
                    $subindicator2->unit_id = $request->satuan;
                    $subindicator2->parent_id = $id;
                    $subindicator2->role = $role;
                    $subindicator2->sub_indicator_name = $kec->nama;
                    $subindicator2->save();
                }
                
            } else {
                $subindicator = subIndicatorModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator2 = new subIndicatorModel;
                    $subindicator2->id = $subindicator2->id;
                    $subindicator2->business_id = $request->business_id;
                    $subindicator2->area_id = $request->area_id;
                    $subindicator2->sub_area_id = $request->sub_area_id;
                    $subindicator2->indicator_id = $request->indicator_id;
                    $subindicator2->check_sub = 0;
                    $subindicator2->unit_id = $request->satuan;
                    $subindicator2->parent_id = $subindicator->id;
                    $subindicator2->role = $role;
                    $subindicator2->sub_indicator_name = $kec->nama;
                    $subindicator2->save();
                }
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 1) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_id' => $request->indicator_id,
                'sub_indicator_name'        =>  $request->uraian,
                'parent_id' => null,
                'check_sub' => $check_sub,
                'role' => $role,
                'unit_id' => ''
            );
            if ($id) {
                $subindicator = subIndicatorModel::find($id);
                if ($subindicator->check_sub === 1 && empty($request->check_sub)) {
                    // subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                subIndicatorModel::whereId($id)->update($form_data);
            } else {
                subIndicatorModel::create($form_data);
            }
        }
        elseif ($role === 'Kelurahan' && $check_sub === 0) {
            $form_data = array(
                'business_id' => $request->business_id,
                'area_id' => $request->area_id,
                'sub_area_id' => $request->sub_area_id,
                'indicator_id' => $request->indicator_id,
                'sub_indicator_name'        =>  $request->uraian,
                'check_sub' => $check_sub,
                'parent_id' => $request->parent,
                'role' => $role,
                'unit_id' => $request->satuan,
            );
            if ($id) {
                $subindicator = subIndicatorModel::find($id);
                if ($subindicator->check_sub === 1 && empty($request->check_sub)) {
                    // subIndicatorModel::where('indicator_id', $id)->update(['active' => 1]);
                }
                subIndicatorModel::whereId($id)->update($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator2 = new subIndicatorModel;
                    $subindicator2->id = $subindicator2->id;
                    $subindicator2->business_id = $request->business_id;
                    $subindicator2->area_id = $request->area_id;
                    $subindicator2->sub_area_id = $request->sub_area_id;
                    $subindicator2->indicator_id =$request->indicator_id;
                    $subindicator2->parent_id = $id;
                    $subindicator2->check_sub = 0;
                    $subindicator2->unit_id = '';
                    $subindicator2->role = "";
                    $subindicator2->sub_indicator_name = $kec->nama;
                    $subindicator2->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subindicator3 = new subIndicatorModel;
                        $subindicator3->id = $subindicator3->id;
                        $subindicator3->business_id = $request->business_id;
                        $subindicator3->area_id = $request->area_id;
                        $subindicator3->sub_area_id = $request->sub_area_id;
                        $subindicator3->indicator_id = $request->indicator_id;
                        $subindicator3->parent_id = $id;
                        $subindicator3->check_sub = 0;
                        $subindicator3->unit_id = $request->satuan;
                        $subindicator3->role = $role;
                        $subindicator3->sub_indicator_name = $kel->nama;
                        $subindicator3->save();
                    }
                }
                
            } else {
                $subindicator = subIndicatorModel::create($form_data);
                $kecamatan = kecamatanModel::all();
                foreach($kecamatan as $kec){
                    $subindicator2 = new subIndicatorModel;
                    $subindicator2->id = $subindicator2->id;
                    $subindicator2->business_id = $request->business_id;
                    $subindicator2->area_id = $request->area_id;
                    $subindicator2->sub_area_id = $request->sub_area_id;
                    $subindicator2->indicator_id = $request->indicator_id;
                    $subindicator2->parent_id = $subindicator->id;
                    $subindicator2->check_sub = 0;
                    $subindicator2->unit_id = '';
                    $subindicator2->role = "";
                    $subindicator2->sub_indicator_name = $kec->nama;
                    $subindicator2->save();

                    $kelurahan = kelurahanModel::where('id_kec', $kec->id)->get();
                    foreach($kelurahan as $kel){
                        $subindicator3 = new subIndicatorModel;
                        $subindicator3->id = $subindicator3->id;
                        $subindicator3->business_id = $request->business_id;
                        $subindicator3->area_id = $request->area_id;
                        $subindicator3->sub_area_id = $request->sub_area_id;
                        $subindicator3->indicator_id = $request->indicator_id;
                        $subindicator3->parent_id = $subindicator->id;
                        $subindicator3->check_sub = 0;
                        $subindicator3->unit_id = $request->satuan;
                        $subindicator3->role = $role;
                        $subindicator3->sub_indicator_name = $kel->nama;
                        $subindicator3->save();
                    }
                }
            }
        }
        else {
            if (isset($request->check_sub)) {
                $form_data = array(
                    'sub_indicator_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'sub_area_id'             =>  $request->sub_area_id,
                    'indicator_id'             =>  $request->indicator_id,
                    'unit_id' => $request->satuan,
                    'check_sub' => 1,
                    'role' => $request->role,
                    'parent_id' => $request->parent
                );
            } else {
                $form_data = array(
                    'sub_indicator_name'        =>  $request->uraian,
                    'business_id'         =>  $request->business_id,
                    'area_id'             =>  $request->area_id,
                    'sub_area_id'             =>  $request->sub_area_id,
                    'indicator_id'             =>  $request->indicator_id,
                    'unit_id' => $request->satuan,
                    'check_sub' => 0,
                    'role' => $request->role,
                    'parent_id' => $request->parent
                );
            }

            if ($id) {
                subIndicatorModel::whereId($id)->update($form_data);
            } else {
                subIndicatorModel::create($form_data);
            }
        }
        
        return back()->with('success', 'Data sub indicator berhasil disimpan!');
        // ============================================================
        // $id   = $request->id;

        // if (isset($request->check_sub)) {
        //     $form_data = array(
        //         'sub_indicator_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'sub_area_id'             =>  $request->sub_area_id,
        //         'indicator_id'             =>  $request->indicator_id,
        //         'unit_id' => $request->satuan,
        //         'check_sub' => 1,
        //         'role' => $request->role,
        //         'parent_id' => $request->parent
        //     );
        // } else {
        //     $form_data = array(
        //         'sub_indicator_name'        =>  $request->uraian,
        //         'business_id'         =>  $request->business_id,
        //         'area_id'             =>  $request->area_id,
        //         'sub_area_id'             =>  $request->sub_area_id,
        //         'indicator_id'             =>  $request->indicator_id,
        //         'unit_id' => $request->satuan,
        //         'check_sub' => 0,
        //         'role' => $request->role,
        //         'parent_id' => $request->parent
        //     );
        // }

        // if ($id) {
        //     subIndicatorModel::whereId($id)->update($form_data);
        // } else {
        //     subIndicatorModel::create($form_data);
        // }
        // return back()->with('success', 'Data indicator berhasil disimpan!');
    }

    public function edit($id)
    {
        $business = businessModel::find($id);
        $satuan = unitsModel::all();
        $skpd = \App\skpdModel::all();
        // echo dd($business);
        return view('business.edit_business', ['business' => $business, 'unit' => $satuan, 'skpd' => $skpd]);
    }

    public function edit_area($id)
    {
        $area = areaModel::find($id);
        $satuan = unitsModel::all();
        $skpd = \App\skpdModel::all();
        // echo dd($business);
        return view('business.edit_area', ['area' => $area, 'unit' => $satuan, 'skpd' => $skpd]);
    }

    public function edit_sub_area($id)
    {
        $subarea = subAreaModel::find($id);
        $satuan = unitsModel::all();
        $skpd = \App\skpdModel::all();
        // echo dd($business);
        return view('business.edit_subarea', ['subarea' => $subarea, 'unit' => $satuan, 'skpd' => $skpd]);
    }

    public function edit_indicator($id)
    {
        $indicator = indicatorModel::find($id);
        $satuan = unitsModel::all();
        $skpd = \App\skpdModel::all();
        // echo dd($business);
        return view('business.edit_indicator', ['indicator' => $indicator, 'unit' => $satuan, 'skpd' => $skpd]);
    }

    public function edit_sub_indicator($id)
    {
        $subindicator = subIndicatorModel::find($id);
        $satuan = unitsModel::all();
        $skpd = \App\skpdModel::all();
        // echo dd($business);
        return view('business.edit_sub_indicator', ['subindicator' => $subindicator, 'unit' => $satuan, 'skpd' => $skpd]);
    }

    public function delete_business(Request $request)
    {

        $id = $request->id;
        $form_data = array(
            'active'        =>  1
        );

        businessModel::whereId($id)->update($form_data);
        return back()->with('success_delete', 'Data urusan berhasil dihapus!');
    }


    public function delete_area(Request $request)
    {

        $id = $request->id;
        $form_data = array(
            'active'        =>  1
        );

        areaModel::whereId($id)->update($form_data);
        return back()->with('success_delete', 'Data Bidang berhasil dihapus!');
    }

    public function delete_sub_area(Request $request)
    {

        $id = $request->id;
        $form_data = array(
            'active'        =>  1
        );

        subAreaModel::whereId($id)->update($form_data);
        return back()->with('success_delete', 'Data Sub Bidang berhasil dihapus!');
    }

    public function delete_indicator(Request $request)
    {

        $id = $request->id;
        $form_data = array(
            'active'        =>  1
        );

        indicatorModel::whereId($id)->update($form_data);
        return back()->with('success_delete', 'Data Sub Bidang berhasil dihapus!');
    }


    public function delete_sub_indicator(Request $request)
    {

        $id = $request->id;
        $form_data = array(
            'active'        =>  1
        );

        subIndicatorModel::whereId($id)->update($form_data);
        return back()->with('success_delete', 'Data Sub Bidang berhasil dihapus!');
    }

    public function import_business(Request $request)
    {
        $nama_file = rand() . $request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new BusinessImport, storage_path('app/public/tmp_import/' . $nama_file));

        Storage::delete('public/tmp_import/' . $nama_file);

        return back()->with('success_import', 'File berhasil di Import!');
    }

    // public function import_area(Request $request)
    // {
    //     $nama_file = rand() . $request->file('uraian_import')->getClientOriginalName();
    //     $path = Storage::putFileAs(
    //         'public/tmp_import',
    //         $request->file('uraian_import'),
    //         $nama_file
    //     );

    //     $import = Excel::import(new BusinessImport, storage_path('app/public/tmp_import/' . $nama_file));

    //     Storage::delete('public/tmp_import/' . $nama_file);

    //     return back()->with('success_import', 'File berhasil di Import!');
    // }

    public function template()
    {
        return Storage::download('public/template/Urusan.xlsx');
    }

    public function excel_business()
    {
        return Excel::download(new BusinessExport, 'Urusan.xlsx');
    }

    public function pdf_business()
    {
        $business = businessModel::where('active', '0')->get();
        if(count($business)>0){
            foreach($business as $row){
                if($row->unit_id >0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else{
                    $unit = null;
                }
                $urusan[] = array('id' => $row->id, 'business_name' => $row->business_name, 'satuan' => $unit);
            }
        }
        else {
            $urusan = array();
        }
        

        view()->share('business', $urusan);
        $pdf = PDF::loadView('master_export.business_export', $urusan);

        return $pdf->stream('Urusan.pdf');
    }

    public function excel_area($id)
    {
        return Excel::download(new AreaExport($id), 'Bidang.xlsx');
    }

    public function pdf_area($id)
    {
        $area = areaModel::where('active', '0')->has('getBusiness')->where('business_id', $id)->get();
        if(count($area)>0){
            foreach($area as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $bidang[] = array('id' => $row->id, 'area_name' => $row->area_name, 'satuan' => $unit);
            }
        }
        else {
            $bidang = array();
        }

        view()->share('area', $bidang);
        $pdf = PDF::loadView('master_export.area_export', $bidang);

        return $pdf->stream('Bidang.pdf');
    }

    public function excel_sub_area($id)
    {
        return Excel::download(new SubAreaExport($id), 'SubBidang.xlsx');
    }

    public function pdf_sub_area($id)
    {
        $subarea = subAreaModel::where('active', '0')->has('getBusiness')->has('getArea')->where('area_id', $id)->get();
        if(count($subarea)>0){
            foreach($subarea as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $subbidang[] = array('id' => $row->id, 'sub_area_name' => $row->subarea_name, 'satuan' => $unit);
            }
        }
        else {
            $subbidang = array();
        }

        view()->share('subarea', $subbidang);
        $pdf = PDF::loadView('master_export.sub_area_export', $subbidang);

        return $pdf->stream('SubBidang.pdf');
    }

    public function excel_indicator($id)
    {
        return Excel::download(new IndicatorExport($id), 'Indikator.xlsx');
    }

    public function pdf_indicator($id)
    {
        $indicator = indicatorModel::where('active', '0')->where('sub_area_id', $id)->get();

        if(count($indicator)>0){
            foreach($indicator as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $indikator[] = array('id' => $row->id, 'indicator_name' => $row->indicator_name, 'satuan' => $unit);
            }
        }
        else{
            $indikator = array();
        }

        view()->share('indicator', $indikator);
        $pdf = PDF::loadView('master_export.indicator_export', $indikator);

        return $pdf->stream('Indikator.pdf');
    }

    public function excel_sub_indicator($id)
    {
        return Excel::download(new SubIndicatorExport($id), 'SubIndikator.xlsx');
    }

    public function pdf_sub_indicator($id)
    {
        $subindicator = subIndicatorModel::where('active', '0')->has('getBusiness')->has('getArea')->has('getsubArea')->has('getIndicator')->where('indicator_id', $id)->get();

        if(count($subindicator)>0){
            foreach($subindicator as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $subindikator[] = array('id' => $row->id, 'sub_indicator_name' => $row->sub_indicator_name, 'satuan' => $unit);
            }
        }
        else{
            $subindikator = array();
        }

        view()->share('subindicator', $subindikator);
        $pdf = PDF::loadView('master_export.sub_indicator_export', $subindikator);

        return $pdf->stream('SubIndikator.pdf');
    }

    public function excel_parent_sub($id)
    {
        return Excel::download(new ParentSubExport($id), 'SubIndikator.xlsx');
    }

    public function pdf_parent_sub($id)
    {
        $subindicator = subIndicatorModel::where('active', '0')->has('getBusiness')->has('getArea')->has('getsubArea')->has('getIndicator')->where('active', 0)->where('parent_id', $id)->get();

        if(count($subindicator)>0){
            foreach($subindicator as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $subindikator[] = array('id' => $row->id, 'sub_indicator_name' => $row->sub_indicator_name, 'satuan' => $unit);
            }
        }
        else{
            $subindikator = array();
        }

        view()->share('subindicator', $subindikator);
        $pdf = PDF::loadView('master_export.parent_sub_export', $subindikator);

        return $pdf->stream('SubIndikator.pdf');
    }
}
