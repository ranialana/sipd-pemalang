<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\skpdModel;
use App\Imports\SkpdImport;
use App\Exports\SKPDExport;
use Maatwebsite\Excel\Facades\Excel;
use DataTables;
use File;
use PDF;

class SkpdController extends Controller
{
    public function index(Request $request){

    	 if ($request->ajax()) {
            $data = skpdModel::latest()->where('active','0')->get();

            return Datatables::of($data)
                    ->addColumn('action', function($row){

                    	  $btn='<a href="#myModalEdit" data-toggle="modal" data-id="'.$row->id.'" data-skpd_name="'.$row->skpd_name.'" class="edit_button"><span class="glyphicon glyphicon-pencil"></span></a>';
                          
                          $btn = $btn.' <a href="#" class="hapus_skpd" data-url="'.url("delete-skpd/".$row->id).'"><span class="glyphicon glyphicon-trash"></span></a>';
                          return $btn;

                    })->addIndexColumn()
                    ->rawColumns(['action'])
                    ->make(true);
                }
    	return view('skpd.skpd_vw');
    }


    public function save_skpd(Request $request){

       $id   =$request->id;

        $form_data = array(
            'skpd_name'        =>  $request->uraian
        );

        if($id){
           skpdModel::whereId($id)->update($form_data);
        }else{
          skpdModel::create($form_data);
        }
        return back()->with('success','Data SKPD berhasil disimpan!');

   }


   public function delete_skpd(Request $request){

       $id=$request->id;
       $form_data = array(
            'active'        =>  1
       );

       skpdModel::whereId($id)->update($form_data);
       return back()->with('success_delete','Data SKPD berhasil disimpan!');
     

   }

    public function import_skpd(Request $request)
    {
        $nama_file = rand().$request->file('uraian_import')->getClientOriginalName();
        $path = Storage::putFileAs(
            'public/tmp_import',
            $request->file('uraian_import'),
            $nama_file
        );

        $import = Excel::import(new SkpdImport, storage_path('app/public/tmp_import/'.$nama_file));
        
        Storage::delete('public/tmp_import/'.$nama_file);

        return back()->with('success_import','File berhasil di Import!');
    }

    public function template()
    {
        return Storage::download('public/template/SKPD.xlsx');
    }

    public function excel()
    {
        return Excel::download(new SKPDExport, 'SKPD.xlsx');
    }

    public function pdf()
    {
        $skpd = skpdModel::all();

        view()->share('skpd', $skpd);
        $pdf = PDF::loadView('master_export.skpd_export', $skpd);

        return $pdf->stream('SKPD.pdf');
    }
}
