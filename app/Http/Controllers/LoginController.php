<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(Request $request)
    {
    
        $login_type = filter_var($request->login, FILTER_VALIDATE_EMAIL )? 'email' : 'username';
        $role = Array('2', '1', '0');

        if ($login_type == "username") {
            if (Auth::attempt(['username' => $request->login, 'password' => $request->password, 'role' => '2', 'active' => 0])) {
                if(Auth::user()->username === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            elseif(Auth::attempt(['username' => $request->login, 'password' => $request->password, 'role' => '1', 'active' => 0])) {
                if(Auth::user()->username === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            elseif(Auth::attempt(['username' => $request->login, 'password' => $request->password, 'role' => '0', 'active' => 0])) {
                if(Auth::user()->username === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            else {
                return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
            }
        }
        elseif($login_type == "email") {
            if (Auth::attempt(['email' => $request->login, 'password' => $request->password, 'role' => '2', 'active' => 0])) {
                if(Auth::user()->email === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            elseif(Auth::attempt(['email' => $request->login, 'password' => $request->password, 'role' => '1', 'active' => 0])) {
                if(Auth::user()->email === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            elseif(Auth::attempt(['email' => $request->login, 'password' => $request->password, 'role' => '0', 'active' => 0])) {
                if(Auth::user()->email === $request->login){
                    return redirect('/dashboard')->with('sukses', 'Login Berhasil');
                }else {
                    Session::flush();
                    return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
                }
            }
            else {
                return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
            }
        }
        else {
            return redirect('/')->with('gagal', 'Email, Username atau Password Anda Salah');
        }

        
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/')->with('sukses', 'Logout Berhasil');
    }
}
