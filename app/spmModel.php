<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class spmModel extends Model
{
    protected $table = 'spm';
    protected $fillable = ['id', 'spm_description', 'unit_id', 'value', 'type_user', 'validate', 'validate_final'];

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }

    public function spm_trans()
    {
        return $this->hasMany(spmtransModel::class, 'id_spm', 'id');
    }
}
