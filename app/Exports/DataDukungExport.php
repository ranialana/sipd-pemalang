<?php

namespace App\Exports;

use App\datadukungModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class DataDukungExport implements FromView, ShouldAutoSize
{
    private $data_datadukung;
    public function __construct($data_datadukung)
    {
        $this->data_datadukung = $data_datadukung;
    }

    public function view(): View
    {
        return view('exports.datadukung_export', [
            'data' => $this->data_datadukung
        ]);
    }
}