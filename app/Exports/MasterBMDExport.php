<?php

namespace App\Exports;

use App\bmdModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MasterBMDExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $bmd_data = bmdModel::all();
        foreach($bmd_data as $dd){
            $data[] = array('id'=>$dd->id, 'bmd_description' => $dd->bmd_description , 'space'=> 0, 'satuan_id'=>$dd->satuan_id, 'value'=>$dd->value, 'type_user'=>$dd->type_user, 'validate'=>$dd->validate, 'validate_final'=>$dd->validate_final, 'created_at'=>$dd->creatd_at, 'updated_at'=>$dd->updated_at, 'master' => $dd->id);
            if(count($dd->child)){
                $data = parent_bmd($dd->child, $data, 0, $dd->id);
            }
        }
        if(count($bmd_data)>0){
            foreach ($data as $row) {
                if ($row['satuan_id']) {
                    if ($row['satuan_id'] > 0) {
                        $units = \App\unitsModel::find($row['satuan_id']);
                        $unit = $units->unit;
                    } else {
                        $unit = '';
                    }
                } else {
                    $unit = '';
                }
                $bmd[] = array('id'=>$row['id'], 'bmd_description' => $row['bmd_description'] , 'space'=> $row['space'], 'satuan'=>$unit, 'value'=>$row['value'], 'type_user'=>$row['type_user'], 'validate'=>$row['validate'], 'validate_final'=>$row['validate_final'], 'created_at'=>$row['created_at'], 'updated_at'=>$row['updated_at'], 'master' => $row['master']);
            }
        }
        else {
            $bmd = array();
        }
        
        return view('master_export.bmd_export', [
            'bmd' => $bmd
        ]);
    }
}
