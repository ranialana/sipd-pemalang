<?php

namespace App\Exports;

use App\subareaModel;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SubAreaExport implements FromView, ShouldAutoSize
{
    private $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        $subarea = subAreaModel::where('active', '0')->has('getBusiness')->has('getArea')->where('area_id', $this->id)->get();
        if(count($subarea)>0){
            foreach($subarea as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $subbidang[] = array('id' => $row->id, 'sub_area_name' => $row->subarea_name, 'satuan' => $unit);
            }
        }
        else {
            $subbidang = array();
        }

        return view('master_export.sub_area_export', [
            'subarea' => $subbidang
        ]);
    }
}
