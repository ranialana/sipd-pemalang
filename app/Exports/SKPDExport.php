<?php

namespace App\Exports;

use App\skpdModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SKPDExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('master_export.skpd_export', [
            'skpd' => skpdModel::all()
        ]);
    }
}
