<?php

namespace App\Exports;

use App\sdgsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SDGsExport implements FromView, ShouldAutoSize
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.sdgs_export', [
            'data' => $this->data
        ]);
    }
}