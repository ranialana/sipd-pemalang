<?php

namespace App\Exports;

use App\businessModel;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BusinessExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $business = businessModel::where('active', '0')->get();
        if(count($business)>0){
            foreach($business as $row){
                if($row->unit_id >0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else{
                    $unit = null;
                }
                $urusan[] = array('id' => $row->id, 'business_name' => $row->business_name, 'satuan' => $unit);
            }
        }
        else {
            $urusan = array();
        }

        return view('master_export.business_export', [
            'business' => $urusan
        ]);
    }
}
