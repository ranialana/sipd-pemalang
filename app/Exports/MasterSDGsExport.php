<?php

namespace App\Exports;

use App\sdgsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MasterSDGsExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('master_export.sdgs_export', [
            'sdgs' => sdgsModel::all()
        ]);
    }
}
