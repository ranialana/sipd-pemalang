<?php

namespace App\Exports;

use App\spmModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MasterSPMExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('master_export.spm_export', [
            'spm' => spmModel::all()
        ]);
    }
}
