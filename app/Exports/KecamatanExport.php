<?php

namespace App\Exports;

use App\kecamatanModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class KecamatanExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('master_export.kecamatan_export', [
            'kecamatan' => kecamatanModel::all()
        ]);
    }
}
