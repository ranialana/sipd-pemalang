<?php

namespace App\Exports;

use App\ikuModel;
use App\iku_detail;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IKUExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $iku_data = ikuModel::latest()->get();

        if(count($iku_data)>0){
            foreach($iku_data as $row){
                $iku_detail = iku_detail::where('iku_id', $row['id'])->whereHas('getBusiness', function ($q) {
                    $q->where('active', '>=', '0');
                })->get();

                if (count($iku_detail) > 0) {

                    foreach ($iku_detail as $key){

                        $detail_iku[] = $key->getBusiness->business_name;

                    }

                    $urusan = implode(', ',$detail_iku);
                } else {

                    $urusan = '-';
                }

                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $iku[] = array('id' => $row->id, 'iku_description' => $row->iku_description, 'urusan' => $urusan, 'satuan' => $unit, 'target' => $row->target);
            }
        }
        else {
            $iku = array();
        }

        return view('exports.iku_export', [
            'iku' => $iku
        ]);
    }
}