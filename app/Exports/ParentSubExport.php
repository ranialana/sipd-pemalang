<?php

namespace App\Exports;

use App\subIndicatorModel;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ParentSubExport implements FromView, ShouldAutoSize
{
    private $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        $subindicator = subIndicatorModel::where('active', '0')->has('getBusiness')->has('getArea')->has('getsubArea')->has('getIndicator')->where('active', 0)->where('parent_id', $this->id)->get();

        if(count($subindicator)>0){
            foreach($subindicator as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $subindikator[] = array('id' => $row->id, 'sub_indicator_name' => $row->sub_indicator_name, 'satuan' => $unit);
            }
        }
        else{
            $subindikator = array();
        }

        return view('master_export.parent_sub_export', [
            'subindicator' => $subindikator
        ]);
    }
}
