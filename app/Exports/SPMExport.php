<?php

namespace App\Exports;

use App\spmModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SPMExport implements FromView, ShouldAutoSize
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.spm_export', [
            'data' => $this->data
        ]);
    }
}