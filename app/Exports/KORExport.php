<?php

namespace App\Exports;

use App\korModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class KORExport implements FromView, ShouldAutoSize
{
    private $data;
    public function __construct($data)
    {
        $this->data = $data;
    }

    public function view(): View
    {
        return view('exports.kor_export', [
            'data' => $this->data
        ]);
    }
}