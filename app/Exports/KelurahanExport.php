<?php

namespace App\Exports;

use App\kelurahanModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class KelurahanExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $kelurahan = kelurahanModel::orderBy('id_kec', 'ASC')->orderBy('nama', 'ASC')->get();
        return view('master_export.kelurahan_export', [
            'kelurahan' => $kelurahan
        ]);
    }
}
