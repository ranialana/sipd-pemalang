<?php

namespace App\Exports;

use App\korModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MasterKORExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        return view('master_export.kor_export', [
            'kor'=> korModel::all()
        ]);
    }
}
