<?php

namespace App\Exports;

use App\areaModel;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AreaExport implements FromView, ShouldAutoSize
{
    private $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        $area = areaModel::where('active', '0')->has('getBusiness')->where('business_id', $this->id)->get();
        if(count($area)>0){
            foreach($area as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $bidang[] = array('id' => $row->id, 'area_name' => $row->area_name, 'satuan' => $unit);
            }
        }
        else {
            $bidang = array();
        }

        return view('master_export.area_export', [
            'area' => $bidang
        ]);
    }
}
