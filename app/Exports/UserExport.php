<?php

namespace App\Exports;

use App\userModel;
use App\skpdUser;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class UserExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $data = userModel::latest()->get();
        if(count($data)>0){
            foreach($data as $row){
                // get role
                if ($row->role === '0') {
                    $role = 'Super Admin';
                } elseif ($row->role === '1') {
                    $role = 'Penyelia';
                } else {
                    $role = 'User';
                }
                // get unit kerja
                $peran_user = skpdUser::where('user_id', $row->id)->whereHas('getSkpd', function ($q) {
                    $q->where('active', '>=', '0');
                })->get();

                if (count($peran_user) > 0) {

                    foreach ($peran_user as $key) {

                        $skpd_user[] = $key->getSkpd->skpd_name;

                    }

                    $skpd = implode(", ", $skpd_user);
                } else {

                    $skpd = '-';
                }
                //get status
                if($row->active == 0){
                    $status = "Aktif";
                }
                else {
                    $status = "Tidak Aktif";
                }

                $user[] = array('id' => $row->id, 'nama' => $row->full_name, 'role' => $role, 'username' => $row->username, 'unit_kerja' => $skpd, 'status' => $status);
            }
        }
        else {
            $user = array();
        }

        return view('master_export.user_export', [
            'user' => $user
        ]);
    }
}
