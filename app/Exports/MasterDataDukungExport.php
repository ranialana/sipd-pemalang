<?php

namespace App\Exports;

use App\datadukungModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MasterDataDukungExport implements FromView, ShouldAutoSize
{
    public function view(): View
    {
        $data_dukung = datadukungModel::all();

        foreach($data_dukung as $dd){
            $data[] = array('id'=>$dd->id, 'data_description' => $dd->data_description , 'space'=> 0, 'unit_id'=>$dd->unit_id, 'value'=>$dd->value, 'type_user'=>$dd->type_user, 'validate'=>$dd->validate, 'validate_final'=>$dd->validate_final, 'created_at'=>$dd->creatd_at, 'updated_at'=>$dd->updated_at, 'master' => $dd->id);
            if(count($dd->child)){
                $data = parent($dd->child, $data, 0, $dd->id);
            }
        }
        if(count($data_dukung)>0){
            foreach ($data as $row) {
                if ($row['unit_id']) {
                    if ($row['unit_id'] > 0) {
                        $units = \App\unitsModel::find($row['unit_id']);
                        $unit = $units->unit;
                    } else {
                        $unit = '';
                    }
                } else {
                    $unit = '';
                }
                $datadukung[] = array('id'=>$row['id'], 'data_description' => $row['data_description'] , 'space'=> $row['space'], 'satuan'=>$unit, 'value'=>$row['value'], 'type_user'=>$row['type_user'], 'validate'=>$row['validate'], 'validate_final'=>$row['validate_final'], 'created_at'=>$row['created_at'], 'updated_at'=>$row['updated_at'], 'master' => $row['master']);
            }
        }
        else {
            $datadukung = array();
        }

        return view('master_export.datadukung_export', [
            'data' => $datadukung
        ]);
    }
}
