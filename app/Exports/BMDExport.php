<?php

namespace App\Exports;

use App\bmdModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class BMDExport implements FromView, ShouldAutoSize
{
    private $data_bmd;
    public function __construct($data_bmd)
    {
        $this->data_bmd = $data_bmd;
    }

    public function view(): View
    {
        return view('exports.bmd_export', [
            'data' => $this->data_bmd
        ]);
    }
}