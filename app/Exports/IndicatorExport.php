<?php

namespace App\Exports;

use App\indicatorModel;
use App\unitsModel;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class IndicatorExport implements FromView, ShouldAutoSize
{
    private $id;
    public function __construct($id)
    {
        $this->id = $id;
    }

    public function view(): View
    {
        $indicator = indicatorModel::where('active', '0')->where('sub_area_id', $this->id)->get();

        if(count($indicator)>0){
            foreach($indicator as $row){
                if($row->unit_id>0){
                    $units = unitsModel::find($row->unit_id);
                    $unit = $units->unit;
                }
                else {
                    $unit = null;
                }
                $indikator[] = array('id' => $row->id, 'indicator_name' => $row->indicator_name, 'satuan' => $unit);
            }
        }
        else{
            $indikator = array();
        }

        return view('master_export.indicator_export', [
            'indicator' => $indikator
        ]);
    }
}
