<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bmdchildModel extends Model
{
    protected $table = "bmd_child";
    protected $fillable = ['id', 'id_bmd', 'bmd_description', 'satuan_id', 'value', 'validate', 'validate_final'];

    public function parent()
    {
        return $this->belongsTo(bmdModel::class, 'id_bmd');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
