<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sdgstransModel extends Model
{
    protected $table = 'sdgs_trans';

    protected $fillable = ['user_id', 'id_sdgs', 'id_kec', 'id_kel', 'id_tahun', 'value', 'check_active', 'status'];

    public function sdgs()
    {
        return $this->belongsTo(sdgsModel::class, 'id_sdgs');
    }
    

    public function user()
    {
        return $this->belongsTo(userModel::class, 'user_id');
    }

    public function tahun()
    {
        return $this->belongsTo(tahunModel::class, 'id_tahun');
    }
}
