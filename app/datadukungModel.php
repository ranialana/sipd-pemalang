<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datadukungModel extends Model
{
    protected $table = "data_supports";
    protected $fillable = ['id', 'data_description', 'unit_id', 'value', 'type_user', 'validate', 'validate_final'];

    public function child()
    {
        return $this->hasMany(datadukungchildModel::class, 'id_data', 'id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
