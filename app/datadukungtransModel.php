<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class datadukungtransModel extends Model
{
    protected $table = 'data_supports_trans';

    protected $fillable = ['user_id', 'id_data', 'id_data_child', 'id_kec', 'id_kel', 'tgl_pengisian', 'id_tahun', 'value', 'check_active', 'status'];

    public function datadukung()
    {
        return $this->belongsTo(datadukungModel::class, 'id_sdgs');
    }
    

    public function user()
    {
        return $this->belongsTo(userModel::class, 'user_id');
    }

    public function tahun()
    {
        return $this->belongsTo(tahunModel::class, 'id_tahun');
    }
}
