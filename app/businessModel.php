<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class businessModel extends Model
{
    protected $table = 'business';

    protected $fillable = [
        'id', 'business_name', 'check_sub', 'role', 'unit_id', 'created_at', 'updated_at', 'active',
    ];


    public function getArea()
    {

        return $this->hasMany(areaModel::class, 'business_id');
    }


    public function getSubArea()
    {

        return $this->hasMany(subAreaModel::class, 'business_id');
    }


    public function getIndicator()
    {

        return $this->hasMany(indicatorModel::class, 'business_id');
    }


    public function getSubIndicator()
    {

        return $this->hasMany(subIndicatorModel::class, 'business_id');
    }

    public function geteDatabase()
    {

        return $this->hasMany(edatabaseModel::class, 'business_id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
