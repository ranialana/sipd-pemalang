<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subAreaModel extends Model
{
    protected $table = 'sub_areas';

    protected $fillable = [
        'id', 'business_id', 'area_id', 'check_sub', 'role', 'unit_id', 'subarea_name', 'created_at', 'updated_at', 'active',
    ];

    public function getIndicator()
    {

        return $this->hasMany(indicatorModel::class, 'sub_area_id');
    }

    public function getSubIndicator()
    {

        return $this->hasMany(subIndicatorModel::class, 'sub_area_id');
    }


    public function getBusiness()
    {

        return $this->belongsTo(businessModel::class, 'business_id', 'id');
    }


    public function getArea()
    {

        return $this->belongsTo(areaModel::class, 'area_id');
    }

    public function geteDatabase()
    {

        return $this->hasMany(edatabaseModel::class, 'business_id');
    }

    public function unit()
    {
        return $this->belongsTo(unitsModel::class, 'unit_id');
    }
}
