<?php
function parent($childs, $data, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        $data[] = array('id' => $c->id, 'id_data' => $c->id_data, 'data_description' => $c->data_description, 'space' => $space, 'unit_id' => $c->unit_id, 'value' => $c->value, 'type_user' => $c->type_user, 'validate' => $c->validate, 'validate_final' => $c->validate_final, 'created_at' => $c->creatd_at, 'updated_at' => $c->updated_at, 'master' => $master);
        // if (count($c->child)) {
        //     $data = parent($c->child, $data, $space, $master);
        // }
    }
    return $data;
}

function parent_bmd($childs, $data, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        $data[] = array('id' => $c->id, 'id_bmd' => $c->id_bmd, 'bmd_description' => $c->bmd_description, 'space' => $space, 'satuan_id' => $c->satuan_id, 'value' => $c->value, 'type_user' => $c->type_user, 'validate' => $c->validate, 'validate_final' => $c->validate_final, 'created_at' => $c->creatd_at, 'updated_at' => $c->updated_at, 'master' => $master);
        // if (count($c->child)) {
        //     $data = parent($c->child, $data, $space, $master);
        // }
    }
    return $data;
}

function parent_si($childs, $data, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        if (count($c->child)) {
            $status = 0;
        } else {
            $status = 1;
        }
        $data[] = array('id' => $c->id, 'sub_indicator_name' => $c->sub_indicator_name, 'space' => $space, 'unit_id' => $c->unit_id, 'indicator_id' => $c->indicator_id, 'status' => $status);
        // if ($c->role === "Kecamatan" && $c->check_sub === 0 || $c->role === "Kelurahan" && $c->check_sub === 0) {
        // } else {
        if (count($c->child)) {
            echo count($c->child);
            $data = parent_si($c->child, $data, $space, $master);
        }
        // }
    }
    return $data;
}

function parent_link($get_parent, $parent, $number){
    $number = $number + 1;
    if($get_parent->parent){
        $parent[] = array('id' => $get_parent->id, 'name' => $get_parent->sub_indicator_name, 'number' => $number);
        $parent = parent_link($get_parent->parent, $parent, $number);
    }
    else {
        $parent[] = array('id' => $get_parent->id, 'name' => $get_parent->sub_indicator_name, 'number' => $number);
    }
    return $parent;
}
// Get Parent Child Sub Indicator Super Admin
function parent_si_edb($childs, $data_edb, $space, $master, $list, $list_parent, $role, $master2, $data_parent, $data_si)
{
    // echo dd($childs);
    $space = $space + 4;
    foreach ($childs as $c) {
        if (in_array($c->id, $list)) {
            $count = array();
            for ($i = 0; $i < count($data_edb); $i++) {

                if (array_key_exists("master2", $data_edb[$i])) {
                    if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                        $count[] = $master2;
                    }
                } else {
                }
            }
            // echo count($count);
            $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
            $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
            // echo dd($data_edb);
            // if ($c->role === "Kecamatan" && $c->check_sub === 0 || $c->role === "Kelurahan" && $c->check_sub === 0) {
            // } else {

        } else {
            // echo dd($list);
            if (in_array($c->id, $list_parent)) {
                // echo $c->id;
                // echo "<br>".$c->id;            

                if (count($c->child)) {
                    // echo count($c->child);
                    $count = array();
                    for ($i = 0; $i < count($data_edb); $i++) {

                        if (array_key_exists("master2", $data_edb[$i])) {
                            if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                                $count[] = $master2;
                            }
                        } else {
                        }
                    }

                    $data_edb[] = array('uraian' => $c->sub_indicator_name, 'role' => $data_parent[$c->id], 'check_sub' => $c->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                    // echo dd($c->child);
                    $data_edb = parent_si_edb($c->child, $data_edb, $space, $master, $list, $list_parent, $role, $c->id, $data_parent, $data_si);
                } else {
                }
            } else {
            }
            // else {
            //     // echo $c->id;
            // }
            // echo count($c->child);
        }
        // print_r($list);
    }
    return $data_edb;
}
// Get Parent Child Sub Indicator Perangkat Daerah
function parent_si_edb_pd($childs, $data_edb, $space, $master, $list, $list_parent, $role, $master2, $data_parent, $data_si, $skpd_name)
{
    $space = $space + 4;
    foreach ($childs as $c) {
        if (in_array($c->id, $list)) {
            $count = array();
            for ($i = 0; $i < count($data_edb); $i++) {

                if (array_key_exists("master2", $data_edb[$i])) {
                    if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                        $count[] = $master2;
                    }
                } else {
                }
            }

            for($r_si=0; $r_si<count($data_si[$c->id]); $r_si++){
                if(in_array($data_si[$c->id][$r_si], $skpd_name)){
                    $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
                    $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                break;
                }
            }

        } else {
            if (in_array($c->id, $list_parent)) {        

                if (count($c->child)) {
                    $count = array();
                    for ($i = 0; $i < count($data_edb); $i++) {

                        if (array_key_exists("master2", $data_edb[$i])) {
                            if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                                $count[] = $master2;
                            }
                        } else {
                        }
                    }
                    for($r_si=0; $r_si<count($data_parent[$c->id]); $r_si++){
                        if(in_array($data_parent[$c->id][$r_si], $skpd_name)){
                            $data_edb[] = array('uraian' => $c->sub_indicator_name, 'role' => $data_parent[$c->id], 'check_sub' => $c->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                            // echo dd($c->child);
                            $data_edb = parent_si_edb_pd($c->child, $data_edb, $space, $master, $list, $list_parent, $role, $c->id, $data_parent, $data_si, $skpd_name);
                        break;
                        }
                    }
                    
                } else {
                }
            } else {
            }
        }
    }
    return $data_edb;
}

// Get Parent Child Sub Indicator Kecamatan
function parent_si_edb_kecamatan($childs, $data_edb, $space, $master, $list, $list_parent, $role, $master2, $data_parent, $data_si, $kecamatan)
{
    $space = $space + 4;
    foreach ($childs as $c) {
        if (in_array($c->id, $list)) {
            $count = array();
            for ($i = 0; $i < count($data_edb); $i++) {

                if (array_key_exists("master2", $data_edb[$i])) {
                    if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                        $count[] = $master2;
                    }
                } else {
                }
            }
                if(in_array("Kecamatan", $data_si[$c->id])){
                    if(count((array)$c->parent) === 0){
                        if($c->getIndicator->role==="Kecamatan" && $c->getIndicator->check_sub === 0){
                            if($c->sub_indicator_name === $kecamatan){
                                $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
                                $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                            }
                        }
                    }
                    else {
                        if($c->parent->role==="Kecamatan" && $c->parent->check_sub === 0){
                            if($c->sub_indicator_name === $kecamatan){
                                $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
                                $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                            }
                        }
                    }
                    
                }
        } else {
            if (in_array($c->id, $list_parent)) {        

                if (count($c->child)) {
                    $count = array();
                    for ($i = 0; $i < count($data_edb); $i++) {

                        if (array_key_exists("master2", $data_edb[$i])) {
                            if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                                $count[] = $master2;
                            }
                        } else {
                        }
                    }
                        if(in_array("Kecamatan", $data_parent[$c->id])){
                            $data_edb[] = array('uraian' => $c->sub_indicator_name, 'role' => $data_parent[$c->id], 'check_sub' => $c->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);

                            $data_edb = parent_si_edb_kecamatan($c->child, $data_edb, $space, $master, $list, $list_parent, $role, $c->id, $data_parent, $data_si, $kecamatan);
                        }
                    
                } else {
                }
            } else {
            }
        }
    }
    return $data_edb;
}

// Get Parent Child Sub Indicator Kecamatan
function parent_si_edb_kelurahan($childs, $data_edb, $space, $master, $list, $list_parent, $role, $master2, $data_parent, $data_si, $kelurahan)
{
    $space = $space + 4;
    foreach ($childs as $c) {
        if (in_array($c->id, $list)) {
            $count = array();
            for ($i = 0; $i < count($data_edb); $i++) {

                if (array_key_exists("master2", $data_edb[$i])) {
                    if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                        $count[] = $master2;
                    }
                } else {
                }
            }
                if(in_array("Kelurahan", $data_si[$c->id])){
                    if(count((array)$c->parent) === 0){
                        if($c->getIndicator->role==="Kelurahan" && $c->getIndicator->check_sub === 0){
                            if($c->sub_indicator_name === $kelurahan){
                                $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
                                $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                            }
                        }
                    }
                    else {
                        if($c->parent->role==="Kelurahan" && $c->parent->check_sub === 0){
                            if($c->sub_indicator_name === $kelurahan){
                                $edb = \App\edatabaseModel::where('sub_indicator_id', $c->id)->first();
                                $data_edb[] = array('id' => $edb->id, 'uraian' => $c->sub_indicator_name, 'role' => $data_si[$c->id], 'check_sub' => $c->check_sub, 'value' => $edb->value, 'satuan' => $edb->unit_id, 'action' => 1, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);
                            }
                        }
                    }
                    
                }
        } else {
            if (in_array($c->id, $list_parent)) {        

                if (count($c->child)) {
                    $count = array();
                    for ($i = 0; $i < count($data_edb); $i++) {

                        if (array_key_exists("master2", $data_edb[$i])) {
                            if ($data_edb[$i]['master2'] === $master2 && $data_edb[$i]['space'] === $space) {
                                $count[] = $master2;
                            }
                        } else {
                        }
                    }
                        if(in_array("Kelurahan", $data_parent[$c->id])){
                            $data_edb[] = array('uraian' => $c->sub_indicator_name, 'role' => $data_parent[$c->id], 'check_sub' => $c->check_sub, 'value' => '-', 'satuan' => 0, 'action' => 0, 'master' => $master, 'number' => count($count) + 1, 'space' => $space, 'master2' => $master2);

                            $data_edb = parent_si_edb_kelurahan($c->child, $data_edb, $space, $master, $list, $list_parent, $role, $c->id, $data_parent, $data_si, $kelurahan);
                        }
                    
                } else {
                }
            } else {
            }
        }
    }
    return $data_edb;
}

function parent_si_role_edb($childs, $parent_si, $subindicator_edb, $role_parent)
{
    foreach($childs as $c){
        if (in_array($c->id, $subindicator_edb)) {
            $role_parent[] = $c->role;
        } else {
            if (in_array($c->id, $parent_si)) {
                if (count($c->child)) {
                        $role_parent = parent_si_role_edb($c->child, $parent_si, $subindicator_edb, $role_parent);
                } else {
                    // $data[] = $childs->role;
                    // echo $c->role;
                }
            } else {
                // $data[] = '';
            }
        }
    }
    // print_r($role_parent);

    return $role_parent;
}

function parent_sub_indicator($childs, $parent_si, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        $parent_si[] = array('id' => $c->id, 'id_data' => $c->id_data, 'name' => $c->sub_indicator_name, 'space' => $space, 'unit_id' => $c->unit_id, 'value' => $c->value, 'type_user' => $c->type_user, 'validate' => $c->validate, 'validate_final' => $c->validate_final, 'created_at' => $c->creatd_at, 'updated_at' => $c->updated_at, 'master' => $master);
        if (count($c->child)) {
            $parent_si = parent_sub_indicator($c->child, $parent_si, $space, $master);
        }
    }
    return $parent_si;
}

function get_parent_si($id, $parent, $data_edb, $array_parent, $master, $array_ssi, $value, $count_si, $parent_array, $count)
{
    // print_r($array_si);

    // print_r($array_parent);
    if ($parent->parent) {
        if (in_array($parent->id, $parent_array)) {
            // echo "<b>".$parent->id."</b>";
            $count = $count + 1;
        } else {
            $array_parent[] = $parent->id;
            $parent_array[] = $parent->id;
            $array_ssi[] = array('uraian' => $parent->sub_indicator_name, 'satuan' => 0, 'role' => $parent->role, 'check_sub' => $parent->check_sub, 'value' => $value, 'action' => 0, 'master' => $master, 'number' => $count_si + 1);
            $count = $count + 1;
        }
        // print_r($array_parent);
        if (in_array($parent->sub_indicator_id, $parent_array)) {
            $count = $count + 1;
        } else {
            if (in_array($parent->parent->id, $parent_array)) {
                $parent_array[] = $parent->parent->id;
                get_parent_si($id, $parent->parent, $data_edb, $array_parent, $master, $array_ssi, $value, $count_si + 1, $parent_array, $count);
            } else {
                // $array_ssi = $parent->sub_indicator_id;
                // $array_parent[] = $parent->parent->id;
                // print_r($array_parent);
                // $array_si = array_merge($array_parent, $array);
                // echo dd($array);
                $parent_array[] = $parent->parent->id;
                $array_ssi[] = array('uraian' => $parent->parent->sub_indicator_name, 'satuan' => 0, 'role' => $parent->parent->role, 'check_sub' => $parent->parent->check_sub, 'value' => $value, 'action' => 0, 'master' => $master, 'number' => $count_si + 1);
                $count = $count + 1;
                get_parent_si($id, $parent->parent, $data_edb, $array_parent, $master, $array_ssi, $value, $count_si + 1, $parent_array, $count);


                // echo dd($array_ssi);
            }
        }
    } else {
        // print_r($array_parent);
        if (in_array($parent->id, $parent_array)) {
            // echo "<b>".$parent->id."</b>";
            $count = $count + 1;
        } else {
            $array_parent[] = $parent->id;
            $parent_array[] = $parent->id;
            $array_ssi[] = array('uraian' => $parent->sub_indicator_name, 'satuan' => 0, 'role' => $parent->role, 'check_sub' => $parent->check_sub, 'value' => $value, 'action' => 0, 'master' => $master, 'number' => $count_si + 1);
            $count = $count + 1;
        }
        // print_r($array_parent);
    }
    $data = array('array_ssi' => $array_ssi, 'array_parent' => $array_parent, 'count' => $count);

    return $data;
}

function get_child_si($childs, $data, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        $data[] = array('id' => $c->id, 'id_bmd' => $c->id_bmd, 'bmd_description' => $c->bmd_description, 'space' => $space, 'satuan_id' => $c->satuan_id, 'value' => $c->value, 'type_user' => $c->type_user, 'validate' => $c->validate, 'validate_final' => $c->validate_final, 'created_at' => $c->creatd_at, 'updated_at' => $c->updated_at, 'master' => $master);
        // if (count($c->child)) {
        //     $data = parent($c->child, $data, $space, $master);
        // }
    }
    return $data;
}

function getparent_si($parent, $array_parent)
{
    $array_parent[] = array($parent->id);
    if ($parent->parent) {

        $array_parent = getparent_si($parent->parent, $array_parent);
    } else {
        $array_parent[] = array($parent->id);
    }
    // echo dd($array_parent);
    // print_r(single_array($array_parent));
    return $array_parent;
}

function single_array($array)
{
    if (!is_array($array)) {
        return FALSE;
    }
    $result = array();
    foreach ($array as $key => $value) {
        if (is_array($value)) {
            $result = array_merge($result, single_array($value));
        } else {
            $result[$key] = $value;
        }
    }
    return $result;
}

function role_edb($data, $user, $skpd, $kec, $kel)
{
    foreach ($data as $u => $urusan) {
        foreach ($urusan as $b => $bidang) {
            foreach ($bidang as $sb => $subbidang) {
                foreach ($subbidang as $i => $indikator) {
                    $role = array();
                    $role_bidang = array();
                    $role_subbidang = array();
                    $role_indicator = array();
                    foreach ($indikator as $si => $subindikator) {
                        if ($subindikator->business_id != null) {
                            if ($subindikator->area_id != null) {
                                if ($subindikator->sub_area_id != null) {
                                    if ($subindikator->indicator_id != null) {
                                        if ($subindikator->sub_indicator_id != null) {
                                            // echo $subindikator->getSubIndicator->role;
                                            if (in_array($subindikator->getSubIndicator->role, $role)) {
                                            } else {
                                                $role[] = $subindikator->getSubIndicator->role;
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_bidang)) {
                                            } else {
                                                $role_bidang[] = $subindikator->getSubIndicator->role;
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_subbidang)) {
                                            } else {
                                                $role_subbidang[] = $subindikator->getSubIndicator->role;
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_indicator)) {
                                            } else {
                                                $role_indicator[] = $subindikator->getSubIndicator->role;
                                            }
                                            $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                        } else {
                                            $role[] = $subindikator->getIndicator->role;
                                            $role_bidang[] = $subindikator->getIndicator->role;
                                            $role_subbidang[] = $subindikator->getIndicator->role;
                                            $role_indicator[] = $subindikator->getIndicator->role;
                                        }
                                    } else {
                                        $role[] = $subindikator->getSubArea->role;
                                        $role_bidang[] = $subindikator->getSubArea->role;
                                        $role_subbidang[] = $subindikator->getSubArea->role;
                                    }
                                } else {
                                    $role[] = $subindikator->getArea->role;
                                    $role_bidang[] = $subindikator->getArea->role;
                                }
                            } else {
                                $role[] = $subindikator->getBusiness->role;
                            }
                        } else {
                        }
                    }
                    $data_business[] = array('id' => $u, 'role' => $role);
                    $data_area[] = array('id' => $b, 'role' => $role_bidang);
                    $data_subarea[] = array('id' => $sb, 'role' => $role_subbidang);
                    $data_indicator[] = array('id' => $i, 'role' => $role_indicator);
                    // echo "<br>".$b; 
                    // $data_area[$b] = array($role_bidang);
                    // $data_subarea[$sb] = array($role_subbidang);
                    // $data_indicator[$i] = array($role_indicator);

                    // echo dd($role_subbidang);
                }
            }
        }
    }
    // echo dd($data_business);
    $role_list = array(
        'data_business' => $data_business,
        'data_area' => $data_area,
        'data_subarea' => $data_subarea,
        'data_indicator' => $data_indicator,
        'data_si' => $data_si
    );
    return $role_list;
}

function edb($data, $user, $skpd, $kec, $kel)
{
    if (is_array($kec)) {
        $kecamatan = single_array($kec);
    } else {
        $kecamatan = 0;
    }
    if (is_array($kec)) {
        $kelurahan = single_array($kel);
    } else {
        $kelurahan = 0;
    }

    foreach ($data as $u => $urusan) {
        foreach ($urusan as $b => $bidang) {
            foreach ($bidang as $sb => $subbidang) {
                foreach ($subbidang as $i => $indikator) {
                    $role = array();
                    $role_bidang = array();
                    $role_subbidang = array();
                    $role_indicator = array();
                    foreach ($indikator as $si => $subindikator) {
                        if ($subindikator->business_id != null) {
                            if ($subindikator->area_id != null) {
                                if ($subindikator->sub_area_id != null) {
                                    if ($subindikator->indicator_id != null) {
                                        if ($subindikator->sub_indicator_id != null) {
                                            if (in_array($subindikator->getSubIndicator->role, $role)) {
                                            } else {
                                                if ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 0) {
                                                    if ($user['role'] === '0') {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        } else {
                                                            // echo dd($kelurahan);
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        }
                                                    }
                                                } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                    if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    }
                                                    if ($user['role'] === '0') {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        }
                                                    }
                                                }
                                                if ($subindikator->getSubIndicator->role === "Kelurahan" && $subindikator->getSubindicator->check_sub === 0) {
                                                    if ($user['role'] === '0') {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        } else {
                                                            $kecamatan = single_array($kec);
                                                            $kelurahan = single_array($kel);
                                                            // echo dd($kelurahan);
                                                            if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                $role[] = $subindikator->getSubIndicator->role;
                                                            } else {
                                                                $role[] = $subindikator->getSubIndicator->role;
                                                            }
                                                        }
                                                    }
                                                } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                    if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    }
                                                    if ($user['role'] === '0') {
                                                        $role[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role[] = array();
                                                                }
                                                            } else {
                                                                $role[] = array();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $role[] = $subindikator->getSubIndicator->role;
                                                }
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_bidang)) {
                                            } else {
                                                if ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 0) {
                                                    if ($user['role'] === '0') {
                                                        $role_bidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_bidang[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_bidang[] = array();
                                                                }
                                                            } else {
                                                                $role_bidang[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                                }
                                                            } else {
                                                                $role_bidang[] = array();
                                                            }
                                                        }
                                                    }
                                                } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                    if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                        $role_bidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        $role_bidang[] = $subindikator->getSubIndicator->role;
                                                    }
                                                    if ($user['role'] === '0') {
                                                        $role_bidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_bidang[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_bidang[] = array();
                                                                }
                                                            } else {
                                                                $role_bidang[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_bidang[] = array();
                                                                }
                                                            } else {
                                                                $role_bidang[] = array();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $role_bidang[] = $subindikator->getSubIndicator->role;
                                                }
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_subbidang)) {
                                            } else {
                                                if ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 0) {
                                                    if ($user['role'] === '0') {
                                                        $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_subbidang[] = array();
                                                                }
                                                            } else {
                                                                $role_subbidang[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                                }
                                                            } else {
                                                                $role_subbidang[] = array();
                                                            }
                                                        }
                                                    }
                                                } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                    if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                        $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                    }
                                                    if ($user['role'] === '0') {
                                                        $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_subbidang[] = array();
                                                                }
                                                            } else {
                                                                $role_subbidang[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_subbidang[] = array();
                                                                }
                                                            } else {
                                                                $role_subbidang[] = array();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $role_subbidang[] = $subindikator->getSubIndicator->role;
                                                }
                                            }
                                            if (in_array($subindikator->getSubIndicator->role, $role_indicator)) {
                                            } else {
                                                if ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 0) {
                                                    if ($user['role'] === '0') {
                                                        $role_indicator[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_indicator[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_indicator[] = array();
                                                                }
                                                            } else {
                                                                $role_indicator[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                                }
                                                            } else {
                                                                $role_indicator[] = array();
                                                            }
                                                        }
                                                    }
                                                } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                    if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                        $role_indicator[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        $role_indicator[] = $subindikator->getSubIndicator->role;
                                                    }
                                                    if ($user['role'] === '0') {
                                                        $role_indicator[] = $subindikator->getSubIndicator->role;
                                                    } else {
                                                        if ($user['type_user'] === 0) {
                                                            $role_indicator[] = $subindikator->getSubIndicator->role;
                                                        } elseif ($user['type_user'] === 1) {
                                                            if (is_array($kecamatan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_indicator[] = array();
                                                                }
                                                            } else {
                                                                $role_indicator[] = array();
                                                            }
                                                        } else {
                                                            if (is_array($kelurahan)) {
                                                                if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                                } else {
                                                                    $role_indicator[] = array();
                                                                }
                                                            } else {
                                                                $role_indicator[] = array();
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $role_indicator[] = $subindikator->getSubIndicator->role;
                                                }
                                            }
                                            if ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 0) {
                                                if ($user['role'] === '0') {
                                                    $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                } else {
                                                    if ($user['type_user'] === 0) {
                                                        $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                    } elseif ($user['type_user'] === 1) {
                                                        if (is_array($kecamatan)) {
                                                            if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                            } else {
                                                                $data_si[$subindikator->sub_indicator_id] = array();
                                                            }
                                                        } else {
                                                            $data_si[$subindikator->sub_indicator_id] = array();
                                                        }
                                                    } else {
                                                        if (is_array($kelurahan)) {
                                                            if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                            } else {
                                                                $data_si[$subindikator->sub_indicator_id] = array();
                                                            }
                                                        } else {
                                                            $data_si[$subindikator->sub_indicator_id] = array();
                                                        }
                                                    }
                                                }
                                            } elseif ($subindikator->getSubIndicator->role === "Kecamatan" && $subindikator->getSubindicator->check_sub === 1) {
                                                if (count((array)$subindikator->getSubindicator->child) > 0) {
                                                    $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                } else {
                                                    $data_si[$subindikator->sub_indicator_id] = array();
                                                }
                                                if ($user['role'] === '0') {
                                                    $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                } else {
                                                    if ($user['type_user'] === 0) {
                                                        $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                    } elseif ($user['type_user'] === 1) {
                                                        if (is_array($kecamatan)) {
                                                            if ($subindikator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                            } else {
                                                                $data_si[$subindikator->sub_indicator_id] = array();
                                                            }
                                                        } else {
                                                            $data_si[$subindikator->sub_indicator_id] = array();
                                                        }
                                                    } else {
                                                        if (is_array($kelurahan)) {
                                                            if ($subindikator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                                            } else {
                                                                $data_si[$subindikator->sub_indicator_id] = array();
                                                            }
                                                        } else {
                                                            $data_si[$subindikator->sub_indicator_id] = array();
                                                        }
                                                    }
                                                }
                                            } else {
                                                $data_si[$subindikator->sub_indicator_id] = array($subindikator->getSubIndicator->role);
                                            }
                                        } else {
                                            $role[] = $subindikator->getIndicator->role;
                                            $role_bidang[] = $subindikator->getIndicator->role;
                                            $role_subbidang[] = $subindikator->getIndicator->role;
                                            $role_indicator[] = $subindikator->getIndicator->role;
                                        }
                                    } else {
                                        $role[] = $subindikator->getSubArea->role;
                                        $role_bidang[] = $subindikator->getSubArea->role;
                                        $role_subbidang[] = $subindikator->getSubArea->role;
                                    }
                                } else {
                                    $role[] = $subindikator->getArea->role;
                                    $role_bidang[] = $subindikator->getArea->role;
                                }
                            } else {
                                $role[] = $subindikator->getBusiness->role;
                            }
                        } else {
                        }
                    }
                    $data_business[$u] = array($role);
                    // echo "<br>".$b; 
                    $data_area[$b] = array($role_bidang);
                    $data_subarea[$sb] = array($role_subbidang);
                    $data_indicator[$i] = array($role_indicator);

                    // echo dd($role_subbidang);
                }
            }
        }
    }
    // echo dd($data_subarea);
    // print_r($kec);


    $array_ssi = array();
    $array = array();
    $array_si = array();
    $array_parent[] = 0;
    $parent_array = array();
    $space = 0;
    $ib = 1;
    $i = 1;
    $array_b = array();
    $count_business = count($array_b) + 1;
    foreach ($data as $b => $business) {
        $ai = 1;
        $count_a = 0;
        $array_a = array();
        $count_area = count($array_a) + 1;
        $role_business = array();
        foreach ($business as $a => $area) {
            $count_a += count($area);
            if ($count_a > 0) {
                $sai = 1;
                $count_sa = 0;
                $array_sa = array();
                $count_sub_area = count($array_sa) + 1;
                $role_area = array();
                foreach ($area as $sa => $subarea) {
                    $count_sa += count($subarea);
                    if ($count_sa > 0) {

                        $ii = 1;
                        $count_i = 1;
                        $array_i = array();
                        $count_indicator = count($array_i) + 1;
                        $role_subarea = array();
                        foreach ($subarea as $i => $indicator) {
                            $count_i += count($indicator);
                            if ($count_i > 0) {

                                $si_i = 1;
                                $count_sub_indicator = count($array_si) + 1;
                                $array_parent = single_array($array_parent);
                                // print_r($array_parent);
                                $array_coba = array();
                                $role_indicator = array();
                                // $role = array();
                                // echo dd($data_business[1]);
                                foreach ($indicator as $si => $subindicator) {

                                    $role_subindicator = array();
                                    if ($user['role'] === '0') {
                                        // echo dd($auth->getSkpdUser());
                                        if ($subindicator->business_id != null) {

                                            if (in_array($subindicator->business_id, $array_b)) {
                                                // echo $count_business.'<br>';
                                            } else {
                                                if ($subindicator->area_id != 0) {
                                                    $value = '-';
                                                    $action = 0;
                                                    $satuan = '';
                                                } else {
                                                    $value = $subindicator->value;
                                                    $action = 1;
                                                    $satuan = $subindicator->getBusiness->unit->unit;
                                                }
                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getBusiness->business_name, 'satuan' => $satuan, 'role' => $subindicator->getBusiness->role, 'check_sub' => $subindicator->getBusiness->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_business++, 'space' => 0);
                                                $array_b[] = $subindicator->business_id;
                                            }
                                        }
                                        if ($subindicator->area_id != null) {
                                            if (in_array($subindicator->area_id, $array_a)) {
                                            } else {
                                                if ($subindicator->sub_area_id != 0) {
                                                    $value = '-';
                                                    $action = 0;
                                                    $satuan = '';
                                                } else {
                                                    $value = $subindicator->value;
                                                    $action = 1;
                                                    $satuan = $subindicator->getArea->unit->unit;
                                                }
                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);

                                                $array_a[] = $subindicator->area_id;
                                            }
                                        }
                                        if ($subindicator->sub_area_id != null) {
                                            if (in_array($subindicator->sub_area_id, $array_sa)) {
                                            } else {
                                                if ($subindicator->indicator_id != 0) {
                                                    $value = '-';
                                                    $action = 0;
                                                    $satuan = '';
                                                } else {
                                                    $value = $subindicator->value;
                                                    $action = 1;
                                                    $satuan = $subindicator->getSubArea->unit->unit;
                                                }
                                                $array_sa[] = $subindicator->sub_area_id;
                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                            }
                                        }
                                        if ($subindicator->indicator_id != null) {
                                            if (in_array($subindicator->indicator_id, $array_i)) {
                                            } else {
                                                if ($subindicator->sub_indicator_id != 0) {
                                                    $value = '-';
                                                    $action = 0;
                                                    $satuan = '';
                                                } else {
                                                    $value = $subindicator->value;
                                                    $action = 1;
                                                    $satuan = $subindicator->getIndicator->unit->unit;
                                                }
                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                $array_i[] = $subindicator->indicator_id;
                                            }
                                        }
                                        if ($subindicator->sub_indicator_id != null) {
                                            // print_r($array_b);
                                            if (in_array($subindicator->sub_indicator_id, $array_si)) {
                                            } else {

                                                if ($subindicator->getSubIndicator->parent) {
                                                    $count = 0;
                                                    $data = get_parent_si(
                                                        $subindicator->getSubIndicator->id,
                                                        $subindicator->getSubIndicator->parent,
                                                        $data_edb,
                                                        $array_parent,
                                                        $b,
                                                        $array_ssi,
                                                        $subindicator->value,
                                                        $count_sub_indicator,
                                                        $parent_array,
                                                        $count
                                                    );
                                                    // $array_parent[] = $data['array_parent'];
                                                    // $array_sub = single_array($array_parent);
                                                    // for ($c = 0; $c < count($array_sub); $c++) {
                                                    //     if (in_array($array_sub[$c], $parent_array)) {
                                                    //     } else {
                                                    //         $parent_array[] = $array_sub[$c];
                                                    //     }
                                                    // }
                                                    // print_r($data['array_parent']);
                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                    $spasi = 12;
                                                    $number = 1;
                                                    for ($dssi = count($data['array_ssi']) - 1; $dssi >= 0; $dssi--) {
                                                        $spasi = $spasi + 4;
                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $data['array_ssi'][$dssi]['uraian'], 'satuan' => $data['array_ssi'][$dssi]['satuan'], 'role' => $data['array_ssi'][$dssi]['role'], 'check_sub' => $data['array_ssi'][$dssi]['check_sub'], 'value' => '-', 'action' => $data['array_ssi'][$dssi]['action'], 'master' => $b, 'number' => $number++, 'space' => $spasi);
                                                    }
                                                    $space = $data['count'] * 4 + 16;
                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => $count_sub_indicator++, 'space' => $space);
                                                } else {
                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => 1, 'space' => 16);

                                                    // if($subindicator[''])
                                                }
                                            }
                                        }
                                    } else {
                                        if ($user['type_user'] === 0) {
                                            $skpd_name = array();
                                            // echo count($skpd);
                                            // echo count((array)$skpd);
                                            // echo dd($skpd);
                                            if (is_array($skpd)) {
                                                foreach ($skpd as $sk) {
                                                    $skpd_name[] = $sk['skpd_name'];
                                                }
                                            } else {
                                                $skpd_name = 0;
                                            }

                                            if ($subindicator->business_id != null) {

                                                if (in_array($subindicator->business_id, $array_b)) {
                                                } else {
                                                    if ($subindicator->area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getBusiness->unit->unit;
                                                    }
                                                    $business_role = single_array($data_business[$subindicator->business_id]);
                                                    for ($rb = 0; $rb < count($business_role); $rb++) {
                                                        if (is_array($skpd_name)) {
                                                            if (in_array($business_role[$rb], $skpd_name)) {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getBusiness->business_name, 'satuan' => $satuan, 'role' => $subindicator->getBusiness->role, 'check_sub' => $subindicator->getBusiness->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_business++, 'space' => 0);
                                                                $array_b[] = $subindicator->business_id;
                                                            } else {
                                                            }
                                                        } else {
                                                            $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->area_id != null) {
                                                if (in_array($subindicator->area_id, $array_a)) {
                                                } else {
                                                    if ($subindicator->sub_area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getArea->unit->unit;
                                                    }
                                                    $area_role = single_array($data_area[$subindicator->area_id]);
                                                    for ($rb = 0; $rb < count($area_role); $rb++) {
                                                        if (is_array($skpd_name)) {
                                                            if (in_array($area_role[$rb], $skpd_name)) {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                $array_a[] = $subindicator->area_id;
                                                                break;
                                                            } else {
                                                            }
                                                        } else {
                                                            $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_area_id != null) {

                                                if (in_array($subindicator->sub_area_id, $array_sa)) {
                                                } else {
                                                    if ($subindicator->indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getSubArea->unit->unit;
                                                    }
                                                    $subarea_role = single_array($data_subarea[$subindicator->sub_area_id]);
                                                    for ($rb = 0; $rb < count($subarea_role); $rb++) {
                                                        if (is_array($skpd_name)) {
                                                            if (in_array($area_role[$rb], $skpd_name)) {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                $array_sa[] = $subindicator->sub_area_id;
                                                                break;
                                                            } else {
                                                            }
                                                        } else {
                                                            $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->indicator_id != null) {
                                                if (in_array($subindicator->indicator_id, $array_i)) {
                                                } else {
                                                    if ($subindicator->sub_indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getIndicator->unit->unit;
                                                    }
                                                    $indicator_role = single_array($data_indicator[$subindicator->indicator_id]);
                                                    for ($rb = 0; $rb < count($indicator_role); $rb++) {
                                                        if (is_array($skpd_name)) {
                                                            if (in_array($indicator_role[$rb], $skpd_name)) {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                $array_i[] = $subindicator->indicator_id;
                                                                break;
                                                            } else {
                                                            }
                                                        } else {
                                                            $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_indicator_id != null) {
                                                // print_r($array_b);
                                                if (in_array($subindicator->sub_indicator_id, $array_si)) {
                                                } else {
                                                    $subindicator_role = single_array($data_si[$subindicator->sub_indicator_id]);
                                                    for ($rb = 0; $rb < count($subindicator_role); $rb++) {
                                                        if (is_array($skpd_name)) {
                                                            if (in_array($subindicator_role[$rb], $skpd_name)) {
                                                                if ($subindicator->getSubIndicator->parent) {
                                                                    $count = 0;
                                                                    $data = get_parent_si(
                                                                        $subindicator->getSubIndicator->id,
                                                                        $subindicator->getSubIndicator->parent,
                                                                        $data_edb,
                                                                        $array_parent,
                                                                        $b,
                                                                        $array_ssi,
                                                                        $subindicator->value,
                                                                        $count_sub_indicator,
                                                                        $parent_array,
                                                                        $count
                                                                    );
                                                                    $array_parent[] = $data['array_parent'];
                                                                    $array_sub = single_array($array_parent);
                                                                    for ($c = 0; $c < count($array_sub); $c++) {
                                                                        if (in_array($array_sub[$c], $parent_array)) {
                                                                        } else {
                                                                            $parent_array[] = $array_sub[$c];
                                                                        }
                                                                    }
                                                                    // echo dd($data['array_parent']);
                                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                                    $spasi = 12;
                                                                    $number = 1;
                                                                    for ($dssi = count($data['array_ssi']) - 1; $dssi >= 0; $dssi--) {
                                                                        $spasi = $spasi + 4;
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $data['array_ssi'][$dssi]['uraian'], 'satuan' => $data['array_ssi'][$dssi]['satuan'], 'role' => $data['array_ssi'][$dssi]['role'], 'check_sub' => $data['array_ssi'][$dssi]['check_sub'], 'value' => '-', 'action' => $data['array_ssi'][$dssi]['action'], 'master' => $b, 'number' => $number++, 'space' => $spasi);
                                                                    }
                                                                    $space = $data['count'] * 4 + 16;
                                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => $count_sub_indicator++, 'space' => $space);
                                                                } else {
                                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => 1, 'space' => 16);

                                                                    // if($subindicator[''])
                                                                }
                                                            } else {
                                                            }
                                                        } else {
                                                            $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                        } elseif ($user['type_user'] === 1) {
                                            // $kecamatan = $kec->nama;
                                            if ($subindicator->business_id != null) {
                                                if (in_array($subindicator->business_id, $array_b)) {
                                                } else {
                                                    if ($subindicator->area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getBusiness->unit->unit;
                                                    }
                                                    $business_role = single_array($data_business[$subindicator->business_id]);
                                                    for ($rb = 0; $rb < count($business_role); $rb++) {

                                                        if ($business_role[$rb] === "Kecamatan") {
                                                            if (is_array($kecamatan)) {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getBusiness->business_name, 'satuan' => $satuan, 'role' => $subindicator->getBusiness->role, 'check_sub' => $subindicator->getBusiness->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_business++, 'space' => 0);
                                                                $array_b[] = $subindicator->business_id;
                                                            } else {
                                                                $data_edb = array();
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->area_id != null) {
                                                if (in_array($subindicator->area_id, $array_a)) {
                                                } else {
                                                    if ($subindicator->sub_area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getArea->unit->unit;
                                                    }
                                                    $area_role = single_array($data_area[$subindicator->area_id]);
                                                    for ($rb = 0; $rb < count($area_role); $rb++) {
                                                        if ($area_role[$rb] === "Kecamatan") {
                                                            if ($subindicator->getBusiness->role === "Kecamatan" && $subindicator->getBusiness->check_sub === 0) {
                                                                if (is_array($kecamatan)) {
                                                                    // =============================
                                                                    if ($subindicator->getArea->area_name === $kecamatan['nama']) {
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                        $array_a[] = $subindicator->area_id;
                                                                    } else {
                                                                    }
                                                                } else {
                                                                    $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                $array_a[] = $subindicator->area_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_area_id != null) {

                                                if (in_array($subindicator->sub_area_id, $array_sa)) {
                                                } else {
                                                    if ($subindicator->indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getSubArea->unit->unit;
                                                    }
                                                    $subarea_role = single_array($data_subarea[$subindicator->sub_area_id]);
                                                    for ($rb = 0; $rb < count($subarea_role); $rb++) {
                                                        if ($subarea_role[$rb] === "Kecamatan") {
                                                            if ($subindicator->getArea->role === "Kecamatan" && $subindicator->getArea->check_sub === 0) {
                                                                if (is_array($kecamatan)) {
                                                                    if ($subindicator->getSubArea->subarea_name === $kecamatan['nama']) {
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                        $array_sa[] = $subindicator->sub_area_id;
                                                                    } else {
                                                                    }
                                                                } else {
                                                                    $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                $array_sa[] = $subindicator->sub_area_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->indicator_id != null) {
                                                if (in_array($subindicator->indicator_id, $array_i)) {
                                                } else {
                                                    if ($subindicator->sub_indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getIndicator->unit->unit;
                                                    }
                                                    $indicator_role = single_array($data_indicator[$subindicator->indicator_id]);
                                                    for ($rb = 0; $rb < count($indicator_role); $rb++) {
                                                        if ($indicator_role[$rb] === "Kecamatan") {
                                                            if ($subindicator->getSubArea->role === "Kecamatan" && $subindicator->getSubArea->check_sub === 0) {
                                                                if (is_array($kecamatan)) {
                                                                    if ($subindicator->getIndicator->indicator_name === $kecamatan['nama']) {
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                        $array_i[] = $subindicator->indicator_id;
                                                                    } else {
                                                                    }
                                                                } else {
                                                                    $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                $array_i[] = $subindicator->indicator_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_indicator_id != null) {
                                                // print_r($array_b);
                                                if (in_array($subindicator->sub_indicator_id, $array_si)) {
                                                } else {
                                                    $subindicator_role = single_array($data_si[$subindicator->sub_indicator_id]);
                                                    for ($rb = 0; $rb < count($subindicator_role); $rb++) {
                                                        if ($subindicator_role[$rb] === "Kecamatan") {
                                                            if ($subindicator->getSubIndicator->parent) {
                                                                if ($subindicator->getSubIndicator->parent->role === "Kecamatan" && $subindicator->getSubindicator->check_sub === 0) {
                                                                    $count = 0;
                                                                    $data = get_parent_si(
                                                                        $subindicator->getSubIndicator->id,
                                                                        $subindicator->getSubIndicator->parent,
                                                                        $data_edb,
                                                                        $array_parent,
                                                                        $b,
                                                                        $array_ssi,
                                                                        $subindicator->value,
                                                                        $count_sub_indicator,
                                                                        $parent_array,
                                                                        $count
                                                                    );
                                                                    $array_parent[] = $data['array_parent'];
                                                                    $array_sub = single_array($array_parent);
                                                                    for ($c = 0; $c < count($array_sub); $c++) {
                                                                        if (in_array($array_sub[$c], $parent_array)) {
                                                                        } else {
                                                                            $parent_array[] = $array_sub[$c];
                                                                        }
                                                                    }
                                                                    // echo dd($data['array_parent']);
                                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                                    $spasi = 12;
                                                                    $number = 1;
                                                                    for ($dssi = count($data['array_ssi']) - 1; $dssi >= 0; $dssi--) {
                                                                        // if($data['array_ssi'][$dssi]['uraian'] === $kecamatan['nama']){
                                                                        $spasi = $spasi + 4;
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $data['array_ssi'][$dssi]['uraian'], 'satuan' => $data['array_ssi'][$dssi]['satuan'], 'role' => $data['array_ssi'][$dssi]['role'], 'check_sub' => $data['array_ssi'][$dssi]['check_sub'], 'value' => '-', 'action' => $data['array_ssi'][$dssi]['action'], 'master' => $b, 'number' => $number++, 'space' => $spasi);
                                                                        // }
                                                                        // else {

                                                                        // }
                                                                    }
                                                                    $space = $data['count'] * 4 + 16;
                                                                    if (is_array($kecamatan)) {
                                                                        if ($subindicator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                            $array_si[] = $subindicator->sub_indicator_id;
                                                                            $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => $count_sub_indicator++, 'space' => $space);
                                                                        }
                                                                    } else {
                                                                        $data_edb = array();
                                                                    }
                                                                }
                                                            } else {
                                                                if ($subindicator->getIndicator->role === "Kecamatan" && $subindicator->getIndicator->check_sub === 0) {
                                                                    if (is_array($kecamatan)) {
                                                                        if ($subindicator->getSubIndicator->sub_indicator_name === $kecamatan['nama']) {
                                                                            $array_si[] = $subindicator->sub_indicator_id;
                                                                            $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => 1, 'space' => 16);
                                                                        } else {
                                                                        }
                                                                    } else {
                                                                        $data_edb = array();
                                                                    }
                                                                } else {
                                                                }


                                                                // if($subindicator[''])
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            // echo dd($data_edb);
                                        } else {
                                            if ($subindicator->business_id != null) {
                                                if (in_array($subindicator->business_id, $array_b)) {
                                                } else {
                                                    if ($subindicator->area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getBusiness->unit->unit;
                                                    }
                                                    $business_role = single_array($data_business[$subindicator->business_id]);
                                                    for ($rb = 0; $rb < count($business_role); $rb++) {
                                                        if ($business_role[$rb] === "Kelurahan") {
                                                            $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getBusiness->business_name, 'satuan' => $satuan, 'role' => $subindicator->getBusiness->role, 'check_sub' => $subindicator->getBusiness->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_business++, 'space' => 0);
                                                            $array_b[] = $subindicator->business_id;
                                                            break;
                                                        } else {
                                                            // $data_edb = array();
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->area_id != null) {
                                                if (in_array($subindicator->area_id, $array_a)) {
                                                } else {
                                                    if ($subindicator->sub_area_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getArea->unit->unit;
                                                    }
                                                    $area_role = single_array($data_area[$subindicator->area_id]);
                                                    for ($rb = 0; $rb < count($area_role); $rb++) {
                                                        if ($area_role[$rb] === "Kelurahan") {
                                                            if ($subindicator->getBusiness->role === "Kelurahan " && $subindicator->getBusiness->check_sub === 0) {
                                                                if ($subindicator->getArea->area_name === $kecamatan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => 0, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                    $array_a[] = $subindicator->area_id;
                                                                } elseif ($subindicator->getArea->area_name === $kelurahan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                    $array_a[] = $subindicator->area_id;
                                                                } else {
                                                                    //  $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getArea->area_name, 'satuan' => $satuan, 'role' => $subindicator->getArea->role, 'check_sub' => $subindicator->getArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => roman_numbered($count_area++), 'space' => 4);
                                                                $array_a[] = $subindicator->area_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_area_id != null) {

                                                if (in_array($subindicator->sub_area_id, $array_sa)) {
                                                } else {
                                                    if ($subindicator->indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getSubArea->unit->unit;
                                                    }
                                                    $subarea_role = single_array($data_subarea[$subindicator->sub_area_id]);
                                                    for ($rb = 0; $rb < count($subarea_role); $rb++) {
                                                        if ($area_role[$rb] === "Kelurahan") {
                                                            if ($subindicator->getArea->role === "Kelurahan" && $subindicator->getArea->check_sub === 0) {
                                                                if ($subindicator->getSubArea->subarea_name === $kecamatan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => 0, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                    $array_sa[] = $subindicator->sub_area_id;
                                                                } elseif ($subindicator->getSubArea->subarea_name === $kelurahan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                    $array_sa[] = $subindicator->sub_area_id;
                                                                } else {
                                                                    // $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubArea->subarea_name, 'satuan' => $satuan, 'role' => $subindicator->getSubArea->role, 'check_sub' => $subindicator->getSubArea->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_sub_area++, 'space' => 8);
                                                                $array_sa[] = $subindicator->sub_area_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->indicator_id != null) {
                                                if (in_array($subindicator->indicator_id, $array_i)) {
                                                } else {
                                                    if ($subindicator->sub_indicator_id != 0) {
                                                        $value = '-';
                                                        $action = 0;
                                                        $satuan = '';
                                                    } else {
                                                        $value = $subindicator->value;
                                                        $action = 1;
                                                        $satuan = $subindicator->getIndicator->unit->unit;
                                                    }
                                                    $indicator_role = single_array($data_indicator[$subindicator->indicator_id]);
                                                    for ($rb = 0; $rb < count($indicator_role); $rb++) {
                                                        if ($indicator_role[$rb] === "Kelurahan") {
                                                            if ($subindicator->getSubArea->role === "Kelurahan" && $subindicator->getSubArea->check_sub === 0) {
                                                                if ($subindicator->getIndicator->indicator_name === $kecamatan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => 0, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                    $array_i[] = $subindicator->indicator_id;
                                                                } elseif ($subindicator->getIndicator->indicator_name === $kelurahan['nama']) {
                                                                    $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                    $array_i[] = $subindicator->indicator_id;
                                                                } else {
                                                                    // $data_edb = array();
                                                                }
                                                            } else {
                                                                $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getIndicator->indicator_name, 'satuan' => $satuan, 'role' => $subindicator->getIndicator->role, 'check_sub' => $subindicator->getIndicator->check_sub, 'value' => $value, 'action' => $action, 'master' => $b, 'number' => $count_indicator++, 'space' => 12);
                                                                $array_i[] = $subindicator->indicator_id;
                                                                break;
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                            if ($subindicator->sub_indicator_id != null) {
                                                // print_r($array_b);
                                                if (in_array($subindicator->sub_indicator_id, $array_si)) {
                                                } else {
                                                    $subindicator_role = single_array($data_si[$subindicator->sub_indicator_id]);
                                                    for ($rb = 0; $rb < count($subindicator_role); $rb++) {
                                                        if ($subindicator_role[$rb] === "Kelurahan") {
                                                            if ($subindicator->getSubIndicator->parent) {
                                                                if ($subindicator->getSubIndicator->parent->role === "Kelurahan" && $subindicator->getSubindicator->check_sub === 0) {
                                                                    $count = 0;
                                                                    $data = get_parent_si(
                                                                        $subindicator->getSubIndicator->id,
                                                                        $subindicator->getSubIndicator->parent,
                                                                        $data_edb,
                                                                        $array_parent,
                                                                        $b,
                                                                        $array_ssi,
                                                                        $subindicator->value,
                                                                        $count_sub_indicator,
                                                                        $parent_array,
                                                                        $count
                                                                    );
                                                                    $array_parent[] = $data['array_parent'];
                                                                    $array_sub = single_array($array_parent);
                                                                    for ($c = 0; $c < count($array_sub); $c++) {
                                                                        if (in_array($array_sub[$c], $parent_array)) {
                                                                        } else {
                                                                            $parent_array[] = $array_sub[$c];
                                                                        }
                                                                    }
                                                                    // echo dd($data['array_parent']);
                                                                    $array_si[] = $subindicator->sub_indicator_id;
                                                                    $spasi = 12;
                                                                    $number = 1;
                                                                    for ($dssi = count($data['array_ssi']) - 1; $dssi >= 0; $dssi--) {
                                                                        // if($data['array_ssi'][$dssi]['uraian'] === $kecamatan['nama']){
                                                                        $spasi = $spasi + 4;
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $data['array_ssi'][$dssi]['uraian'], 'satuan' => $data['array_ssi'][$dssi]['satuan'], 'role' => $data['array_ssi'][$dssi]['role'], 'check_sub' => $data['array_ssi'][$dssi]['check_sub'], 'value' => '-', 'action' => $data['array_ssi'][$dssi]['action'], 'master' => $b, 'number' => $number++, 'space' => $spasi);
                                                                        // }
                                                                        // else {

                                                                        // }
                                                                    }
                                                                    $space = $data['count'] * 4 + 16;
                                                                    if ($subindicator->getSubIndicator->sub_indicator_name === $kecamatan['nama'] || $subindicator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                        $array_si[] = $subindicator->sub_indicator_id;
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => $count_sub_indicator++, 'space' => $space);
                                                                    }
                                                                }
                                                            } else {
                                                                if ($subindicator->getIndicator->role === "Kelurahan" && $subindicator->getIndicator->check_sub === 0) {
                                                                    if ($subindicator->getSubIndicator->sub_indicator_name === $kecamatan['nama'] || $subindicator->getSubIndicator->sub_indicator_name === $kelurahan['nama']) {
                                                                        $array_si[] = $subindicator->sub_indicator_id;
                                                                        $data_edb[] = array('id' => $subindicator->id, 'uraian' => $subindicator->getSubIndicator->sub_indicator_name, 'satuan' => $subindicator->getSubIndicator->unit_id, 'role' => $subindicator->getSubIndicator->role, 'check_sub' => $subindicator->getSubIndicator->check_sub, 'value' => $subindicator->value, 'action' => 1, 'master' => $b, 'number' => 1, 'space' => 16);
                                                                    } else {
                                                                    }
                                                                } else {
                                                                }


                                                                // if($subindicator[''])
                                                            }
                                                        } else {
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // echo "<br>";
                                    // print_r($role);
                                    // echo "<br>";

                                    // echo "<br>";
                                    // $array_parent = single_array($array_parent);
                                    // print_r($array_parent);
                                    // echo "<hr>";
                                    // echo "<b>Contoh</b>";
                                    // print_r($array_coba);
                                }
                                // print_r($role_business);
                                // echo "<br>";
                                // $array_parent = single_array($array_parent);
                                // print_r($array_parent);
                                // echo "<hr>";
                                //     echo "<b>Contoh</b>";

                            }
                            // $sai++;
                        }
                    }
                    // $sai++;
                }
            }
            // $ai++;
        }
        // $i++;
    }
    // echo dd($array_si);
    // echo dd($role_business);
    // echo dd($data_edb);
    // print_r($array_si);
    // array('')
    // $parent_array = single_array($array_parent);

    // echo dd($data_si);
    return $data_edb;
}

function area($childs, $data, $space, $master)
{
    $space = $space + 2;
    foreach ($childs as $c) {
        $data[] = array('id' => $c->id, 'id_bmd' => $c->id_bmd, 'bmd_description' => $c->bmd_description, 'space' => $space, 'satuan_id' => $c->satuan_id, 'value' => $c->value, 'type_user' => $c->type_user, 'validate' => $c->validate, 'validate_final' => $c->validate_final, 'created_at' => $c->creatd_at, 'updated_at' => $c->updated_at, 'master' => $master);
        // if (count($c->child)) {
        //     $data = parent($c->child, $data, $space, $master);
        // }
    }
    return $data;
}

function upload_import($import_name)
{
    return public_path('/sipd-pemalang/public');
}

function roman_numbered($integer)
{
    $integer = intval($integer);
    $result = '';

    $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400, 'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40, 'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
    foreach ($lookup as $roman => $value) {
        $matches = intval($integer / $value);
        $result .= str_repeat($roman, $matches);
        $integer = $integer % $value;
    }
    return $result;
}

function alpha_numbered($integer, $type)
{
    $alphabet_a = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    $alphabet_A = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

    if ($type === 'a') {
        $alpha_flip = array_flip($alphabet_a);
        if ($integer <= 25) {
            echo $alphabet_a[$integer];
        } elseif ($integer > 25) {
            $dividend = ($integer + 1);
            $alpha = '';
            $modulo;
            while ($dividend > 0) {
                $modulo = ($dividend - 1) % 26;
                $alpha = $alphabet_a[$modulo] . $alpha;
                $dividend = floor((($dividend - $modulo) / 26));
            }
            echo $alpha;
        }
    } elseif ($type === 'A') {
        $alpha_flip = array_flip($alphabet_A);
        if ($integer <= 25) {
            echo $alphabet_a[$integer];
        } elseif ($integer > 25) {
            $dividend = ($integer + 1);
            $alpha = '';
            $modulo;
            while ($dividend > 0) {
                $modulo = ($dividend - 1) % 26;
                $alpha = $alphabet_A[$modulo] . $alpha;
                $dividend = floor((($dividend - $modulo) / 26));
            }
            echo $alpha;
        }
    } else {
        return '';
    }
}
