<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class skpdModel extends Model
{
    protected $table='skpd';

    protected $fillable = [
        'id','skpd_name','created_at','updated_at','active',
    ]; 
}
